<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->model('Api_model');
    }

    public function login(){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            echo json_encode(array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Api_model->check_auth_client();

            if($check_auth_client == true){
                //$params = json_decode(file_get_contents('php://input'), TRUE);
                $params = (array)json_decode($this->input->post('params'));
                //$params = (array)json_decode($this->input->get_request_header('Parameter', TRUE));

                if(!empty($params)) {
                    if (empty($params['username']) || empty($params['password']) || empty($params['ime_no'])) {
                        $response = array('status' => 400, 'message' => 'Parameters Can not be empty','action'=>'failed');
                    } else {
                        $username = $params['username'];
                        $password = $params['password'];
                        $ime_no = $params['ime_no'];
                        $response = $this->Api_model->login($username, $password, $ime_no);
                    }
                    echo json_encode($response);
                }else{
                    echo json_encode(array('status' => 500,'message' => 'Parameter is not formatted','action'=>'failed'));
                }


            }else{
                echo json_encode(array('status' => 401,'message' => 'Unauthorized.','action'=>'failed'));
            }
        }
    }

    public function logout(){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            echo json_encode(array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Api_model->check_auth_client();
            if($check_auth_client == true){
                $response = $this->Api_model->logout();
                echo json_encode($response);
            }
        }
    }



}