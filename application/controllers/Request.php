<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Api_model');
        $this->load->model('Post_model');
    }


    public function eheck_params($params,$type){
        $info = array(
            "meeting" =>array(
                "meeting_name",
                "no_of_participation",
                "no_of_participation_end",
                "meeting_start_time",
                "meeting_end_time",
                "duration",
                "union_id",
                "word_id",
                "meeting_location_longitude",
                "meeting_location_latitude",
                "meeting_location_longitude_middle",
                "meeting_location_latitude_middle",
                "meeting_location_longitude_end",
                "meeting_location_latitude_end",
                "start_picture_time",
                "end_picture_time"
            ),
            "physical_location" => array(
                "longitude",
                "latitude",
                "ime_no",
                "meeting_id",
                "date_time"
            ),
            "video" => array(
                "video_title",
                "video_date",
                "user_id"
            )

        );

        foreach ($params as $key => $val) {
            if (!in_array($key, $info[$type])) {
                unset($params[$key]);
            }
        }

        return $params;
    }

    public function check_request_type($type){
        $info = array("physical_location","meeting");
        if (!in_array($type, $info)) {
            return false;
        }
        return true;
    }

    public function api_request($type)
    {
        if($this->check_request_type($type)) {
            $method = $_SERVER['REQUEST_METHOD'];
            if ($method != 'POST') {
                echo json_encode(array('status' => 400, 'message' => 'Bad request.','action'=>'failed'));
            } else {
                $check_auth_client = $this->Api_model->check_auth_client();
                if ($check_auth_client == true) {
                    //$response = $this->Api_model->auth(); //temporary off
                    $response['status'] = 200; //temporary check
                    if($response['status'] == 200) {
                            //$params = json_decode(file_get_contents('php://input'), TRUE);
                            $params = (array)json_decode($this->input->post('params'));
                            $message = "";
                            if(!empty($params)) {
                                $params = $this->eheck_params($params, $type);
                                    $file_upload = 1;
                                    if($type=='meeting') {
                                        //For Meeting Start
                                        if(!empty($_FILES['meeting_start_picture']['name'])){
                                            $file_name = "meeting_start_picture";
                                            $upload_path = './assets/upload/meeting/';
                                            $result = $this->Common_operation->image_upload($file_name, $upload_path);

                                            if(!$result['flag']){
                                                $data['error'] = $result['error'];
                                                $message = array('status' => 500,'message' =>  $data['error'],'action'=>'failed');
                                                $file_upload = 0;

                                            }else{
                                                $file_upload = 1;
                                                $params['meeting_start_picture_name'] = $result['picture'];
                                            }

                                        }else{
                                            $file_upload = 0;
                                            $message = array('status' => 500,'message' => 'Please upload image the start image','action'=>'failed');
                                        }

                                        //For Meeting End
                                        if(!empty($_FILES['meeting_end_picture']['name'])){
                                            $file_name = "meeting_end_picture";
                                            $upload_path = './assets/upload/meeting/';
                                            $result = $this->Common_operation->image_upload($file_name, $upload_path);

                                            if(!$result['flag']){
                                                $data['error'] = $result['error'];
                                                $message = array('status' => 500,'message' =>  $data['error'],'action'=>'failed');
                                                $file_upload = 0;

                                            }else{
                                                $file_upload = 1;
                                                $params['meeting_start_picture_end'] = $result['picture'];
                                            }

                                        }else{
                                            $file_upload = 0;
                                            $message = array('status' => 500,'message' => 'Please upload image the finishing image','action'=>'failed');
                                        }


                                        /*added by Arzon Barua 7/2/2017 4:54 PM*/
                                        if($file_upload){
                                            $users_id = $this->input->get_request_header('User-ID', TRUE);
                                            $data['result'] = $this->Post_model->getAllByFieldName('app_user_info','id', $users_id);
                                            
                                              //$params['district_id'] = $data['result'][0]->district_id;
                                              //$params['upozila_id'] = $data['result'][0]->upazila_id; //need to change here
                                              
                                              $union_result = $this->Post_model->getAllByFieldName('union','id', $params['union_id']);                                              
                                              $params['upozila_id'] = $union_result[0]->upazila_id;
                                              $params['district_id'] = $union_result[0]->district_id;

                                              //Get District ID
                                
                                            $meeting = $params['meeting_name'];
                                            $meetingArr = explode("_", $meeting);

                                            // echo "<pre>";
                                            // print_r($meetingArr);
                                            // echo "</pre>";


                                            if(count($meetingArr)>1){
                                                $params['meeting_name'] = $meetingArr[1];
                                                $params['meeting_type'] = $meetingArr[0]; 
                                            }else{
                                                $params['meeting_name'] = $meeting;
                                            }

                                        }

                                    }

                                    if($file_upload) {
                                        $resp = $this->Api_model->insert_info($params, $type);
                                        if($type=='meeting') {

                                            $insert_id = $this->db->insert_id();
                                            $input['users_id'] = $users_id;
                                            $input['meeting_id'] = $insert_id;
                                            $input['latitude'] = $params['meeting_location_latitude'];
                                            $input['longitude'] = $params['meeting_location_longitude'];
                                            $input['date_time'] = $params['meeting_end_time'];

                                            $this->Api_model->insert_info($input, "physical_location");
                                        }
                                    }else{
                                        $resp = $message;
                                    }
                                //}
                            }else{
                                $resp = array('status' => 500,'message' => 'Parameter is not formatted','action'=>'failed');
                            }
                    }else{
                        $resp = $response;
                    }
                }else{
                    $resp = array('status' => 401,'message' => 'Unauthorized.','action'=>'failed');
                }
            }
        }else{
            $resp = array('status' => 500, 'message' => 'Bad url','action'=>'failed');
        }
        echo json_encode($resp);
    }


    public function get_question(){
        $method = $_SERVER['REQUEST_METHOD'];
        $id = $this->uri->segment(3);

        if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
             $resp = array('status' => 400,'message' => 'Bad request.','action'=>'failed');
        }else{
            $check_auth_client = $this->Api_model->check_auth_client();
            if($check_auth_client == true){
                //$response = $this->Api_model->auth(); //temporary off

                $response['status'] = 200; //temporary check
                if($response['status'] == 200){
                  $resp =  $this->Api_model->show_question($id);
                }else{
                  $resp =  $response;
                }
            }else{
                 $resp =  $check_auth_client;
            }
        }


        echo json_encode($resp);
    }


    // public function save_question_info(){
    //     $method = $_SERVER['REQUEST_METHOD'];
    //     $id = $this->uri->segment(3);

    //     if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
    //         json_output(400,array('status' => 400,'message' => 'Bad request.'));
    //     }else{
    //         $check_auth_client = $this->Api_model->check_auth_client();
    //         if($check_auth_client == true){
    //             $response = $this->Api_model->auth();
    //             if($response['status'] == 200){
    //               $resp =  "show";
    //             }else{
    //               $resp =  $response;
    //             }
    //         }else{
    //              $resp =  $check_auth_client;
    //         }
    //     }

    //     echo "<pre>";
    //     print_r($resp);
    //     echo '</pre>';

    //     //echo json_encode($resp);
    // }

 
    public function save_question_info()
    {     

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            $resp = array('status' => 400,'message' => 'Bad request.','action'=>'failed');
        } else {
            $check_auth_client = $this->Api_model->check_auth_client();
            if ($check_auth_client == true) {
                //$response = $this->Api_model->auth(); // temporary off

                $response['status'] = 200; //tempory check

                if($response['status'] == 200) {
                        //$params = json_decode(file_get_contents('php://input'), TRUE);
                        $params = (array)json_decode($this->input->post('params'));
                        $message = "";
                        if(!empty($params)) {

                            $input = array();
                            $array = array(1000,1002,1003,1005,1006,1007,1008,1004,1009,1010);
                            
                            $characters = '0123456789';
                            $randomString = '';
                              
                            for ($j = 0; $j < 4; $j++) {
                                $randomString .= $characters[rand(0, strlen($characters) - 1)];
                            }


                            $input['form_code'] = date('YmdHis').$randomString;
                            $users_id  = $this->input->get_request_header('User-ID', TRUE);
                            $input['login_op_id'] = $users_id;
                            $data = $this->Post_model->getAllByFieldName('app_user_info','id',$users_id);
                            $input['projects_id'] = $data[0]->project_id;
                            $input['created_at'] = date('Y-m-d h:i:s');
                            $input['created_by'] = $users_id;

                            //union and ward temporary

                            //  echo "<pre>";
                            //     print_r($_POST);
                            // echo "</pre>";
                            foreach($params as $key=>$val) {
                                foreach ($val->ans as $keyValue => $valValue) {
                                    $input_values['created_at'] = date('Y-m-d h:i:s');
                                    $input_values['created_by'] = $users_id;
                                    $input_values['form_code'] = $input['form_code'];
                                    $input['survey_info_id'] = $val->form_id;


                                    if (isset($input_values['review_ans'])) {
                                        unset($input_values['review_ans']);
                                    }

                                    if (isset($input_values['option_id'])) {
                                        unset($input_values['option_id']);
                                    }

                                    if (isset($valValue->q_id)) {
                                        if (in_array($valValue->q_id, $array)) {

                                            if ($valValue->q_id == '1000')
                                                $input['ans_name'] = isset($valValue->ans[0]) ? $valValue->ans[0] : "" ;

                                            if ($valValue->q_id == '1002')
                                                $input['ans_age'] = isset($valValue->ans[0]) ? $valValue->ans[0] : "" ;

                                            if ($valValue->q_id == '1003')
                                                $input['ans_gender'] = isset($valValue->ans[0]) ? $valValue->ans[0] : "" ;

                                            if ($valValue->q_id == '1004')
                                                $input['mobile_number'] = isset($valValue->ans[0]) ? $valValue->ans[0] : "" ;

                                            if ($valValue->q_id == '1005')
                                                $input['name_garments'] = isset($valValue->ans[0]) ? $valValue->ans[0] : "" ;

                                            if ($valValue->q_id == '1006')
                                                $input['department_name'] = isset($valValue->ans[0]) ? $valValue->ans[0] : "" ;

                                            if ($valValue->q_id == '1007')
                                                $input['id_number'] = isset($valValue->ans[0]) ? $valValue->ans[0] : "" ;

                                            if ($valValue->q_id == '1008')
                                                $input['address_garments'] = isset($valValue->ans[0]) ? $valValue->ans[0] : "" ;

                                            if ($valValue->q_id == '1009') {
                                                $input['district_id'] =  isset($data[0]->district_id) ? $data[0]->district_id : 0;
                                                $input['union_id'] =  isset($valValue->ans[0]) ? $valValue->ans[0] : 0;
                                            }

                                            if ($valValue->q_id == '1010') {
                                                //$input['upozila_id'] = isset($data[0]->upazila_id) ? $data[0]->upazila_id : 0;
                                                $input['word_id'] = isset($valValue->ans[0]) ? $valValue->ans[0] : 0;
                                            }

                                        } else {

                                            foreach ($valValue->ans as $ans) {
                                                //check question type
                                                $qs_id = $valValue->q_id;
                                                $question_type = $this->Post_model->custom_query("Select survey_qs_type from survey_qs where id = '$qs_id'");

                                                if((string)$question_type[0]->survey_qs_type=='text'){
                                                     $input_values['survey_qs_id'] = $valValue->q_id;
                                                     $input_values['review_ans'] = $ans;
                                                     $this->Post_model->insert('assessment_info', $input_values);
                                                }else{
                                                     $input_values['survey_qs_id'] = $valValue->q_id;
                                                     $input_values['option_id'] = $ans;
                                                     $this->Post_model->insert('assessment_info', $input_values);
                                                }    


                                                // if (is_numeric($ans)) {
                                                //     $input_values['survey_qs_id'] = $valValue->q_id;
                                                //     $input_values['option_id'] = $ans;
                                                //     $this->Post_model->insert('assessment_info', $input_values);
                                                // } else {
                                                //     $input_values['survey_qs_id'] = $valValue->q_id;
                                                //     $input_values['review_ans'] = $ans;
                                                //     $this->Post_model->insert('assessment_info', $input_values);
                                                // }
                                            }
                                        }
                                    }
                                }
                            }

                            //die;
                            /*
                             foreach($params as $key => $val){
                                 foreach($val as $keyValue=> $valValue ){
                                     //print_r($valValue);
                                     $input_values['created_at'] = date('Y-m-d h:i:s');
                                     $input_values['created_by'] = $users_id;
                                     $input_values['form_code'] =  $input['form_code'];

                                     if(isset($input_values['review_ans'])){
                                         unset($input_values['review_ans']);
                                     }

                                     if(isset($input_values['option_id'])){
                                         unset($input_values['option_id']);
                                     }

                                     if(isset($valValue->q_type)){
                                         $input['survey_info_id'] = $valValue->q_type;
                                     }

                                     if(isset($valValue->q_id)){
                                         if(in_array($valValue->q_id,$array)){

                                             if($valValue->q_id=='1000')
                                                 $input['ans_name'] = $valValue->ans[0];

                                             if($valValue->q_id=='1002')
                                                 $input['ans_age'] = $valValue->ans[0];

                                             if($valValue->q_id=='1003')
                                                 $input['ans_gender'] = $valValue->ans[0];

                                             if($valValue->q_id=='1004')
                                                 $input['mobile_number'] = $valValue->ans[0];

                                             if($valValue->q_id=='1005')
                                                 $input['name_garments'] = $valValue->ans[0];

                                             if($valValue->q_id=='1006')
                                                 $input['department_name'] = $valValue->ans[0];

                                             if($valValue->q_id=='1007')
                                                 $input['id_number'] = $valValue->ans[0];

                                             if($valValue->q_id=='1008')
                                                 $input['address_garments'] = $valValue->ans[0];

                                             if($valValue->q_id=='1009'){
                                                 $input['district_id'] = $data[0]->district_id;
                                                 $input['union_id'] = $valValue->ans[0];
                                             }

                                             if($valValue->q_id=='1010'){
                                                 $input['upozila_id'] = $data[0]->upazila_id;
                                                 $input['word_id'] = $valValue->ans[0];
                                              }

                                         }else{

                                             foreach($valValue->ans as $ans){
                                                 if(is_numeric($ans)){
                                                     $input_values['survey_qs_id'] = $valValue->q_id;
                                                     $input_values['option_id'] = $ans;
                                                     $this->Post_model->insert('assessment_info',$input_values);
                                                 }else{
                                                     $input_values['survey_qs_id'] = $valValue->q_id;
                                                     $input_values['review_ans'] = $ans;
                                                     $this->Post_model->insert('assessment_info',$input_values);
                                                 }
                                             }
                                         }

                                     }
                                 }
                             }
                            */
                            if(isset($input['union_id'])){
                                $union_result = $this->Post_model->getAllByFieldName('union','id', $input['union_id']);
                                $input['upozila_id'] = $union_result[0]->upazila_id;
                            }


                            $this->Post_model->insert('form_info',$input);
                            $resp = array('status' => 201,'message' => 'Data has been inserted.','action'=>'success');
                           
                        }else{
                            $resp = array('status' => 500,'message' => 'Parameter is not formatted','action'=>'failed');
                        }
                }else{
                    $resp = $response;
                }
            }else{
                $resp = array('status' => 401,'message' => 'Unauthorized.','action'=>'failed');
            }
        }

     

        echo json_encode($resp);


    }

    public function get_question_form(){
        $method =  $_SERVER['REQUEST_METHOD'];
        //$id = $this->uri->segment(3);
        if($method != 'GET'){
           $resp = array('status' => 400,'message' => 'Bad request.','action'=>'failed');
        }else{
            $check_auth_client = $this->Api_model->check_auth_client();
            if($check_auth_client == true){
                //$response = $this->Api_model->auth(); //temporary off
                $response['status'] = 200; //temporary check
                if($response['status'] == 200){
                  $resp =  $this->Api_model->get_question_forms();
                }else{
                  $resp =  $response;
                }
            }else{
                 $resp =  $check_auth_client;
            }
        }
        echo json_encode($resp);
    }

    public function video_upload(){
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            $resp = array('status' => 400,'message' => 'Bad request.','action'=>'failed');
        }else{
            $check_auth_client = $this->Api_model->check_auth_client();
            if ($check_auth_client == true) {
                //$response = $this->Api_model->auth(); //temporary off

                $response['status'] = 200; // temporary check
                if($response['status'] == 200) {
                    $params = (array)json_decode($this->input->post('params'));
                    if(!empty($params)) {
                        $params = $this->eheck_params($params, "video");
                        $file_upload = 1;
                        if(!empty($_FILES['video_name']['name'])){
                            $file_name = "video_name";
                            $upload_path = './assets/upload/video/';
                            $result = $this->Common_operation->video_upload($file_name, $upload_path);
                            if(!$result['flag']){
                                $data['error'] = $result['error'];
                                $message = array('status' => 500,'message' =>  $data['error'],'action'=>'failed');
                                $file_upload = 0;
                            }else{
                                $file_upload = 1;
                                $params['video_name'] = $result['file_name'];
                            }
                        }else{
                            $file_upload = 0;
                            $message = array('status' => 500,'message' => 'Please upload image the start image','action'=>'failed');
                        }
                        if($file_upload) {
                            $users_id = $this->input->get_request_header('User-ID', TRUE);

                            $params['video_date'] = date('Y-m-d h:i:s',strtotime($params['video_date']));
//                            $params['user_id'] = $users_id;
                            $params['upload_date'] = date("Y-m-d h:i:s");
                            $params['created_at'] = $params['upload_date'];
                            $resp = $this->Api_model->insert_info($params, "video");


                        }else{
                            $resp = $message;
                        }
                    }else{
                        $resp = array('status' => 500,'message' => 'Parameter is not formatted','action'=>'failed');
                    }
                }else{
                    $resp = $response;
                }
            }else{
                $resp = array('status' => 401,'message' => 'Unauthorized.','action'=>'failed');
            }
        }
        echo json_encode($resp);
    }
}