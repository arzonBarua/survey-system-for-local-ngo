<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Api_model');
    }


    public function eheck_params($params,$type){
        $info = array(
            "meeting" =>array(
                "meeting_name",
                "no_of_participation",
                "meeting_start_time",
                "meeting_end_time",
                "duration",
                "district_id",
                "upozila_id",
                "union_id",
                "word_id",
                "meeting_location_longitude",
                "meeting_location_latitude",
                "start_picture_time",
                "end_picture_time"
            ),
            "physical_location" => array(
                "longitude",
                "latitude",
                "ime_no",
                "meeting_id",
                "date_time"
            ),

        );

        foreach ($params as $key => $val) {
            if (!in_array($key, $info[$type])) {
                unset($params[$key]);
            }
        }

//        if (count($params) < count($info[$type])) {
//            return true;
//        }

        return $params;
    }

    public function check_request_type($type){
        $info = array("physical_location","meeting");
        if (!in_array($type, $info)) {
            return false;
        }
        return true;
    }


//    public function api_request($type)
//    {
//        if($this->check_request_type($type)) {
//            $method = $_SERVER['REQUEST_METHOD'];
//            if ($method != 'POST') {
//                echo json_encode(array('status' => 400, 'message' => 'Bad request.'));
//            } else {
//                $check_auth_client = $this->Api_model->check_auth_client();
//                if ($check_auth_client == true) {
//                    $response = $this->Api_model->auth();
//                    if ($response['status'] == 200) {
//                        $params = json_decode(file_get_contents('php://input'), TRUE);
//
//                            foreach($params as $key=> $val){
//                                $request = $this->eheck_params($val, $type);
//                                if ($request) {
//                                    $step = 0;
//                                    $resp = array('status' => 400, 'message' => 'Parameters Can not be empty');
//                                    break;
//                                }else{
//                                    $step = 1;
//                                }
//                            }
//
//                            if($step){
//                                    foreach($params as $key=> $val){
//                                        $params[$key]['users_id'] = $response['users_id'];
//                                    }
//                                    $resp = $this->Api_model->insert_info($params, $type);
//                            }
//
//
//                    }
//                }
//            }
//        }else{
//            $resp = array('status' => 500, 'message' => 'Bad url');
//        }
//
//        echo json_encode($resp);
//    }

    public function api_request($type)
    {
        if($this->check_request_type($type)) {
            $method = $_SERVER['REQUEST_METHOD'];
            if ($method != 'POST') {
                echo json_encode(array('status' => 400, 'message' => 'Bad request.','action'=>'failed'));
            } else {
                $check_auth_client = $this->Api_model->check_auth_client();
                if ($check_auth_client == true) {
                    $response = $this->Api_model->auth();

                    if($response['status'] == 200) {
                            //$params = json_decode(file_get_contents('php://input'), TRUE);
                            $params = (array)json_decode($this->input->post('params'));
                            $message = "";
                            if(!empty($params)) {
                                $params = $this->eheck_params($params, $type);
//                                if ($request) {
//                                    $resp = array('status' => 400, 'message' => 'Parameters Can not be empty','action'=>'failed');
//                                } else {
                                    $file_upload = 1;
                                    if($type=='meeting') {
                                        //For Meeting Start
                                        if(!empty($_FILES['meeting_start_picture']['name'])){
                                            $file_name = "meeting_start_picture";
                                            $upload_path = './assets/upload/meeting/';
                                            $result = $this->Common_operation->image_upload($file_name, $upload_path);

                                            if(!$result['flag']){
                                                $data['error'] = $result['error'];
                                                $message = array('status' => 500,'message' =>  $data['error'],'action'=>'failed');
                                                $file_upload = 0;

                                            }else{
                                                $file_upload = 1;
                                                $params['meeting_start_picture_name'] = $result['picture'];
                                            }

                                        }else{
                                            $file_upload = 0;
                                            $message = array('status' => 500,'message' => 'Please upload image the start image','action'=>'failed');
                                        }

                                        //For Meeting End
                                        if(!empty($_FILES['meeting_end_picture']['name'])){
                                            $file_name = "meeting_end_picture";
                                            $upload_path = './assets/upload/meeting/';
                                            $result = $this->Common_operation->image_upload($file_name, $upload_path);

                                            if(!$result['flag']){
                                                $data['error'] = $result['error'];
                                                $message = array('status' => 500,'message' =>  $data['error'],'action'=>'failed');
                                                $file_upload = 0;

                                            }else{
                                                $file_upload = 1;
                                                $params['meeting_start_picture_end'] = $result['picture'];
                                            }

                                        }else{
                                            $file_upload = 0;
                                            $message = array('status' => 500,'message' => 'Please upload image the finishing image','action'=>'failed');
                                        }
                                    }

                                    if($file_upload) {
                                        $resp = $this->Api_model->insert_info($params, $type);
                                        $users_id  = $this->input->get_request_header('User-ID', TRUE);
                                        $insert_id = $this->db->insert_id();

                                        $input['users_id'] = $users_id;
                                        $input['meeting_id'] = $insert_id;
                                        $input['latitude'] = $params['meeting_location_latitude'];
                                        $input['longitude'] = $params['meeting_location_longitude'];
                                        $input['date_time'] = $params['meeting_end_time'];

                                        $this->Api_model->insert_info($input, "physical_location");

                                    }else{
                                        $resp = $message;
                                    }
                                //}
                            }else{
                                $resp = array('status' => 500,'message' => 'Parameter is not formatted','action'=>'failed');
                            }
                    }else{
                        $resp = $response;
                    }
                }else{
                    $resp = array('status' => 401,'message' => 'Unauthorized.','action'=>'failed');
                }
            }
        }else{
            $resp = array('status' => 500, 'message' => 'Bad url','action'=>'failed');
        }
        echo json_encode($resp);
    }

//    public function eheck_phisical_info($params){
//        $phisicalInfo = array("longitude","latitude","ime_no");
//
//            foreach ($params as $key => $val) {
//                if (!in_array($key, $phisicalInfo)) {
//                    return true;
//                }
//            }
//            if (count($params) < count($phisicalInfo)) {
//                return true;
//            }
//            return false;
//    }

//    public function meeting_info()
//    {
//        $method = $_SERVER['REQUEST_METHOD'];
//        if($method != 'POST'){
//            echo json_encode(array('status' => 400,'message' => 'Bad request.'));
//        } else {
//            $check_auth_client = $this->Api_model->check_auth_client();
//            if($check_auth_client == true){
//                $response = $this->Api_model->auth();
//                if($response['status'] == 200){
//                    $params = json_decode(file_get_contents('php://input'), TRUE);
//
//                    if(!empty($params)) {
//                        $request = $this->eheck_meeting_info($params);
//                        if ($request) {
//                            $resp = array('status' => 400, 'message' => 'Parameters Can not be empty');
//                        } else {
//                            $params['users_id'] = $response['users_id'];
//                            $resp = $this->Api_model->insert_meeting_info($params);
//                        }
//                    }else{
//                        $resp = array('status' => 500, 'message' => 'Parameter is not json formatted');
//                    }
//                    echo json_encode($resp);
//                }
//            }
//        }
//    }

}