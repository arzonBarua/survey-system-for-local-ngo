<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->model('Post_model');
	}

	public function get_ajax() {
       //echo "hellow world";
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $className = $this->input->post('field');

        $fieldName =  $className."_id";

        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);

        echo $str;

        //echo "SELECT FROM ".$tableName." WHERE ".$fieldName." = ".$id;
    }

    public function label_ajax(){
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $fieldName = $this->input->post('field');
        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
        echo $str;
    }

    public function label_view_ajax($project, $year){       
        $qry = $this->db->query("SELECT * FROM `mf__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' GROUP BY `quarter` ORDER BY `quarter` ASC");
        $data['result'] = $qry->result();
        $this->load->view('admin/label/label-view-search', $data);
    }

    public function corner_view_ajax($project, $year, $division, $district){        
        $sql = "SELECT * FROM `pr__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' AND `division` = '".$division."' AND `district` = '".$district."' GROUP BY `quarter` ORDER BY `quarter` ASC";
        $qry = $this->db->query($sql);
        $data['result'] = $qry->result();

        $this->load->view('admin/label/corner-view-search', $data);
    }

    public function referral_view_ajax($project, $year, $division, $district){        
        $sql = "SELECT * FROM `rpc__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' AND `division` = '".$division."' AND `district` = '".$district."' GROUP BY `quarter` ORDER BY `quarter` ASC";
        $qry = $this->db->query($sql);
        $data['result'] = $qry->result();

        $this->load->view('admin/label/referral-view-search', $data);
    }

    public function pc_report_ajax($project, $year, $quarter){
        if( isset($project) && $project == 5 ) {
            $_project = $this->Post_model->getAllByFieldName('projects', 'id', '');
            $_project = json_decode(json_encode($_project), true);
            
            array_pop($_project);    
            $column = array_column($_project, 'id');
            $project = "'".implode("' , '", $column)."'";

            $_project_name = 'All Organization';
        } else {
            $_project = $this->Post_model->getAllByFieldName('projects', 'id', $project);
            $project = "'".$project."'";            
            $_project_name = $_project[0]->name;
        }

        $data['year']    = $year;
        $data['quarter'] = $quarter;
        $data['project'] = $project;
        $qry = $this->db->query("SELECT * FROM `division` ORDER BY `division_name` ASC");
        $data['result']  = $qry->result();

        $data['data_project'] = $_project_name;
        
        $this->load->view('admin/label/pc-report-search', $data);
    }

    public function rpc_report_ajax($project, $year, $quarter){
        if( isset($project) && $project == 5 ) {
            $_project = $this->Post_model->getAllByFieldName('projects', 'id', '');
            $_project = json_decode(json_encode($_project), true);
            
            array_pop($_project);    
            $column = array_column($_project, 'id');
            $project = "'".implode("' , '", $column)."'";

            $_project_name = 'All Organization';
        } else {
            $_project = $this->Post_model->getAllByFieldName('projects', 'id', $project);
            $project = "'".$project."'";            
            $_project_name = $_project[0]->name;
        }


        $data['year']    = $year;
        $data['quarter'] = $quarter;
        $data['project'] = $project;
        $qry = $this->db->query("SELECT * FROM `division` ORDER BY `division_name` ASC");
        $data['result']  = $qry->result();

        $data['data_project'] = $_project_name;
      
        $this->load->view('admin/label/rpc-report-search', $data);
    }

    public function get_project_type(){
          
           $project_id = $this->input->post('projects_type');

           $arrayProjectName = $this->Common_operation->get_user_type();

          

            $string = "<option value=''>--Select Type--</option>";
             
            foreach($arrayProjectName[$project_id] as $key=>$val){
                 $string .= "<option value=$key>$val</option>";
             }
              

          echo $string;
    }
    
    public function show_apo_list(){

        $project_id = $this->input->post('projects_type');
        $arrayProjectName = $this->Common_operation->get_user_type();

         //$result = $this->Post_model->getAllByFieldName('user_acl','project_id',$project_id);
        $string = "<option value=''>--Select Type--</option>";

        if($project_id != 5){
             $result = $this->Post_model->custom_query("select admin_login.id, admin_login.full_name, admin_login.user_type from admin_login where id in (select user_id as id from user_acl where project_id='$project_id' and user_type=5) and admin_login.user_type in (5,4,3,2) order by admin_login.user_type asc");
        }else{
            $result = $this->Post_model->custom_query("select  admin_login.id, admin_login.full_name, admin_login.user_type from admin_login where project_id=5");

        }

        foreach ($result as $key=>$val){
            $string .= "<option value=".$val->id.">".$val->full_name." | User type: ".$arrayProjectName[$project_id][$val->user_type]."</option>";
        }

        echo $string;


    }


    public function district_for_phulki(){
        $result = $this->Post_model->custom_query("SELECT district.* FROM district join division on district.div_id = division.id where division.division_name LIKE '%Dhaka%' ");
        $string = "<option value=''>--Select Type--</option>";
        foreach($result as $key=>$val){
            if((string)$val->name == "Dhaka"){
                $id = $val->id;
            }
            $array[] = $val->name;
        }
      
        $string .= "<option value=".$id .">".implode(", ", $array)."</option>";
        echo $string;
    }

    public function show_district(){
        $result = $this->Post_model->custom_query("SELECT district.* FROM district");

         $string = "<option value=''>--SELECT--</option>";
        foreach($result as $key=>$val){
            $string .= "<option value=".$val->id.">".$val->name."</option>";
        }

        echo $string;
    }

    public function mf_report_ajax($year, $quarter){
        if( isset($quarter) && $quarter == 5 ) $quarter = '1,2,3,4';

        $data['year']   = $year;
        $data['quarter']= $quarter;
        $qry = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '0' AND `status` = '1' ORDER BY `order_id` ASC");
        $data['result'] = $qry->result();
        $this->load->view('admin/label/mf-report-search', $data);
    }

    public function show_project_divisions(){
        $project_id = $this->input->post('project_id');

        $result = $this->Post_model->custom_query("SELECT * from division where project_id = '$project_id'");

        $string = "<option value=''>--SELECT DIVISION--</option>";
            
         foreach ($result as $key=>$val){
                 $string .= "<option value=".$val->id.">".$val->division_name."</option>";
         }

         echo $string;


    }

    public function data_table(){

       $aColumns = array('SL','device','meeting_name', 'meeting_start_time', 'meeting_end_time', 'username', 'no_of_participation', 'no_of_participation_end', 'district_name', 'upazila_name', 'union_name', 'action');
       $afilterColumns = array('meeting.id','meeting.flag','meeting_name', 'meeting_start_time', 'meeting_end_time', 'username', 'no_of_participation', 'no_of_participation_end', 'district.name', 'upazila.name', 'union.name');
       $aCustomColumns = array('meeting.id as meeting_id','meeting_name','meeting.flag as device', 'meeting_start_time', 'meeting_end_time', 'username', 'no_of_participation', 'no_of_participation_end', 'district.name as district_name', 'upazila.name as upazila_name', 'union.name as union_name');

       $qry_str = $this->session->userdata('qry_str');

       $sIndexColumn = "id";
       $sTable = "meeting";

        /*Paging*/
        $sLimit = " LIMIT 20";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".$_GET['iDisplayStart'].", ".$_GET['iDisplayLength'];
        }

        /*Ordering*/
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $afilterColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                    ".$_GET['sSortDir_'.$i].", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }else{
            $sOrder = "order by meeting.meeting_start_time desc";
        }

        /* Filtering*/
        $sWhere = "";
        if ( $_GET['sSearch'] != "" ){
            if($qry_str == ''){
                $sWhere = "WHERE (";
            }else{
                $sWhere = "AND (";
            }
            
            for ( $i=0 ; $i<count($afilterColumns) ; $i++ ){
                $sWhere .= $afilterColumns[$i]." LIKE '%".$_GET['sSearch']."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }


        /* Individual column filtering */
        for ( $i=0 ; $i<count($afilterColumns) ; $i++ )
        {
            if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $qry_str == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                $sWhere .= $afilterColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
            }
        }

        $join = "join district on meeting.district_id = district.id join upazila on meeting.upozila_id = upazila.id join `union` on meeting.union_id = union.id join ward on meeting.word_id = ward.id left join app_user_info on meeting.users_id = app_user_info.id";
        
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aCustomColumns))." FROM $sTable $join $qry_str $sWhere $sOrder $sLimit";

        $rResult = $this->Post_model->custom_query($sQuery);

        
        $sQuery = "SELECT FOUND_ROWS()";
        $iFilteredTotal = $this->Post_model->custom_query_single_row($sQuery);
       

        $sQuery = "SELECT COUNT(".$sIndexColumn.") FROM  $sTable";
        $iTotal = $this->Post_model->custom_query_single_row($sQuery);

        /*Output*/
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        $j = 1;
        $index = 0;

        if(!empty($rResult)){
            foreach($rResult as $aRow){
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                    if($aColumns[$i] == 'SL'){
                        $row[] = $j;
                    }elseif($aColumns[$i] == 'device'){
                        $row[] = ($aRow->device=="1") ? 'pc' : 'mobile';
                    }elseif($aColumns[$i] == 'meeting_start_time'){
                        $row[] = date("d F Y, h:i:s A", strtotime($aRow->meeting_start_time));
                    }elseif($aColumns[$i] == 'meeting_end_time'){
                        $row[] = date("d F Y, h:i:s A", strtotime($aRow->meeting_end_time));
                    }elseif($aColumns[$i] == 'action'){
                        $row[] = "<a href='javascript:void(0)'' title='view' onclick = \" modal_show(".$aRow->meeting_id.") \" ><i class='fa fa-fw fa-eye'></i></a> /
                                    <a href='meeting_edit/".$aRow->meeting_id."' title='Edit'><i class='fa fa-fw fa-edit'></i></a> / <a href='meeting_map/".$aRow->meeting_id."' target='_blank' title='View meeting in google map'><i
                                            class='fa fa-map-marker'></i></a> / <a href='meeting_delete/".$aRow->meeting_id."' onclick= \" return confirm('Are you sure you wish to delete this ?') \"
                                       title='Delete'><i class='fa fa-fw fa-trash-o'></i></a>";;
                    }
                    elseif($aColumns[$i] != ' '){
                        $row[] = $aRow->$aColumns[$i];
                    }

                     
                }
                $j++;  
                $output['aaData'][] = $row;
            }
        }    
    

        echo json_encode( $output );
    }

    public function data_table_survey(){

        $aColumns = array('SL','username', 'ans_name', 'created_at', 'disname', 'upaname', 'unname', 'wardname','action');
        $afilterColumns = array('form_info.id', 'app_user_info.username', 'form_info.ans_name', 'form_info.created_at', 'district.name', 'upazila.name', '`union`.name', 'ward.name');
        $aCustomColumns = array('form_info.id as id', 'app_user_info.username as username', 'form_info.ans_name as ans_name', 'form_info.created_at as created_at', 'district.name as disname', 'upazila.name as upaname', '`union`.name as unname', 'ward.name as wardname');
 
        $qry_str = $this->session->userdata('qry_str');
 
        $sIndexColumn = "id";
        $sTable = "form_info";
 
         /*Paging*/
         $sLimit = " LIMIT 20";
         if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
             $sLimit = "LIMIT ".$_GET['iDisplayStart'].", ".$_GET['iDisplayLength'];
         }
 
         /*Ordering*/
         if ( isset( $_GET['iSortCol_0'] ) ){
             $sOrder = "ORDER BY  ";
             for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                 if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                     $sOrder .= $afilterColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                     ".$_GET['sSortDir_'.$i].", ";
                 }
             }
 
             $sOrder = substr_replace( $sOrder, "", -2 );
             if ( $sOrder == "ORDER BY" ){
                 $sOrder = "";
             }
         }else{
             $sOrder = "order by form_info.created_at desc";
         }
 
         /* Filtering*/
         $sWhere = "";
         if ( $_GET['sSearch'] != "" ){
             if($qry_str == ''){
                 $sWhere = "WHERE (";
             }else{
                 $sWhere = "AND (";
             }
             
             for ( $i=0 ; $i<count($afilterColumns) ; $i++ ){
                 $sWhere .= $afilterColumns[$i]." LIKE '%".$_GET['sSearch']."%' OR ";
             }
             $sWhere = substr_replace( $sWhere, "", -3 );
             $sWhere .= ')';
         }
 
 
         /* Individual column filtering */
         for ( $i=0 ; $i<count($afilterColumns) ; $i++ )
         {
             if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
             {
                 if ( $qry_str == "" )
                 {
                     $sWhere = "WHERE ";
                 }
                 else
                 {
                     $sWhere .= " AND ";
                 }
                 $sWhere .= $afilterColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
             }
         }
 
         $join = "join district on form_info.district_id = district.id join upazila on form_info.upozila_id = upazila.id join `union` on form_info.union_id = union.id join ward on form_info.word_id = ward.id left join app_user_info on form_info.login_op_id = app_user_info.id";
         
         $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aCustomColumns)).", form_info.form_code as form_code, form_info.id as id, form_info.survey_info_id as survey_info_id  FROM $sTable $join $qry_str $sWhere $sOrder $sLimit";
 
         $rResult = $this->Post_model->custom_query($sQuery);

        //  echo "<pre>";
        //  print_r( $rResult);
        //  echo "</pre>";
 
         $sQuery = "SELECT FOUND_ROWS()";
         $iFilteredTotal = $this->Post_model->custom_query_single_row($sQuery);
        
 
         $sQuery = "SELECT COUNT(".$sIndexColumn.") FROM  $sTable";
         $iTotal = $this->Post_model->custom_query_single_row($sQuery);
 
         /*Output*/
         $output = array(
             "sEcho" => intval($_GET['sEcho']),
             "iTotalRecords" => $iTotal,
             "iTotalDisplayRecords" => $iFilteredTotal,
             "aaData" => array()
         );
 
         $j = 1;
         $index = 0;
 
         if(!empty($rResult)){
             foreach($rResult as $aRow){
                
                 $row = array();
                 for ( $i=0 ; $i<count($aColumns) ; $i++ ){

                     if($aColumns[$i] == 'SL'){
                         $row[] = $j;
                     }elseif($aColumns[$i] == 'created_at'){
                         $row[] = date("d F Y, h:i:s A", strtotime($aRow->created_at));
                     }elseif($aColumns[$i] == 'action'){
                         $row[] = "<a href='survey_question_view/".$aRow->form_code."/".$aRow->survey_info_id."' title='view' target='_blank'><i class='fa fa-fw fa-eye'></i></a> /
                                        <a href='survey_qs_edit/".$aRow->id."/".$aRow->survey_info_id."' title='Edit'><i class='fa fa-fw fa-edit'></i></a> / 
                                        <a href='survey_question_report_delete/".$aRow->form_code."' onclick= \" return confirm('Are you sure you wish to delete this ?') \" title='Delete'><i class='fa fa-fw fa-trash-o'></i></a>";;
                     }else{
                         $row[] = $aRow->$aColumns[$i];
                     }

                    //  echo "<pre>";
                    //  print_r($aColumns[$i]);
                    //  echo '</pre>';
                 }
                 $j++;  
                 $output['aaData'][] = $row;
             }
         }    
     
 
         echo json_encode( $output );
     }


    public function get_meeting_info($meeting_id){
       $data = array(); 
       $sTable = "meeting";
       $aCustomColumns = array('meeting.id as meeting_id','meeting_name','meeting.flag as device', 'meeting_start_time', 'meeting_end_time', 'username', 'no_of_participation', 'no_of_participation_end', 'district.name as district_name', 'upazila.name as upazila_name', 'union.name as union_name', 'ward.name as ward_name','meeting.meeting_start_picture_name as meeting_start_picture_name','meeting.meeting_start_picture_end as meeting_start_picture_end, meeting.meeting_start_time as meeting_start_time, meeting.meeting_end_time as meeting_end_time, meeting.start_picture_time as start_picture_time, meeting.end_picture_time as end_picture_time');

       $join = "join district on meeting.district_id = district.id join upazila on meeting.upozila_id = upazila.id join `union` on meeting.union_id = union.id join ward on meeting.word_id = ward.id left join app_user_info on meeting.users_id = app_user_info.id";
       $query = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aCustomColumns))." FROM $sTable $join WHERE meeting.id = '$meeting_id'";

       $data['results'] = $this->Post_model->custom_query($query);
      
       $this->load->view('admin/meeting/meeting-view-details', $data);

    }

    
}