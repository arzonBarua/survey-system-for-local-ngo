<?php defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 10:40 AM
 */
class Location extends CI_Controller
{
    public $ids = array();
    public function __construct(){

        parent::__construct();

        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }


        if($this->session->userdata('user_type')!='1'){
             $this->load->library('permission');
             $this->ids = $this->permission->get_assigned_ids();
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        $this->Menu_model->menu_model_info();
        $this->load->library('form_validation');
        $this->load->model('Post_model');
    }

    /* Division Work */
    public function division_add(){
        $data['menu_title'] = "Add Division";

        if(empty($_POST)){
            $this->load->view('admin/location/division-add',$data);
        }else{
            $input['division_name'] = $this->input->post('division_name');
            $input['status'] = $this->input->post('status');

            if ($this->Post_model->insert('division', $input)) {
                $this->session->set_flashdata('success_message', 'Data inserted successfully');
                redirect('admin/location/division_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/division-add', $data);
            }
        }
    }

    public function division_edit($id = null) {
        $this->session->set_userdata('referrel_link','division_view');

        $data['menu_title'] = "Edit Division";

        $id = ($id == null) ? $this->input->post("id") : $id;

        $data['result'] = $this->Post_model->getAll('division', '', '', $id);

        if (empty($_POST)) {
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');

            $this->load->view('admin/location/division-edit', $data);
        }else{

            $input['division_name'] = $this->input->post('division_name');
            $input['status'] = $this->input->post('status');

            if ($this->Post_model->update('division', $input, $id)) {
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/location/division_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/division-edit/', $data);
            }
        }
    }

    public function division_view(){
        $data['menu_title'] = "View Division";
        $data['result'] = $this->Post_model->getAll('division', 'id', 'ASC');


        if ($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/location/division-view', $data);
    }

    public function division_delete($id){
        $this->db->query("DELETE FROM `ward` WHERE `div_id` = '".$id."'");
        $this->db->query("DELETE FROM `union` WHERE `div_id` = '".$id."'");
        $this->db->query("DELETE FROM `upazila` WHERE `div_id` = '".$id."'");
        $this->db->query("DELETE FROM `district` WHERE `div_id` = '".$id."'");
        $this->Post_model->delete('division', $id, '');
        
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/location/division_view');
    }


    /* District Work */
    public function district_add(){
        $data['menu_title'] = "Add District";

        $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');

        if(empty($_POST)){
            $this->load->view('admin/location/district-add',$data);
        }else{
            $input['div_id'] = $this->input->post('div_id');
            $input['name']   = $this->input->post('name');
            $input['status'] = $this->input->post('status');

            if ($this->Post_model->insert('district', $input)) {
                $this->session->set_flashdata('success_message', 'Data inserted successfully');
                redirect('admin/location/district_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/district-add', $data);
            }
        }
    }

    public function district_edit($id = null) {
         $this->session->set_userdata('referrel_link','district_view');
        $data['menu_title'] = "Edit District";

        $id = ($id == null) ? $this->input->post("id") : $id;
        $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');

        $data['result'] = $this->Post_model->getAllByFieldName('district', 'id', $id);

        if (empty($_POST)) {
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');

            $this->load->view('admin/location/district-edit', $data);
        }else{

            $input['div_id'] = $this->input->post('div_id');
            $input['name'] = $this->input->post('name');
            $input['status'] = $this->input->post('status');

            if ($this->Post_model->update('district', $input, $id)) {
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/location/district_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/district-edit/', $data);
            }
        }
    }

    public function district_view(){
        $data['menu_title'] = "View District";
        $data['result'] = $this->Post_model->custom_query("SELECT `district`.*, `division`.`division_name` FROM `district` LEFT JOIN `division` ON `district`.`div_id` = `division`.`id`");


        if ($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/location/district-view', $data);
    }

    public function district_delete($id){
        $this->db->query("DELETE FROM `ward` WHERE `district_id` = '".$id."'");
        $this->db->query("DELETE FROM `union` WHERE `district_id` = '".$id."'");
        $this->db->query("DELETE FROM `upazila` WHERE `district_id` = '".$id."'");
        $this->Post_model->delete('district', $id, '');
        
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/location/district_view');
    }


    /* upazila Work */
    public function upazila_add(){
        $data['menu_title'] = "Add Upazila";

        $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        $data['district'] = $this->Post_model->getAll('district', 'id', 'ASC');

        if(empty($_POST)){
            $this->load->view('admin/location/upazila-add',$data);
        }else{
            $input['div_id'] = $this->input->post('div_id');
            $input['district_id'] = $this->input->post('district_id');
            $input['name']   = $this->input->post('name');

            if ($this->Post_model->insert('upazila', $input)) {
                $this->session->set_flashdata('success_message', 'Data inserted successfully');
                redirect('admin/location/upazila_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/upazila-add', $data);
            }
        }
    }

    public function upazila_edit($id = null) {
        $this->session->set_userdata('referrel_link','upazila_view');
        $data['menu_title'] = "Edit Upazila";

        $id = ($id == null) ? $this->input->post("id") : $id;
        // $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        // $data['district'] = $this->Post_model->getAll('district', 'id', 'ASC');
        $data['result'] = $this->Post_model->getAllByFieldName('upazila', 'id', $id);

        $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        $data['district'] = $this->Post_model->getAllByFieldName('district', 'div_id', $data['result'][0]->div_id);
        

        if (empty($_POST)) {
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');

            $this->load->view('admin/location/upazila-edit', $data);
        }else{

            $input['div_id'] = $this->input->post('div_id');
            $input['district_id'] = $this->input->post('district_id');
            $input['name'] = $this->input->post('name');

            if ($this->Post_model->update('upazila', $input, $id)) {
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/location/upazila_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/upazila-edit/', $data);
            }
        }
    }

    public function upazila_view(){
        $data['menu_title'] = "View Upazila";
        $data['result'] = $this->Post_model->custom_query("SELECT `upazila`.*, `district`.`name` as district_name, `division`.`division_name` FROM `upazila` LEFT JOIN `district` ON `district`.`id` = `upazila`.`district_id` LEFT JOIN `division` ON `upazila`.`div_id` = `division`.`id`");


        if ($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/location/upazila-view', $data);
    }

    public function upazila_delete($id){
        $this->db->query("DELETE FROM `ward` WHERE `upazila_id` = '".$id."'");
        $this->db->query("DELETE FROM `union` WHERE `upazila_id` = '".$id."'");
        $this->Post_model->delete('upazila', $id, '');
        
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/location/upazila_view');
    }

    /* Union Work */
    public function union_add(){
        $data['menu_title'] = "Add Union";

        $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        $data['district'] = $this->Post_model->getAll('district', 'id', 'ASC');
        $data['upazila'] = $this->Post_model->getAll('upazila', 'id', 'ASC');

        if(empty($_POST)){
            $this->load->view('admin/location/union-add',$data);
        }else{
            $input['div_id'] = $this->input->post('div_id');
            $input['district_id'] = $this->input->post('district_id'); 
            $input['upazila_id'] = $this->input->post('upazila_id');
            $input['name']   = $this->input->post('name');

            if ($this->Post_model->insert('union', $input)) {
                $this->session->set_flashdata('success_message', 'Data inserted successfully');
                redirect('admin/location/union_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/union-add', $data);
            }
        }
    }

    public function union_edit($id = null) {
        $this->session->set_userdata('referrel_link','union_view');
        $data['menu_title'] = "Edit Unoin";

        $id = ($id == null) ? $this->input->post("id") : $id;
        // $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        // $data['district'] = $this->Post_model->getAll('district', 'id', 'ASC');
        $data['result'] = $this->Post_model->getAllByFieldName('union', 'id', $id);

        $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        $data['district'] = $this->Post_model->getAllByFieldName('district', 'div_id', $data['result'][0]->div_id);
        $data['upazila'] = $this->Post_model->getAllByFieldName('upazila', 'district_id', $data['result'][0]->district_id);
        

        if (empty($_POST)) {
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');

            $this->load->view('admin/location/union-edit', $data);
        }else{

            $input['div_id'] = $this->input->post('div_id');
            $input['district_id'] = $this->input->post('district_id');
            $input['upazila_id'] = $this->input->post('upazila_id');
            $input['name'] = $this->input->post('name');

            if ($this->Post_model->update('union', $input, $id)) {
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/location/union_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/union-edit/', $data);
            }
        }
    }

    public function union_view(){
        $data['menu_title'] = "View Union";
        $data['result'] = $this->Post_model->custom_query("SELECT `union`.*, `upazila`.`name` as upazila_name, `district`.`name` as district_name, `division`.`division_name` FROM `union` 
                            LEFT JOIN `upazila` ON `upazila`.`id` = `union`.`upazila_id` 
                            LEFT JOIN `district` ON `district`.`id` = `union`.`district_id` 
                            LEFT JOIN `division` ON `division`.`id` = `union`.`div_id`");


        if ($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/location/union-view', $data);
    }

    public function union_delete($id){
        $this->db->query("DELETE FROM `ward` WHERE `union_id` = '".$id."'");
        $this->Post_model->delete('union', $id, '');
        
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/location/union_view');
    }


    /* Ward Work */
    public function ward_add(){
        $data['menu_title'] = "Add Ward";

        $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        $data['district'] = $this->Post_model->getAll('district', 'id', 'ASC');
        $data['upazila'] = $this->Post_model->getAll('upazila', 'id', 'ASC');
        $data['union'] = $this->Post_model->getAll('union', 'id', 'ASC');

        if(empty($_POST)){
            $this->load->view('admin/location/ward-add',$data);
        }else{
            $input['div_id'] = $this->input->post('div_id');
            $input['district_id'] = $this->input->post('district_id');
            $input['upazila_id'] = $this->input->post('upazila_id');
            $input['union_id'] = $this->input->post('union_id');
            $input['name']   = $this->input->post('name');
            if ($this->Post_model->insert('ward', $input)) {
                $this->session->set_flashdata('success_message', 'Data inserted successfully');
                redirect('admin/location/ward_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/ward-add', $data);
            }
        }
    }

    public function ward_edit($id = null) {
         $this->session->set_userdata('referrel_link','ward_view');
        $data['menu_title'] = "Edit Ward";

        $id = ($id == null) ? $this->input->post("id") : $id;
        // $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        // $data['district'] = $this->Post_model->getAll('district', 'id', 'ASC');
        $data['result'] = $this->Post_model->getAllByFieldName('ward', 'id', $id);

        $data['division'] = $this->Post_model->getAll('division', 'id', 'ASC');
        $data['district'] = $this->Post_model->getAllByFieldName('district', 'div_id', $data['result'][0]->div_id);
        $data['upazila'] = $this->Post_model->getAllByFieldName('upazila', 'district_id', $data['result'][0]->district_id);
        $data['union'] = $this->Post_model->getAllByFieldName('union', 'upazila_id', $data['result'][0]->upazila_id);
        
        if (empty($_POST)) {
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');

            $this->load->view('admin/location/ward-edit', $data);
        }else{

            $input['div_id'] = $this->input->post('div_id');
            $input['district_id'] = $this->input->post('district_id');
            $input['upazila_id'] = $this->input->post('upazila_id');
            $input['union_id'] = $this->input->post('union_id');
            $input['name'] = $this->input->post('name');

            if ($this->Post_model->update('ward', $input, $id)) {
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/location/ward_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/location/ward-edit/', $data);
            }
        }
    }

    public function ward_view(){
        $data['menu_title'] = "View Ward";
        $data['result'] = $this->Post_model->custom_query("SELECT `ward`.*, `union`.`name` as union_name, `upazila`.`name` as upazila_name, `district`.`name` as district_name, `division`.`division_name` FROM `ward` 
                            LEFT JOIN `union` ON `union`.`id` = `ward`.`union_id` 
                            LEFT JOIN `upazila` ON `upazila`.`id` = `ward`.`upazila_id` 
                            LEFT JOIN `district` ON `district`.`id` = `ward`.`district_id` 
                            LEFT JOIN `division` ON `division`.`id` = `ward`.`div_id`");


        if ($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/location/ward-view', $data);
    }

    public function ward_delete($id){        
        $this->Post_model->delete('ward', $id, '');
        
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/location/ward_view');
    }
}
