<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }

        if($this->session->userdata('user_type')!='1'){
             $this->load->library('permission');
             $this->ids = $this->permission->get_assigned_ids();
        }

        $this->Menu_model->menu_model_info();

        $this->load->library('form_validation');
        $this->load->helper('file');
        $this->load->model('User_model');
        $this->load->model('Post_model');
    }

    public function index()
    {
        $data['menu_title'] = "Dashboard";

       
        $qry_str_date = "";

        if(!empty($this->ids)){
            //$qry_str = " where physical_location.users_id in (".implode(',',$this->ids).") ";
            $qry_str_date = "and physical_location.users_id in (".implode(',',$this->ids).")";
        }

        // echo $string_query = "select max(date_time)as max_date_time from physical_location $qry_str";
        // $data['max_date'] = $this->Post_model->custom_query($string_query);
        $max_date_time = date('Y-m-d');
        
        //$string_query = "select * from physical_location where date_time like '$max_date_time%'";
//        $string_query = "select physical_location.*, meeting.meeting_name meeting from physical_location join meeting on physical_location.meeting_id = meeting.id where physical_location.date_time like '$max_date_time%' and physical_location.users_id='$user_id'";
       $string_query = "select physical_location.*, meeting.meeting_name meeting from physical_location join meeting on physical_location.meeting_id = meeting.id where physical_location.date_time like '$max_date_time%' $qry_str_date";

        $data['result'] = $this->Post_model->custom_query($string_query);

        // echo "<pre>";
        // print_r($data['result']);
        // echo "</pre>";
        // die;

        

        $locationData = "";


        if($data['result']) {
            $i = 0;
            foreach ($data['result'] as $key => $val) {
                if($i==0){
                    $data['first_lat'] = "'".$val->latitude."'";
                    $data['first_lon'] = "'".$val->longitude."'";
                    $i++;
                }
                $locationData .= "['" . $val->meeting . "','" . $val->latitude . "','" . $val->longitude . "'],";
            }
            $data['locationData'] = "[" . rtrim($locationData, ',') . "]";
        }



        $this->load->view('admin/index',$data);
    }


    public function deny_page(){
        $data['menu_title'] = "Permission";
        $this->load->view('admin/deny',$data);
    }

    public function user_profile(){
        $id = $this->session->userdata('id');
        $data['menu_title'] = "User Profile";
        $data['type_list'] = $this->Post_model->getAll('admin_type','id');
        $data['result'] = $this->Post_model->getAll('admin_login','','',$id);

        if(empty($_POST)){
            if($data['result']==0)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/user/user-profile', $data);
        }else{

            $input['full_name'] = $this->input->post('full_name');
            $input['department'] = $this->input->post('department');
            $input['designation'] = $this->input->post('designation');
            $input['email'] = $this->input->post('email');

            if($this->input->post('password')){
                $input['password'] = md5($this->input->post('password'));
            }

            $input['updated_at'] = date('Y-m-d');


            /*For File Upload*/
            $file_upload = 1;
            if(!empty($_FILES['userfile']['name'])){

                $file_name = 'userfile';
                $upload_path = './assets/upload/profile/';

                $file_name_folder = (string)$data['result'][0]->picture;
                $full_path = $upload_path.$file_name_folder;

                //delete files

                if (file_exists($full_path)) {
                    $this->Common_operation->file_delete($full_path);
                }

                //delete files

                $result = $this->Common_operation->image_upload($file_name,$upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/user/user-profile', $data);
                    $file_upload = 0;
                }else{
                    $input['picture'] = $result['picture'];
                }
            }
            /*For File Upload*/
            if($file_upload){
                if ($this->Post_model->update('admin_login',$input,$id)){
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/home/user_profile');
                }else{
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/user/user-profile', $data);
                }
            }

        }

    }



    public function logout(){
        $this->session->unset_userdata('temp_id');
        $this->session->unset_userdata('temp_parent_id');
        $this->session->unset_userdata('project_id');

        $this->session->unset_userdata('picture');
        $this->session->unset_userdata('full_name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('designation');
        $this->session->unset_userdata('created_at');
        $this->session->unset_userdata('user_type');
        $this->session->unset_userdata('access');
        $this->session->unset_userdata('isLogedNirapod');
        $this->session->set_flashdata('logged_out', 'You have been logged out');

        redirect('admin/index');
    }


}
