<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 10:40 AM
 */
class Banner extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }


        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        $this->Menu_model->menu_model_info();
        $this->load->library('form_validation');
        $this->load->model('Post_model');
    }

    public function banner_add(){
        $data['menu_title'] = "Add Banner";
        if(empty($_POST)) {
            $this->load->view('admin/banner/banner-add', $data);
        }else{
            $this->form_validation->set_rules('banner_title','Banner Title', 'required');

            if($this->form_validation->run() == FALSE ){
                $this->load->view('admin/banner/banner-add', $data);
            }else{
                $input['banner_title'] = $this->input->post('banner_title');
                $input['details'] = $this->input->post('details');
                $input['created_at'] = date('Y-m-d');

               /*For File Upload*/
                $file_upload = 1;
                if(!empty($_FILES['userfile']['name'])){
                    $file_name = 'userfile';
                    $upload_path = './assets/upload/banner/';
                    $result = $this->Common_operation->image_upload($file_name, $upload_path);

                    if(!$result['flag']){
                        $data['error'] = $result['error'];
                        $this->load->view('admin/banner/banner-add', $data);
                        $file_upload = 0;
                    }else{
                        $input['picture'] = $result['picture'];
                    }
                }

                /*For File Upload*/
                if($file_upload){
                    if ($this->Post_model->insert('banner', $input)) {
                        $this->session->set_flashdata('success_message', 'Data inserted successfully');
                        redirect('admin/banner/banner_view');
                    } else {
                        $data = array('error' => "somehting went wrong please inform the webmaster");
                        $this->load->view('admin/banner/banner-add', $data);
                    }
                }
            }
        }
    }

    public function banner_edit($id){
        $data['menu_title'] = "Edit Banner";


        $data['result'] = $this->Post_model->getAll('banner', '', '', $id);

        if(empty($_POST)){
            if($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');

            $this->load->view('admin/banner/banner-edit', $data);
        }else{

            $input['banner_title'] = $this->input->post('banner_title');
            $input['details'] = $this->input->post('details');
            $input['updated_at'] = date('Y-m-d');

            /*For File Upload*/
            $file_upload = 1;
            if(!empty($_FILES['userfile']['name'])){
                $file_name = 'userfile';
                $upload_path = './assets/upload/banner/';

                $file_name_folder = (string)$data['result'][0]->picture;
                $full_path = $upload_path.$file_name_folder;

                if (file_exists($full_path)) {
                    $this->Common_operation->file_delete($full_path);
                }



                $result = $this->Common_operation->image_upload($file_name, $upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/banner/banner-edit', $data);
                    $file_upload = 0;
                }else{
                    $input['picture'] = $result['picture'];
                }
            }
            /*For File Upload*/
            if($file_upload){
                if ($this->Post_model->update('banner', $input, $id)){
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/banner/banner_view');
                }else{
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/banner/banner-edit', $data);
                }
            }
        }
    }

    public function banner_view(){
        $data['menu_title'] = "View Banner";
        $data['result'] = $this->Post_model->getAll('banner', 'banner_title', '');
        if($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/banner/banner-view', $data);
    }

    public function banner_delete($id){
        $result = $this->Post_model->getAll('banner', '', '', $id);
        $file_name = (string)$result[0]->picture;

        if(!empty($file_name)){
            $upload_path = './assets/upload/banner/'.$file_name;
            $this->Post_model->delete('banner', $id, $upload_path);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/banner/banner_view');
    }
}
