<?php defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();

class Survey extends CI_Controller {

    public $ids = array();

    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }

        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            $this->ids = $this->permission->get_assigned_ids();
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }

        $this->Menu_model->menu_model_info();

        $this->load->library('form_validation');
        $this->load->helper('file');
        $this->load->model('User_model');
        $this->load->model('Post_model');


        // Excel intregation 
        include APPPATH . 'third_party/Excel.php';
    }

    /*-----------------------------------------------------------------------Survey Info Portion-----------------------------------------------------------------*/

    public function survey_add(){
        
        $data['menu_title'] = "Add Questionnaire Type";

        if(empty($_POST)){
            //$data['projects_id'] = $this->Post_model->getAll('projects');



            $this->load->view('admin/survey/survey-info-add',$data);
        }else{

            $input['survey_name'] = $this->input->post('survey_name');
            //$input['project_type'] = $this->input->post('project_type');
            $input['publish'] = 1;

            // $dateRange = explode("-",$this->input->post('date_range'));

            // $input['start_date'] = date('Y-m-d',strtotime($dateRange[0]));
            // $input['end_date'] = date('Y-m-d',strtotime($dateRange[1]));

            $input['created_at'] = date('Y-m-d');


            if ($this->Post_model->insert('survey_info', $input)) {
                $this->session->set_flashdata('success_message', 'Data inserted successfully');
                redirect('admin/survey/survey_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/survey/survey-info-add', $data);
            }

        }

    }

    public function survey_edit($id = null){
        $data['menu_title'] = "Edit Questionnaire Type";

        $id = ($id == null) ? $this->input->post("id") : $id;

        $data['result'] = $this->Post_model->getAll('survey_info', '', '', $id);


        if (empty($_POST)) {
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');


            //$data['projects_id'] = $this->Post_model->getAll('projects');
            $this->load->view('admin/survey/survey-info-edit', $data);
        }else{

                $input['survey_name'] = $this->input->post('survey_name');
                //$input['project_type'] = $this->input->post('project_type');
                $input['publish'] = 1;

            // $input['publish'] = $this->input->post('publish');
            // $dateRange = explode("-",$this->input->post('date_range'));

            // $input['start_date'] = date('Y-m-d',strtotime($dateRange[0]));
            // $input['end_date'] = date('Y-m-d',strtotime($dateRange[1]));

            $input['updated_at'] = date('Y-m-d');

            if ($this->Post_model->update('survey_info', $input, $id)) {
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/survey/survey_view');
            } else {
                $data = array('error' => "somehting went wrong please inform the webmaster");
                $this->load->view('admin/survey/survey-info-edit/', $data);
            }

        }


    }

    public function survey_view(){
        $data['menu_title'] = "View Questionnaire Type";
        $data['result'] = $this->Post_model->getAll('survey_info', 'id', 'ASC');

        if ($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/survey/survey-info-view', $data);
    }

    public function survey_delete($id)
    {
        $this->Post_model->delete('survey_info', $id, '', '');

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/survey/survey_view');
    }


    /*-----------------------------------------------------------------------Survey Question Information Portion-----------------------------------------------------------------*/

    public function survey_qs_add(){
        $data['menu_title'] = "Add Survey Question";
        $data['result'] = $this->Post_model->getAll('survey_info', 'id', 'ASC');


        if(empty($_POST)){
            $this->load->view('admin/survey/survey-qs-add',$data);
        }else{
            $count = count($this->input->post('count_qs'));

            $input_qs = array();
            $input_option = array();

            // echo "<pre>";
            // print_r($_POST);
            // echo '</pre>';
            // die;
            for($j=0;$j<$count;$j++){
                // $track = 1;
                // if($j == 4 || $j == 5 || $j == 6 || $j == 7){
                //     $track = 0;
                // }

                //if($track){
                    $count_qs = $_POST['count_qs'][$j];
                    $input_qs['survey_info_id'] = $_POST['survey_info_id'];
                    $input_qs['survey_qs'] = $_POST["survey_qs_$count_qs"];
                    $input_qs['survey_qs_type'] = $_POST["survey_qs_type_$count_qs"];
                    $input_qs['main_ques_id'] = $_POST["main_ques_id_$count_qs"];
                    $input_qs['order_id'] = $_POST["order_id_$count_qs"];
                    $input_qs['created_at'] = date('Y-m-d h:i:s');
                    $input_qs['created_by'] = $this->session->userdata('id');

                    $this->Post_model->insert('survey_qs', $input_qs);
                    $survey_qs_id = $this->db->insert_id();

                    $qs_count = count($_POST["option_$count_qs"]);
                    for($k=0;$k<$qs_count;$k++){
                        $input_option['survey_qs_id'] = $survey_qs_id;
                        $input_option['option_value'] = $_POST["option_$count_qs"][$k];
                        $input_option['option_code'] = $_POST["meta_$count_qs"][$k];
                        $input_option['option_order'] = $_POST["option_order_$count_qs"][$k];

                        $input_option['created_at'] = date('Y-m-d h:i:s');
                        $input_option['created_by'] = $this->session->userdata('id');
                        $this->Post_model->insert('survey_option', $input_option);
                    }
                //}

            }

            $this->session->set_flashdata('success_message', 'Data Deleted successfully');
            redirect('admin/survey/survey_qs_view');

        }

    }

    public function survey_qs_view(){
        $data['menu_title'] = "View Survey Question";
        $data['result'] = $this->Post_model->getAll('survey_info', 'id', 'DESC');

        if ($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/survey/survey-info-qs-view', $data);
    }

/*
    public function get_ajax()
    {
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $className = $this->input->post('field');

        $fieldName =  $className."_id";

        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
        echo $str;
    }
    */


    /***************************************Questionior Part ****************************************/

    public function form_show(){
        $data['menu_title'] = "Add New Survey";
        $data['result'] = $this->Post_model->getAll('survey_info', 'id', 'DESC');

        $data['district'] = $this->Post_model->getAll('district', 'name', 'ASC');




        if (empty($_POST)) {
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');

            $this->load->view('admin/survey/survey-qs-form-select', $data);
        }else{
            $id = $this->input->post('survey_info_id');
            $data['survey_info'] = $this->Post_model->getAllByFieldName('survey_info','id',$id);

            $acl_ext_query = "";
            if(!empty($this->ids)){
               $acl_ext_query = " where id in (".implode(',',$this->ids).") ";
            }
            $data['op_ids'] = $this->Post_model->custom_query("SELECT * FROM app_user_info $acl_ext_query");
            
            //$data['op_ids'] = $this->Post_model->getAll('app_user_info');
            $data['projects_id'] = $this->Post_model->custom_query('SELECT * FROM projects where id != 5');



            $data['survey_question'] = $this->Post_model->custom_query("select * from survey_qs where survey_info_id='$id' order by order_id ASC");


            $this->load->view('admin/survey/survey-as-form-entry',$data);
        }
    }

    public function survey_question_entry(){

        $characters = '0123456789';
        $randomString = '';
        for ($j = 0; $j < 4; $j++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        // echo "<pre>";
        // print_r($_POST);
        // echo "</pre>";

        //die;


        $input['form_code'] = date('YmdHis').$randomString;
        $input['project_type'] = $this->input->post('project_type');

        $input['survey_info_id'] = $this->input->post('survey_info_id');
        $input['ans_name'] = $this->input->post('ans_name');
        $input['ans_age'] = $this->input->post('ans_age');
        $input['ans_gender'] = $this->input->post('ans_gender');

        if($input['project_type']=='phulki'){
            $input['name_garments'] = $this->input->post('name_garments');
            $input['department_name'] = $this->input->post('department_name');
            $input['id_number'] = $this->input->post('id_number');
            $input['address_garments'] = $this->input->post('address_garments');
        }else{
            $input['district_id'] = $this->input->post('district_id');
            $input['upozila_id'] = $this->input->post('upozila_id');
            $input['union_id'] = $this->input->post('union_id');
            $input['word_id'] = $this->input->post('word_id');
        }

        unset($input['project_type']);

        $input['mobile_number'] = $this->input->post('mobile_number');
        $input['login_op_id'] = $this->input->post('login_op_id');
        $input['projects_id'] = $this->input->post('projects_id');
        $input['created_at'] = date('Y-m-d h:i:s');
        $input['created_by'] = $this->session->userdata('id');


        $this->Post_model->insert('form_info',$input);

        $data['result'] = $this->Post_model->getAllByFieldName('survey_qs','survey_info_id',$input['survey_info_id']);

        // echo '<pre>';
        // print_r($data['result']);
        // echo '</pre>';

        foreach($data['result'] as $key => $val ){
            
             $input_values['form_code'] = $input['form_code'];
             $input_values['created_at'] = date('Y-m-d h:i:s');
             $input_values['created_by'] = $this->session->userdata('id');

            if(isset($input_values['review_ans'])){
                unset($input_values['review_ans']);
            }
            if(isset($input_values['option_id'])){
                unset($input_values['option_id']);
            }

             if((string)$val->survey_qs_type=='checkbox'){
                
                if( !empty($_POST[$val->id]) && (count($_POST[$val->id]) > 0) ){
                    foreach($_POST[$val->id] as $values){
                        $qs_split = explode('_',$values);
                        $input_values['survey_qs_id'] = $qs_split[0];
                        $input_values['option_id']  =  $qs_split[1];   
                        $this->Post_model->insert('assessment_info',$input_values); 
                    }
                }else{
                     $input_values['survey_qs_id'] = $val->id;
                     $input_values['option_id']  =  9999; 
                     $this->Post_model->insert('assessment_info',$input_values);
                }
                

             }elseif((string)$val->survey_qs_type=='text'){
                
                if(!empty($_POST[$val->id])){
                    $input_values['survey_qs_id'] = $val->id;
                    $input_values['review_ans'] = $_POST[$val->id];
                }else{
                     $input_values['survey_qs_id'] = $val->id;
                     $input_values['review_ans']  =  'N/A';
                }
                $this->Post_model->insert('assessment_info',$input_values);

             }elseif((string)$val->survey_qs_type=='datepicker'){
                
                if(!empty($_POST[$val->id])){
                    $input_values['survey_qs_id'] = $val->id;
                    $input_values['review_ans'] = date('Y-m-d',strtotime($_POST[$val->id])); 
                }else{
                     $input_values['survey_qs_id'] = $val->id;
                     $input_values['review_ans']  =  'N/A';
                }
                $this->Post_model->insert('assessment_info',$input_values);   

             }elseif((string)$val->survey_qs_type=='time'){

                if(!empty($_POST[$val->id])){
                    $input_values['survey_qs_id'] = $val->id;
                    $input_values['review_ans'] = date('h:i:s',strtotime($_POST[$val->id]));
                }else{
                    $input_values['survey_qs_id'] = $val->id;
                    $input_values['review_ans']  =  'N/A';
                }
                $this->Post_model->insert('assessment_info',$input_values);    

             }elseif((string)$val->survey_qs_type=='radio'){

                
                if(!empty($_POST[$val->id])){
                     $qs_split = explode('_',$_POST[$val->id]);
                     $input_values['survey_qs_id'] = $qs_split[0];
                     $input_values['option_id']  =  $qs_split[1];  
                }else{
                    $input_values['survey_qs_id'] = $val->id;
                    $input_values['option_id']  =  9999;
                }

                // echo "<pre>";
                // print_r($_POST[$val->id]);
                // echo '</pre>';

                // echo "<pre>";
                // print_r($input_values);
                // echo '</pre>';

                // die;
                $this->Post_model->insert('assessment_info',$input_values); 

             }

        }

        $this->session->set_flashdata('success_message', 'Data inserted successfully');
        redirect('admin/survey/form_show');

    }

    public function survey_question_report(){
        $data['menu_title'] = "View Survey Result";
        $data['result'] = $this->Post_model->getAll('survey_info', 'id', 'DESC');
        //$data['projects_id'] = $this->Post_model->getAll('projects');
         $data['projects_id'] = $this->Post_model->custom_query('SELECT * FROM `projects` WHERE id !=5');

        $data['report_info'] = 0;
        $data['survey_info_id'] = 0;
        $data['projects_ids'] = 0;

        $qry_str = "";
        if(empty($_POST)){
            if ($data['result'] === false)
                 $this->session->set_flashdata('error_message', 'No data Found');
        }else{

            $qry_str = array();



            if(!empty($_POST)){
                $values = $_POST;
                if($values['daterangepicker'] != ""){
                    list($from_date, $to_date) = explode("-", $values['daterangepicker']);
                    $qry_str[] = "DATE_FORMAT(`form_info`.`created_at`, '%m/%d/%Y') BETWEEN '".trim($from_date)."' AND '".trim($to_date)."'";
                    //$qry_str[] = "DATE_FORMAT(`meeting_start_time`, '%m/%d/%Y') = '".$to_date."'";
                    $data['search_detail']['from_date'] = $from_date;
                    $data['search_detail']['to_date'] = $to_date;
                }

                if($values['survey_info_id']  != ""){
                    $qry_str[] = "`form_info`.`survey_info_id` = '".$values['survey_info_id']."'";
                    $data['search_detail']['survey_info_id'] = $values['survey_info_id'];
                }

                if($values['district_id']  != ""){
                    $qry_str[] = "`form_info`.`district_id` = '".$values['district_id']."'";
                    $data['search_detail']['district_id'] = $values['district_id'];
                }

                if($values['upozila_id']  != ""){
                    $qry_str[] = "`form_info`.`upozila_id` = '".$values['upozila_id']."'";
                    $data['search_detail']['upozila_id'] = $values['upozila_id'];
                }

                if($values['union_id']  != ""){
                    $qry_str[] = "`form_info`.`union_id` = '".$values['union_id']."'";
                    $data['search_detail']['union_id'] = $values['union_id'];
                }

                if($values['projects_id']  != ""){
                    $qry_str[] = "`form_info`.`projects_id` = '".$values['projects_id']."'";
                    $data['search_detail']['projects_id'] = $values['projects_id'];
                }

                if($values['login_op_id'] != ""){
                    $qry_str[] = "`form_info`.`login_op_id` = '".$values['login_op_id']."'";
                    $data['search_detail']['login_op_id'] = $values['login_op_id'];
                }
                
            }
        }

        if(!empty($this->ids)){
            $qry_str[] = " form_info.login_op_id in (".implode(',',$this->ids).") ";
            $data['ids'] = $this->ids;
        }

        $qry_str = ( $qry_str != '' ) ? ' WHERE '.implode(' AND ', $qry_str) : '';

        $string_query = "select form_info.*, district.name as disname, upazila.name as upaname, `union`.name as unname,  ward.name as wardname ,app_user_info.username as username from form_info left join district on form_info.district_id = district.id left join upazila on form_info.upozila_id = upazila.id left join `union` on form_info.union_id = union.id left join ward on form_info.word_id = ward.id left join app_user_info on form_info.login_op_id = app_user_info.id ".$qry_str." order by form_info.created_at desc";

        $data['report_info'] = $this->Post_model->custom_query($string_query);

        if($data['report_info']===false){
            $this->session->set_flashdata('error_message', 'No data Found');
        }

        $_SESSION['survey_result'] = $data['report_info'];

        $this->load->view('admin/survey/survey-qs-form-report', $data);
    }


    public function survey_question_view($form_code,$survey_info_id){

        $data['menu_title'] = "View Questionior Answer";

        $query = "select sq.id, sq.survey_qs, sq.order_id, ai.option_value, ai.review_ans, survey_info.survey_name from survey_qs sq left join (select assessment_info.survey_qs_id, assessment_info.review_ans, survey_option.option_value from assessment_info left join survey_option on assessment_info.option_id=survey_option.id where assessment_info.form_code='$form_code') ai on ai.survey_qs_id=sq.id left join survey_info on sq.survey_info_id = survey_info.id where sq.survey_info_id= '$survey_info_id' order by sq.order_id";

        $data['result'] = $this->Post_model->custom_query($query);


        $query = "select form_info.*, district.name as disname, upazila.name as upname, `union`.name as uniname, ward.name as wardname, projects.name as projectName, app_user_info.username as appname from form_info left join district on form_info.district_id = district.id left join upazila on form_info.upozila_id = upazila.id left join `union` on form_info.union_id = `union`.id left join ward on form_info.word_id = ward.id left join projects on form_info.projects_id = projects.id left join app_user_info on form_info.login_op_id = app_user_info.id where form_code='$form_code'";

        $data['common_results'] = $this->Post_model->custom_query($query);        

        $data['survey_name'] = $data['result'][0]->survey_name;

        $this->load->view('admin/survey/survey-qs-form-report-view',$data);
    }
    

    public function survey_download(){
        $report_array = array();
        $id_array = array();
        $code_array = array();
        $qs_name_array = array();
        $id = $this->input->post('survey_info_id');

        $query = "select *, assessment_info.survey_qs_id as sq_id, survey_qs.survey_qs as qs_tag, survey_option.option_value as option_tag, survey_option.option_code, projects.english_name as project_name, district.name as dis_name, upazila.name as upazila_name, ward.name as ward_name, `union`.name as uni_name, app_user_info.full_name as res_name  from form_info join assessment_info on form_info.form_code = assessment_info.form_code join survey_qs on assessment_info.survey_qs_id=survey_qs.id left join survey_option on assessment_info.option_id=survey_option.id left join district on form_info.district_id = district.id left join upazila on form_info.upozila_id = upazila.id left join `union` on form_info.union_id = `union`.id left join ward on form_info.word_id = ward.id left join projects on form_info.projects_id = projects.id left join app_user_info on form_info.login_op_id = app_user_info.id where form_info.survey_info_id='$id' order by form_info.form_code, survey_qs.order_id";


        $result = $this->Post_model->custom_query($query);

        // echo '<pre>';
        // print_r($result);
        // echo '</pre>';
        // die;

        foreach($result as $key => $val){
            if(!in_array($val->form_code, $code_array)) {
                
                $id_array = array();
                $report_array[$val->form_code]['surveyor_name'] = $val->res_name;
                $report_array[$val->form_code]['project_name'] = $val->project_name;
                $report_array[$val->form_code]['ans_gender'] = $val->ans_gender;
                $report_array[$val->form_code]['ans_age'] = $val->ans_gender;
                $report_array[$val->form_code]['respondent_name'] = $val->ans_name;

                if ($id != '3') {
                    $report_array[$val->form_code]['dis_name'] = $val->dis_name;
                    $report_array[$val->form_code]['upazila_name'] = $val->upazila_name;
                    $report_array[$val->form_code]['ward_name'] = $val->ward_name;
                    $report_array[$val->form_code]['uni_name'] = $val->uni_name;
                } else {
                    $report_array[$val->form_code]['name_garments'] = $val->name_garments;
                    $report_array[$val->form_code]['department_name'] = $val->department_name;
                    $report_array[$val->form_code]['id_number'] = $val->id_number;
                    $report_array[$val->form_code]['address_garments'] = $val->address_garments;
                }

                $report_array[$val->form_code]['mobile_number'] = $val->mobile_number;

                array_push($code_array,$val->form_code);
            }

            $order_id = str_replace(".", "_", $val->order_id);

            if(!in_array($val->sq_id, $id_array))
            {
                $report_array[$val->form_code][$order_id]['answer'] = $val->option_tag;
                $report_array[$val->form_code][$order_id]['qs_tag'] = $val->qs_tag;
                $report_array[$val->form_code][$order_id]['option_tag'] = $val->option_tag;
                $report_array[$val->form_code][$order_id]['review_ans'] = $val->review_ans;

                if($val->survey_qs_type == "checkbox")
                    $report_array[$val->form_code][$order_id]['multi_ans'] = $val->option_tag;

                array_push($id_array,$val->sq_id);

                // echo '<pre>';
                // print_r($report_array);
                // echo '</pre>';
                // die;

            }else{

                $report_array[$val->form_code][$order_id]['answer'] = $report_array[$val->form_code][$order_id]['answer'].";".$val->option_tag;

                if($val->survey_qs_type == "checkbox")
                    $report_array[$val->form_code][$order_id]['multi_ans'] = $report_array[$val->form_code][$order_id]['multi_ans'].";".$val->option_tag;

                if($val->review_ans !="" )
                    $report_array[$val->form_code][$order_id]['review_ans'] = (( $report_array[$val->form_code][$order_id]['review_ans']!="")?$report_array[$val->form_code][$order_id]['review_ans'].";".$val->review_ans:$val->review_ans);


                // echo '<pre>';
                // print_r($report_array);
                // echo '</pre>';

            }
        }

        //die;
        // echo '<pre>';
        // print_r($id_array);
        // echo '</pre>';

        // echo '<pre>';
        // print_r($report_array);
        // echo '</pre>';
        // die;
       

        

    header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename="sample.xls"');
    


    $output = "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\">
    <html><head><meta http-equiv=\"Content-type\" content=\"text/html;charset=utf-8\" /></head><body>";

/*    
echo "
    <html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\">
    <html>
        <head><meta http-equiv=\"Content-type\" content=\"text/html;charset=utf-8\" /></head>
        <body>
";*/
/*
echo "
    <table>
      <tr>
        <th rowspan=\"2\" nowrap=\"nowrap\">เลขที่บัญชี</th>
        <th rowspan=\"2\" nowrap=\"nowrap\">ชื่อ-สกุล ลูกค้า</th>
        <th rowspan=\"2\" nowrap=\"nowrap\">OS/Balance</th>
        <th rowspan=\"2\" nowrap=\"nowrap\">วันที่</th>
        <th rowspan=\"2\" nowrap=\"nowrap\">เวลา</th>
        <th rowspan=\"2\" nowrap=\"nowrap\">Action Code</th>
        <th rowspan=\"2\" nowrap=\"nowrap\">Amount</th>
        <th colspan=\"5\" nowrap=\"nowrap\">ผลการติดตาม</th>
      </tr>
      <tr>
        <th nowrap=\"nowrap\">ที่อยู่บ้าน</th>
        <th nowrap=\"nowrap\">เบอร์โทรบ้าน</th>
        <th nowrap=\"nowrap\">เบอร์โทรมือถือ</th>
        <th nowrap=\"nowrap\">ที่อยู่ที่ทำงาน</th>
        <th nowrap=\"nowrap\">เบอร์โทรที่ทำงาน</th>
      </tr>
      <tr>
        <td>acc</td>
        <td>name</td>
        <td>balance</td>
        <td>date</td>
        <td>time</td>
        <td>code</td>
        <td>amount</td>
        <td>h-addr</td>
        <td>h-tel</td>
        <td>cell</td>
        <td>w-addr</td>
        <td>w-tel</td>
      </tr>
    </table>
";

echo "</body></html>";
*/


$output .= "<table  border='1'>
                <tr>
                    <th>Sl</th>
                    <th>Surveyor Name</th>
                    <th>Project Name</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Mobile number</th>
                    <th>Respondent Name</th>
            ";

 if($id!=3) {
    $output .= "<th>District Name</th>
                <th>Upazila Name</th>
                <th>Union Name</th>
                <th>Ward Name</th>";
}else{
     $output .= "<th>Garment name</th>
                <th>Department Name</th>
                <th>ID number</th>
                <th>Garment address</th>";
}

$query = "select * from survey_qs where survey_info_id='$id' order by order_id";
$result = $this->Post_model->custom_query($query);

foreach($result as $key=>$val){ 
    array_push($qs_name_array,str_replace(".", "_", $val->order_id));
    $output .= "<th>".$val->survey_qs."</th>";  
}
$output .= "</tr>"; 


// echo "<pre>";
// print_r($code_array);
// echo "</pre>";

// echo "<pre>";
// print_r($report_array);
// echo "</pre>";

// die;



$sl = 1;
$count = count($code_array);
        for($i=0;$i<$count;$i++){
           $output .= "<tr>"; 
                $output .= "<td>". $sl."</td>";
                $output .= "<td>". $report_array[$code_array[$i]]['surveyor_name']."</td>";
                $output .= "<td>". $report_array[$code_array[$i]]['project_name']."</td>";
                $output .= "<td>". $report_array[$code_array[$i]]['ans_gender']."</td>";
                $output .= "<td>". $report_array[$code_array[$i]]['ans_age']."</td>";
                $output .= "<td>". $report_array[$code_array[$i]]['mobile_number']."</td>";
                $output .= "<td>". $report_array[$code_array[$i]]['respondent_name']."</td>";

            if($id!=3) {
                    $output .= "<td>". $report_array[$code_array[$i]]['dis_name']."</td>";
                    $output .= "<td>". $report_array[$code_array[$i]]['upazila_name']."</td>";
                    $output .= "<td>". $report_array[$code_array[$i]]['uni_name']."</td>";
                    $output .= "<td>". $report_array[$code_array[$i]]['ward_name']."</td>";        
            }else{
                    $output .= "<td>". $report_array[$code_array[$i]]['name_garments']."</td>";
                    $output .= "<td>". $report_array[$code_array[$i]]['department_name']."</td>";
                    $output .= "<td>". $report_array[$code_array[$i]]['id_number']."</td>";
                    $output .= "<td>". $report_array[$code_array[$i]]['address_garments']."</td>";  
            }

            foreach($qs_name_array as $qs_name){
                if(!empty($report_array[$code_array[$i]][$qs_name]['multi_ans']))
                    $output .= "<td>". $report_array[$code_array[$i]][$qs_name]['multi_ans']."</td>";  
                else {
                        if (!empty($report_array[$code_array[$i]][$qs_name]['option_tag']))
                            $output .= "<td>". $report_array[$code_array[$i]][$qs_name]['option_tag']."</td>";
                        else{
                            if(!empty($report_array[$code_array[$i]][$qs_name]['review_ans']))
                                $output .= "<td>". $report_array[$code_array[$i]][$qs_name]['review_ans']."</td>";
                            else
                                $output .= "<td></td>";
                        }
                }
            }
            $output .= "</tr>"; 

            $sl++;
        }
        

$output .= "</table>";






/*
// Convert to UTF-16LE
$output = mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');

// Prepend BOM
$output = "\xFF\xFE" . $output;

header('Pragma: public');
header("Content-type: application/x-msexcel");
header('Content-Disposition: attachment;  filename="utf8_bom.xls"');
*/

echo $output;

//redirect('admin/survey/survey_question_report');

            
/*
  $output = <<<EOT
<table>
    <tr>
        <td>Sl</td>
        <td>Surveyor Name</td>
        <td>Project Name</td>
        <td>Gender</td>
        <td>Age</td>
        <td>Mobile number</td>
        <td>Respondent Name</td>
        <td>District Name</td>
        <td>Upazila Name</td>
        <td>Union Name</td>
        <td>Ward Name</td>
        <?php 
            foreach($result as $key=>$val){ 
                    array_push($qs_name_array,str_replace(".", "_", $val->order_id));
                ?>
                <td><?php echo $val->survey_qs ?></td>
            <?php } ?>    
        ?>
    </tr>
</table>
EOT;

// Convert to UTF-16LE
$output = mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');

// Prepend BOM
$output = "\xFF\xFE" . $output;

header('Pragma: public');
header("Content-type: application/x-msexcel");
header('Content-Disposition: attachment;  filename="utf8_bom.xls"');

echo $output;
*/

        /* Arzon
        foreach($result as $key => $val){

            if(!in_array($val->form_code, $code_array)) {
                $report_array[$val->form_code]['surveyor_name'] = $val->res_name;
                $report_array[$val->form_code]['project_name'] = $val->project_name;
                $report_array[$val->form_code]['ans_gender'] = $val->ans_gender;
                $report_array[$val->form_code]['ans_age'] = $val->ans_gender;
                $report_array[$val->form_code]['respondent_name'] = $val->ans_name;

                if ($id != '3') {
                    $report_array[$val->form_code]['dis_name'] = $val->dis_name;
                    $report_array[$val->form_code]['upazila_name'] = $val->upazila_name;
                    $report_array[$val->form_code]['ward_name'] = $val->ward_name;
                    $report_array[$val->form_code]['uni_name'] = $val->uni_name;
                } else {
                    $report_array[$val->form_code]['name_garments'] = $val->name_garments;
                    $report_array[$val->form_code]['department_name'] = $val->department_name;
                    $report_array[$val->form_code]['id_number'] = $val->id_number;
                    $report_array[$val->form_code]['address_garments'] = $val->address_garments;
                }

                $report_array[$val->form_code]['mobile_number'] = $val->mobile_number;

                array_push($code_array,$val->form_code);
            }

            $order_id = str_replace(".", "_", $val->order_id);


            if(!in_array($val->sq_id, $id_array))
            {
                $report_array[$val->form_code][$order_id]['main_question_id'] = $val->order_id;
                $report_array[$val->form_code][$order_id]['option_code'] = $val->option_code;
                $report_array[$val->form_code][$order_id]['review_ans'] = $val->review_ans;

                if($val->survey_qs_type == "checkbox")
                    $report_array[$val->form_code][$order_id]['multi_ans'] = $val->option_code;

                array_push($id_array,$val->sq_id);
            }else{

                $report_array[$val->form_code][$order_id]['option_code'] = $report_array[$val->form_code][$order_id]['option_code'].";".$val->option_code;

                if($val->survey_qs_type == "checkbox")
                    $report_array[$val->form_code][$order_id]['multi_ans'] = $report_array[$val->form_code][$order_id]['multi_ans'].";".$val->option_code;

                if($val->review_ans !="" )
                    $report_array[$val->form_code][$order_id]['review_ans'] = (( $report_array[$val->form_code][$order_id]['review_ans']!="")?$report_array[$val->form_code][$order_id]['review_ans'].";".$val->review_ans:$val->review_ans);
            }
        }
        */

/*
     $output = <<<EOT
<table>
    <tr>
        <td>আমার নাম মুরাদ হাসান তানভীর</td>
        <td>আমার নাম মুরাদ হাসান তানভীর</td>
    </tr>
    <tr>
        <td>আমার নাম মুরাদ হাসান তানভীর<td>
        <td>আমার নাম মুরাদ হাসান তানভীর</td>
    </tr>
</table>
EOT;

// Convert to UTF-16LE
$output = mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');

// Prepend BOM
$output = "\xFF\xFE" . $output;

header('Pragma: public');
header("Content-type: application/x-msexcel");
header('Content-Disposition: attachment;  filename="utf8_bom.xls"');

echo $output; */
        /*test portion*/

        /*test in Bangla*/



        /* Arzon 3/6/2017 */
        /*

        $gender_arr = array("1" => "Male", "2" => "Female");

        $xls = new Excel('Survey Information Filter Result');

        if($id != '3') {
            $arr = array("Sl", "Surveyor Name", "Project Name", "Gender", "Age", "Mobile number","Respondent Name","District Name","Upazila Name","Union Name","Ward Name");
        }else{
            $arr = array("Sl", "Surveyor Name", "Project Name", "Gender", "Age", "Mobile number","Respondent Name","Garment's name","Department Name","ID number","Garment's address");
        }

        $query = "select * from survey_qs where survey_info_id='$id' order by order_id";
        $result = $this->Post_model->custom_query($query);

        foreach($result as $key=>$val){
            array_push($arr,str_replace(".", "_", $val->order_id));
            array_push($qs_name_array,str_replace(".", "_", $val->order_id));
        }
        $xls->home();

        $count_arr = count($arr);
        for($i = 0; $i < $count_arr; $i++){
            $xls->label($arr[$i]);
            $xls->right(1);
        }
        $xls->down();

        $sl = 1;
        $count = count($code_array);
        for($i=0;$i<$count;$i++){
            $xls->home();
            if($id!=3) {
                $array = array($sl, $report_array[$code_array[$i]]['surveyor_name'], $report_array[$code_array[$i]]['project_name'], $gender_arr[$report_array[$code_array[$i]]['ans_gender']], $report_array[$code_array[$i]]['ans_age'], $report_array[$code_array[$i]]['mobile_number'], $report_array[$code_array[$i]]['respondent_name'], $report_array[$code_array[$i]]['dis_name'], $report_array[$code_array[$i]]['upazila_name'], $report_array[$code_array[$i]]['uni_name'], $report_array[$code_array[$i]]['ward_name']);
            }else{
                $array = array($sl, $report_array[$code_array[$i]]['surveyor_name'], $report_array[$code_array[$i]]['project_name'], $gender_arr[$report_array[$code_array[$i]]['ans_gender']], $report_array[$code_array[$i]]['ans_age'], $report_array[$code_array[$i]]['mobile_number'], $report_array[$code_array[$i]]['respondent_name'],$report_array[$code_array[$i]]['name_garments'],$report_array[$code_array[$i]]['department_name'],$report_array[$code_array[$i]]['id_number'],$report_array[$code_array[$i]]['address_garments'] );
            }

            foreach($qs_name_array as $qs_name){
                if(!empty($report_array[$code_array[$i]][$qs_name]['multi_ans']))
                    array_push($array,$report_array[$code_array[$i]][$qs_name]['multi_ans']);
                else {
                        if ($report_array[$code_array[$i]][$qs_name]['option_code'] != "")
                            array_push($array,$report_array[$code_array[$i]][$qs_name]['option_code']);
                        else
                            array_push($array,$report_array[$code_array[$i]][$qs_name]['review_ans']);
                }
            }

            for($i = 0; $i < $count_arr; $i++){
                $xls->label($array[$i]);
                $xls->right(1);
            }
            $xls->down();
            $sl++;
        }

        $data = ob_get_clean();
        $xls->send();
        */


        /*Murad 3/6/2017*/
        /*
        if(isset($_SESSION['survey_result'])){
            //$gender_arr = array("1" => "Male", "2" => "Female");
           // $xls = new Excel('Survey Information Filter Result');

           // $arr = array("Sl", "Surveyor", "Respondent", "District", "Upazila", "Union", "Ward", "Mobile", "Gender", "Survey Date", "Age", "Garments Name", "Department", "ID");
            //$xls->home();

            $count = count($arr);
            for($i = 0; $i < $count; $i++){
                $xls->label($arr[$i]);
                $xls->right(1);
            }
            $xls->down();

            $sl = 1;
            foreach($_SESSION['survey_result'] as $key => $value){
                $xls->home();
                $array = array($sl, $value->username, $value->ans_name, $value->disname, $value->upaname, $value->unname, $value->wardname, $value->mobile_number, $value->ans_gender, $value->created_at, $value->ans_age, $value->name_garments, $value->department_name, $value->id_number);
                for($i = 0; $i < $count; $i++){
                    $xls->label($array[$i]);
                    $xls->right(1);
                }
                $xls->down();
                $sl++;
            }
            $data = ob_get_clean();
            $xls->send();

            unset($_SESSION['survey_result']);
        } else
            redirect('admin/survey/survey_question_report');
        */
    }

     public function survey_qs_report_show(){
         $survey_info_id = $this->input->post('survey_info_id');

         $query = "select form_info.*, district.name as disname, upazila.name as upaname from form_info join district on form_info.district_id = district.id join upazila on form_info.upozila_id = upazila.id join `union` on form_info.union_id = union.id join ward on form_info.word_id = ward.id where form_info.survey_info_id = '$survey_info_id'";

         $data['result'] = $this->Post_model->custom_query($query);
     }

     public function survey_qs_edit($id,$survey_info_id){
        $data['menu_title'] = "Survey Feedback Edit";
        $data['id'] = $id;
        $data['survey_info_id'] = $survey_info_id;

        if(empty($_POST)){

            $report_array = array();
            $review_array = array();

            $qs_array = array();
            // $qs_order_array = array();

            $code_array = array();

            $query = "select *, assessment_info.survey_qs_id as qs_id, form_info.ans_name, form_info.ans_age, form_info.ans_gender, form_info.name_garments, form_info.department_name, form_info.id_number, form_info.address_garments, form_info.mobile_number, district.name as dis_name, upazila.name as upa_name, `union`.name as uni_name, ward.name as wa_name from assessment_info join form_info on form_info.form_code=assessment_info.form_code join survey_qs on survey_qs.id=assessment_info.survey_qs_id left join survey_option on survey_option.id=assessment_info.option_id left join district on form_info.`district_id` = district.id left join upazila on form_info.`upozila_id` = upazila.id left join `union` on `form_info`.`union_id` = `union`.id left join ward on form_info.`word_id` = ward.id left join projects on form_info.`projects_id` = projects.id  where form_info.id='$id'";

            $result = $this->Post_model->custom_query($query);

            // echo "<pre>";
            // print_r($result);
            // echo "</pre>";
            // die;

            // $survey_info_id = 3;
        
            foreach($result as $key => $val ){
                if(!in_array($val->form_code,$code_array)){
                    $data['ans_name'] = $val->ans_name;
                    $data['ans_age'] = $val->ans_age;
                    $data['ans_gender'] = $val->ans_gender;

                    if($survey_info_id != 3){

                        $data['dis_name'] = $val->dis_name;
                        $data['upa_name'] = $val->upa_name;
                        $data['uni_name'] = $val->uni_name;
                        $data['wa_name'] = $val->wa_name;

                        $data['district_id'] = $val->district_id;
                        $data['upozila_id'] = $val->upozila_id;
                        $data['union_id'] = $val->union_id;
                        $data['word_id'] = $val->word_id;


                    }else{

                        $data['name_garments'] = $val->name_garments;
                        $data['department_name'] = $val->department_name;
                        $data['id_number'] = $val->id_number;
                        $data['address_garments'] = $val->address_garments;

                    }

                    $data['mobile_number'] = $val->mobile_number;
                    $data['login_op_id'] = $val->login_op_id;
                    $data['project_id'] = $val->projects_id;
                    $data['form_code'] = $val->form_code;
                }

                if($val->option_id !="" || $val->option_id != "NULL")
                {
                    array_push($report_array,$val->qs_id."_".$val->option_id);
                    if(!empty($val->main_ques_id)){
                        array_push($qs_array,$val->qs_id);
                    }
                    // array_push($qs_array,$val->qs_id);
                    // array_push($qs_order_array,$val->order_id."_".$val->option_order);

                }

                if(!empty($val->review_ans)){
                    $review_array[$val->qs_id."_".$val->option_id] = $val->review_ans;
                     if(!empty($val->main_ques_id)){
                        array_push($qs_array,$val->qs_id);
                    }
                }
            }

             $data['report_array'] = $report_array;
             $data['review_array'] = $review_array;
             $data['qs_array'] = $qs_array;

             // echo "<pre>";
             // print_r($qs_array);
             // echo "</pre>";
             // die;
           
             $data['survey_info'] = $this->Post_model->getAllByFieldName('survey_info','id',$survey_info_id);
             $data['op_ids'] = $this->Post_model->getAll('app_user_info');
             $data['projects_id'] = $this->Post_model->getAll('projects');
             $data['survey_question'] = $this->Post_model->custom_query("select * from survey_qs where survey_info_id='$survey_info_id' order by order_id ASC");

             $this->load->view('admin/survey/survey-as-form-edit',$data);

         }else{
                
                $characters = '0123456789';
                $randomString = '';
                for ($j = 0; $j < 4; $j++) {
                    $randomString .= $characters[rand(0, strlen($characters) - 1)];
                }
                // echo "<pre>";
                // print_r($_POST);
                // echo "</pre>";
                //die;
                
                //delete from form_info and assessment_info
                    $form_code_old = $this->input->post('form_code');
                    $this->db->query("delete from form_info where form_code = '$form_code_old'");
                    $this->db->query("delete from assessment_info where form_code = '$form_code_old'");
                //End of end portion    

                $input['project_type'] = $this->input->post('project_type');
                $input['form_code'] = date('YmdHis').$randomString;

                $input['survey_info_id'] = $this->input->post('survey_info_id');
                $input['ans_name'] = $this->input->post('ans_name');
                $input['ans_age'] = $this->input->post('ans_age');
                $input['ans_gender'] = $this->input->post('ans_gender');

                if($input['project_type']=='phulki'){
                    $input['name_garments'] = $this->input->post('name_garments');
                    $input['department_name'] = $this->input->post('department_name');
                    $input['id_number'] = $this->input->post('id_number');
                    $input['address_garments'] = $this->input->post('address_garments');
                }else{
                    $input['district_id'] = $this->input->post('district_id');
                    $input['upozila_id'] = $this->input->post('upozila_id');
                    $input['union_id'] = $this->input->post('union_id');
                    $input['word_id'] = $this->input->post('word_id');
                }

                unset($input['project_type']);

                $input['mobile_number'] = $this->input->post('mobile_number');
                $input['login_op_id'] = $this->input->post('login_op_id');
                $input['projects_id'] = $this->input->post('projects_id');
                $input['created_at'] = date('Y-m-d h:i:s');
                $input['created_by'] = $this->session->userdata('id');


                $this->Post_model->insert('form_info',$input);

                $data['result'] = $this->Post_model->getAllByFieldName('survey_qs','survey_info_id',$input['survey_info_id']);

            // echo '<pre>';
            // print_r($data['result']);
            // echo '</pre>';

                foreach($data['result'] as $key => $val ){
                    
                     $input_values['form_code'] = $input['form_code'];
                     $input_values['created_at'] = date('Y-m-d h:i:s');
                     $input_values['created_by'] = $this->session->userdata('id');

                    if(isset($input_values['review_ans'])){
                        unset($input_values['review_ans']);
                    }
                    if(isset($input_values['option_id'])){
                        unset($input_values['option_id']);
                    }

                     if((string)$val->survey_qs_type=='checkbox'){
                        
                        if( !empty($_POST[$val->id]) && (count($_POST[$val->id]) > 0) ){
                            foreach($_POST[$val->id] as $values){
                                $qs_split = explode('_',$values);
                                $input_values['survey_qs_id'] = $qs_split[0];
                                $input_values['option_id']  =  $qs_split[1];   
                                $this->Post_model->insert('assessment_info',$input_values); 
                            }
                        }else{
                             $input_values['survey_qs_id'] = $val->id;
                             $input_values['option_id']  =  9999; 
                             $this->Post_model->insert('assessment_info',$input_values);
                        }
                        

                     }elseif((string)$val->survey_qs_type=='text'){
                        
                        if(!empty($_POST[$val->id])){
                            $input_values['survey_qs_id'] = $val->id;
                            $input_values['review_ans'] = $_POST[$val->id];
                        }else{
                             $input_values['survey_qs_id'] = $val->id;
                             $input_values['review_ans']  =  'N/A';
                        }
                        $this->Post_model->insert('assessment_info',$input_values);

                     }elseif((string)$val->survey_qs_type=='datepicker'){
                        
                        if(!empty($_POST[$val->id])){
                            $input_values['survey_qs_id'] = $val->id;
                            $input_values['review_ans'] = date('Y-m-d',strtotime($_POST[$val->id])); 
                        }else{
                             $input_values['survey_qs_id'] = $val->id;
                             $input_values['review_ans']  =  'N/A';
                        }
                        $this->Post_model->insert('assessment_info',$input_values);   

                     }elseif((string)$val->survey_qs_type=='time'){

                        if(!empty($_POST[$val->id])){
                            $input_values['survey_qs_id'] = $val->id;
                            $input_values['review_ans'] = date('h:i:s',strtotime($_POST[$val->id]));
                        }else{
                            $input_values['survey_qs_id'] = $val->id;
                            $input_values['review_ans']  =  'N/A';
                        }
                        $this->Post_model->insert('assessment_info',$input_values);    

                     }elseif((string)$val->survey_qs_type=='radio'){

                        
                        if(!empty($_POST[$val->id])){
                             $qs_split = explode('_',$_POST[$val->id]);
                             $input_values['survey_qs_id'] = $qs_split[0];
                             $input_values['option_id']  =  $qs_split[1];  
                        }else{
                            $input_values['survey_qs_id'] = $val->id;
                            $input_values['option_id']  =  9999;
                        }

                        // echo "<pre>";
                        // print_r($_POST[$val->id]);
                        // echo '</pre>';

                        // echo "<pre>";
                        // print_r($input_values);
                        // echo '</pre>';

                        // die;
                        $this->Post_model->insert('assessment_info',$input_values); 

                     }

                }
                $this->session->set_flashdata('success_message', 'Question Edited Successfully');
                redirect('admin/survey/survey_question_report');
         }
        
     }

}