<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acl extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLogedNirapod')) ){
            redirect('admin/index');
        }
        $this->Menu_model->menu_model_info();
        $this->load->model("User_model");
        $this->load->model("Access_control");

        /*For ACL*/
        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        /*For ACL*/
    }

    public function menu_list(){
        $result = $this->Menu_model->get_all_menu();
        foreach($result as $row){
            $id = ($row->parent_id==0) ? $row->id : $row->parent_id;
            $name_list[$row->id] = $row->title;
            if($id != $row->id)
                $acl_list[$id][] = $row->id;
        }
        $data['name'] = $name_list;
        $data['result'] = $acl_list;

        return $data;
    }

    public function acl_add()
    {
        if(empty($_POST)) {
            $data = $this->menu_list();
            $data['menu_title'] = "ACL Add";

            $data['get_user_all'] = $this->User_model->get_user_all();

            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
            // die;

            $this->load->view('admin/acl/acl-add', $data);
        }else{
            $data['login_id'] = $this->input->post("login_id");
            $data['access'] = json_encode($this->input->post("access"));
            $data['created_date'] = date("Y-m-d");

            if(!$this->Access_control->check($data['login_id'])) {
                if ($this->Access_control->insert($data)) {
                    $this->session->set_flashdata('success_message', 'Data inserted successfully');
                    redirect('admin/acl/acl_view');
                } else {
                    $this->session->set_flashdata('Insertion_failed', 'Something went wrong please try again');
                    redirect('admin/acl/acl_add');
                }
            }else{
                $this->session->set_flashdata('Insertion_failed', 'Already Exists');
                redirect('admin/acl/acl_add');
            }
        }
    }

    public function acl_view(){
        $data['menu_title'] = "ACL View";
        $data['result'] = $this->Access_control->acl_list();

        if($data['result']==0)
            $this->session->set_flashdata('error_message', 'No data Found');

        $nameList = $this->menu_list();
        $data['name'] = $nameList['name'];

        $this->load->view('admin/acl/acl-view',$data);
    }

    public function acl_edit($id=null){
        if(empty($_POST)) {
            $data = $this->menu_list();
            $data['menu_title'] = "ACL Edit";
            $data['get_user_all'] = $this->User_model->get_user_all();

            $data['id'] = $id;
            $data['get_acl'] = $this->Access_control->get_acl($id);
            $data['get_acl'][0]->access = json_decode($data['get_acl'][0]->access);

            $this->load->view('admin/acl/acl-edit', $data);
        }else{

            $id = $this->input->post("id");
            //$data['login_id'] = $this->input->post("login_id");
            $data['access'] = json_encode($this->input->post("access"));
            $data['modified'] = date("Y-m-d");

            if($this->Access_control->update($data,$id)){
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/acl/acl_view');
            }else{
                $this->session->set_flashdata('update_failed', 'Something went wrong please try again');
                redirect('admin/acl/acl_edit/'.$id);
            }

        }

    }

    public function acl_delete($id){
        $this->Access_control->delete($id);
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/acl/acl_view');

    }

}
