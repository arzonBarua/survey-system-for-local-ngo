<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 10:40 AM
 */
class Video extends CI_Controller
{
    public $ids = array();
    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }


        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            $this->ids = $this->permission->get_assigned_ids();
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        $this->Menu_model->menu_model_info();
        $this->load->library('form_validation');
        $this->load->model('Post_model');
    }

    public function video_add(){
        $data['menu_title'] = "Add Video";
        if(empty($_POST)) {
            //$data['op_ids'] = $this->Post_model->getAll('app_user_info');

            $acl_ext_query = "";
            if(!empty($this->ids)){
               $acl_ext_query = " where id in (".implode(',',$this->ids).") ";
            }
            $data['op_ids'] = $this->Post_model->custom_query("SELECT * FROM app_user_info $acl_ext_query");

            $this->load->view('admin/video/video-add', $data);
        }else{
            
            $this->form_validation->set_rules('user_id','User Name', 'required');
            $this->form_validation->set_rules('video_title','Video Title', 'required');

            if($this->form_validation->run() == FALSE ){
                $this->load->view('admin/video/video-add', $data);
            }else{
                $input['users_id'] = $this->input->post('user_id');
                $input['video_title'] = $this->input->post('video_title');
                $input['video_date'] = date('Y-m-d H:i:s', strtotime($this->input->post('video_date')));
                $input['created_at'] = date('Y-m-d H:i:s ');

               /*For File Upload*/
                $file_upload = 1;
                if(!empty($_FILES['userfile']['name'])){
                    $file_name = 'userfile';
                    $upload_path = './assets/upload/video/';
                    $result = $this->Common_operation->video_upload($file_name, $upload_path);

                    if(!$result['flag']){
                        $data['error'] = $result['error'];
                        $this->load->view('admin/video/video-add', $data);
                        $file_upload = 0;
                    }else{
                        $input['video_name'] = $result['file_name'];
                    }
                }

                /*For File Upload*/
                if($file_upload){
                    if ($this->Post_model->insert('video', $input)) {
                        $this->session->set_flashdata('success_message', 'Data inserted successfully');
                        redirect('admin/video/video_view');
                    } else {
                        $data = array('error' => "somehting went wrong please inform the webmaster");
                        $this->load->view('admin/video/video-add', $data);
                    }
                }
            }
        }
    }

    public function video_edit($id){
        $data['menu_title'] = "Edit Video";


        $data['result'] = $this->Post_model->getAll('video', '', '', $id);

        if(empty($_POST)){
            if($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');

            $data['op_ids'] = $this->Post_model->getAll('app_user_info');
            $this->load->view('admin/video/video-edit', $data);
        }else{
            $this->form_validation->set_rules('user_id','User Name', 'required');
            $this->form_validation->set_rules('video_title','Video Title', 'required');

            $input['users_id'] = $this->input->post('user_id');
            $input['video_title'] = $this->input->post('video_title');
            $input['video_date'] = date('Y-m-d H:i:s', strtotime($this->input->post('video_date')));
            $input['created_at'] = date('Y-m-d H:i:s ');

           /*For File Upload*/
            $file_upload = 1;
            if(!empty($_FILES['userfile']['name'])){
                $file_name = 'userfile';
                $upload_path = './assets/upload/video/';

                $file_name_folder = (string)$data['result'][0]->video_name;
                $full_path = $upload_path.$file_name_folder;

                if (file_exists($full_path)) {
                    $this->Common_operation->file_delete($full_path);
                }

                $result = $this->Common_operation->video_upload($file_name, $upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/video/video-add', $data);
                    $file_upload = 0;
                }else{
                    $input['video_name'] = $result['file_name'];
                }
            }

            /*For File Upload*/
            if($file_upload){
                if ($this->Post_model->update('video', $input, $id)){
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/video/video_view');
                }else{
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/video/video-edit', $data);
                }
            }
        }
    }

    public function video_view(){

        $qry_str = "";

        if(!empty($this->ids)){
            $qry_str[] = " video.users_id in (".implode(',',$this->ids).") ";
            $data['ids'] = $this->ids;
        }
        $qry_str = ( $qry_str != '' ) ? ' WHERE '.implode(' AND ', $qry_str) : '';

        $data['menu_title'] = "View Video";
        //$data['result'] = $this->Post_model->getAll('video', 'video_title', '');

        $data['result'] = $this->Post_model->custom_query('SELECT * FROM video $qry_str');

        if($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/video/video-view', $data);
    }

    public function video_delete($id){
        $result = $this->Post_model->getAll('video', '', '', $id);
        $file_name = (string)$result[0]->video_name;

        if(!empty($file_name)){
            $upload_path = './assets/upload/video/'.$file_name;
            $this->Post_model->delete('video', $id, $upload_path);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/video/video_view');
    }
}
