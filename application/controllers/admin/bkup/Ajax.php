<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->model('Post_model');
	}

	public function get_ajax() {
       //echo "hellow world";
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $className = $this->input->post('field');

        $fieldName =  $className."_id";

        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);

        echo $str;
    }

    public function label_ajax(){
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $fieldName = $this->input->post('field');
        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
        echo $str;
    }

    public function label_view_ajax($project, $year){       
        $qry = $this->db->query("SELECT * FROM `mf__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' GROUP BY `quarter` ORDER BY `quarter` ASC");
        $data['result'] = $qry->result();
        $this->load->view('admin/label/label-view-search', $data);
    }

     public function corner_view_ajax($project, $year, $division, $district){        
        $sql = "SELECT * FROM `pr__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' AND `division` = '".$division."' AND `district` = '".$district."' GROUP BY `quarter` ORDER BY `quarter` ASC";
        $qry = $this->db->query($sql);
        $data['result'] = $qry->result();

        $this->load->view('admin/label/corner-view-search', $data);
    }

    public function pc_report_ajax($project, $year, $quarter){
        // if( isset($quarter) && $quarter == 5 ) $quarter = '1,2,3,4';

        $data['year']    = $year;
        $data['quarter'] = $quarter;
        $data['project'] = $project;
        $qry = $this->db->query("SELECT * FROM `division` ORDER BY `division_name` ASC");
        $data['result']  = $qry->result();
        $this->load->view('admin/label/pc-report-search', $data);
    }

    public function get_project_type(){
          
           $project_id = $this->input->post('projects_type');

           $arrayProjectName = $this->Common_operation->get_user_type();

          

            $string = "<option>--Select Type--</option>";
             
            foreach($arrayProjectName[$project_id] as $key=>$val){
                 $string .= "<option value=$key>$val</option>";
             }
              

          echo $string;
    }
    
    public function show_apo_list(){

         $project_id = $this->input->post('projects_type');
         //$result = $this->Post_model->getAllByFieldName('user_acl','project_id',$project_id);

         $result = $this->Post_model->custom_query("select admin_login.id, admin_login.full_name from admin_login where id in (select user_id as id from user_acl where project_id='$project_id' and user_type=5)");


         $string = "<option>--Select Type--</option>";
            
         foreach ($result as $key=>$val){
                 $string .= "<option value=".$val->id.">".$val->full_name."</option>";
         }

         echo $string;


    }

    public function mf_report_ajax($year, $quarter){
        if( isset($quarter) && $quarter == 5 ) $quarter = '1,2,3,4';

        $data['year']   = $year;
        $data['quarter']= $quarter;
        $qry = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '0' AND `status` = '1' ORDER BY `order_id` ASC");
        $data['result'] = $qry->result();
        $this->load->view('admin/label/mf-report-search', $data);
    }


    
}