<?php defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 10:40 AM
 */
class Meeting extends CI_Controller
{
    public $ids = array();
    public function __construct(){

        parent::__construct();

        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }

        if($this->session->userdata('user_type')!='1'){
             $this->load->library('permission');
             $this->ids = $this->permission->get_assigned_ids();
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        $this->Menu_model->menu_model_info();
        $this->load->library('form_validation');
        $this->load->model('Post_model');


        // Excel intregation 
        include APPPATH . 'third_party/Excel.php';
    }


    public function meeting_add(){
        $data['menu_title'] = "Add Meeting";
        $data['result'] = $this->Post_model->getAll('district', 'name', 'ASC');

        if(empty($_POST)){
            $acl_ext_query = "";
            if(!empty($this->ids)){
               $acl_ext_query = " where id in (".implode(',',$this->ids).") ";
            }
            $data['op_ids'] = $this->Post_model->custom_query("SELECT * FROM app_user_info $acl_ext_query");

            //$data['op_ids'] = $this->Post_model->getAll('app_user_info');
            $this->load->view('admin/meeting/meeting-add',$data);
        }else{
            $file_upload = 1;
            $input['users_id'] = $this->input->post('users_id');
            $input['meeting_name'] = $this->input->post('meeting_name');
            $input['no_of_participation'] = $this->input->post('no_of_participation');
            $input['meeting_start_time'] = date('Y-m-d',strtotime($this->input->post('meeting_date')))." ". date('h:i:s',strtotime($this->input->post('start_time')));
            $input['meeting_end_time'] = date('Y-m-d',strtotime($this->input->post('meeting_date')))." ". date('h:i:s',strtotime($this->input->post('end_time')));
            $input['district_id'] = $this->input->post('district_id');
            $input['upozila_id'] = $this->input->post('upozila_id');
            $input['union_id'] = $this->input->post('union_id');
            $input['word_id'] = $this->input->post('word_id');

            $input['duration'] =  $this->Common_operation->getTimeDiff($input['meeting_end_time'],$input['meeting_start_time']);
            $input['created_at'] = date("Y-m-d h:i:s");
            $input['created_by'] = $this->session->userdata('id');
            $input['flag'] = 1;


            if(!empty($_FILES['meeting_start_picture_name']['name'])){
                $file_name = 'meeting_start_picture_name';
                $upload_path = './assets/upload/meeting/';
                $result = $this->Common_operation->image_upload($file_name, $upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/meeting/meeting-add', $data);
                    $file_upload = 0;
                }else{
                    $input['meeting_start_picture_name'] = $result['picture'];
                }
            }

            if(!empty($_FILES['meeting_start_picture_end']['name'])){
                $file_name = 'meeting_start_picture_end';
                $upload_path = './assets/upload/meeting/';
                $result = $this->Common_operation->image_upload($file_name, $upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/meeting/meeting-add', $data);
                    $file_upload = 0;
                }else{
                    $input['meeting_start_picture_end'] = $result['picture'];
                }
            }

            /*For File Upload*/
            if($file_upload){
                if ($this->Post_model->insert('meeting', $input)) {
                    $this->session->set_flashdata('success_message', 'Data inserted successfully');
                    redirect('admin/meeting/meeting_view');
                } else {
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/meeting/meeting-add', $data);
                }
            }

        }
    }

    public function meeting_view(){
        $data['menu_title'] = "View Meeting";

        $qry_str = '';
        if(!empty($_POST)){
            $values = $_POST;
            if($values['daterangepicker'] != ""){
                list($from_date, $to_date) = explode("-", $values['daterangepicker']);
                $qry_str[] = "DATE_FORMAT(`meeting`.`meeting_start_time`, '%m/%d/%Y') BETWEEN '".trim($from_date)."' AND '".trim($to_date)."'";
                //$qry_str[] = "DATE_FORMAT(`meeting_start_time`, '%m/%d/%Y') = '".$to_date."'";
                $data['search_detail']['from_date'] = $from_date;
                $data['search_detail']['to_date'] = $to_date;
            }

            // if($district_id  != ""){
            //     $qry_str[] = "`district_id` = '".$district_id."'";
            // }

            if($values['district_id']  != ""){
                $qry_str[] = "`meeting`.`district_id` = '".$values['district_id']."'";
                $data['search_detail']['district_id'] = $values['district_id'];
            }

            if($values['upozila_id']  != ""){
                $qry_str[] = "`meeting`.`upozila_id` = '".$values['upozila_id']."'";
                $data['search_detail']['upozila_id'] = $values['upozila_id'];
            }

            if($values['union_id']  != ""){
                $qry_str[] = "`meeting`.`union_id` = '".$values['union_id']."'";
                $data['search_detail']['union_id'] = $values['union_id'];
            }

            if($values['type_name']  != ""){
                $qry_str[] = "`meeting`.`meeting_type` = '".$values['type_name']."'";
                $data['search_detail']['type_name'] = $values['type_name'];
            }

            if($values['user_name']  != ""){
                $qry_str[] = "`meeting`.`users_id` = '".$values['user_name']."'";
                $data['search_detail']['user_name'] = $values['user_name'];
            }
        }

        /*Need to ACl*/
        
        if(!empty($this->ids)){
            $qry_str[] = " meeting.users_id in (".implode(',',$this->ids).") ";
            $data['ids'] = $this->ids;
        }

        $qry_str = ( $qry_str != '' ) ? ' WHERE '.implode(' AND ', $qry_str) : '';

        $string_query = "select meeting.*, app_user_info.username, district.name as district_name, upazila.name as upazila_name, union.name as union_name, ward.name as ward_name from meeting join district on meeting.district_id = district.id join upazila on meeting.upozila_id = upazila.id join `union` on meeting.union_id = union.id join ward on meeting.word_id = ward.id join app_user_info on meeting.users_id = app_user_info.id ".$qry_str." order by meeting.meeting_start_time desc";
        


        $data['result'] = $this->Post_model->custom_query($string_query);
        $_SESSION['meeting_result'] = $data['result'];

        if ($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/meeting/meeting-view', $data);
    }

    public function meeting_edit($id){
        $data['menu_title'] = "Edit Meeting";

        $string_query = "select meeting.*, app_user_info.username, district.name as district_name, upazila.name as upazila_name, union.name as union_name, ward.name as ward_name from meeting join district on meeting.district_id = district.id join upazila on meeting.upozila_id = upazila.id join `union` on meeting.union_id = union.id join ward on meeting.word_id = ward.id join app_user_info on meeting.users_id = app_user_info.id where meeting.id = '$id'";

        $data['result'] = $this->Post_model->custom_query($string_query);
        $data['district'] = $this->Post_model->getAll('district','','');


        if(empty($_POST)){
            if($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');
   
            
            $acl_ext_query = "";
            if(!empty($this->ids)){
               $acl_ext_query = " where id in (".implode(',',$this->ids).") ";
            }
            $data['op_ids'] = $this->Post_model->custom_query("SELECT * FROM app_user_info $acl_ext_query");

            //$data['op_ids'] = $this->Post_model->getAll('app_user_info');
            $this->load->view('admin/meeting/meeting-edit', $data);
        }else{

            $file_upload = 1;
            $input['users_id'] = $this->input->post('users_id');
            $input['meeting_name'] = $this->input->post('meeting_name');
            $input['no_of_participation'] = $this->input->post('no_of_participation');
            $input['no_of_participation_end'] = $this->input->post('no_of_participation_end');
            $input['meeting_start_time'] = date('Y-m-d',strtotime($this->input->post('meeting_date')))." ". date('h:i:s',strtotime($this->input->post('start_time')));
            $input['meeting_end_time'] = date('Y-m-d',strtotime($this->input->post('meeting_date')))." ". date('h:i:s',strtotime($this->input->post('end_time')));
            $input['district_id'] = $this->input->post('district_id');
            $input['upozila_id'] = $this->input->post('upozila_id');
            $input['union_id'] = $this->input->post('union_id');
            $input['word_id'] = $this->input->post('word_id');


            $input['updated_at'] = date("Y-m-d h:i:s");
            $input['updated_by'] = $this->session->userdata('id');


            if(!empty($_FILES['meeting_start_picture_name']['name'])){
                $file_name = 'meeting_start_picture_name';
                $upload_path = './assets/upload/meeting/';

                $file_name_folder = (string)$data['result'][0]->meeting_start_picture_name;
                $full_path = $upload_path.$file_name_folder;

                if (file_exists($full_path)) {
                    $this->Common_operation->file_delete($full_path);
                }

                $result = $this->Common_operation->image_upload($file_name, $upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/meeting/meeting-edit', $data);
                    $file_upload = 0;
                }else{
                    $input['meeting_start_picture_name'] = $result['picture'];
                }
            }
            if(!empty($_FILES['meeting_start_picture_end']['name'])){
                $file_name = 'meeting_start_picture_end';
                $upload_path = './assets/upload/meeting/';

                $file_name_folder = (string)$data['result'][0]->meeting_start_picture_end;
                $full_path = $upload_path.$file_name_folder;

                if (file_exists($full_path)) {
                    $this->Common_operation->file_delete($full_path);
                }

                $result = $this->Common_operation->image_upload($file_name, $upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/meeting/meeting-edit', $data);
                    $file_upload = 0;
                }else{
                    $input['meeting_start_picture_end'] = $result['picture'];
                }
            }

            if($file_upload) {
                if ($this->Post_model->update('meeting', $input, $id)) {
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/meeting/meeting_view');
                } else {
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/meeting/meeting-edit/', $data);
                }
            }

        }
    }
/*
    public function get_ajax()
    {
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $className = $this->input->post('field');

        $fieldName =  $className."_id";

        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
        echo $str;
    }
    */

    public function meeting_map($id){
        $data['menu_title'] = "Meeting location during meeting";
        $data['result'] = $this->Post_model->getAllByFieldName('meeting','id',$id);

        $locationData = "";

        if($data['result']) {

            $data['first_lat'] = "'".$data['result'][0]->meeting_location_latitude."'";
            $data['first_lon'] = "'".$data['result'][0]->meeting_location_longitude."'";

            $locationData .= "['" . $data['result'][0]->meeting_name . "','" . $data['result'][0]->meeting_location_latitude . "','" . $data['result'][0]->meeting_location_longitude . "'],";
            $locationData .= "['" . $data['result'][0]->meeting_name . "','" . $data['result'][0]->meeting_location_latitude_middle . "','" . $data['result'][0]->meeting_location_longitude_middle . "'],";
            $locationData .= "['" . $data['result'][0]->meeting_name . "','" . $data['result'][0]->meeting_location_latitude_end . "','" . $data['result'][0]->meeting_location_longitude_end . "'],";

            $data['locationData'] = "[" . rtrim($locationData, ',') . "]";
        }

        $this->load->view('admin/meeting/meeting-map', $data);
    }

    public function meeting_graph_area(){
        $data['menu_title'] = "Area wise meeting graph view";
        $data['month_date'] = date("m/Y");

        if(!empty($_POST)) {
            $values = $_POST;
            if ($values['daterangepicker'] != "") {
                $month_date_array = explode('/', $values['daterangepicker']);
                //list($from_date, $to_date) = explode("-", $values['daterangepicker']);
//                $qry_str[] = "DATE_FORMAT(`meeting`.`meeting_start_time`, '%m/%d/%Y') BETWEEN '".trim($month_date_array[0] . "/01/" . $month_date_array[1])."' AND '".trim($month_date_array[0] . "/01/" . $month_date_array[1])."'";
                $dateForSearch = date("Y-m-d", strtotime($month_date_array[0] . "/01/" . $month_date_array[1]));
                $qry_str[] = "MONTH(`meeting`.`meeting_start_time`)=MONTH('$dateForSearch')";

                //$qry_str[] = "DATE_FORMAT(`meeting_start_time`, '%m/%d/%Y') = '".$to_date."'";
                $data['search_detail']['daterangepicker'] = $month_date_array[0] . "/01/" . $month_date_array[1];

            }
            $data['month_date'] = $month_date_array[0] . "/" . $month_date_array[1];
            // if($district_id  != ""){
            //     $qry_str[] = "`district_id` = '".$district_id."'";
            // }

            if ($values['district_id'] != "") {
                $qry_str[] = "`meeting`.`district_id` = '" . $values['district_id'] . "'";
                $data['search_detail']['district_id'] = $values['district_id'];
            }

            if ($values['upozila_id'] != "") {
                $qry_str[] = "`meeting`.`upozila_id` = '" . $values['upozila_id'] . "'";
                $data['search_detail']['upozila_id'] = $values['upozila_id'];
            }

            if ($values['union_id'] != "") {
                $qry_str[] = "`meeting`.`union_id` = '" . $values['union_id'] . "'";
                $data['search_detail']['union_id'] = $values['union_id'];
            }

            if ($values['type_name'] != "") {
                $qry_str[] = "`meeting`.`meeting_type` = '" . $values['type_name'] . "'";
                $data['search_detail']['type_name'] = $values['type_name'];
            }

            if ($values['user_name'] != "") {
                $qry_str[] = "`meeting`.`users_id` = '" . $values['user_name'] . "'";
                $data['search_detail']['user_name'] = $values['user_name'];
            }

        }else {
            $month_date_array = explode('/', $data['month_date']);
            //list($from_date, $to_date) = explode("-", $values['daterangepicker']);
//                $qry_str[] = "DATE_FORMAT(`meeting`.`meeting_start_time`, '%m/%d/%Y') BETWEEN '".trim($month_date_array[0] . "/01/" . $month_date_array[1])."' AND '".trim($month_date_array[0] . "/01/" . $month_date_array[1])."'";
            $dateForSearch = date("Y-m-d", strtotime($month_date_array[0] . "/01/" . $month_date_array[1]));
            $qry_str[] = "MONTH(`meeting`.`meeting_start_time`)=MONTH('$dateForSearch')";

            //$qry_str[] = "DATE_FORMAT(`meeting_start_time`, '%m/%d/%Y') = '".$to_date."'";
            $data['search_detail']['daterangepicker'] = $month_date_array[0] . "/01/" . $month_date_array[1];
        }


        
        if(!empty($this->ids)){
            $qry_str[] = " meeting.users_id in (".implode(',',$this->ids).") ";
            $data['ids'] = $this->ids;
        }

        $qry_str = ( $qry_str != '' ) ? ' WHERE '.implode(' AND ', $qry_str) : '';

        $string_query = "select meeting.*, app_user_info.username, district.name as district_name, upazila.name as upazila_name, union.name as union_name, ward.name as ward_name from meeting join district on meeting.district_id = district.id join upazila on meeting.upozila_id = upazila.id join `union` on meeting.union_id = union.id join ward on meeting.word_id = ward.id join app_user_info on meeting.users_id = app_user_info.id ".$qry_str." order by meeting.meeting_start_time desc";

        $data['result'] = $this->Post_model->custom_query($string_query);

        $day = cal_days_in_month(CAL_GREGORIAN, $month_date_array[0], $month_date_array[1]);
        $param = array();

            /*monthly number of meeting*/
        if ($data['result']) {
            foreach ($data['result'] as $value) {
                $key = date('d-m-Y', strtotime($value->meeting_start_time));
                if (array_key_exists($key, $param)) {
                    $param[$key] += 1;
                } else {
                    $param[$key] = 1;
                }
            }
        }

        $dateWise = "";
        for ($i = 1; $i <= $day; $i++) {
            $dateFormat = $i . "-" . $month_date_array[0] . "-" . $month_date_array[1];
            $date = date('d-m-Y', strtotime($dateFormat));
            $dateMeetingCount = array_key_exists($date, $param) ? $param[$date] : 0;
            $dateWise .= "['" . date('d', strtotime($date)) . "'," . $dateMeetingCount . "],";
        }
        $data['date_wise_data'] = rtrim($dateWise, ',');

        $this->load->view('admin/meeting/meeting-graph-report-area', $data);
    }


    public function meeting_graph()
    {
        $data['menu_title'] = "Meeting graph view";
        $data['month_date'] = date("m/Y");

//        echo "<pre>";
//        print_r($_POST);
//        echo "</pre>";
//        die;
        $data['project_names'] = $this->Post_model->custom_query('select * from meeting_type order by order_id ASC');

        $meeting_type = array();
        foreach ($data['project_names'] as $key => $val) {
            $meeting_type[$val->meeting_type]["meeting_start"] = 0;
            $meeting_type[$val->meeting_type]['meeting_end'] = 0;
        }

        if(!empty($_POST)) {
            $values = $_POST;
            if($values['daterangepicker'] != ""){
                $month_date_array = explode('/', $values['daterangepicker']);
                //list($from_date, $to_date) = explode("-", $values['daterangepicker']);
//                $qry_str[] = "DATE_FORMAT(`meeting`.`meeting_start_time`, '%m/%d/%Y') BETWEEN '".trim($month_date_array[0] . "/01/" . $month_date_array[1])."' AND '".trim($month_date_array[0] . "/01/" . $month_date_array[1])."'";
                $dateForSearch = date("Y-m-d",strtotime($month_date_array[0] . "/01/" . $month_date_array[1]));
                $qry_str[] =  "MONTH(`meeting`.`meeting_start_time`)=MONTH('$dateForSearch')";

                //$qry_str[] = "DATE_FORMAT(`meeting_start_time`, '%m/%d/%Y') = '".$to_date."'";
                $data['search_detail']['daterangepicker'] = $month_date_array[0] . "/01/" . $month_date_array[1];

            }
            $data['month_date'] = $month_date_array[0]."/".$month_date_array[1];


            if($values['district_id']  != ""){
                $qry_str[] = "`meeting`.`district_id` = '".$values['district_id']."'";
                $data['search_detail']['district_id'] = $values['district_id'];
            }

            if($values['upozila_id']  != ""){
                $qry_str[] = "`meeting`.`upozila_id` = '".$values['upozila_id']."'";
                $data['search_detail']['upozila_id'] = $values['upozila_id'];
            }

            if($values['union_id']  != ""){
                $qry_str[] = "`meeting`.`union_id` = '".$values['union_id']."'";
                $data['search_detail']['union_id'] = $values['union_id'];
            }

            if($values['type_name']  != ""){
                $qry_str[] = "`meeting`.`meeting_type` = '".$values['type_name']."'";
                $data['search_detail']['type_name'] = $values['type_name'];
            }

            if($values['user_name']  != ""){
                $qry_str[] = "`meeting`.`users_id` = '".$values['user_name']."'";
                $data['search_detail']['user_name'] = $values['user_name'];
            }
        }else{
            $month_date_array = explode('/', $data['month_date']);
            $dateForSearch = date("Y-m-d",strtotime($month_date_array[0] . "/01/" . $month_date_array[1]));
            $qry_str[] =  "MONTH(`meeting`.`meeting_start_time`)=MONTH('$dateForSearch')";
            //$qry_str[] = "DATE_FORMAT(`meeting_start_time`, '%m/%d/%Y') = '".$to_date."'";
            $data['search_detail']['daterangepicker'] = $month_date_array[0] . "/01/" . $month_date_array[1];
        }


        if(!empty($this->ids)){
            $qry_str[] = " meeting.users_id in (".implode(',',$this->ids).") ";
            $data['ids'] = $this->ids;
        }

        $qry_str = ( $qry_str != '' ) ? ' WHERE '.implode(' AND ', $qry_str) : '';

        $string_query = "select meeting.*, app_user_info.username, district.name as district_name, upazila.name as upazila_name, union.name as union_name, ward.name as ward_name from meeting join district on meeting.district_id = district.id join upazila on meeting.upozila_id = upazila.id join `union` on meeting.union_id = union.id join ward on meeting.word_id = ward.id left join app_user_info on meeting.users_id = app_user_info.id ".$qry_str." order by meeting.meeting_start_time desc";


        $data['meeting_result'] = $this->Post_model->custom_query($string_query);
        if ($data['meeting_result']) {
            foreach ($data['meeting_result'] as $value) {
                $meeting_type[$value->meeting_name]["meeting_start"] = $value->no_of_participation;
                $meeting_type[$value->meeting_name]['meeting_end'] = $value->no_of_participation_end;
            }
        }

        $data['meeting_type'] = $meeting_type;

       $this->load->view('admin/meeting/meeting-graph-report',$data);
    }

    public function meeting_delete($id){
        $result = $this->Post_model->getAll('meeting', '', '', $id);
        $file_name1 = (string)$result[0]->meeting_start_picture_name;
        $file_name2 = (string)$result[0]->meeting_start_picture_end;

        if(!empty($file_name1)){
            $upload_path = './assets/upload/meeting/'.$file_name1;
            $this->Post_model->delete('meeting', $id, $upload_path);
        }
        if(!empty($file_name2)){
            $upload_path = './assets/upload/meeting/'.$file_name2;
            $this->Post_model->delete('meeting', $id, $upload_path);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/meeting/meeting_view');
    }

    public function meeting_download(){
        if(isset($_SESSION['meeting_result'])){
            $xls = new Excel('Meeting Filter Result');

            $arr = array("Sl", "User Name", "Meeting Name", "Meeting Type", "No of Participation", "No of Participation End", "Meeting Start Time", "Meeting End Time", "Duration", "District", "Upazila", "Union", "Ward", "Start Picture Time", "End Picture Time");
            $xls->home();

            $count = count($arr);
            for($i = 0; $i < $count; $i++){
                $xls->label($arr[$i]);
                $xls->right(1);
            }    
            $xls->down();

            $sl = 1;
            foreach($_SESSION['meeting_result'] as $key => $value){
                $xls->home();
                $array = array($sl, $value->username, $value->meeting_name, $value->meeting_type, $value->no_of_participation, $value->no_of_participation_end, $value->meeting_start_time, $value->meeting_end_time, $value->duration, $value->district_name, $value->upazila_name, $value->union_name, $value->ward_name, $value->start_picture_time, $value->end_picture_time);
                for($i = 0; $i < $count; $i++){
                    $xls->label($array[$i]);
                    $xls->right(1);
                }
                $xls->down();
                $sl++;
            }
            $data = ob_get_clean();
            $xls->send();

            unset($_SESSION['meeting_result']);
        } else
            redirect('admin/meeting/meeting_view');
    }
}
