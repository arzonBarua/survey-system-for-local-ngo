<?php defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
/**
 * Author ID: 0290
 * Date: 5th Feb 2017
 */
class Label extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }

        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
             $this->ids = $this->permission->get_assigned_ids();
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }        

        $this->Menu_model->menu_model_info();
        $this->load->library('form_validation');
        $this->load->model('Post_model');

        // Excel intregation 
        include APPPATH . 'third_party/Excel.php';
    }

    public function label_add_pre(){
        $data['menu_title'] = "Add Master Activity Log";        
        unset($_SESSION['mf_label']); /* Reseting the session values */        

        if(empty($_POST)) {
            $data['projects'] = $this->Post_model->custom_query('SELECT * FROM `projects` WHERE id !=5');
            $this->load->view('admin/label/label-add-pre', $data);
        }else {
            if($this->input->post('quarter')){
                $_SESSION['mf_label']['project_id'] = $this->input->post('project_id');
                $_SESSION['mf_label']['quarter'] = $this->input->post('quarter');
                $_SESSION['mf_label']['year'] = $this->input->post('year');
                redirect('admin/label/label_add');
            }
            else{
                $data['error'] = "Please Select Quarter";
                $this->load->view('admin/label/label-add-pre', $data);
            }
        }
    }

    public function label_add(){
        $data['menu_title'] = "Add Master Activity Log";

        $qry = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '0' AND `status` = '1' ORDER BY `order_id` ASC");
        $data['num_page'] = $qry->num_rows();
        if(empty($_POST)) {
            $data['page'] = $this->uri->segment('4');
            $this->load->view('admin/label/label-add', $data);
        }else{
            $project_id = intval($this->input->post('project_id'));
            $quarter = intval($this->input->post('quarter'));
            $year = intval($this->input->post('year'));
            $page = intval($this->input->post('page'));
            $data['page'] = $page;
            $_SESSION['mf_label']['page'] = $page; /* Collect total Phases / pages */


            /* Gather the posted data into the session everytime the form submission */
            $k_label_id = key($this->input->post('label_id'));
            $_SESSION['mf_label']['label_id'][$k_label_id]  = $_POST['label_id'][$k_label_id];

            $k_label_order = key($this->input->post('label_order'));
            $_SESSION['mf_label']['label_order'][$k_label_order]  = $_POST['label_order'][$k_label_order];

            $k_target = key($this->input->post('target'));
            $_SESSION['mf_label']['target'][$k_target]  = $_POST['target'][$k_target];

            $k_achive = key($this->input->post('achive'));
            $_SESSION['mf_label']['achive'][$k_achive]  = $_POST['achive'][$k_achive];

            // $k_percent = key($_POST['percent']);
            // $_SESSION['mf_label']['percent'][$k_percent] = $_POST['percent'][$k_percent];

            $k_remarks = key($this->input->post('remarks'));
            $_SESSION['mf_label']['remarks'][$k_remarks] = $_POST['remarks'][$k_remarks];


            /* Check the final phase is it or not */
            if($this->input->post('page_amt') == $page){
                $cat_size = $_SESSION['mf_label']['page'];
                // $project_id = 1;
                // $quarter = floor((date('n') - 1) / 3) + 1;
                // $year = date("Y");
                $entry_date = date("Y-m-d");

                for($j = 0; $j < $cat_size; $j++){
                  $sub_keys = array_keys($_SESSION['mf_label']['label_id']);
                  $sub_size = count($_SESSION['mf_label']['label_id'][$sub_keys[$j]]);
                  for($k = 0; $k < $sub_size; $k++){
                    $label_id = $_SESSION['mf_label']['label_id'][$sub_keys[$j]][$k];
                    $label_order = $_SESSION['mf_label']['label_order'][$sub_keys[$j]][$k];
                    $target   = $_SESSION['mf_label']['target'][$sub_keys[$j]][$k];
                    $achive   = $_SESSION['mf_label']['achive'][$sub_keys[$j]][$k];
                    //$percent  = $_SESSION['mf_label']['percent'][$sub_keys[$j]][$k];

                    $div = ($target != "") ? $target : 1;
                    $percent  = ($achive/$div) * 100;
                    $remarks  = addslashes($_SESSION['mf_label']['remarks'][$sub_keys[$j]][$k]);

                    $qry = $this->db->query("INSERT INTO `mf__labels_data` VALUES('', '$label_id', '$project_id', '$quarter', '$year', '$target', '$achive', '$percent', '$remarks', '$label_order', '$entry_date', '1')");
                  }
                }
                unset($_SESSION['mf_label']); /* Reseting the session values */
                redirect('admin/label/label_view/'.$project_id.'/'.$year.'/'.$quarter);
            }else{
                $this->load->view('admin/label/label-add', $data);
            }
        }
    }

    public function label_edit(){
        $data['menu_title'] = "Edit Master Activity Log";

        $qry = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '0' AND `status` = '1' ORDER BY `order_id` ASC");
        $data['num_page'] = $qry->num_rows();
        if(empty($_POST)) {
            $_SESSION['mf_label']['project_id'] = $this->uri->segment('4');
            $_SESSION['mf_label']['year'] = $this->uri->segment('5');
            $_SESSION['mf_label']['quarter'] = $this->uri->segment('6');
                   
            $data['page'] = $this->uri->segment('7');
            if( $data['page'] == "") {
                $j = 1;
                foreach ($qry->result() as $row){                
                    $qry2 = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '".$row->id."' AND `status` = '1' ORDER BY `order_id` ASC");
                    $result2 = $qry2->result();
                    $i = 0;
                    foreach ($result2 as $key => $value) {
                        $sql_2 ="SELECT * FROM `mf__labels_data` WHERE `project_id` = '".$_SESSION['mf_label']['project_id']."' AND `year` = '".$_SESSION['mf_label']['year']."' AND `quarter` = '".$_SESSION['mf_label']['quarter']."' AND `label_id` = '".$value->id."' ORDER BY `quarter`,`order_id` ASC";
                        $qry_2 = $this->db->query($sql_2);
                        $result_2 = $qry_2->row();
                        $_SESSION['mf_label']['label_id'][$j][$i] = @$result_2->label_id;
                        $_SESSION['mf_label']['target'][$j][$i]   = @$result_2->target;
                        $_SESSION['mf_label']['achive'][$j][$i]   = @$result_2->achieve;
                        $_SESSION['mf_label']['remarks'][$j][$i]  = @$result_2->remarks;
                        $i++;
                    }
                    $j++;
                }
            }
            
            $this->load->view('admin/label/label-edit', $data);
        }else{
            $project_id = intval($this->input->post('project'));
            $year = intval($this->input->post('year'));
            $quarter = intval($this->input->post('quarter'));            
            $page = intval($this->input->post('page'));
            $data['page'] = $page;
            $_SESSION['mf_label']['page'] = $page; /* Collect total Phases / pages */
            $_SESSION['mf_label']['project_id'] = $project_id;
            $_SESSION['mf_label']['year'] = $year;
            $_SESSION['mf_label']['quarter'] = $quarter;


            /* Gather the posted data into the session everytime the form submission */
            $k_label_id = key($this->input->post('label_id'));
            $_SESSION['mf_label']['label_id'][$k_label_id]  = $_POST['label_id'][$k_label_id];

            $k_label_order = key($this->input->post('label_order'));
            $_SESSION['mf_label']['label_order'][$k_label_order]  = $_POST['label_order'][$k_label_order];

            $k_target = key($this->input->post('target'));
            $_SESSION['mf_label']['target'][$k_target]  = $_POST['target'][$k_target];

            $k_achive = key($this->input->post('achive'));
            $_SESSION['mf_label']['achive'][$k_achive]  = $_POST['achive'][$k_achive];

            // $k_percent = key($_POST['percent']);
            // $_SESSION['mf_label']['percent'][$k_percent] = $_POST['percent'][$k_percent];

            $k_remarks = key($this->input->post('remarks'));
            $_SESSION['mf_label']['remarks'][$k_remarks] = $_POST['remarks'][$k_remarks];


            /* Check the final phase is it or not */
            if($this->input->post('page_amt') == $page){
                $cat_size = $_SESSION['mf_label']['page'];
                //$project_id = 1;
                // $quarter = floor((date('n') - 1) / 3) + 1;
                // $year = date("Y");
                $entry_date = date("Y-m-d");

                for($j = 0; $j < $cat_size; $j++){
                  $sub_keys = array_keys($_SESSION['mf_label']['label_id']);
                  $sub_size = count($_SESSION['mf_label']['label_id'][$sub_keys[$j]]);
                  for($k = 0; $k < $sub_size; $k++){
                    
                    $label_id = $_SESSION['mf_label']['label_id'][$sub_keys[$j]][$k];
                    $label_order = $_SESSION['mf_label']['label_order'][$sub_keys[$j]][$k];
                    $target   = $_SESSION['mf_label']['target'][$sub_keys[$j]][$k];
                    $achive   = $_SESSION['mf_label']['achive'][$sub_keys[$j]][$k];
                    //$percent  = $_SESSION['mf_label']['percent'][$sub_keys[$j]][$k];

                    $div = ($target != "") ? $target : 1;
                    $percent  = @(($achive/ $div) * 100);
                    $remarks  = addslashes($_SESSION['mf_label']['remarks'][$sub_keys[$j]][$k]);

                    //$qry = $this->db->query("INSERT INTO `mf__labels_data` VALUES('', '$label_id', '$project_id', '$quarter', '$year', '$target', '$achive', '$percent', '$remarks', '$label_order', '$entry_date', '1')");
                    $qry = $this->db->query("UPDATE `mf__labels_data` SET `target` = '".$target."', `achieve` = '".$achive."', `percent` = '".$percent."', `remarks` = '".$remarks."' WHERE `label_id` = '".$label_id."' AND `project_id` = '".$project_id."' AND `quarter` = '".$quarter."' AND `year` = '".$year."'");
                  }
                }
                unset($_SESSION['mf_label']); /* Reseting the session values */
                redirect('admin/label/label_view/'.$project_id.'/'.$year.'/'.$quarter);
            }else{
                $this->load->view('admin/label/label-edit', $data);
            }
        }
    }

    public function label_view_pre(){
        $data['menu_title'] = "View Master Activity Log";
        
        $data['projects'] = $this->Post_model->custom_query('SELECT * FROM projects WHERE id !=5');

        // echo "<pre>";
        // print_r($data['projects']);
        // echo "</pre>";
        // die;
        $this->load->view('admin/label/label-view-pre', $data);
    }
/*
    public function label_view_ajax($project, $year){       
        $qry = $this->db->query("SELECT * FROM `mf__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' GROUP BY `quarter` ORDER BY `quarter` ASC");
        $data['result'] = $qry->result();
        $this->load->view('admin/label/label-view-search', $data);
    }
    */

    public function label_view($project, $year, $quarter){
        $data['menu_title'] = "View Master Activity Log";
        $data['project'] = $project;
        $data['year'] = $year;
        $data['quarter'] = $quarter;

        $qry = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '0' AND `status` = '1' ORDER BY `order_id` ASC");
        // $qry = $this->db->query("SELECT `mf__labels_data`.*, `mf__labels`.`label_name` label_name, `mf__projects`.`project_name` project_name, `mf__projects`.`manager_name` manager_name 
        //                         FROM  `mf__labels_data`
        //                         LEFT JOIN `mf__labels` ON `mf__labels`.`id` = `mf__labels_data`.`label_id`
        //                         LEFT JOIN `mf__projects` ON `mf__projects`.`id` = `mf__labels_data`.`project_id`
        //                         WHERE `mf__labels_data`.`status` = '1'
        //                         ORDER BY `mf__labels_data`.`order_id` ASC");
        $data['result'] = $qry->result();

        $_SESSION['mf_result'] = $data['result'];
        $_SESSION['mf_result_projects'] = array($project, $year, $quarter);
        $this->load->view('admin/label/label-view', $data);
    }

    public function corner_add_pre(){
        $data['menu_title'] = "Add MR,FP &amp; PAC Corner";
        if(empty($_POST)) {
            $qry = $this->db->query("SELECT * FROM `division` WHERE `status` = '1' ORDER BY `division_name` ASC");
            $data['result'] = $qry->result();

            $data['projects'] = $this->Post_model->custom_query('SELECT * FROM `projects` WHERE id !=5');
            $this->load->view('admin/label/corner-add-pre', $data);
        }else {
            if($this->input->post('quarter')){
                $_SESSION['mf_label']['project_id'] = $this->input->post('project_id');
                $_SESSION['pr_corner']['quarter']   = $this->input->post('quarter');
                $_SESSION['pr_corner']['year']      = $this->input->post('year');
                $_SESSION['pr_corner']['division']  = $this->input->post('division');
                $_SESSION['pr_corner']['district']  = $this->input->post('district');
                redirect('admin/label/corner_add');
            }
            else{
                $data['error'] = "Please Select Quarter";
                $this->load->view('admin/label/corner-add-pre', $data);
            }
        }
    }

    public function corner_add(){
        $data['menu_title'] = "Add MR,FP &amp; PAC Corner";        
        if(empty($_POST)) {
            $this->load->view('admin/label/corner-add', $data);
        }else{
            $mr_reject      = $this->input->post('mr_reject');
            $mr_success     = $this->input->post('mr_success');
            $mrm            = $this->input->post('mrm');
            $condom_person  = $this->input->post('condom_person');
            $pill           = $this->input->post('pill');
            $injectable     = $this->input->post('injectable');
            $iud            = $this->input->post('iud');
            $implant        = $this->input->post('implant');
            $liagation      = $this->input->post('liagation');
            $nsv            = $this->input->post('nsv');
            $pac            = $this->input->post('pac');
            $via_test       = $this->input->post('via_test');
            $condom_pieces  = $this->input->post('condom_pieces');

            $quarter        = $this->input->post('quarter');
            $year           = $this->input->post('year');
            $district       = $this->input->post('district');
            $division       = $this->input->post('division');
            //$year           = date("Y");
            $project_id     = intval($this->input->post('project_id'));
            $entry_date     = date("Y-m-d");
            
              
            $qry = $this->db->query("INSERT INTO `pr__labels_data` VALUES('', '$project_id', '$quarter', '$year', '$division', '$district', '$mr_reject', '$mr_success', '$mrm', '$condom_person', '$pill', '$injectable', '$iud', '$implant', '$liagation', '$nsv', '$pac', '$via_test', '$condom_pieces', '$entry_date', '1')");
            if($qry)
                redirect('admin/label/corner_view_pre');
            else{
                $data['error'] = 'Something wrong..';
                $this->load->view('admin/label/corner-add', $data);
            }
        }
    }

    public function corner_edit(){
        $data['menu_title'] = "Edit MR,FP &amp; PAC Corner";

        if(empty($_POST)) {
            $project_id = $this->uri->segment('4');
            $year       = $this->uri->segment('5');
            $quarter    = $this->uri->segment('6');
            $division    = $this->uri->segment('7');
            $district   = $this->uri->segment('8');

            $_SESSION['pr_corner']['project_id']= $project_id;
            $_SESSION['pr_corner']['quarter']   = $quarter;
            $_SESSION['pr_corner']['year']      = $year;
            $_SESSION['pr_corner']['division']  = $division;
            $_SESSION['pr_corner']['district']  = $district;

            $qry = $this->db->query("SELECT * FROM `pr__labels_data` WHERE `project_id` = '".$project_id."' AND `quarter` = '".$quarter."' AND `year` = '".$year."' AND `division` = '".$division."' AND `district` = '".$district."' AND `status` = '1'");

            $data['result'] = $qry->result();
            $this->load->view('admin/label/corner-edit', $data);
        }else{
            $project_id     = $_SESSION['pr_corner']['project_id']; 
            $year           = $_SESSION['pr_corner']['year']; 
            $quarter        = $_SESSION['pr_corner']['quarter']; 
            $divison        = $_SESSION['pr_corner']['division']; 
            $district       = $_SESSION['pr_corner']['district']; 

            $mr_reject      = $this->input->post('mr_reject');
            $mr_success     = $this->input->post('mr_success');
            $mrm            = $this->input->post('mrm');
            $condom_person  = $this->input->post('condom_person');
            $pill           = $this->input->post('pill');
            $injectable     = $this->input->post('injectable');
            $iud            = $this->input->post('iud');
            $implant        = $this->input->post('implant');
            $liagation      = $this->input->post('liagation');
            $nsv            = $this->input->post('nsv');
            $pac            = $this->input->post('pac');
            $via_test       = $this->input->post('via_test');
            $condom_pieces  = $this->input->post('condom_pieces');            
             
            $qry = $this->db->query("UPDATE `pr__labels_data` SET `mr_reject` = '$mr_reject', `mr_success` = '$mr_success', `mrm` = '$mrm', `condom_person` = '$condom_person', `pill` = '$pill', `injectable` = '$injectable', `iud` = '$iud', `implant` = '$implant', `liagation` = '$liagation', `nsv` = '$nsv', `pac` = '$pac', `via_test` = '$via_test', `condom_pieces` = '$condom_pieces' WHERE `project_id` = '".$project_id."' AND `year` = '".$year."' AND `quarter` = '".$quarter."'");
            if($qry)
                redirect('admin/label/corner_view_pre');
            else{
                $data['error'] = 'Something wrong..';
                $this->load->view('admin/label/corner-add', $data);
            }
        }
    }

    public function corner_view(){
        $project_id = $this->uri->segment('4');
        $year       = $this->uri->segment('5');
        $quarter    = $this->uri->segment('6');
        $division   = $this->uri->segment('7');
        $district   = $this->uri->segment('8');
        
        $data['menu_title'] = "View MR,FP &amp; PAC Corner";
        $sql = "SELECT `prld`.*, `district`.`name` AS district_name, `division`.`division_name`, `projects`.`name` AS project_name
                FROM `pr__labels_data` prld
                LEFT JOIN `division` ON `prld`.`division` = `division`.`id`
                LEFT JOIN `district` ON `prld`.`district` = `district`.`id`
                LEFT JOIN `projects` ON `prld`.`project_id` = `projects`.`id`
                WHERE `prld`.`status` = '1' AND `prld`.`project_id` = '".$project_id."' AND  `prld`.`quarter` = '".$quarter."' AND `prld`.`year` = '".$year."' AND `prld`.`division` = '".$division."' AND `prld`.`district` = '".$district."' LIMIT 1";

        $qry = $this->db->query($sql);
        $data['result'] = $qry->row();
        
        $_SESSION['pc_result'] = $data['result'];
        $this->load->view('admin/label/corner-view', $data);
    }

    public function corner_view_pre(){
        $data['menu_title'] = "View MR,FP &amp; PAC Corner";
        
        $qry = $this->db->query("SELECT * FROM `division` WHERE `status` = '1' ORDER BY `division_name` ASC");
        $data['result'] = $qry->result();
        //$data['projects'] = $this->Post_model->getAll('projects');
        $data['projects'] = $this->Post_model->custom_query('SELECT * FROM `projects` WHERE id !=5');
        $this->load->view('admin/label/corner-view-pre', $data);
    }



    /*  Reporting of These two Labels */    
    public function mf_report(){
        $data['menu_title'] = "Report on Master Activity Log";
                
        $this->load->view('admin/label/mf-report', $data);
    }

    

    public function pc_report(){
        $data['menu_title'] = "Report on MR,FP &amp; PAC Corner";
        //$data['projects'] = $this->Post_model->getAll('projects');
        $data['projects'] = $this->Post_model->custom_query('SELECT * FROM `projects` WHERE id !=5');


        $this->load->view('admin/label/pc-report', $data);
    }

    

/*
    public function get_ajax() {
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $fieldName = $this->input->post('field');
        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
        echo $str;
    }
    */

    public function mf_download(){
        if(isset($_SESSION['mf_result'])){
            $project = $_SESSION['mf_result_projects'][0];
            $year    = $_SESSION['mf_result_projects'][1];
            $quarter = $_SESSION['mf_result_projects'][2];
            $xls = new Excel('Master File Activity Log Result');

            $arr = array("Description of the component", "Targets", "Achive", "Percent", "Remarks");
            $xls->home();

            $count = count($arr);
            for($i = 0; $i < $count; $i++){
                $xls->label($arr[$i]);
                $xls->right(1);
            }    
            $xls->down();

            foreach($_SESSION['mf_result'] as $key => $value){
                $xls->home();
                $xls->label($value->label_name);
                $xls->down();

                $xls->home();
                $qry2 = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '".$value->id."' AND `status` = '1' ORDER BY `order_id` ASC");
                foreach ($qry2->result() as $data2){
                  $qry3 = $this->db->query("SELECT * FROM `mf__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' AND `quarter` = '".$quarter."' AND `label_id` = '".$data2->id."' AND `status` = '1' ORDER BY `order_id` ASC");
                  $xls->home();
                  $data3 = $qry3->row();
                  if($data2->is_head){
                    $xls->label($data2->label_name);
                  } else {
                    $array = array($data2->label_name, $data3->target, $data3->achieve, $data3->percent, $data3->remarks);
                    for($i = 0; $i < $count; $i++){
                        $xls->label($array[$i]);
                        $xls->right(1);
                    }
                    $xls->down();
                  }
                }
            }
            $data = ob_get_clean();
            $xls->send();

            unset($_SESSION['mf_result']);
            unset($_SESSION['mf_result_projects']);
        } else
            redirect('admin/label/label_view_pre');
    }

    public function pc_download(){
        if(isset($_SESSION['pc_result'])){        
            $xls = new Excel('MR,FP & PAC Corner Result');
            $xls->home();
            $xls->label("Project Name");
            $xls->right(1);
            //$xls->label($_SESSION['pc_result']->project_name);
            $xls->label("\xFF\xFE" .mb_convert_encoding('মুরাদ', 'UTF-16LE', 'UTF-8') );
            $xls->down();
            
            $xls->home();
            $xls->label("Year");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->year);
            $xls->down();
            
            $xls->home();
            $xls->label("Quarter");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->quarter);
            $xls->down();
            
            $xls->home();
            $xls->label("Division");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->division_name);
            $xls->down();
            
            $xls->home();
            $xls->label("District");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->district_name);
            $xls->down();

            $xls->home();
            $xls->label("MR Services");            
            $xls->down();
            
            $xls->home();
            $xls->label("MR Reject");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->mr_reject);
            $xls->down();
            
            $xls->home();
            $xls->label("MR Successfull");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->mr_success);
            $xls->down();
            
            $xls->home();
            $xls->label("MRM");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->mrm);
            $xls->down();
            
            $xls->home();
            $xls->label("Other Services");
            $xls->down();

            $xls->home();
            $xls->label("1. Short Term Method");
            $xls->down();
            
            $xls->home();
            $xls->label("Condom (Person)");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->condom_person);
            $xls->down();
            
            $xls->home();
            $xls->label("Pill");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->pill);
            $xls->down();
            
            $xls->home();
            $xls->label("Injectable");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->injectable);
            $xls->down();
            
            $xls->home();
            $xls->label("2. Long Term Method");            
            $xls->down();
            
            $xls->home();
            $xls->label("IUD");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->iud);
            $xls->down();
            
            $xls->home();
            $xls->label("Implant");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->implant);
            $xls->down();

            $xls->home();
            $xls->label("3. Permanent Method");
            $xls->down();
            
            $xls->home();
            $xls->label("Ligation");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->liagation);
            $xls->down();
            
            $xls->home();
            $xls->label("NSV");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->nsv);
            $xls->down();

            $xls->home();
            $xls->label("PAC");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->pac);
            $xls->down();
            
            $xls->home();
            $xls->label("VIA Test");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->via_test);
            $xls->down();

            $xls->home();
            $xls->label("Condom Pieces");
            $xls->right(1);
            $xls->label($_SESSION['pc_result']->condom_pieces);
            $xls->down();

            $data = ob_get_clean();
            $xls->send();

            unset($_SESSION['pc_result']);
        } else
            redirect('admin/label/label_view_pre');
    }
}