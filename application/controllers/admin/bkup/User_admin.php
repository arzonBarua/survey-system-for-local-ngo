<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 10:40 AM
 */
class User_admin extends CI_Controller
{
    

    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }

        /*For ACL*/
        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        /*For ACL*/
        $this->Menu_model->menu_model_info();
        $this->load->library('form_validation');
        $this->load->helper('file');
        $this->load->model('User_model');
        $this->load->model('Post_model');
    }
    

    public function user_add(){
        $data['menu_title'] = "Add User";
        $data['type_list'] = $this->Post_model->getAll('admin_type','id');
        $data['projects'] = $this->Post_model->getAll('projects');


        if(empty($_POST)) {
            $this->load->view('admin/user/user-add', $data);
        }else{
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('user_type','User Type','required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_emailExisting');
            $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

            if($this->form_validation->run() == FALSE ){
                $this->load->view('admin/user/user-add', $data);
            }else{

                $input['full_name'] = $this->input->post('full_name');
                $input['department'] = $this->input->post('department');
                $input['designation'] = $this->input->post('designation');
                $input['user_type'] = $this->input->post('user_type');
                $input['project_id']= $this->input->post('project_id');
                $input['email'] = $this->input->post('email');

                if($this->input->post('ip_restriction')) {
                    $input['ip_restriction'] = $this->input->post('ip_restriction');
                }

                $input['password'] = md5($this->input->post('password'));
                $input['ip_address'] = $this->input->post('ip_address');
                $input['created_at'] = date('Y-m-d');

               /*For File Upload*/
                $file_upload = 1;
                if(!empty($_FILES['userfile']['name'])){
                    $file_name = 'userfile';
                    $upload_path = './assets/upload/profile/';
                    $result = $this->Common_operation->image_upload($file_name,$upload_path);

                    if(!$result['flag']){
                        $data['error'] = $result['error'];
                        $this->load->view('admin/user/user-add', $data);
                        $file_upload = 0;
                    }else{
                        $input['picture'] = $result['picture'];
                    }
                }
                /*For File Upload*/
                if($file_upload){
                    if ($this->Post_model->insert('admin_login',$input)) {
                        if($input['project_id'] != 5){
                            $input_user_acl['project_id'] = $input['project_id'];
                            $input_user_acl['user_type'] = $input['user_type'];
                            $input_user_acl['user_id'] = $this->db->insert_id();
                            $this->Post_model->insert('user_acl',$input_user_acl);
                        }
                        $this->session->set_flashdata('success_message', 'Data inserted successfully');
                        redirect('admin/user_admin/user_view');
                    } else {
                        $data = array('error' => "somehting went wrong please inform the webmaster");
                        $this->load->view('admin/user/user-add', $data);
                    }
                }

            }
        }
    }

    public function user_edit($id){
        $data['menu_title'] = "Edit User";
        $data['type_list'] = $this->Post_model->getAll('admin_type','id');
        $data['projects'] = $this->Post_model->getAll('projects');

        $data['result'] = $this->Post_model->getAll('admin_login','','',$id);

        // echo '<pre>';
        // print_r($data['result']);
        // echo '</pre>';
        // die;


        if(empty($_POST)){
            if($data['result']==0)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/user/user-edit',$data);
        }else{
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('user_type','User Type','required');

            if((string)$data['result'][0]->email != $this->input->post('email')){
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_emailExisting');
            }

            if($this->input->post('password')){
                $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
            }

            if($this->form_validation->run() == FALSE){
                $this->load->view('admin/user/user-edit', $data);
            }else{

                $input['full_name'] = $this->input->post('full_name');
                $input['department'] = $this->input->post('department');
                $input['designation'] = $this->input->post('designation');
                $input['user_type'] = $this->input->post('user_type');
                $input['project_id']= $this->input->post('project_id');
                $input['email'] = $this->input->post('email');

                if($this->input->post('password')){
                    $input['password'] = md5($this->input->post('password'));
                }

                $input['ip_restriction'] = $this->input->post('ip_restriction');

                $input['ip_address'] = $this->input->post('ip_address');
                $input['updated_at'] = date('Y-m-d');

                /*For File Upload*/
                $file_upload = 1;
                if(!empty($_FILES['userfile']['name'])){
                    $file_name = 'userfile';
                    $upload_path = './assets/upload/profile/';

                    $file_name_folder = (string)$data['result'][0]->picture;
                    $full_path = $upload_path.$file_name_folder;

                    //delete files
                    if (file_exists($full_path)) {
                        $this->Common_operation->file_delete($full_path);
                    }

                    //delete files

                    $result = $this->Common_operation->image_upload($file_name,$upload_path);

                    if(!$result['flag']){
                        $data['error'] = $result['error'];
                        $this->load->view('admin/user/user-edit', $data);
                        $file_upload = 0;
                    }else{
                        $input['picture'] = $result['picture'];
                    }
                }
                /*For File Upload*/
                if($file_upload){
                    if ($this->Post_model->update('admin_login',$input,$id)){

                        $input_project['project_id'] = $input['project_id'];

                        $this->Post_model->update('app_user_info',$input_project,$id);

                        $this->db->query("delete from user_acl where user_id='$id'");

                            if($input['project_id'] != 5){
                                $input_user_acl['project_id'] = $input['project_id'];
                                $input_user_acl['user_type'] = $input['user_type'];
                                $input_user_acl['user_id'] = $id;
                                $this->Post_model->insert('user_acl',$input_user_acl);
                            }

                        $this->session->set_flashdata('success_message', 'Data updated successfully');
                        redirect('admin/user_admin/user_view');
                    }else{
                        $data = array('error' => "somehting went wrong please inform the webmaster");
                        $this->load->view('admin/user/user-edit', $data);
                    }
                }
            }


        }

    }


    public function user_view(){
        $data['menu_title'] = "View User";
        $data['result'] = $this->User_model->get_user_all_type();

        // echo '<pre>';
        // print_r($data['result']);
        // echo '</pre>';
        // die;

        if($data['result']==0)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/user/user-view',$data);
    }

    public function user_delete($id){
        $result = $this->Post_model->getAll('admin_login','','',$id);
        $file_name = (string)$result[0]->picture;

        if(!empty($file_name)){
            $upload_path = './assets/upload/profile/'.$file_name;
            $this->Post_model->delete('admin_login',$id,$upload_path);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/user_admin/user_view');
    }



    public function passwordCheck($lgn_pwd){
        if (preg_match("/^(?=.*?[A-Z,a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-+()]).{8,}$/", $lgn_pwd)){
            return TRUE;
        }else{
            $this->form_validation->set_message('passwordCheck', 'Please Follow the password convention');
            return FALSE;
        }
    }

    public function emailExisting($email){

        if($this->User_model->check_email($email)){
            return TRUE;
        }else{
            $this->form_validation->set_message('emailExisting', 'Email Already Exists');
            return FALSE;
        }

    }


    /*  App User Information */
    public function add_app_user(){
        $data['menu_title'] = "Add App User";
        $data['projects'] = $this->Post_model->getAll('projects');
        $data['district'] = $this->Post_model->getAll('district');


        if(empty($_POST)) {
            $this->load->view('admin/user/app-user-add', $data);
        }else{
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('designation','Designation','required');
            $this->form_validation->set_rules('login_id', 'Project Manager', 'required');
            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_rules('district', 'District Name', 'required');
            $this->form_validation->set_rules('upazilla', 'Upazilla Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

            if($this->form_validation->run() == FALSE ){
                $this->load->view('admin/user/app-user-add', $data);
            }else{                
                $input['full_name'] = $this->input->post('full_name');
                $input['designation'] = $this->input->post('designation');
                $input['login_id'] = $this->input->post('login_id');
                $input['username'] = $this->input->post('username');
                $input['district_id'] = $this->input->post('district');
                $input['upazila_id'] = $this->input->post('upazilla');
                $input['status'] = $this->input->post('status');

                $res = $this->Post_model->getAllByFieldName('admin_login', 'id', $input['login_id']);
                $input['project_id'] = $res[0]->project_id;

                $input['password'] = md5($this->input->post('password'));

                if ($this->Post_model->insert('app_user_info', $input)) {
                    $this->session->set_flashdata('success_message', 'Data inserted successfully');
                    redirect('admin/user_admin/view_app_user');
                } else {
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/user/app-user-add', $data);
                }
            }
        }
    }

    public function app_user_edit($id){
        $data['menu_title'] = "Edit App User";
        $data['projects'] = $this->Post_model->getAll('projects');
        $data['district'] = $this->Post_model->getAll('district');

        //$data['result'] = $this->Post_model->getAll('app_user_info', '', '', $id);
        $data['result'] = $this->Post_model->custom_query("select app_user_info.*, admin_login.user_type from app_user_info join admin_login on app_user_info.login_id = admin_login.id where app_user_info.id='$id'");

        // echo '<pre>';
        // print_r($data['result']);
        // echo '</pre>';
        // die;
        $project_id = $data['result'][0]->project_id;
        //die;

        $data['user_type_list'] = $this->Post_model->custom_query("select admin_login.id, admin_login.full_name from admin_login where id in (select user_id as id from user_acl where project_id='$project_id' and user_type=5)");




        if(empty($_POST)) {            
            if($data['result'] == 0)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/user/app-user-edit', $data);
        }else{
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('designation','Designation','required');
            $this->form_validation->set_rules('login_id', 'Project Manager', 'required');
            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_rules('district', 'District Name', 'required');
            $this->form_validation->set_rules('upazilla', 'Upazilla Name', 'required');

            if($this->input->post('password')){
                $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
            }

            if($this->form_validation->run() == FALSE ){
                $this->load->view('admin/user/app-user-add', $data);
            }else{                
                $input['full_name']     = $this->input->post('full_name');
                $input['designation']   = $this->input->post('designation');
                $input['login_id']      = $this->input->post('login_id');
                $input['username']      = $this->input->post('username');
                $input['district_id']   = $this->input->post('district');
                $input['upazila_id']    = $this->input->post('upazilla');
                $input['status']        = $this->input->post('status');
                
                $res = $this->Post_model->getAllByFieldName('admin_login', 'id', $input['login_id']);
                $this->project_id = $res[0]->project_id;    

                $input['project_id'] = $this->project_id;

            
                $input['password'] = md5($this->input->post('password'));

                if ($this->Post_model->update('app_user_info', $input, $id)) {
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/user_admin/view_app_user');
                } else {
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/user/app-user-edit', $data);
                }
            }
        }
    }

    public function view_app_user(){
        $data['menu_title'] = "View App User";
        $data['result'] = $this->User_model->get_app_user();

        
       

        // echo '<pre>';
        // print_r($data['result']);
        // echo '</pre>';
        // die;    

        if($data['result']==0)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/user/app-user-view',$data);
    }

    public function app_user_delete($id){
        $this->Post_model->delete('app_user_info', $id, '');
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/user_admin/view_app_user');
    }

    public function get_ajax() {
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $fieldName = $this->input->post('field');
        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
        echo $str;
    }

    public function get_project_type(){
          
           $project_id = $this->input->post('projects_type');

           $arrayProjectName = $this->Common_operation->get_user_type();

          

            $string = "<option>--Select Type--</option>";
             
            foreach($arrayProjectName[$project_id] as $key=>$val){
                 $string .= "<option value=$key>$val</option>";
             }
              

          echo $string;
    }




    // public function get_ajax_api()
    // {
    //     $id = $this->input->post('id');
    //     $tableName = $this->input->post('table');
    //     $className = $this->input->post('field');

    //     $fieldName =  $className."_id";

    //     $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
    //     echo $str;
    // }

}