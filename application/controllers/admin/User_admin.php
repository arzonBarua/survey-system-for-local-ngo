<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 10:40 AM
 */
class User_admin extends CI_Controller
{
    
    public $ids = array();
    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLogedNirapod'))){
            redirect('admin/index');
        }

        /*For ACL*/
       
        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            $this->ids = $this->permission->get_assigned_ids();
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        /*For ACL*/
        $this->Menu_model->menu_model_info();
        $this->load->library('form_validation');
        $this->load->helper('file');
        $this->load->model('User_model');
        $this->load->model('Post_model');
    }
    

    public function user_add(){
        $data['menu_title'] = "Add User";
        $data['type_list'] = $this->Post_model->getAll('admin_type','id');
        $data['projects'] = $this->Post_model->getAll('projects');


        if(empty($_POST)) {
            $this->load->view('admin/user/user-add', $data);
        }else{
            
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('user_type','User Type','required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_emailExisting');
            $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

            if($this->form_validation->run() == FALSE ){
                $this->load->view('admin/user/user-add', $data);
            }else{

                $input['full_name'] = $this->input->post('full_name');
                $input['department'] = $this->input->post('department');
                $input['designation'] = $this->input->post('designation');
                $input['user_type'] = $this->input->post('user_type');
                $input['project_id']= $this->input->post('project_id');
                $input['email'] = $this->input->post('email');
                $input['mobile'] = $this->input->post('mobile');

                if($this->input->post('ip_restriction')) {
                    $input['ip_restriction'] = $this->input->post('ip_restriction');
                }

                $input['password'] = md5($this->input->post('password'));
                $input['ip_address'] = $this->input->post('ip_address');
                $input['created_at'] = date('Y-m-d');

               /*For File Upload*/
                $file_upload = 1;
                if(!empty($_FILES['userfile']['name'])){
                    $file_name = 'userfile';
                    $upload_path = './assets/upload/profile/';
                    $result = $this->Common_operation->image_upload($file_name,$upload_path);

                    if(!$result['flag']){
                        $data['error'] = $result['error'];
                        $this->load->view('admin/user/user-add', $data);
                        $file_upload = 0;
                    }else{
                        $input['picture'] = $result['picture'];
                    }
                }
                /*For File Upload*/
                if($file_upload){
                    if ($this->Post_model->insert('admin_login',$input)) {
                        $input_user_acl['user_id']  = $this->db->insert_id();
                        

                        if($input['project_id'] != 5){
                            $input_user_acl['project_id'] = $input['project_id'];
                            $input_user_acl['user_type'] = $input['user_type'];
                            $this->Post_model->insert('user_acl',$input_user_acl);
                        }


                        if($input['mobile']==1){

                            if($input['user_type'] != 5){
                                $input_user_acl_other['project_id'] = $input['project_id'];
                                $input_user_acl_other['user_type'] = 5;
                                $input_user_acl_other['user_id'] = $input_user_acl['user_id'];
                                $this->Post_model->insert('user_acl',$input_user_acl_other);
                            }

                            $input_user_app['full_name'] = $input['full_name'];
                            $input_user_app['designation']  = $input['designation'];
                            $input_user_app['username'] = $input['email'];
                            $input_user_app['password'] = $input['password'];
                            $input_user_app['project_id'] = $input['project_id'];
                            $input_user_app['login_id'] = $input_user_acl['user_id'];
                            //$input_user_app['status'] = 1;

                            
                            $this->Post_model->insert('app_user_info',$input_user_app);
                            $input_track['main_id'] = $input_user_acl['user_id'];
                            $input_track['user_id'] = $this->db->insert_id();

                            $this->Post_model->insert('track_others',$input_track);

                        }


                        $this->session->set_flashdata('success_message', 'Data inserted successfully');
                        redirect('admin/user_admin/user_view');
                    } else {
                        $data = array('error' => "somehting went wrong please inform the webmaster");
                        $this->load->view('admin/user/user-add', $data);
                    }
                }

            }
        }
    }

    public function user_edit($id){
        $data['menu_title'] = "Edit User";
        $data['type_list'] = $this->Post_model->getAll('admin_type','id');
        $data['projects'] = $this->Post_model->getAll('projects');

        $data['result'] = $this->Post_model->getAll('admin_login','','',$id);

        // echo '<pre>';
        // print_r($data['result']);
        // echo '</pre>';
        // die;

        $this->session->set_userdata('referrel_link','user_view');


        if(empty($_POST)){
            if($data['result']==0)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/user/user-edit',$data);
        }else{
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('user_type','User Type','required');

            if((string)$data['result'][0]->email != $this->input->post('email')){
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_emailExisting');
            }

            if($this->input->post('password')){
                $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
            }

            if($this->form_validation->run() == FALSE){
                $this->load->view('admin/user/user-edit', $data);
            }else{

                $input['full_name'] = $this->input->post('full_name');
                $input['department'] = $this->input->post('department');
                $input['designation'] = $this->input->post('designation');
                $input['user_type'] = $this->input->post('user_type');
                $input['project_id']= $this->input->post('project_id');
                $input['email'] = $this->input->post('email');
                $input['mobile'] = $this->input->post('mobile');

                if($this->input->post('password')){
                    $input['password'] = md5($this->input->post('password'));
                }

                $input['ip_restriction'] = $this->input->post('ip_restriction');

                $input['ip_address'] = $this->input->post('ip_address');
                $input['updated_at'] = date('Y-m-d');

                /*For File Upload*/
                $file_upload = 1;
                if(!empty($_FILES['userfile']['name'])){
                    $file_name = 'userfile';
                    $upload_path = './assets/upload/profile/';

                    $file_name_folder = (string)$data['result'][0]->picture;
                    $full_path = $upload_path.$file_name_folder;

                    //delete files
                    if (file_exists($full_path)) {
                        $this->Common_operation->file_delete($full_path);
                    }

                    //delete files

                    $result = $this->Common_operation->image_upload($file_name,$upload_path);

                    if(!$result['flag']){
                        $data['error'] = $result['error'];
                        $this->load->view('admin/user/user-edit', $data);
                        $file_upload = 0;
                    }else{
                        $input['picture'] = $result['picture'];
                    }
                }
                /*For File Upload*/ 

                if($file_upload){
                    if ($this->Post_model->update('admin_login',$input,$id)){

                        $input_project['project_id'] = $input['project_id'];

                        $this->Post_model->update('app_user_info',$input_project,$id);

                        $this->db->query("delete from user_acl where user_id='$id'");

                            if($input['project_id'] != 5){
                                $input_user_acl['project_id'] = $input['project_id'];
                                $input_user_acl['user_type'] = $input['user_type'];
                                $input_user_acl['user_id'] = $id;
                                $this->Post_model->insert('user_acl',$input_user_acl);
                            }


                            $check_id = $this->Post_model->custom_query("select * from track_others where main_id = '$id'");
                            

                                

                                if($input['mobile']==1){

                                    if($input['user_type'] != 5){
                                        $input_user_acl_other['project_id'] = $input['project_id'];
                                        $input_user_acl_other['user_type'] = 5;
                                        $input_user_acl_other['user_id'] = $id;
                                        $this->Post_model->insert('user_acl',$input_user_acl_other);
                                    }

                                    $input_user_app['full_name'] = $input['full_name'];
                                    $input_user_app['designation']  = $input['designation'];
                                    $input_user_app['username'] = $input['email'];
                                        //$input_user_app['password'] = $input['password'];
                                    if($this->input->post('password')){
                                        $input_user_app['password'] = md5($this->input->post('password'));
                                    }else{
                                        $input_user_app['password'] =  $data['result'][0]->password;
                                    }
                                    $input_user_app['project_id'] = $input['project_id'];
                                    $input_user_app['login_id'] = $id;
                                    //$input_user_app['status'] = 1;

                                    if($check_id){  
                                        $unique_id = $check_id[0]->user_id; 
                                        $this->Post_model->update('app_user_info',$input_user_app,$unique_id);
                                    }else{
                                        $this->Post_model->insert('app_user_info',$input_user_app);
                                        $input_track['user_id'] = $this->db->insert_id();
                                        $input_track['main_id'] = $id;
                                        $this->Post_model->insert('track_others',$input_track);
                                    }  

                                }else{
                                     if($check_id){  
                                        $unique_id = $check_id[0]->user_id; 
                                        $input_check['status'] = 0;
                                        $this->Post_model->update('app_user_info',$input_check,$unique_id);
                                     }
                                }


                            $this->session->set_flashdata('success_message', 'Data updated successfully');
                            redirect('admin/user_admin/user_view');
                    }else{
                        $data = array('error' => "somehting went wrong please inform the webmaster");
                        $this->load->view('admin/user/user-edit', $data);
                    }
                }
            }


        }

    }


    public function user_view(){
        $data['menu_title'] = "View User";

        // echo "<pre>";
        // print_r($this->ids);
        // echo "</pre>";
        // die;

        $data['result'] = $this->User_model->get_user_all_type($this->ids);

        
        if($data['result']==0)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/user/user-view',$data);
    }

    public function user_delete($id){
        $result = $this->Post_model->getAll('admin_login','','',$id);
        $file_name = (string)$result[0]->picture;

        if(!empty($file_name)){
            $upload_path = './assets/upload/profile/'.$file_name;
            $this->Post_model->delete('admin_login',$id,$upload_path);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/user_admin/user_view');
    }



    public function passwordCheck($lgn_pwd){
        if (preg_match("/^(?=.*?[A-Z,a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-+()]).{8,}$/", $lgn_pwd)){
            return TRUE;
        }else{
            $this->form_validation->set_message('passwordCheck', 'Please Follow the password convention');
            return FALSE;
        }
    }

    public function emailExisting($email){

        if($this->User_model->check_email($email)){
            return TRUE;
        }else{
            $this->form_validation->set_message('emailExisting', 'Email Already Exists');
            return FALSE;
        }

    }

    public function get_phulki(){
        $phulki_query = $this->Post_model->custom_query("SELECT district.* FROM district join division on district.div_id = division.id where division.division_name LIKE '%Dhaka%' ");

        //$phulkiString = "<option>--SELECT--</option>";
        
        foreach($phulki_query as $key=>$val){
            if((string)$val->name == "Dhaka"){
                $id = $val->id;
            }
            $array[] = $val->name;
        }
      
        $phulkiString = "<option value=".$id .">".implode(", ", $array)."</option>";

        return $phulkiString;
    }

    public function show_all_district(){
        $result = $this->Post_model->custom_query("SELECT district.* FROM district");

        $string  = "";
        foreach($result as $key=>$val){
            $string .= "<option value=".$val->id.">".$val->name."</option>";
        }

        return $string;

    }


    /*  App User Information */
    public function add_app_user(){
        $data['menu_title'] = "Add App User";
       // $data['projects'] = $this->Post_model->getAll('projects');
        $data['projects'] = $this->Post_model->custom_query('SELECT * FROM `projects` ');
        $data['district'] = $this->Post_model->getAll('district');


        /*-------Phulki District-------*/
        $data['phulki_string'] = $this->get_phulki();

        /*-------All District-------*/
        $data['district_string'] = $this->show_all_district();

        
        if(empty($_POST)) {
            $this->load->view('admin/user/app-user-add', $data);
        }else{
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('designation','Designation','required');
            $this->form_validation->set_rules('login_id', 'Project Manager', 'required');
            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_rules('district', 'District Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

            if($this->form_validation->run() == FALSE ){
                $this->load->view('admin/user/app-user-add', $data);
            }else{                
                $input['full_name'] = $this->input->post('full_name');
                $input['designation'] = $this->input->post('designation');
                $input['login_id'] = $this->input->post('login_id');
                $input['username'] = $this->input->post('username');
                $input['district_id'] = $this->input->post('district');
                // $input['upazila_id'] = $this->input->post('upazilla');
                $input['status'] = $this->input->post('status');

                $res = $this->Post_model->getAllByFieldName('admin_login', 'id', $input['login_id']);
                $input['project_id'] = $res[0]->project_id;

                $input['password'] = md5($this->input->post('password'));

                if ($this->Post_model->insert('app_user_info', $input)) {
                    $this->session->set_flashdata('success_message', 'Data inserted successfully');
                    redirect('admin/user_admin/view_app_user');
                } else {
                    $data['error'] = "username already exits";
                    $this->load->view('admin/user/app-user-add', $data);
                }
            }
        }
    }

    public function app_user_edit($id){
        $this->session->set_userdata('referrel_link','view_app_user');
        $data['menu_title'] = "Edit App User";
        $data['projects'] = $this->Post_model->getAll('projects');
        $data['district'] = $this->Post_model->getAll('district');

        //$data['result'] = $this->Post_model->getAll('app_user_info', '', '', $id);
        $data['result'] = $this->Post_model->custom_query("select app_user_info.*, admin_login.user_type from app_user_info join admin_login on app_user_info.login_id = admin_login.id where app_user_info.id='$id'");

        // echo '<pre>';
        // print_r($data['result']);
        // echo '</pre>';
        // die;
        $project_id = $data['result'][0]->project_id;
        //die;
        if($project_id != 5){
            $data['user_type_list'] = $this->Post_model->custom_query("select admin_login.id, admin_login.full_name, admin_login.user_type, admin_login.project_id from admin_login where id in (select user_id as id from user_acl where project_id='$project_id' and user_type=5) and admin_login.user_type in (5,4,3,2) order by admin_login.user_type asc");
        }else{
             $data['user_type_list'] = $this->Post_model->custom_query("select  admin_login.id, admin_login.full_name, admin_login.user_type,admin_login.project_id from admin_login where project_id=5");
        }



        /*-------Phulki District-------*/
        $data['phulki_string'] = $this->get_phulki();

        /*-------All District-------*/
        $data['district_string'] = $this->show_all_district();




        if(empty($_POST)) {            
            if($data['result'] == 0)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/user/app-user-edit', $data);
        }else{




            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('designation','Designation','required');
            $this->form_validation->set_rules('login_id', 'Project Manager', 'required');
            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_rules('district', 'District Name', 'required');


            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>";

            // die;
            


            if($this->input->post('password')){
                $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
            }

            if($this->form_validation->run() == FALSE ){
                 //die; 
                $this->load->view('admin/user/app-user-edit', $data);

            }else{   

            

            // die;             
                $input['full_name']     = $this->input->post('full_name');
                $input['designation']   = $this->input->post('designation');
                $input['login_id']      = $this->input->post('login_id');
                $input['username']      = $this->input->post('username');
                $input['district_id']   = $this->input->post('district');
                // $input['upazila_id']    = $this->input->post('upazilla');
                $input['status']        = $this->input->post('status');
                
                $res = $this->Post_model->getAllByFieldName('admin_login', 'id', $input['login_id']);
                $this->project_id = $res[0]->project_id;    

                $input['project_id'] = $this->project_id;


                if($this->input->post('password')){
                    $input['password'] = md5($this->input->post('password'));
                }

                if ($this->Post_model->update('app_user_info', $input, $id)) {
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/user_admin/view_app_user');
                } else {
                    $data['error'] = "username already exits";
                    $this->load->view('admin/user/app-user-edit', $data);
                }
            }
        }
    }

    public function view_app_user(){
        $data['menu_title'] = "View App User";
        $data['result'] = $this->User_model->get_app_user($this->ids);  

        if($data['result']==0)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/user/app-user-view',$data);
    }

    public function app_user_delete($id){
        $this->Post_model->delete('app_user_info', $id, '');
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/user_admin/view_app_user');
    }

    public function get_ajax() {
        $id = $this->input->post('id');
        $tableName = $this->input->post('table');
        $fieldName = $this->input->post('field');
        $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
        echo $str;
    }

    public function get_project_type(){
          
           $project_id = $this->input->post('projects_type');

           $arrayProjectName = $this->Common_operation->get_user_type();

          

            $string = "<option>--Select Type--</option>";
             
            foreach($arrayProjectName[$project_id] as $key=>$val){
                 $string .= "<option value=$key>$val</option>";
             }
              

          echo $string;
    }

    // public function app_user_others(){

    //     $data['menu_title'] = "View App User";
    //     $data['result'] = $this->User_model->get_app_user($this->ids);
    //     echo "<pre>";
    //     print_r($data['result']);
    //     echo "</pre>";
    // }




    // public function get_ajax_api()
    // {
    //     $id = $this->input->post('id');
    //     $tableName = $this->input->post('table');
    //     $className = $this->input->post('field');

    //     $fieldName =  $className."_id";

    //     $str = $this->Post_model->getAjaxHTML($tableName, $fieldName, $id);
    //     echo $str;
    // }

}