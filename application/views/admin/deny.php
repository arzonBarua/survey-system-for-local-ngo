<?php $this->load->view('admin/header'); ?>

    <div class="box">

        <div class="box-body">
            <div class="alert alert-danger">
                <h3>You are not authorise for this module</h3>
            </div>
        </div><!-- /.box-body -->

    </div>

<?php $this->load->view('admin/footer-link'); ?>

<?php $this->load->view('admin/footer'); ?>