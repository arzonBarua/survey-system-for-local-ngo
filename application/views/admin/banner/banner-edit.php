<?php 
$data['title'] = 'Edit Banner';
$this->load->view('./admin/header', $data); 
?>

    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Banner Information</h3>
                </div><!-- /.box-header -->

                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>
                <!-- form start -->
                    <?php
                    if(validation_errors() || isset($error)){
                        echo "<div class='alert alert-danger'>";
                        echo validation_errors();
                        echo isset($error) ? $error : "";
                        echo "</div>";
                    }
					echo form_open_multipart('admin/banner/banner_edit/'.$result[0]->id); ?>
					<div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Banner Title</label>
                            <input type="text" name="banner_title" class="form-control" id="banner_title" placeholder="Banner Title goes here" autocomplete="off" value="<?php echo (set_value('banner_title') ? set_value('banner_title') : $result[0]->banner_title); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" name="userfile" id="userfile">
                            <p class="help-block">Supported extensions are: JPG / GIF / PNG</p>
                            <p class="img-show">
                                <?php if(!empty($value->picture)): ?>
                                <img src="<?php echo base_url();?>assets/upload/banner/thumb/<?php echo $this->Common_operation->show_thumb($value->picture) ?>"/>
                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Details</label>
                            <textarea name="details" class="form-control" rows="8" cols="40"><?php echo (set_value('details') ? set_value('details') : $result[0]->details); ?></textarea>
                        </div>

					</div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    <?php echo form_close(); ?>
                </div><!-- /.box -->
            <?php } ?>
        </div>
    </div>

<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>
