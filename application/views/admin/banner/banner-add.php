<?php 
$data['title'] = 'Add Banner';
$this->load->view('./admin/header', $data); 
?>

    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Banner Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                echo form_open_multipart('admin/banner/banner_add'); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Banner Title</label>
                            <input type="text" name="banner_title" class="form-control" id="banner_title" placeholder="Banner Title goes here" autocomplete="off" value="<?php echo set_value('banner_title'); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" name="userfile" id="userfile">
                            <p class="help-block">Supported formats are: JPG / GIF / PNG</p>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputFile">Details</label>
                            <textarea name="details" class="form-control" rows="8" cols="40"></textarea>
                        </div>

					</div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->
        </div>
    </div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>
