<?php 
$data['title'] = 'View Meeting';
$this->load->view('./admin/header', $data); 

if(isset($posted_arr))
    print_r($posted_arr);
?>
<div class="row">
    <div class="col-md-12">
        <div class="box" style="width: 60%;">
            <div class="box-header">
                <h3 class="box-title">Search Meeting Information</h3>
            </div>
            <div class="box-body">
                <?php echo form_open_multipart('admin/meeting/meeting_view');?>
                <table class="table table-bordered">
                    <tr>
                        <th width="20%">Date Wise</th>
                        <td>
                            <!-- <input type="text" name="daterangepicker" class="datetimepicker form-control" /> -->
                            <div class="input-group">
                                <input type="text" class="form-control pull-right" id="reservation" name="daterangepicker" >
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>                              
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Area Wise</th>
                        <td>
                            <label>District Name</label>
                            <select class="form-control union district call_ajax" name="district_id" id="upazila">
                                <option value=""> - SELECT - </option>
                                <?php 
                                $qry = $this->db->query("SELECT * FROM `district` ORDER BY UPPER(`name`) ASC");
                                $result_1 = $qry->result();
                                foreach($result_1 as $key=> $val): ?>
                                    <option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
                                <?php endforeach; ?>
                            </select>

                            <label for="upazila_id">Upazilla Name</label>
                            <select class="form-control ward upazila call_ajax" name="upozila_id" id="union">
                                <option value=""> - SELECT -</option>
                            </select>

                            <label for="upazila_id">Union Name</label>
                            <select class="form-control ward-name union call_ajax" name="union_id" id="ward">
                                <option value=""> - SELECT -</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Type Wise</th>
                        <td>
                            <select class="form-control" name="type_name" >
                                <option value=""> - SELECT - </option>
                                <?php 
                                $qry = $this->db->query("SELECT * FROM `meeting_type` WHERE `meeting_type` != '' ORDER BY UPPER(`meeting_type`) ASC");
                                $result_2 = $qry->result();
                                foreach($result_2 as $key=> $val): ?>
                                    <option value="<?php echo $val->meeting_type_value; ?>"><?php echo $val->meeting_type; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Surveyor Wise</th>
                        <td>
                            <select class="form-control" name="user_name">
                                <option value=""> - SELECT - </option>
                                <?php
                                //                                            $qry = $this->db->query("SELECT DISTINCT `users_id` FROM `meeting`");
                                //   
                                $acl_ext_query = "";                                         
                                if(!empty($ids)){
                                    $acl_ext_query = " where id in (".implode(',',$ids).") ";
                                }

                                $result_3 = $this->Post_model->custom_query("select id,`username` from app_user_info ".$acl_ext_query."order by `username` asc");
                                foreach($result_3 as $key=> $val):
//                                                $qry_user = $this->db->query("SELECT `full_name` FROM `admin_login` WHERE `id` = '".$val->users_id."'");
//                                                $data_user = $qry_user->row(); ?>
                                    <option value="<?php echo $val->id; ?>"><?php echo $val->username; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <th></th>
                        <td>
                            <input type="submit" name="search" class="btn btn-primary" value="Search" />
                        </td>
                    </tr>
                </table>
                <?php echo form_close(); ?>
            </div>
        </div>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    Meeting Information 
                </h3>
                <?php 
                if( isset($search_detail) && count($search_detail) > 0){
                    echo '[';
                    if(isset($search_detail['from_date']) && $search_detail['from_date'] != "")
                        echo '<b>&nbsp;&nbsp;Duration: </b> '.date("d, F Y", strtotime($search_detail['from_date'])).' - '.date("d, F Y", strtotime($search_detail['to_date']));
                    if(isset($search_detail['district_id']) && $search_detail['district_id'] != ""){
                        $qry = $this->db->query("SELECT `name` FROM `district` WHERE `id` = '".$search_detail['district_id']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;District Name: </b>".$data->name;
                    }

                    if(isset($search_detail['upozila_id']) && $search_detail['upozila_id'] != ""){
                        $qry = $this->db->query("SELECT `name` FROM `upazila` WHERE `id` = '".$search_detail['upozila_id']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;Upazila Name: </b>".$data->name;
                    }

                    if( isset($search_detail['union_id']) && $search_detail['union_id'] != ""){
                        $qry = $this->db->query("SELECT `name` FROM `union` WHERE `id` = '".$search_detail['union_id']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;Union Name: </b>".$data->name;
                    }

                    if(isset($search_detail['type_name']) && $search_detail['type_name'] != ""){
                        $qry = $this->db->query("SELECT `meeting_type` FROM `meeting_type` WHERE `meeting_type_value` = '".$search_detail['type_name']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;Meeting Type: </b>".$data->meeting_type;
                    }

                    if(isset($search_detail['user_name']) && $search_detail['user_name'] != ""){
                        $qry = $this->db->query("SELECT `username` FROM `app_user_info` WHERE `id` = '".$search_detail['user_name']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;Surveyor Name: </b>".$data->username;
                    }
                    echo ' ]';
                }
                ?>
                <a href="meeting_download" class="btn btn-primary pull-right">Download</a>
            </div>
            <!-- /.box-header -->


            <?php if ($result === false) { ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } else { ?>

                <?php if ($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>

                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="custom">SL</th>
                            <th>Device</th>
                            <th>Meeting Name</th>
                            <th>Meeting Start Time</th>
                            <th>Meeting End Time</th>
                            <th>Username</th>
                            <th style="width: 10px">No of participation (Start)</th>
                            <th style="width: 10px">No of participation (End)</th>
                            <th>District Name</th>
                            <th>Upazila Name</th>
                            <th>Union Name</th>
                            <!-- <th>Ward Name</th> -->
                            <th class="custom_last">Action</th>
                        </tr>
                        </thead>
                        <!-- <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                        </tfoot> -->
                        <tbody>
                        <?php
                        $sl = 1;
                        foreach ($result as $value) {
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo ($value->flag==1) ? 'Pc' : 'Mobile' ; ?></td>
                                <td><?php echo $value->meeting_name; ?></td>
                                <td><?php echo date("d F Y, H:i A", strtotime($value->meeting_start_time)); ?></td>
                                <td><?php echo date("d F Y, H:i A", strtotime($value->meeting_end_time)); ?></td>
                                <td><?php echo $value->username; ?></td>
                                <td><?php echo $value->no_of_participation; ?></td>
                                <td><?php echo $value->no_of_participation_end; ?></td>
                                <td><?php echo $value->district_name; ?></td>
                                <td><?php echo $value->upazila_name; ?></td>
                                <td><?php echo $value->union_name; ?></td>
                                <!-- <td><?php echo $value->ward_name; ?></td> -->
                                <td>
                                    <div class="modal fade" id="<?php echo $value->id ?>" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Meeting Information</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table id="rpt_table_<?php echo $value->id ?>" class="table table-bordered table-striped">
                                                        <tbody>
                                                            <tr>
                                                                <td>Meeting Name</td>
                                                                <td><?php echo $value->meeting_name; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>User Name</td>
                                                                <td><?php echo $value->username; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>No of Participation (Start Meeting)</td>
                                                                <td><?php echo $value->no_of_participation; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>No of Participation (End Meeting)</td>
                                                                <td><?php echo $value->no_of_participation_end; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>District Name</td>
                                                                <td><?php echo $value->district_name; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Upazila Name</td>
                                                                <td><?php echo $value->upazila_name; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Union Name</td>
                                                                <td><?php echo $value->union_name; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Ward Name</td>
                                                                <td><?php echo $value->ward_name; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Meeting Start Time</td>
                                                                <td><?php echo date("d F Y, h:i:s A", strtotime($value->meeting_start_time)); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Meeting End Time</td>
                                                                <td><?php echo date("d F Y, h:i:s A", strtotime($value->meeting_end_time)); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Start Meeting Picture time</td>
                                                                <td><?php echo date("d F Y, h:i:s A", strtotime($value->start_picture_time)); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>End Meeting Picture Time</td>
                                                                <td><?php echo date("d F Y, h:i:s A", strtotime($value->end_picture_time)); ?></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Duration</td>
                                                                <td><?php echo $value->duration." Hour"; ?></td>
                                                            </tr>
                                                            <?php if(!empty($value->meeting_start_picture_name)): ?>
                                                                <tr>
                                                                    <td>Meeting Start Picture</td>
                                                                    <td><img src="<?php echo base_url() ?>assets/upload/meeting/<?php echo $value->meeting_start_picture_name; ?>" style="width:340px"/></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                            <?php if(!empty($value->meeting_start_picture_end)): ?>
                                                                <tr>
                                                                    <td>Meeting End Picture</td>
                                                                    <td><img src="<?php echo base_url() ?>assets/upload/meeting/<?php echo $value->meeting_start_picture_end; ?>" style="width:340px"/></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" title="view" data-toggle="modal" data-target="#<?php echo $value->id ?>" ><i class="fa fa-fw fa-eye"></i></a> /
                                    <a href="meeting_edit/<?php echo $value->id ?>" title="Edit"><i
                                            class="fa fa-fw fa-edit"></i></a> /

                                    <a href="meeting_map/<?php echo $value->id ?>" target="_blank" title="View meeting in google map"><i
                                            class="fa fa-map-marker"></i></a> /
                                            
                                            
                                    <a href="meeting_delete/<?php echo $value->id ?>"
                                       onclick="return confirm('Are you sure you wish to delete this ?')"
                                       title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <?php $sl++;
                        } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
        </div>
        <!-- /.box -->
    </div>
</div>

<?php $this->load->view('./admin/footer-link') ?>

<!-- <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script> -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#example1").DataTable({
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
            // initComplete: function () {
            //     this.api().columns([1,6,7]).every(function () {
            //         var column = this;
            //         var select = $('<select class="form-control"><option value="">-Select-</option></select>')
            //             .appendTo($(column.footer()).empty())
            //             .on('change', function () {
            //                 var val = $.fn.dataTable.util.escapeRegex(
            //                     $(this).val()
            //                 );

            //                 column
            //                     .search(val ? '^' + val + '$' : '', true, false)
            //                     .draw();
            //             });

            //         column.data().unique().sort().each(function (d, j) {
            //             select.append('<option value="' + d + '">' + d + '</option>')
            //         });
            //     });
            // }
        });

        $(".call_ajax").on("change", function (e) {

            e.preventDefault();
            var id = $(this).val();
            var tableName = $(this).attr('id');
            var classNameArr = $(this).attr('class');
            var className = classNameArr.split(" ");

            //alert('show');

            
            /*$.post("get_ajax", { 'id': id,'table' : tableName, 'field': className[2] }, function(data){
                // $("#"+className[1]).html(data);
                console.log(data);
            });*/

            $.ajax({
                //url: 'get_ajax',
                url: '<?php echo base_url()."admin/ajax/" ?>get_ajax',
                data: {'id': id,'table' : tableName, 'field': className[2]},
                type: "post",
                dataType: 'text',
                success: function (data) {
                    //console.log(data);
                    $("#"+className[1]).html(data);
                }
            });
            return false;
        });

        $("#reservation").daterangepicker();
    });
</script>


<?php $this->load->view('./admin/footer'); ?>
