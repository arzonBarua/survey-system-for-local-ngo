<?php 
$data['title'] = 'View Graph Report';
$this->load->view('./admin/header', $data);
$month_array = explode("/",$month_date);
$monthName = date('F', mktime(0, 0, 0, $month_array[0], 10));
$month_array = explode("/",$month_date);
$monthName = date('F', mktime(0, 0, 0, $month_array[0], 10));
?>
<style>
    .highcharts-credits{
        display:none !important;
    }
    /*.highcharts-point{*/
        /*pointer-events: none !important;*/
    /*}*/
</style>
<div class="row">
    <div class="col-md-4">
        <!-- general form elements -->
<!--        <div class="box box-primary">-->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Search Meeting Information</h3>
                </div>
                <div class="box-body">

                    <?php
                    $attribute = array('id'=>'form_submit');
                    echo form_open_multipart('admin/meeting/meeting_graph_area',$attribute);?>
                    <table class="table table-bordered">
                        <tr>
                            <th width="20%">Date Wise</th>
                            <td>
                                <!-- <input type="text" name="daterangepicker" class="datetimepicker form-control" /> -->
                                <div class="input-group">
                                    <input type="text" class="form-control pull-right datepicker" id="reservation" name="daterangepicker" value="<?php echo isset($daterangepicker) ? $daterangepicker : '' ?>" required>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Area Wise</th>
                            <td>
                                <label>District Name</label>
                                <select class="form-control union district call_ajax" name="district_id" id="upazila">
                                    <option value=""> - SELECT - </option>
                                    <?php
                                    $qry = $this->db->query("SELECT * FROM `district` ORDER BY UPPER(`name`) ASC");
                                    $result_1 = $qry->result();
                                    foreach($result_1 as $key=> $val): ?>
                                        <option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
                                    <?php endforeach; ?>
                                </select>

                                <label for="upazila_id">Upazilla Name</label>
                                <select class="form-control ward upazila call_ajax" name="upozila_id" id="union">
                                    <option value=""> - SELECT -</option>
                                </select>

                                <label for="upazila_id">Union Name</label>
                                <select class="form-control ward-name union call_ajax" name="union_id" id="ward">
                                    <option value=""> - SELECT -</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Type Wise</th>
                            <td>
                                <select class="form-control" name="type_name" >
                                    <option value=""> - SELECT - </option>
                                    <?php
                                    $qry = $this->db->query("SELECT * FROM `meeting_type` WHERE `meeting_type` != '' ORDER BY UPPER(`meeting_type`) ASC");
                                    $result_2 = $qry->result();
                                    foreach($result_2 as $key=> $val): ?>
                                        <option value="<?php echo $val->meeting_type_value; ?>"><?php echo $val->meeting_type; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                                    <th>Surveyor Wise</th>
                                    <td>
                                        <select class="form-control" name="user_name">
                                            <option value=""> - SELECT - </option>
                                            <?php
                                            //                                            $qry = $this->db->query("SELECT DISTINCT `users_id` FROM `meeting`");
                                            //   
                                            $acl_ext_query = "";                                         
                                            if(!empty($ids)){
                                                $acl_ext_query = " where id in (".implode(',',$ids).") ";
                                            }

                                            $result_3 = $this->Post_model->custom_query("select id,`username` from app_user_info ".$acl_ext_query."order by `username` asc");
                                            foreach($result_3 as $key=> $val):
            //                                                $qry_user = $this->db->query("SELECT `full_name` FROM `admin_login` WHERE `id` = '".$val->users_id."'");
            //                                                $data_user = $qry_user->row(); ?>
                                                <option value="<?php echo $val->id; ?>"><?php echo $val->username; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                        <tr>
                            <th></th>
                            <td>
                                <input type="submit" name="search" class="btn btn-primary" value="Search" />
                            </td>
                        </tr>
                    </table>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Graph Result for Meeting:</h3>
                    <?php
                    if( isset($search_detail) && count($search_detail) > 0){
                        echo '[';
                        if(isset($search_detail['daterangepicker']))
                            echo '<b>&nbsp;&nbsp;Month: </b> '.date("F Y", strtotime($search_detail['daterangepicker']));
                        if(isset($search_detail['district_id']) && $search_detail['district_id'] != ""){
                            $qry = $this->db->query("SELECT `name` FROM `district` WHERE `id` = '".$search_detail['district_id']."'");
                            $data = $qry->row();
                            echo "<b>&nbsp;&nbsp;District Name: </b>".$data->name;
                        }

                        if(isset($search_detail['upozila_id']) && $search_detail['upozila_id'] != ""){
                            $qry = $this->db->query("SELECT `name` FROM `upazila` WHERE `id` = '".$search_detail['upozila_id']."'");
                            $data = $qry->row();
                            echo "<b>&nbsp;&nbsp;Upazila Name: </b>".$data->name;
                        }

                        if( isset($search_detail['union_id']) && $search_detail['union_id'] != ""){
                            $qry = $this->db->query("SELECT `name` FROM `union` WHERE `id` = '".$search_detail['union_id']."'");
                            $data = $qry->row();
                            echo "<b>&nbsp;&nbsp;Union Name: </b>".$data->name;
                        }

                        if(isset($search_detail['type_name']) && $search_detail['type_name'] != ""){
                            $qry = $this->db->query("SELECT `meeting_type` FROM `meeting_type` WHERE `meeting_type_value` = '".$search_detail['type_name']."'");
                            $data = $qry->row();
                            echo "<b>&nbsp;&nbsp;Meeting Type: </b>".$data->meeting_type;
                        }

                        if(isset($search_detail['user_name']) && $search_detail['user_name'] != ""){
                            $qry = $this->db->query("SELECT `username` FROM `app_user_info` WHERE `id` = '".$search_detail['user_name']."'");
                            $data = $qry->row();
                            echo "<b>&nbsp;&nbsp;Surveyor Name: </b>".$data->username;
                        }
                        echo ' ]';
                    }
                    ?>
                </div>

                <div class="box-body">

                        <div id="container" style="min-width: 300px; height: 420px; margin: 0 auto"></div>

<!--                        <div id="No_Data" style="min-width: 300px; height: 400px; margin: 0 auto;text-align: center">-->
<!--                            <h3>Please use filter to see graph Result</3>-->
<!--                            <div class="overlay" style="display:none">-->
<!--                                <i class="fa fa-refresh fa-spin"></i>-->
<!--                            </div>-->
<!--                        </div>-->

                </div>
            </div>
        </div>

                <!-- /.box-body -->
        </div><!-- /.box -->
<!--</div>-->

    <?php $this->load->view('./admin/footer-link') ?>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>


    <script type="text/javascript">




        $(function(){

            $('.datepicker').datepicker({
                autoclose: true,
                minViewMode: 1,
                format: 'mm/yyyy'
            });

            
            $(".call_ajax").on("change", function (e) {

                e.preventDefault();
                var id = $(this).val();
                var tableName = $(this).attr('id');
                var classNameArr = $(this).attr('class');
                var className = classNameArr.split(" ");

                //alert('show');

                
                /*$.post("get_ajax", { 'id': id,'table' : tableName, 'field': className[2] }, function(data){
                    // $("#"+className[1]).html(data);
                    console.log(data);
                });*/

                $.ajax({
                    //url: 'get_ajax',
                    url: '<?php echo base_url()."admin/ajax/" ?>get_ajax',
                    data: {'id': id,'table' : tableName, 'field': className[2]},
                    type: "post",
                    dataType: 'text',
                    success: function (data) {
                        //console.log(data);
                        $("#"+className[1]).html(data);
                    }
                });
                return false;
            });

            

            date_wise_data = [<?php echo isset($date_wise_data) ? $date_wise_data : 0;  ?>];
            monthName = '<?php echo isset($monthName) ? $monthName : ""  ?>';
            yearName = '<?php echo isset($month_array[1]) ? $month_array[1] : ""  ?>';
            //graph_data(date_wise_data);
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Graphical view for number of meeting in '+monthName+', '+yearName
                },

                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: 0,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Number of meeting'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        return '<b>Date:'+this.point.name+' '+this.series.name+'</b><br>'+
                            'Number of meeting: '+this.point.y;
                    }
                },
                series: [{
                    name: "population",
                    data: date_wise_data,
                    dataLabels: {
                        enabled: false,
                        rotation: -360,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }]
            });


//            $( "#form_submit" ).submit(function( event ) {
//                event.preventDefault();
//
//                var string = "";
//
//                $(".overlay").show();
//
//                $.ajax({
//                    type: "POST",
//                    url:  "meeting_graph_area",
//                    async: true,
//                    data: $("#form_submit").serialize(),
//                    dataType: "json"
//                }).done(function( data ){
//                    $(".overlay").hide();
//                    $("#No_Data").hide();
//                    $("#container").show();
//                    console.log(data);
//                    var myObj = [];
//                    //var departement = []; // create array here
////                    $.each(data, function (index, personne) {
////
////                        myObj[index] = "";
////                    });
//
//                    $.each(data, function (index, personne) {
//                        myObj[index] = [];
//                        myObj[index].push(personne);
//                    });
//
//                    var test = new Array();
//                    test[0]=[];
//                    test[0]['a'] = 'test';
//                    test[1]=[];
//                    test[1]['b'] = 'test b';
//                    var json = JSON.stringify(test);
//                    alert(json);
//
//                    var json = JSON.stringify(myObj);
//                    alert(json);
//
//                    console.log(json);
//
//                    graph_data(myObj);
//
//                });
//            });

        });
    </script>


<?php $this->load->view('./admin/footer'); ?>
