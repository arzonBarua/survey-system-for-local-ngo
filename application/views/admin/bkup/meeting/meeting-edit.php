<?php 
$data['title'] = 'Edit Meeting';
$this->load->view('./admin/header', $data); 
?>

<!--Body Portin-->
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">

            <?php
            if(validation_errors() || isset($error)){
                echo "<div class='alert alert-danger'>";
                echo validation_errors();
                echo isset($error) ? $error : "";
                echo "</div>";
            }

            echo form_open_multipart('admin/meeting/meeting_edit/'.$result[0]->id); ?>
            <div class="box-header">
                <h3 class="box-title">Meeting Information</h3>
            </div>
            <div class="box-body">

                <div class="form-group">
                    <label>District Name</label>
                    <select class="form-control union district call_ajax" name="district_id" id="upazila" required >
                        <option value="">--Select District--</option>
                        <?php foreach($district as $key=> $val): ?>
                            <option value="<?php echo $val->id; ?>" <?php echo ($result[0]->district_id == $val->id) ? 'selected' : '' ?> ><?php echo $val->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="upazila_id">Upazilla Name</label>
                    <select class="form-control ward upazila call_ajax" name="upozila_id" id="union" required>
                        <option value="<?php echo $result[0]->upozila_id ?>"><?php echo $result[0]->upazila_name ?></option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="upazila_id">Union Name</label>
                    <select class="form-control ward-name union call_ajax" name="union_id" id="ward" required>
                        <option value="<?php echo $result[0]->union_id ?>"><?php echo $result[0]->union_name ?></option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="ward">Ward</label>
                    <select class="form-control" name="word_id" id="ward-name" required>
                        <option value="<?php echo $result[0]->word_id ?>"><?php echo $result[0]->ward_name ?></option>
                    </select>
                </div>


                <div class="form-group">
                    <label for="meeting_name">Meeting Name</label>
                    <input type="text" name="meeting_name" class="form-control" id="meeting_name" placeholder="Enter Meeting Name" autocomplete="off" value="<?php echo $result[0]->meeting_name ?>" required>
                </div>

                <div class="form-group">
                    <label for="no_of_participation">Number of Participation(Start Meeting)</label>
                    <input type="number" name="no_of_participation" class="form-control" id="no_of_participation" placeholder="Number of participent" value="<?php echo  $result[0]->no_of_participation ?>" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label for="no_of_participation">Number of Participation (End Meeting)</label>
                    <input type="number" name="no_of_participation_end" class="form-control" id="no_of_participation_end" placeholder="Number of participent" value="<?php echo $result[0]->no_of_participation_end ?>" autocomplete="off" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Meeting Date</label>

                    <input type="text" name="meeting_date" class="form-control datepicker" id="no_of_participation" placeholder="MM/DD/YYYY" value="<?php echo date('m/d/Y',strtotime($result[0]->meeting_start_time)); ?>" autocomplete="off" required>
                </div>

                <div class="bootstrap-timepicker">
                    <div class="form-group">
                        <label>Meeting Start time:</label>
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="start_time" value="<?php echo date('h:i:A',strtotime($result[0]->meeting_start_time)); ?>" placeholder="hr/min/AM or PM"/>
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->
                </div>
                <div class="bootstrap-timepicker">
                    <div class="form-group">
                        <label>Meeting End time:</label>
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="end_time" value="<?php echo date('h:i:A',strtotime($result[0]->meeting_end_time)); ?>" placeholder="hr/min/AM or PM"/>
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->
                </div>



                <div class="form-group">
                    <label for="meeting_start_picture_name">Meeting Start Picture</label>
                    <input type="file" name="meeting_start_picture_name" id="meeting_start_picture_name">
                </div>

                <div class="form-group">
                    <label for="meeting_start_picture_end">Meeting End Picture</label>
                    <input type="file" name="meeting_start_picture_end" id="meeting_start_picture_end">
                </div>

                <div class="form-group">
                    <label>Project coordinator</label>
                    <select class="form-control margin-left" name="users_id" id="users_id" required>
                        <option value="">--Select Drop Down--</option>
                        <?php foreach($op_ids as $key=>$val): ?>
                            <option value="<?php echo $val->id ?>" <?php echo ($result[0]->users_id==$val->id) ? 'selected' : '' ?> ><?php echo $val->username ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>


                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div><!-- /.box-body -->
            <?php echo form_close(); ?>


        </div><!-- /.box -->
    </div>
</div>
<!--End Body Portion-->


<?php $this->load->view('./admin/footer-link'); ?>



<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>




<script type="text/javascript">
    $(function () {
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });

//        $('.timepicker').val('');

        $(".datepicker").datepicker();

        $(".datepicker").on("change",function(e){
            $(this).datepicker('hide');
        });


        $(".call_ajax").on("change", function (e) {

            e.preventDefault();
            var id = $(this).val();
            var tableName = $(this).attr('id');
            var classNameArr = $(this).attr('class');
            var className = classNameArr.split(" ");


            $.ajax({
                url: "<?php echo base_url()."admin/ajax/" ?>get_ajax",
                data: {'id': id,'table' : tableName, 'field': className[2]},
                type: "post",
                dataType: 'text',
                success: function (data) {
                    $("#"+className[1]).html(data);
                }
            });
            return false;
        });
    });
</script>
<?php $this->load->view('./admin/footer'); ?>
