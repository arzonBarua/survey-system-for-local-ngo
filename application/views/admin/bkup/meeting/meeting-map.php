<?php $this->load->view('admin/header'); ?>

<style>
    #map {
        height: 500px;
        width: 500px;
        margin: 10px;
        padding: 0px
    }
</style>
<!--For google map-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1wF4b_yvf71bOOTIRYYjnJV6rwQG-cDI&v=3.exp&libraries=places&sensor=true"></script>
<!--<script src="assets/js/markerwithlabel.js" type="text/javascript"></script>-->


<?php if($result==0){ ?>
    <div class="callout callout-danger lead">
        <h4>No Data Found</h4>
    </div>

<?php }else{ ?>

        <h3>Meeting start map view</h3>
        <div id="map_0" style="width:100%; height:400px; border: 2px solid #3872ac;"></div>

        <h3>Meeting middle map view</h3>
        <div id="map_1" style="width:100%; height:400px; border: 2px solid #3872ac;"></div>

        <h3>Meeting end map view</h3>
        <div id="map_2" style="width:100%; height:400px; border: 2px solid #3872ac;"></div>

        <?php
            // echo "<pre>";
            // print_r($first_location);
            // echo "</pre>";

            // die;
        ?>

        <script>

            window.onload = on_map();

            function on_map(){
                var locationData = <?php echo $locationData ?>;
                var startLat = <?php echo $first_lat ?>;
                var first_lon = <?php echo $first_lon ?>;

                //alert(locationData);
                var locationData = <?php echo $locationData ?>;
                var first_lat = <?php echo $first_lat ?>;
                var first_lon = <?php echo $first_lon ?>;

                var myCenter=new google.maps.LatLng(first_lat,first_lon);
                var locations = locationData;
                for (i = 0; i < locations.length; i++) {
                    var map = new google.maps.Map(document.getElementById('map_'+i), {
                        zoom: 10,
                        center: myCenter,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    var infowindow = new google.maps.InfoWindow();

                        marker = new google.maps.Marker({
                            icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                            map: map,
                            draggable: true,
                            raiseOnDrag: true,
                            map: map
                        });
                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                                infowindow.setContent(locations[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                }
            }







        </script>
<?php } ?>

<?php $this->load->view('admin/footer-link'); ?>
<!--For Extra js Link-->
<?php $this->load->view('admin/footer'); ?>


