<?php 
$data['title'] = 'ACL Edit';
$this->load->view('./admin/header', $data); 
?>

<div class="row">
    <div class="col-md-6">
        <!-- general form elements disabled -->
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">Access Control List</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php if($this->session->flashdata('update_failed')): ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('update_failed'); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open('admin/acl/acl_edit/'); ?>
                <div class="form-group">
                    <label>Select User Type</label>
                    <select class="form-control" name="login_id" disabled>
                        <option>--Select--</option>
                        <?php foreach($get_user_all as $key => $val){ ?>
                            <option value="<?php echo $key ?>" <?php echo ($get_acl[0]->login_id==$key) ? "selected" : ""  ?> ><?php echo $val; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <?php foreach($result as $key => $val ){ ?>
                    <div class="form-group">
                        <label class="control-label"><i class="fa fa-fw fa-unlock-alt"></i> <?php echo $name[$key]; ?></label>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <?php foreach($val as $value){ ?>
                                <label>
                                    <input type="checkbox" name="access[<?php echo $key ?>][]" value="<?php echo $value; ?>"
                                        <?php
                                        if(isset($get_acl[0]->access->$key)) {
                                            echo in_array($value, $get_acl[0]->access->$key) ? "checked" : "";
                                        }
                                        ?> class="<?php echo current(explode(' ',$name[$key])); ?>" />
                                    <?php echo $name[$value]; ?>
                                </label>
                            <?php } ?>
                            <label>
                                <input type="checkbox" class="select-all" id="<?php echo current(explode(' ',$name[$key])); ?>">Full
                            </label>
                        </div>
                    </div>
                <?php } ?>
                <div class="box-footer">
                    <input type="hidden" name="id" value=<?php echo $id ?> >
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!--/.col (right) -->
</div>

<?php $this->load->view('./admin/footer-link'); ?>
<script>
    $(function(){
        $('.select-all').click(function(){
            var val = $(this).attr('id');
            if($(this).is(":checked")){
                $('.'+val).prop('checked',true);
            }else{
                $('.'+val).prop('checked',false);
            }
        });
    });
</script>

<?php $this->load->view('./admin/footer'); ?>



