<?php 
$data['title'] = 'Add Survey Question';
$this->load->view('./admin/header', $data); 
?>
<div class="row">
    <!--Body Portin-->
    <div class="col-md-6">
        <div class="box box-primary" id="profile">
        <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }

        echo form_open_multipart('admin/survey/survey_qs_add'); ?>
            <div class="box-header">
                <h3 class="box-title">Add Survey Question</h3>
            </div>
            <div class="box-body">
                <div id="buttons">

                        <div class="form-group">
                            <label>Survey For</label>
                            <select class="form-control" name="survey_info_id" required >
                                <option value="">--Select Survey Question--</option>
                                <?php foreach ($result as  $value ): ?>
                                    <option value="<?php echo $value->id ?>"><?php echo $value->survey_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div id="remove_0">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Question Name</label>
                                    <input type="text" name="survey_qs_0" class="form-control" id="survey_name" placeholder="Enter Survey Name" autocomplete="off" required>
                                </div>
                                <div class="form-group">
                                    <label>Question Type</label>
                                    <select class="form-control" name="survey_qs_type_0" required >
                                        <option value="">--Select Type--</option>
                                        <option value="dropdown">Dropdown</option>
                                        <option value="radio">Radio</option>
                                        <option value="checkbox">Checkbox</option>
                                        <option value="text">Text</option>
                                        <option value="datepicker">Datepicker</option>
                                        <option value="time">Time</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Main Question ID</label>
                                    <input type="text" name="main_ques_id_0" class="form-control" placeholder="" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Order ID</label>
                                    <input type="text" name="order_id_0" class="form-control" placeholder="" autocomplete="off" required>
                                </div>

                                <div class="get-option">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="option-text">Option 1</label>
                                        <input type="text" name="option_0[]" class="form-control" placeholder="" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="meta-text">Option Code 1</label>
                                        <input type="text" name="meta_0[]" class="form-control" placeholder="" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="option-order">Option Order 1</label>
                                        <input type="text" name="option_order_0[]" class="form-control to-remove" placeholder="" autocomplete="off">
                                    </div>

                                </div>
                                <div class="form-group" style="text-align: center">
                                    <input type="button" id="optAdd" class="btn btn-primary" onclick="add_option(this)" value="Add More Option">
                                </div>

                                <input type="hidden" name="count_qs[]" value="0" />
                        </div>
                </div>
                <div class="form-group" style="text-align: center">
                    <input type="button" id="btnAdd" class="btn btn-primary" value="Add More Question">
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>


<!--                </div>-->
            </div><!-- /.box-body -->

        <?php echo form_close(); ?>


        </div><!-- /.box -->
    </div>

</div>

    <!--End Body Portion-->
<?php $this->load->view('./admin/footer-link'); ?>

<script type="text/javascript">

    /*Stop Enter Button*/
    $('html').bind('keypress', function(e)
    {
        if(e.keyCode == 13)
        {
            return false;
        }
    });
    /*End Stop Enter Button*/


    var c = 0;
    /*To add option*/
    function add_option(ob)
    {
        var divId = $(ob).parent().parent().attr('id');

        var cloned = $('#'+divId).find(".get-option:last").clone();
        $(cloned).find("input[type=text]").each(function() {
            $(this).val('');
        });

        var text = $(cloned).find(".meta-text").text();
        var textArr = text.split(" ");

        var count = textArr[2];
        if(count == 1){
            var  tds = '<div style="text-align: center;margin-top: 10px"><a class="btn btn-warning remove" style="margin-bottom:20px" onclick="removeRow(this)">Remove</a></div>';
            $(cloned).find('.to-remove').after(tds);
        }
        count++;
        $(cloned).find(".option-text").text("Option "+count+" :");
        $(cloned).find(".meta-text").text("Option Code "+count+" :");
        $(cloned).find(".option-order").text("Option Order "+count+" :");

        $('#'+divId+' .get-option:last').after(cloned);
    }
    function removeRow(ob){
        $(ob).parent().parent().parent().remove();

    }


    $(function(){
        var i = 1;
        var f = 1;
        c++;
        $("#btnAdd").click(function (e) {
            $("#buttons").append('<div id="remove_'+i+'">' +
                                    '<div class="form-group">'+
                                        '<label for="exampleInputEmail1">Question Name</label>'+
                                        '<input type="text" name="survey_qs_'+i+'" class="form-control" id="survey_qs_'+i+'" placeholder="Enter Survey Name" autocomplete="off" required>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<label>Question Type</label>'+
                                        '<select class="form-control" name="survey_qs_type_'+i+'" id="survey_qs_type_'+i+'" required >'+
                                            '<option value="">--Select Type--</option>'+
                                            '<option value="dropdown">Dropdown</option>'+
                                            '<option value="radio">Radio</option>'+
                                            '<option value="checkbox">Checkbox</option>'+
                                            '<option value="text">Text</option>'+
                                            '<option value="datepicker">Datepicker</option>'+
                                            '<option value="time">Time</option>'+
                                        '</select>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<label for="exampleInputEmail1">Main Question ID</label>'+
                                        '<input type="text" name="main_ques_id_'+i+'" id="main_ques_id_'+i+'" class="form-control" placeholder="" autocomplete="off">'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<label for="exampleInputEmail1">Order ID</label>'+
                                        '<input type="text" name="order_id_'+i+'" class="form-control" id="order_id_'+i+'" placeholder="Enter Survey Name" autocomplete="off" required>'+
                                    '</div>'+
                                    '<div class="get-option">'+
                                        '<div class="form-group">'+
                                            '<label for="exampleInputEmail1" class="option-text">Option 1</label>'+
                                            '<input type="text" name="option_'+i+'[]" id="option_'+i+'" class="form-control" placeholder="Enter Survey Name" autocomplete="off">'+
                                        '</div>'+
                                        '<div class="form-group">'+
                                            '<label for="exampleInputEmail1" class="meta-text">Option Code 1</label>'+
                                            '<input type="text" name="meta_'+i+'[]" id="meta_'+i+'" class="form-control" placeholder="Enter Survey Name" autocomplete="off">'+
                                        '</div>'+
                                        '<div class="form-group">'+
                                            '<label for="exampleInputEmail1" class="option-order">Option Order 1</label>'+
                                            '<input type="text" name="option_order_'+i+'[]" id="option_order_'+i+'" class="form-control to-remove" placeholder="Enter Survey Name" autocomplete="off">'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="form-group" style="text-align: center">'+
                                        '<input type="button" id="optAdd" class="btn btn-primary" onclick="add_option(this)" value="Add More Option">'+
                                    '</div>'+
                                    '<input type="hidden" name="count_qs[]" value="'+i+'" id="count_qs_'+i+'" />'+
                                    '<div class="form-group" style="text-align: center">'+
                                        '<button id="btnRemove_'+i+'" class="btn btn-danger remove_button" value="'+i+'">Remove</button>'+
                                    '</div>'+
                                '</div>');
            i++;
        });

        $("body").on("click", ".remove_button", function (e) {
            i--;
            var str = $(this).val();
            var track = str;


            $('#remove_'+str).remove();

            ++str;

            var myDiv = $("#remove_"+str);


            while (myDiv.length) {
                $("#remove_"+str).attr('id', 'remove_'+track);

                $("#count_qs_"+str).val(track);
                $("#count_qs_"+str).attr('id','count_qs_'+track);

                $("#survey_qs_"+str).attr('name', 'survey_qs_'+track);
                $("#survey_qs_"+str).attr('id', 'survey_qs_'+track);

                $("#survey_qs_type_"+str).attr('name', 'survey_qs_type_'+track);
                $("#survey_qs_type_"+str).attr('id', 'survey_qs_type_'+track);


                $("#main_ques_id_"+str).attr('name', 'main_ques_id_'+track);
                $("#main_ques_id_"+str).attr('id', 'main_ques_id_'+track);



                $("#order_id_"+str).attr('name', 'order_id_'+track);
                $("#order_id_"+str).attr('id', 'order_id_'+track);

                $("#option_"+str).attr('name', 'option_'+track);
                $("#option_"+str).attr('id', 'option_'+track);

                $("#meta_"+str).attr('name', 'meta_'+track);
                $("#meta_"+str).attr('id', 'meta_'+track);

                $('#option_order_'+str).attr('name', 'option_order_'+track);
                $('#option_order_'+str).attr('id', 'option_order_'+track);

                $('#btnRemove_'+str).val(track);
                $('#btnRemove_'+str).attr('id', 'btnRemove_'+track);

                ++str;
                ++track;
                myDiv = $("#remove_"+str);
            }

        });
    });
</script>

<?php $this->load->view('./admin/footer'); ?>
