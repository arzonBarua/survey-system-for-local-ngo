<?php 
$data['title'] = 'Add New Survey';
$this->load->view('./admin/header', $data); 


?>
<style>

    .form-group {
        margin-bottom: 40px;
    }
    legend{
        text-align: center;
    }
</style>
<!--Body Portin-->
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">

            <?php
            if(validation_errors() || isset($error)){
                echo "<div class='alert alert-danger'>";
                echo validation_errors();
                echo isset($error) ? $error : "";
                echo "</div>";
            }

            echo form_open_multipart('admin/survey/survey_qs_edit/'.$id.'/'.$survey_info_id); ?>


            <div class="box-header" >
                <h3 class="box-title" style="display:block;text-align: center;"><?php echo $survey_info[0]->survey_name; ?></h3>
                <input type="hidden" value="<?php echo $survey_info[0]->id; ?>" name="survey_info_id">
            </div>
            <div class="box-body">


            <!--Common Fields-->
            <fieldset>
            <legend style="font-size: 14px">উত্তরদাতা সম্পর্কিত তথ্য:</legend>

            <div class="form-group">
                    <label>নাম</label>
                    <input type="text" name="ans_name" class="form-control margin-left" placeholder="" autocomplete="off" value="<?php echo $ans_name ?>" required>
            </div>

            <div class="form-group">
                    <label>বয়স</label>
                    <input type="text" name="ans_age" class="form-control margin-left" placeholder="" autocomplete="off" value="<?php echo $ans_age ?>" required>
            </div>

             <div class="form-group">
                    <label style="margin-right:22px">উত্তরদাতার লিঙ্গ</label>
                    <input type="radio" class="minimal" name="ans_gender" value="1" <?php echo ($ans_gender=="1") ? 'checked' : '' ?> required/>
                    পুরুষ
                    <input type="radio" class="minimal" name="ans_gender" value="2" <?php echo ($ans_gender=="2") ? 'checked' : '' ?> required/>
                    মহিলা
            </div>
            <div>
                <input type="hidden" name="project_type" value="<?php echo $survey_info[0]->project_type ?>">
                <input type="hidden" name="form_code" value="<?php echo $form_code ?>">
            </div>

            <?php if($survey_info[0]->project_type != 'phulki'){ ?>

                    <div class="form-group">
                        <label>জেলা</label>
                        <select class="form-control union district call_ajax" name="district_id" id="upazila" required >
                            <option value="">--Select District--</option>
                            <?php foreach($district as $key=> $val): ?>
                                <option value="<?php echo $val->id; ?>" <?php echo ($val->id==$district_id) ? 'checked' : '' ?> ><?php echo $val->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="upazila_id">উপজেলা</label>
                        <select class="form-control ward upazila call_ajax" name="upozila_id" id="union" required>
                            <option value="<?php echo $val->upozila_id; ?>"><?php echo $val->upa_name; ?></option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="upazila_id">ইউনিয়ন</label>
                        <select class="form-control ward-name union call_ajax" name="union_id" id="ward" required>
                            <option value="<?php echo $val->union_id; ?>"> <?php echo $val->uni_name; ?></option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="ward">গ্রাম / Ward</label>
                        <select class="form-control" name="word_id" id="ward-name" required>
                             <option value="<?php echo $val->word_id; ?>"> <?php echo $val->wa_name; ?></option>
                        </select>
                    </div>

            <?php }else{ ?>

            <div class="form-group">
                    <label>গার্মেন্টস এর নাম</label>
                    <input type="text" name="name_garments" class="form-control margin-left" placeholder="" value="<?php echo $name_garments ?>" autocomplete="off" required>
            </div>

            <div class="form-group">
                    <label>কোন  বিভাগ কাজ করেন</label>
                    <input type="text" name="department_name" class="form-control margin-left" placeholder="" value="<?php echo $department_name ?>" autocomplete="off" required>
            </div>

            <div class="form-group">
                    <label>আইডি নাম্বার</label>
                    <input type="text" name="id_number" class="form-control margin-left" placeholder="" value="<?php echo $id_number ?>" autocomplete="off" required>
            </div>
            <div class="form-group">
                    <label>গার্মেন্টস এর ঠিকানা</label>
                    <input type="text" name="address_garments" class="form-control margin-left" placeholder="" value="<?php echo $address_garments ?>" autocomplete="off" required>
            </div>


            <?php    } ?>


             <div class="form-group">
                    <label>মোবাইল নাম্বার</label>
                    <input type="text" name="mobile_number" class="form-control margin-left" placeholder="" value="<?php echo $mobile_number ?>" autocomplete="off" required>
            </div>




            <div class="form-group">
                    <label>সাক্ষাৎকার গ্রহিতার নাম</label>
                    <select class="form-control margin-left" name="login_op_id" id="login_op_id" required>
                        <option value="">--Select Drop Down--</option>
                        <?php foreach($op_ids as $key=>$val): ?>
                            <option value="<?php echo $val->id ?>" <?php echo ($val->id==$login_op_id) ? 'selected' : '' ?> ><?php echo $val->username ?></option>
                        <?php endforeach; ?>    
                    </select>    
            </div>

            <?php  ?>




            </fieldset>


            <div class="form-group">
                    <label style="margin-right:22px">সহযোগী সংস্থার নাম</label>
                    <?php foreach($projects_id as $value): ?>
                        <input type="radio" class="minimal" name="projects_id" value="<?php echo $value->id ?>" <?php echo ($value->id==$project_id) ? 'checked' : '' ?> required/>
                        <?php echo $value->name ?>
                    <?php endforeach; ?>
                   <!--  <input type="radio" class="minimal" name="projects_id" value="2"/>
                    সুশীলন
                     <input type="radio" class="minimal" name="projects_id" value="3"/>
                    বাপ্সা
                    <input type="radio" class="minimal" name="projects_id" value="4"/>
                    ফুলকি -->
            </div>



            <legend style="font-size: 14px"> সাধারন তথ্য:</legend>
           


            <!--End Common Fields-->


            <?php 

            if(!empty($survey_question)){

            foreach($survey_question as $quesKey => $questVal ):
                 $style = "";
                 $class = "";


                 // if(!empty($questVal->main_ques_id)){
                 //     if(in_array($questVal->id,$qs_array)){
                 //        $style = "style='display:block'";
                 //     }else{
                 //        $style = "style='display:none'";
                 //     }   
                 //     $class = $questVal->main_ques_id;
                 // }

               

                 $question_result = $this->Post_model->custom_query("Select * from  survey_option where survey_qs_id='$questVal->id' order by option_order ASC");



                ?>

                <!--Text Box Format-->
                <?php if((string)$questVal->survey_qs_type == 'text'): ?>
                    <div class="form-group <?php echo $class ?>"  <?php echo $style ?> >
                        <label ><?php echo $this->Post_model->en2bnNumber($questVal->order_id) ?>. <?php echo $questVal->survey_qs; ?></label>
                        <input type="text" name="<?php echo $questVal->id ?>" class="form-control margin-left" placeholder="" value="<?php echo $review_array[$questVal->id.'_0'] ?>" autocomplete="off" <?php echo empty($style) ? "required" : '' ?> >
                    </div>
                <?php endif; ?>
                <!--Text Box Format-->


                <!--Drop Down-->
                <?php if((string)$questVal->survey_qs_type == 'dropdown'): ?>
                    <div class="form-group <?php echo $class ?>" <?php echo $style ?> >
                        <label><?php echo $this->Post_model->en2bnNumber($questVal->order_id) ?>. <?php echo $questVal->survey_qs; ?></label>
                        <select class="form-control margin-left" name="<?php echo $questVal->id ?>" id="upazila" <?php echo empty($style) ? "required" : '' ?>>
                            <option value="">--Select Drop Down--</option>
                            <?php foreach($question_result as $questionValue): ?>
                                <option value="<?php echo $questionValue->survey_qs_id.'_'.$questionValue->id  ?>" ><?php echo  $questionValue->option_value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>
                <!--Drop Down-->

                <!--Checkbox-->
                <?php if((string)$questVal->survey_qs_type == 'checkbox'): ?>
                    <div class="form-group <?php echo $class ?>" <?php echo $style ?> >
                        <label><?php echo $this->Post_model->en2bnNumber($questVal->order_id) ?>. <?php echo $questVal->survey_qs; ?></label>
                            <div class="form-group margin-left">
                                <?php foreach($question_result as $questionValue): ?>
                                    <div>
                                        <label>
                                            <input type="checkbox" name="<?php echo $questVal->id ?>[]" class="minimal <?php echo $questionValue->survey_qs_id ?>_checkbox" value="<?php echo $questionValue->survey_qs_id.'_'.$questionValue->id  ?>" onclick="show_qs_check('<?php echo $questionValue->survey_qs_id ?>')" <?php if(in_array($questionValue->survey_qs_id.'_'.$questionValue->id, $report_array)) echo "checked"; ?> >
                                            <?php echo  $questionValue->option_value ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                    </div>
                <?php endif; ?>
                <!--Check box-->

                <!--Radio Button-->
                <?php if((string)$questVal->survey_qs_type == 'radio'): ?>
                    <div class="form-group <?php echo $class ?>" <?php echo $style ?> >
                        <label for="ip_restriction"><?php echo $this->Post_model->en2bnNumber($questVal->order_id) ?>. <?php echo $questVal->survey_qs; ?></label>

                        <?php foreach($question_result as $questionValue): ?>
                            <div class="radio margin-left">
                                <label>
                                    <input type="radio" name="<?php echo $questVal->id ?>" value="<?php echo $questionValue->survey_qs_id.'_'.$questionValue->id  ?>" onclick="checkDiv('<?php echo $questVal->order_id."_".$questionValue->option_order ?>')" <?php echo empty($style) ? "required" : '' ?> <?php if(in_array($questionValue->survey_qs_id.'_'.$questionValue->id, $report_array)) echo "checked"; ?> >
                                    <?php echo  $questionValue->option_value ?>
                                </label>
                            </div>
                        <?php endforeach; ?>

                    </div>
                <?php endif; ?>
                <!--Radio Button-->

                <!--Datepicker Format-->
                <?php if((string)$questVal->survey_qs_type == 'datepicker'): ?>
                    <div class="form-group <?php echo $class ?>" <?php echo $style ?> >
                        <label><?php echo $this->Post_model->en2bnNumber($questVal->order_id) ?>. <?php echo $questVal->survey_qs; ?></label>
                        <input type="text" name="<?php echo $questVal->id ?>" class="form-control datepicker margin-left" id="" placeholder="" autocomplete="off" <?php echo empty($style) ? "required" : '' ?>>
                    </div>
                <?php endif; ?>
                <!--Text Box Format-->


                <!--Time Format-->
                <?php if((string)$questVal->survey_qs_type == 'time'): ?>
                    <div class="bootstrap-timepicker <?php echo $class ?>" <?php echo $style ?> >
                        <div class="form-group">
                            <label><?php echo $this->Post_model->en2bnNumber($questVal->order_id) ?>. <?php echo $questVal->survey_qs; ?></label>
                            <div class="input-group margin-left">
                                <input type="text" class="form-control timepicker" name="<?php echo $questVal->id ?>" placeholder="hr/min/AM or PM" <?php echo empty($style) ? "required" : '' ?>/>
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->
                    </div>
                <?php endif; ?>
                <!--Time Format-->

             <?php endforeach; ?>


                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div><!-- /.box-body -->
            <?php echo form_close(); 
            }else{
                echo "now question found";
            }
            ?>

        </div><!-- /.box -->
    </div>
</div>
<!--End Body Portion-->


<?php $this->load->view('./admin/footer-link'); ?>



<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>




<script type="text/javascript">
    function show_qs_check(className){
        var len = $("."+className+"_checkbox:checked").length;
        if(len>0)
            $("."+className+"_checkbox").removeAttr('required');
        else
            $("."+className+"_checkbox").attr('required','required');
    }

    function checkDiv(div){


        // if($("."+div).length){
        //     $("."+div).show();
            
        //     // $("."+div+' input:checkbox').each(function() {
        //     //     $(this).prop('required',true);
        //     // });


        //     // $("."+div+' input:radio').each(function() {
        //     //     $(this).prop('required',true);
        //     // });

        //     // $("."+div+' input:text').each(function() {
        //     //      $(this).prop('required',true);
        //     // });

        // }else{
        //     splitDiv = div.split("_");
        //     str = 1;
        //     var myDiv;

        //     for(i=1;i<=3;i++){
        //         myDiv = $('.'+splitDiv[0]+"_"+i);
        //         if(myDiv.length){
        //             $('.'+splitDiv[0]+"_"+i).hide();
        //             divId = '.'+splitDiv[0]+"_"+i;

        //             $(divId+' input:checked').each(function() {
        //                 $(this).removeAttr('checked');
        //             });

        //             $(divId+' input:checkbox').each(function() {
        //                 //$(this).removeAttr('required');
        //             });

        //             $(divId+' input:radio').each(function() {
        //                 $(this).removeAttr('checked');
        //                 //$(this).removeAttr('required');
        //             });

        //             $(divId+' input:text').each(function() {
        //                 $(this).val('');
        //                 //$(this).removeAttr('required');
        //             });

        //         }
        //     }
        // }
    }

    $(function () {
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });

//        $('.timepicker').val('');

        $(".datepicker").datepicker();

        $(".datepicker").on("change",function(e){
            $(this).datepicker('hide');
        });


        $(".call_ajax").on("change", function (e) {

            e.preventDefault();
            var id = $(this).val();
            var tableName = $(this).attr('id');
            var classNameArr = $(this).attr('class');
            var className = classNameArr.split(" ");


            $.ajax({
                url: 'get_ajax',
                data: {'id': id,'table' : tableName, 'field': className[2]},
                type: "post",
                dataType: 'text',
                success: function (data) {
                    $("#"+className[1]).html(data);
                }
            });
            return false;
        });





    });
</script>
<?php $this->load->view('./admin/footer'); ?>
