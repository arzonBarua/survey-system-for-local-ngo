<?php 
$data['title'] = 'View Questionnaire Type';
$this->load->view('./admin/header', $data); 
?>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Table Contact Category View</h3>
            </div>
            <!-- /.box-header -->
            <?php if ($result === false) { ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } else { ?>

                <?php if ($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>

                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped" style="width: 60%">
                        <thead>
                        <tr>
                            <th class="custom">SL</th>
                            <th>Survey Name</th>
                           <!--  <th>Project Name</th> -->
                           <!--  <th>Start Date</th>
                            <th>End Date</th> -->
                           <!--  <th>Status</th> -->
                            <th class="custom_last">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $sl = 1;
                        $status = array("0" => "<span style='color:red'>unpublish</span>", "1" => "<span style='color:green'>publish</span>");
                        foreach ($result as $value) {
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->survey_name; ?></td>
                                 <!-- <td><?php //echo $value->project_type; ?></td> -->
                               <!--  <td><?php //echo date('m/d/Y',strtotime($value->start_date)); ?></td>
                                <td><?php //echo date('m/d/Y',strtotime($value->end_date)); ?></td> -->
                               <!--  <td><?php //echo ($value->publish == 0) ? '<span style="color:red">Unpublish</span>' : '<span style="color:green">Publish</span>'; ?></td> -->
                                <td>
                                    <a href="survey_edit/<?php echo $value->id ?>" title="Edit"><i
                                            class="fa fa-fw fa-edit"></i></a> /
                                    <a href="survey_delete/<?php echo $value->id ?>"
                                       onclick="return confirm('Are you sure you wish to delete this ?')"
                                       title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <?php $sl++;
                        } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            <?php } ?>
        </div>
        <!-- /.box -->
    </div>
</div>

<?php $this->load->view('./admin/footer-link') ?>

<!-- <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script> -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"
        type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#example1").DataTable();
    });
</script>


<?php $this->load->view('./admin/footer'); ?>
