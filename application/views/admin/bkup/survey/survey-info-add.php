<?php 
$data['title'] = 'Add Questionnaire Type';
$this->load->view('./admin/header', $data); 
?>

<div class="row">
    <!--Body Portin-->
    <div class="col-md-6">
        <div class="box box-primary">

        <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }

        echo form_open_multipart('admin/survey/survey_add'); ?>
            <div class="box-header">
                <h3 class="box-title">Survey Information</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Survey Name</label>
                    <input type="text" name="survey_name" class="form-control" id="survey_name" placeholder="Enter Survey Name" autocomplete="off" required>
                </div>
               <!--  <div class="form-group">
                    <label>Project Type</label>
                    <select class="form-control" name="project_type" required >
                        <option value="">--Select Type--</option>
                        <?php foreach($projects_id as $key => $val): ?>
                            <option value="<?php echo $val->english_name ?>"><?php echo $val->english_name ?></option>
                        <?php endforeach; ?>    
                    </select>
                </div> -->


               
                <!-- <div class="form-group">
                    <label>Date range:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="date_range" class="form-control pull-right" id="reservation" required />
                    </div>
                </div> -->

                <!-- /.form group -->

               <!--  <div class="form-group">
                    <label>Select</label>
                    <select class="form-control" name="publish" required >
                        <option value="">--Select Type--</option>
                        <option value="1">Publish</option>
                        <option value="0">Unpublish</option>
                    </select>
                </div> -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div><!-- /.box-body -->
        <?php echo form_close(); ?>


        </div><!-- /.box -->
    </div>
    <!--End Body Portion-->
</div>


<?php $this->load->view('./admin/footer-link'); ?>



<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(function () {
            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    startDate: moment().subtract('days', 29),
                    endDate: moment()
                },
                function (start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false
            });
        });
    </script>
<?php $this->load->view('./admin/footer'); ?>
