<?php 
$data['title'] = 'Add New Survey';
$this->load->view('./admin/header', $data); 
?>

<div class="row">
    <!--Body Portin-->
    <div class="col-md-6">
        <div class="box box-primary">
            <?php if ($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
            <?php endif; ?>
            
            <?php
            echo form_open_multipart('admin/survey/form_show'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label>Select Questioner Form</label>
                    <select class="form-control" name="survey_info_id" required >
                        <option value="">--Select Type--</option>
                        <?php foreach($result as $key => $val): ?>
                                <option value="<?php echo $val->id ?>"><?php echo $val->survey_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div><!-- /.box-body -->
            <?php echo form_close(); ?>
        </div><!-- /.box -->
    </div>
    <!--End Body Portion-->
</div>


<?php $this->load->view('./admin/footer-link'); ?>



<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>

<?php $this->load->view('./admin/footer'); ?>
