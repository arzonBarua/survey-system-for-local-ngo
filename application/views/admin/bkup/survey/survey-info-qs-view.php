<?php 
$data['title'] = 'View Survey Question';
$this->load->view('./admin/header', $data); 
?>
<style>
    .modal-dialog{
        width: 50% !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Table of View Survey Question</h3>
            </div><!-- /.box-header -->



            <?php if($result === false){ ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php }else{ ?>

                <?php if($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>

                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="custom">SL</th>
                            <th>Survey Name</th>
                           <!--  <th>Date Range</th> -->
                            <th>Questionnaires</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $sl = 1;
                        foreach($result as $value){
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->survey_name; ?></td>
                               <!--  <td><?php //echo date('M j, Y',strtotime($value->start_date))." to ".date('M j, Y',strtotime($value->end_date));?></td> -->
                                <td>
                                    <div class="modal fade" id="<?php echo $value->id ?>" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">View Survey Question</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table id="rpt_table_<?php echo $value->id ?>" class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th style="width:81px">Order ID</th>
                                                            <th>Question</th>
                                                            <th>Type</th>
                                                            <th style="width: 251px;">Options</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $qs_query = $this->Post_model->getAllByFieldNameOpition('survey_qs','survey_info_id',$value->id,"order_id");
                                                        if(!empty($qs_query)) {
                                                            foreach ($qs_query as $val) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $val->order_id ?></td>
                                                                    <td><?php echo $val->survey_qs ?></td>
                                                                    <td><?php echo $val->survey_qs_type ?></td>

                                                                    <td>
                                                                        <table>
                                                                            <?php
                                                                            $option_query = $this->Post_model->getAllByFieldNameOpition('survey_option', 'survey_qs_id', $val->id,"option_order");
                                                                            if(!empty($option_query)) { ?>
                                                                            <?php foreach ($option_query as $valOption) { ?>
                                                                                    <tr>
                                                                                        <td><?php echo (!empty($valOption->option_value)) ? $valOption->option_order.'. '.$valOption->option_value : "" ?></td>
                                                                                    </tr>
                                                                                <?php }
                                                                            } ?>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            <?php }
                                                        }?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" title="view" data-toggle="modal" data-target="#<?php echo $value->id ?>" ><i class="fa fa-fw fa-eye"></i></a>
                                </td>
                            </tr>
                            <?php $sl++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            <?php } ?>
        </div><!-- /.box -->
    </div>
</div>

<?php $this->load->view('./admin/footer-link') ?>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#example1").dataTable();
    });
</script>


<?php $this->load->view('./admin/footer'); ?>
