<?php 
$data['title'] = 'Add User';
$this->load->view('./admin/header', $data); 
?>
<style type="text/css">
    /*.projects{ display: none; }*/
</style>
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">User Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/user_admin/user_add'); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="full_name" class="form-control" id="Name" placeholder="Enter Name" autocomplete="off" value="<?php echo set_value('full_name'); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Department</label>
                            <input type="text" name="department" class="form-control" id="Department" placeholder="Enter Department" value="<?php echo set_value('department'); ?>" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Designation</label>
                            <input type="text" name="designation" class="form-control" id="Designation" placeholder="Enter Designation" value="<?php echo set_value('designation'); ?>" autocomplete="off">
                        </div>
                          <div class="form-group">
                            <label>Organization</label>
                            <select name="project_id" class="form-control projects" required>
                                <option value="">--Select Organization--</option>
                                <?php foreach($projects as $project): ?>
                                <option value="<?php echo $project->id ?>" <?php echo (set_value('project_id')==$project->id) ? "selected" : "" ?> ><?php echo $project->name;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>   

                        <div class="form-group">
                            <label>User Type</label>
                            <select name="user_type" class="form-control user_type" required>
                                <option value="">--Select Type--</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo set_value('email'); ?>" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password</label>
                            <input type="password" name="password" class="form-control" id="Password" placeholder="Password" required>
                            <p>
                                [Password must have at least one alphabetical character, one numeric character, one special character and length not less than eight characters]
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="passconf">Retype Password</label>
                            <input type="password" name="passconf" class="form-control" id="RetypePassword" placeholder="Password" required>

                        </div>

                        <div class="form-group">
                            <label for="ip_restriction">IP Restriction</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="ip_restriction" id="ip_restriction" class="ip_restriction" value="1">
                                    Yes
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="ip_restriction" id="ip_restriction" class="ip_restriction" value="0">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="form-group" id="ip_address">
                            <label for="ip_address">IP Address or CIDR Mask:</label>
                            <input type="text" name="ip_address" class="form-control" id="ip_address_val" placeholder="[Like 111.12.42.5 or 111.12.42.0/24]"  autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="userfile">File input</label>
                            <input type="file" name="userfile" id="userfile">
                            <p class="help-block">Example block-level help text here.</p>
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->







        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>

<script>
    $(function(){
       $('.ip_restriction').change(function(){
            if($(this).is(":checked")){
                var val = $(this).val();
                if(val==1){
                    $('#ip_address').show();
                    $('#ip_address_val').attr('required','required');
                }else{
                    $('#ip_address').hide();
                    $('#ip_address_val').removeAttr('required');
                    $('#ip_address_val').val("");
                }
            }
       });

       $(".projects").on("change",function(e){
            var projects_type = $(this).val();
            e.preventDefault();

            $.ajax({
                    url: "<?php echo base_url()."admin/ajax/" ?>get_project_type",
                    data: {'projects_type': projects_type},
                    type: "post",
                    dataType: 'text',
                    success: function (data) {
                        $(".user_type").html(data);
                    }
                });

            //alert(projects_type);
       });
       

       //For reference
       /* 
            $(".call_ajax").on("change", function (e) {

                e.preventDefault();
                var id = $(this).val();
                var tableName = $(this).attr('id');
                var classNameArr = $(this).attr('class');
                var className = classNameArr.split(" ");


                $.ajax({
                    url: "<?php echo base_url()."admin/meeting/" ?>get_ajax",
                    data: {'id': id,'table' : tableName, 'field': className[2]},
                    type: "post",
                    dataType: 'text',
                    success: function (data) {
                        $("#"+className[1]).html(data);
                    }
                });
                return false;
            });
        */


       // $(".user_type").on("change", function(){
       //      var user_type = $(this).val();
       //      if(user_type == '2' || user_type == '3' || user_type == '4' || user_type == '5'){
       //          $(".projects").css({ "display" : "block" });
       //      } else {
       //          $(".projects").css({ "display" : "none" });
       //      }
       // });
    });
</script>

<?php $this->load->view('./admin/footer'); ?>