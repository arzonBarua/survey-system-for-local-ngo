<?php 
$data['title'] = 'Edit App User';
$this->load->view('./admin/header', $data); 
?>

    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">App User Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/user_admin/app_user_edit/'.$result[0]->id); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="full_name" class="form-control" id="Name" placeholder="Enter Name" autocomplete="off" value="<?php echo (set_value('full_name') ? set_value('full_name') : $result[0]->full_name); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Designation</label>
                            <input type="text" name="designation" class="form-control" id="Designation" placeholder="Enter Designation" value="<?php echo (set_value('designation') ? set_value('designation') : $result[0]->designation); ?>" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Projects</label>
                            <select name="project_id" class="form-control projects" required>
                                <option>--Select Project--</option>
                                <?php foreach($projects as $project): ?>
                                <option value="<?php echo $project->id ?>" <?php echo ($result[0]->project_id==$project->id) ? 'selected' : '' ?> ><?php echo $project->name;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>   

                        <div class="form-group">
                            <label>User Type</label>
                            <select name="login_id" class="form-control user_type" required>
                                <?php foreach ($user_type_list as $key=>$val): ?>
                                    <option value="<?php echo $val->id ?>" <?php echo ($val->id==$result[0]->login_id) ? 'selected' : '' ?> ><?php echo $val->full_name ?></option>
                                <?php endforeach; ?>    
                            </select>
                        </div>
						
						<div class="form-group">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" name="username" class="form-control" id="username" placeholder="Enter User Name" autocomplete="off" value="<?php echo (set_value('username') ? set_value('username') : $result[0]->username); ?>" required />
                        </div>
                        <div class="form-group">
                            <label for="Password">Password</label>
                            <input type="password" name="password" class="form-control" id="Password" placeholder="Password" />
                            <p>
                                [Password must have at least one alphabetical character, one numeric character, one special character and length not less than eight characters]
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="passconf">Retype Password</label>
                            <input type="password" name="passconf" class="form-control" id="RetypePassword" placeholder="Password" />
                        </div>

                        <div class="form-group">
                            <label for="district">District</label>                              
                            <select class="form-control union district call_ajax" name="district" id="upazila" required="">
                                <option value=""> --SELECT-- </option>
                                <?php                                 
	                            foreach($district as $val){
                                $district_id = set_value('district') ? set_value('district') : $result[0]->district_id;
                                ?>
                            	<option <?php echo ($district_id==$val->id) ? "selected='selected'" : "" ?> value="<?php echo $val->id;?>"><?php echo $val->name;?></option>
	                            <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="upazilla">Upazilla</label>                              
                            <select class="form-control ward upazila" name="upazilla" id="union"  required="">
                                <option value=""> --SELECT-- </option>
                                <?php                                 
                                $upazilla_qry = $this->db->query("SELECT * FROM `upazila` WHERE `district_id` = '".$result[0]->district_id."' ORDER BY `name` ASC");
                                $upazilla_data = $upazilla_qry->result();
                                foreach ($upazilla_data as $key => $value) {
                                $upazilla_id = set_value('upazilla') ? set_value('upazilla') : $result[0]->upazila_id;
                                ?>
                                <option <?php echo ($upazilla_id==$value->id) ? "selected='selected'" : "" ?> value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <div class="radio">
                                <label>
                                    <?php $chk = ($result[0]->status == '0') ? 'checked=""' : '';?>
                                    <input type="radio" <?php echo $chk;?> name="status" class="status" value="1">
                                    Active
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <?php $chk = ($result[0]->status == '0') ? 'checked=""' : '';?>
                                    <input type="radio" <?php echo $chk;?> name="status" class="status" value="0">
                                    Inactive
                                </label>
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer'); 
?>
<script type="text/javascript">
  $(function(){
    /*
    $(".call_ajax").on("change", function (e) {
            e.preventDefault();
            var id = $(this).val();
            var tableName = 'upazila';
            alert(id);
      
            $.ajax({
                    url: "<?php echo base_url()."admin/meeting/" ?>get_ajax",
                    data: {'id': id, 'table' : tableName, 'field' : 'district_id' },
                    type: "post",
                    dataType: 'text',
                    success: function (data) {
          	             console.log(data);
                        $("#upazilla").html(data);
                    }
      });

      return false;
    });
    */
    $(".projects").on("change",function(e){
            var projects_type = $(this).val();
            e.preventDefault();
            
            $.ajax({
                    url: "<?php echo base_url()."admin/ajax/" ?>show_apo_list",
                    data: {'projects_type': projects_type},
                    type: "post",
                    dataType: 'text',
                    success: function (data) {
                        $(".user_type").html(data);
                    }
                });
       });

    $(".call_ajax").on("change", function (e) {

            e.preventDefault();
            var id = $(this).val();
            var tableName = $(this).attr('id');
            var classNameArr = $(this).attr('class');
            var className = classNameArr.split(" ");


            $.ajax({
                url: "<?php echo base_url()."admin/ajax/" ?>get_ajax",
                data: {'id': id,'table' : tableName, 'field': className[2]},
                type: "post",
                dataType: 'text',
                success: function (data) {
                    $("#"+className[1]).html(data);
                }
            });
            return false;
        });
  });
</script>