<?php 
$data['title'] = 'User Profie';
$this->load->view('./admin/header', $data); 
?>

<style type="text/css">    
    .form-group { overflow: auto; margin-bottom: 10px; }
    .form-group label { padding: 7px 15px; margin: 0px; }
</style>
    <div class="row">
        <!-- left column -->
        <?php
        echo form_open_multipart('admin/home/user_profile'); ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="text-center">
                <img src="<?php echo ($this->session->userdata('picture')) ? base_url().'assets/upload/profile/thumb/'.$this->Common_operation->show_thumb($this->session->userdata('picture')) : base_url().'assets/dist/img/avatar.png';  ?>" class="avatar img-circle img-thumbnail" alt="avatar">
                <h6>Upload a different photo...</h6>
                <input name="userfile" type="file" class="text-center center-block well well-sm">
            </div>
        </div>
        <!-- edit form column -->
        <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
            <h3>Personal info</h3>
            <?php
            if(validation_errors() || isset($error)){
                echo "<div class='alert alert-danger'>";
                echo validation_errors();
                echo isset($error) ? $error : "";
                echo "</div>";
            }
            ?>
            <?php if($this->session->flashdata('success_message')): ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <label class="col-lg-3 control-label">Name:</label>
                <div class="col-lg-8">
                    <input name="full_name" class="form-control" value="<?php echo (set_value('full_name') ? set_value('full_name') : $result[0]->full_name); ?>" type="text">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Department:</label>
                <div class="col-lg-8">
                    <input name="department" class="form-control" value="<?php echo (set_value('department') ? set_value('department') : $result[0]->department); ?>" type="text">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Designation:</label>
                <div class="col-lg-8">
                    <input name="designation" class="form-control" value="<?php echo (set_value('designation') ? set_value('designation') : $result[0]->designation); ?>" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label">Email:</label>
                <div class="col-lg-8">
                    <input name="email" class="form-control" value="<?php echo (set_value('email') ? set_value('email') : $result[0]->email); ?>" type="email">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Password:</label>
                <div class="col-md-8">
                    <input name="password" class="form-control" value="" type="password">
                    <p>
                        [Password must have at least one alphabetical character, one numeric character, one special character and length not less than eight characters]
                    </p>
                </div>

            </div>
            <div class="form-group ">
                <label class="col-md-3 control-label">Confirm password:</label>
                <div class="col-md-8">
                    <input name="passconf"  class="form-control margin-bottom-10" value="" type="password">
                </div>
            </div>


            <div class="form-group submit-proflie">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8">
                    <input class="btn btn-primary" value="Save Changes" type="submit">
                </div>
            </div>

        </div>
        <?php echo form_close(); ?>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>

    <script>
        $(function(){
            $('.ip_restriction').change(function(){
                if($(this).is(":checked")){
                    var val = $(this).val();
                    if(val==1){
                        $('#ip_address').show();
                        $('#ip_address_val').attr('required','required');
                    }else{
                        $('#ip_address').hide();
                        $('#ip_address_val').removeAttr('required');
                        $('#ip_address_val').val("");
                    }
                }
            });
        });
    </script>

<?php $this->load->view('./admin/footer'); ?>