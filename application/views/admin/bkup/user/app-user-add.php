<?php 
$data['title'] = 'Add App User';
$this->load->view('./admin/header', $data); 
?>

    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">App User Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/user_admin/add_app_user'); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="full_name" class="form-control" id="Name" placeholder="Enter Name" autocomplete="off" value="<?php echo set_value('full_name'); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Designation</label>
                            <input type="text" name="designation" class="form-control" id="Designation" placeholder="Enter Designation" value="<?php echo set_value('designation'); ?>" autocomplete="off">
                        </div>
                      
                        <div class="form-group">
                            <label>Projects</label>
                            <select name="project_id" class="form-control projects" required>
                                <option>--Select Project--</option>
                                <?php foreach($projects as $project): ?>
                                <option value="<?php echo $project->id ?>" <?php echo (set_value('project_id')==$project->id) ? "selected" : "" ?> ><?php echo $project->name;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>User Type</label>
                            <select name="login_id" class="form-control user_type" required>
                                <option>--Select Type--</option>
                            </select>
                        </div>

                        
						<div class="form-group">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" name="username" class="form-control" id="username" placeholder="Enter User Name" autocomplete="off" value="<?php echo set_value('username'); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password</label>
                            <input type="password" name="password" class="form-control" id="Password" placeholder="Password" required>
                            <p>
                                [Password must have at least one alphabetical character, one numeric character, one special character and length not less than eight characters]
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="passconf">Retype Password</label>
                            <input type="password" name="passconf" class="form-control" id="RetypePassword" placeholder="Password" required>
                        </div>

                        <div class="form-group">
                            <label for="district">District</label>                              
                            <select class="form-control call_ajax" name="district" id="district" required="">
                                <option value=""> --SELECT-- </option>
                                <?php                                 
	                            foreach($district as $val){
	                            	echo '<option value="'.$val->id.'">'.$val->name.'</option>';
	                            }
	                            ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="upazilla">Upazilla</label>                              
                            <select class="form-control" name="upazilla" id="upazilla" required="">
                                <option value=""> --SELECT-- </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" checked="" name="status" class="status" value="1">
                                    Active
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="status" class="status" value="0">
                                    Inactive
                                </label>
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer'); 
?>
<script type="text/javascript">
  $(function(){
    //  var option_value = <?php echo ($this->session->userdata('project_id')) ? $this->session->userdata('project_id') : null ;  ?>;

    // //alert(option_value);


    // if(option_value != null)
    //     $('.projects>option:eq('+option_value+')').attr('selected', true).siblings().attr('disabled',true);


    $(".call_ajax").on("change", function (e) {
      e.preventDefault();
      var id = $(this).val();
      var tableName = 'upazila';
      
      $.ajax({
          url: "<?php echo base_url()."admin/ajax/" ?>label_ajax",
          data: {'id': id, 'table' : tableName, 'field' : 'district_id' },
          type: "post",
          dataType: 'text',
          success: function (data) {
          	console.log(data);
            $("#upazilla").html(data);
          }
      });
      return false;
    });


    $(".projects").on("change",function(e){
            var projects_type = $(this).val();
            e.preventDefault();
            
            $.ajax({
                    url: "<?php echo base_url()."admin/ajax/" ?>show_apo_list",
                    data: {'projects_type': projects_type},
                    type: "post",
                    dataType: 'text',
                    success: function (data) {
                        $(".user_type").html(data);
                    }
                });
       });
  });
</script>