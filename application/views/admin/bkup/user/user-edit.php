<?php 
$data['title'] = 'Edit User';
$this->load->view('./admin/header', $data); 
$arrayProjectName = $this->Common_operation->get_user_type();
?>
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">User Information</h3>
                </div><!-- /.box-header -->

                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>
                <!-- form start -->
                    <?php
                    if(validation_errors() || isset($error)){
                        echo "<div class='alert alert-danger'>";
                        echo validation_errors();
                        echo isset($error) ? $error : "";
                        echo "</div>";
                    }
                    ?>
                    <?php
                //$array = array('enctype'=>'multipart/form-data');
                    echo form_open_multipart('admin/user_admin/user_edit/'.$result[0]->id); ?>
                        <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="full_name" class="form-control" id="Name" placeholder="Enter Name" autocomplete="off" value="<?php echo (set_value('full_name') ? set_value('full_name') : $result[0]->full_name); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Department</label>
                            <input type="text" name="department" class="form-control" id="Department" placeholder="Enter Department" value="<?php echo (set_value('department') ? set_value('department') : $result[0]->department); ?>" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Designation</label>
                            <input type="text" name="designation" class="form-control" id="Designation" placeholder="Enter Designation" value="<?php echo (set_value('designation') ? set_value('designation') : $result[0]->designation); ?>" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Organization</label>
                            <select name="project_id" class="form-control projects" required>
                                <option value="">--Select Organization--</option>
                                <?php foreach($projects as $project): 
                                $project_id = set_value('project_id') ? set_value('project_id') : $result[0]->project_id;
                                ?>
                                <option value="<?php echo $project->id ?>" <?php echo ($project_id==$project->id) ? "selected" : "" ?> ><?php echo $project->name;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>User Type</label>
                            <select name="user_type" class="form-control user_type" required>
                                <option value="">--Select Type--</option>
                                <?php foreach($arrayProjectName[$result[0]->project_id] as $key => $val):
                                     $userType = set_value('user_type') ? set_value('user_type') : $result[0]->user_type; ?>

                                    <option value="<?php echo $key ?>" <?php echo ($userType==$key) ? "selected" : "" ?> ><?php echo $val ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo (set_value('email') ? set_value('email') : $result[0]->email); ?>" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password</label>
                            <input type="password" name="password" class="form-control" id="Password" placeholder="Password">
                            <p>
                                [Password must have at least one alphabetical character, one numeric character, one special character and length not less than eight characters]
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="passconf">Retype Password</label>
                            <input type="password" name="passconf" class="form-control" id="RetypePassword" placeholder="Password">
                        </div>

                        <div class="form-group">
                            <label for="ip_restriction">IP Restriction</label>
                            <div class="radio">
                                <label>
                                    <?php  $ip_restriction = (set_value('ip_restriction') ? set_value('ip_restriction') : $result[0]->ip_restriction); ?>
                                    <input type="radio" name="ip_restriction" id="ip_restriction" class="ip_restriction" <?php echo ($ip_restriction==1) ? 'checked' : ''; ?> value="1">
                                    Yes
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="ip_restriction" id="ip_restriction" class="ip_restriction" <?php echo ($ip_restriction==0) ? 'checked' : ''; ?> value="0">
                                    No
                                </label>
                            </div>
                        </div>

                            <div class="form-group" id="ip_address" style="display: <?php echo ($ip_restriction==1) ? 'block' : 'none'; ?>">
                                <label for="ip_address_val">IP Address or CIDR Mask:</label>
                                <input type="text" name="ip_address" class="form-control" id="ip_address_val" placeholder="[Like 111.12.42.5 or 111.12.42.0/24]" value="<?php echo (set_value('ip_address') ? set_value('ip_address') : $result[0]->ip_address); ?>"  autocomplete="off">
                            </div>


                        <div class="form-group">
                            <label for="userfile">File input</label>
                            <input type="file" name="userfile" id="userfile">
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    <?php echo form_close(); ?>
                </div><!-- /.box -->
            <?php } ?>
        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>

    <script>
        $(function(){

            $('.ip_restriction').change(function(){
                if($(this).is(":checked")){
                    var val = $(this).val();
                    if(val==1){
                        $('#ip_address').show();
                        $('#ip_address_val').attr('required','required');
                    }else{
                        $('#ip_address').hide();
                        $('#ip_address_val').removeAttr('required');
                        $('#ip_address_val').val("");
                    }
                }
            });


            $(".projects").on("change",function(e){
                var projects_type = $(this).val();
                e.preventDefault();

                $.ajax({
                        url: "<?php echo base_url()."admin/ajax/" ?>get_project_type",
                        data: {'projects_type': projects_type},
                        type: "post",
                        dataType: 'text',
                        success: function (data) {
                            $(".user_type").html(data);
                        }
                    });

            //alert(projects_type);
            });

            
        });
    </script>

<?php $this->load->view('./admin/footer'); ?>