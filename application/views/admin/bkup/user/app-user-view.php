<?php 
$data['title'] = 'View App User';
$this->load->view('./admin/header', $data); 

$arrayProjectName = $this->Common_operation->get_user_type();
?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Table App User View</h3>
                </div><!-- /.box-header -->

                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>

                    <?php if($this->session->flashdata('success_message')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="custom">SL</th>
                                <th>Organization Name</th>
                                <th>Full Name</th>
                                <th>Designation</th>
                                <th>User Name</th>
                                <th>District Name</th>
                                <th>Upazilla Name</th>
                                <th>Assigned Under</th>
                                <th>Status</th>
                                <th class="custom_last">Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                            <tbody>
                            <?php $sl = 1;
                            $status = array('Inactive', 'Active');
                            foreach($result as $value){ ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value->project_name ?></td>
                                    <td><?php echo $value->full_name; ?></td>
                                    <td><?php echo $value->designation; ?></td>
                                    <td><?php echo $value->username;?></td>
                                    <td><?php echo $value->district_name;?></td>
                                    <td><?php echo $value->upazila_name;?></td>
                                    <td><?php echo $value->ass_full_name."<br>User Type: ".$arrayProjectName[$value->project_id][$value->ass_user_type]; ?></td>
                                    <td><?php echo isset($status[$value->status]) ? $status[$value->status] : '' ;?></td>
                                    <td>                                        
                                        <a href="app_user_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                        <a href="app_user_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $sl++; } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                <?php } ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php $this->load->view('./admin/footer-link') ?>

    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable({
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            initComplete: function () {
                this.api().columns([1]).every(function () {
                    var column = this;
                    var select = $('<select class="form-control"><option value="">-Select-</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });
        });
    </script>


<?php $this->load->view('./admin/footer'); ?>