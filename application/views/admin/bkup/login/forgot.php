<?php $this->load->view('admin/login/header'); ?>

<div class="login-box">
    <div class="login-logo">
        <a href="../../index2.html"><b>CAG Admin</b> Panel</a>
    </div><!-- /.login-logo -->

    <div class="login-box-body">
        <p class="login-box-msg">Please Sign In</p>
        <?php echo form_open('admin/user/form_process'); ?>
        <div class="row">
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
        </div>
        <?php echo form_close(); ?>
        <a href="#">I forgot my password</a><br>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<?php $this->load->view('admin/login/footer'); ?>
