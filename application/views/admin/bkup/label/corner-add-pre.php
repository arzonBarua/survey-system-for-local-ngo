<?php 
$data['title'] = 'Add MR,FP & PAC Corner';
$this->load->view('./admin/header', $data); 
?>

    <div class="row">
        <div class="col-md-10">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">MR,FP &amp; PAC Corner Add</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }                

                echo form_open_multipart('admin/label/corner_add_pre'); ?>
                    <div class="box-body">                      
                        <table class="table table-bordered" style="width: 60%">
                          <tbody>
                          <tr>
                            <th width="30%">Project</th>
                            <td> 
                              <select class="selectBox form-control project_id" name="project_id">
                              <option value=""> Select Project </option>
                              <?php 
                                foreach($projects as $val){ ?>
                                    <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                                  <?php }
                                ?>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <th width="30%">Year</th>
                            <td>
                              <select class="form-control" name="year" required="">
                              <option value=""> --SELECT-- </option>
                              <?php   
                              $arr = range(2016, date("Y"));                           
                              foreach($arr as $val){
                                echo '<option value="'.$val.'">'.$val.'</option>';
                              }
                              ?>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <th width="30%">Quarter</th>
                            <td>
                              <select class="form-control" name="quarter" required="">
                              <option value=""> --SELECT-- </option>
                              <?php   
                              $arr = range(1,4);                           
                              foreach($arr as $val){
                                echo '<option value="'.$val.'">'.$val.'</option>';
                              }
                              ?>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <th width="30%">Division</th>
                            <td>
                              <select class="form-control call_ajax" name="division" required="">
                              <option value=""> --SELECT-- </option>
                              <?php                                 
                              foreach($result as $val){
                                echo '<option value="'.$val->id.'">'.$val->division_name.'</option>';
                              }
                              ?>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <th width="30%">Distict</th>
                            <td>
                              <select class="form-control" name="district" id="district" required="">
                                <option value=""> --SELECT-- </option>
                              </select>
                            </td>
                          </tr>
                          </tbody>
                        </table>                        
                    </div> 
                    <div class="box-footer">                    
                      <input class="btn bg-primary" type="submit" name="submit" value="Submit"/>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->
        </div>
    </div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>
<script type="text/javascript">
  $(function(){
    var option_value = <?php echo ($this->session->userdata('project_id')) ? $this->session->userdata('project_id') : null ;  ?>;

    //alert(option_value);


    if(option_value != null)
        $('select>option:eq('+option_value+')').attr('selected', true).siblings().attr('disabled',true);

      
    $(".call_ajax").on("change", function (e) {
      e.preventDefault();
      var id = $(this).val();
      var tableName = 'district';
      
      $.ajax({
          url: '<?php echo base_url()."admin/ajax/" ?>label_ajax',
          data: {'id': id, 'table' : tableName, 'field' : 'div_id' },
          type: "post",
          dataType: 'text',
          success: function (data) {
            console.log(data);
            $("#district").html(data);
          }
      });
      return false;
    });
  });
</script>