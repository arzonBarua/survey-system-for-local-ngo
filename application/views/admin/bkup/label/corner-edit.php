<?php 
$data['title'] = 'Edit MR,FP & PAC Corner';
$this->load->view('./admin/header', $data); 
?>

    <div class="row">
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">MR,FP &amp; PAC Corner Edit for <b><?php echo $this->Common_operation->addOrdinalNumberSuffix($_SESSION['pr_corner']['quarter']) ."</b> quarter of <b>". $_SESSION['pr_corner']['year'];?></b></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                echo form_open_multipart('admin/label/corner_edit'); ?>
                    <div class="box-body">                      
                        <table class="table table-bordered">
		                  <tbody>		                  
		                  <tr bgcolor="#ddd">
		                    <th colspan="3">MR Services</th>
		                  </tr>
		                  <tr>
		                    <th>MR Reject</th>
		                    <td>
		                    	<!-- <input type="hidden" name="quarter" value="<?php echo $_SESSION['pr_corner']['quarter'];?>">
		                    	<input type="hidden" name="year" value="<?php echo $_SESSION['pr_corner']['year'];?>">
		                    	<input type="hidden" name="year" value="<?php echo $_SESSION['pr_corner']['year'];?>"> -->		                    	
		                      	<input type="text" name="mr_reject" class="form-control" value="<?php echo $result[0]->mr_reject;?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>MR Successfull</th>
		                    <td>
		                      <input type="text" name="mr_success" class="form-control" value="<?php echo $result[0]->mr_success;?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>MRM</th>
		                    <td>
		                      <input type="text" name="mrm" class="form-control" value="<?php echo $result[0]->mrm;?>">
		                    </td>
		                  </tr>
		                  <tr bgcolor="#ddd">
		                    <th colspan="2">Other Services</th>
		                  </tr>

		                  <tr bgcolor="#eee">
		                    <th colspan="2">1. Short Term Method</th>
		                  </tr>
		                  <tr>
		                    <th>Condom (Person)</th>
		                    <td>
		                      <input type="text" name="condom_person" class="form-control" value="<?php echo $result[0]->condom_person;?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Pill</th>
		                    <td>
		                      <input type="text" name="pill" class="form-control" value="<?php echo $result[0]->pill;?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Injectable</th>
		                    <td>
		                      <input type="text" name="injectable" class="form-control" value="<?php echo $result[0]->injectable;?>">
		                    </td>
		                  </tr>

		                  <tr bgcolor="#eee">
		                    <th colspan="2">2. Long Term Method</th>
		                  </tr>
		                  <tr>
		                    <th>IUD</th>
		                    <td>
		                      <input type="text" name="iud" class="form-control" value="<?php echo $result[0]->iud;?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Implant</th>
		                    <td>
		                      <input type="text" name="implant" class="form-control" value="<?php echo $result[0]->implant;?>">
		                    </td>
		                  </tr>

		                  <tr bgcolor="#eee">
		                    <th colspan="2">3. Permanent Method</th>
		                  </tr>
		                  <tr>
		                    <th>Ligation</th>
		                    <td>
		                      <input type="text" name="liagation" class="form-control" value="<?php echo $result[0]->liagation;?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>NSV</th>
		                    <td>
		                      <input type="text" name="nsv" class="form-control" value="<?php echo $result[0]->nsv;?>">
		                    </td>
		                  </tr>

		                  <tr>
		                    <th colspan="2">&nbsp;</th>
		                  </tr>
		                  <tr>
		                    <th>PAC</th>
		                    <td>
		                      <input type="text" name="pac" class="form-control" value="<?php echo $result[0]->pac;?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>VIA Test</th>
		                    <td>
		                      <input type="text" name="via_test" class="form-control" value="<?php echo $result[0]->via_test;?>">
		                    </td>
		                  </tr>
		                  <!-- <tr>
		                    <th>Total</th>
		                    <td>
		                      <input type="text" name="total" class="form-control">
		                    </td>
		                  </tr> -->
		                  <tr>
		                    <th>Condom Pieces</th>
		                    <td>
		                      <input type="text" name="condom_pieces" class="form-control" value="<?php echo $result[0]->condom_pieces;?>">
		                    </td>
		                  </tr>
		                  </tbody>
		                </table>                       
                    </div> 
                    <div class="box-footer">
                        <input class="btn bg-primary" type="submit" name="submit" value="Submit"/>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->
        </div>
    </div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>          