<?php 
$data['title'] = 'View Master Activity Log';
$this->load->view('./admin/header', $data); 
?>
<div class="row">
  <div class="col-md-10">
      <!-- general form elements -->
      <div class="box box-primary">
          <div class="box-header">
              <h3 class="box-title">Master Activity Log View</h3>
              <a href="../../../mf_download" class="btn btn-primary pull-right">Download</a>
          </div>
          
          <div class="box-body">
            <table class="table table-bordered">
              <tbody>
              <tr bgcolor="#ddd">
                <th width="55%">Description of the component</th>
                <th class="text-center" width="10%">Targets</th>
                <th class="text-center" width="10%">Achive</th>
                <th class="text-center" width="10%">Percent</th>
                <th width="15%">Remarks</th>
              </tr>
              <?php
              foreach ($result as $data){
              ?>
                <tr bgcolor="#eee">
                  <th colspan="5"><?php echo $data->label_name;?></th>
                </tr>
                <?php
                $qry2 = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '".$data->id."' AND `status` = '1' ORDER BY `order_id` ASC");
                $i = 0;
                foreach ($qry2->result() as $data2){
                  $qry3 = $this->db->query("SELECT * FROM `mf__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' AND `quarter` = '".$quarter."' AND `label_id` = '".$data2->id."' AND `status` = '1' ORDER BY `order_id` ASC");
                  $data3 = $qry3->row();
                  if($data2->is_head){ ?>
                  <tr bgcolor="#ffe">
                    <th colspan="5"><?php echo $data2->label_name;?></th>                          
                  </tr>
                  <?php } else {
                  ?>
                  <tr bgcolor="#fff">
                    <td><?php echo $data2->label_name;?></td>
                    <td class="text-center"><?php echo $data3->target?></td>                    
                    <td class="text-center"><?php echo $data3->achieve?></td>
                    <td class="text-center"><?php echo $data3->percent?></td>
                    <td><?php echo $data3->remarks?></td>
                  </tr>
                  <?php
                  $i++;
                  }
                }
              }              
              ?>                    
              </tbody>
            </table>
          </div> 
          <div class="box-footer">
          </div>                
      </div>
  </div>
</div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>