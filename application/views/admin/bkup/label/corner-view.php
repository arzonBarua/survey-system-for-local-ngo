<?php 
$data['title'] = 'View MR,FP & PAC Corner';
$this->load->view('./admin/header', $data); 
?>
<div class="row">
  <div class="col-md-8">
      <!-- general form elements -->
      <div class="box box-primary">
          <div class="box-header">
              <h3 class="box-title">MR,FP &amp; PAC Corner View</h3>
                [
                <b>Project: </b><?php echo $result->project_name;?>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Year: </b><?php echo $result->year;?>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Quarter: </b><?php echo $result->quarter;?>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Divisoin: </b><?php echo $result->division_name;?>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>District: </b><?php echo $result->district_name;?> 
                ]
                <a href="../../../../../pc_download" class="btn btn-primary pull-right">Download</a>
          </div>
          
          <div class="box-body">
            <table class="table table-bordered" style="width: 40%">
              <tbody>                     
              <tr bgcolor="#ddd">
                <th colspan="3">MR Services</th>
              </tr>
              <tr>
                <th>MR Reject</th>
                <td><?php echo $result->mr_reject;?></td>
              </tr>
              <tr>
                <th>MR Successfull</th>
                <td><?php echo $result->mr_success;?></td>
              </tr>
              <tr>
                <th>MRM</th>
                <td><?php echo $result->mrm;?></td>
              </tr>
              <tr bgcolor="#ddd">
                <th colspan="2">Other Services</th>
              </tr>

              <tr bgcolor="#eee">
                <th colspan="2">1. Short Term Method</th>
              </tr>
              <tr>
                <th>Condom (Person)</th>
                <td><?php echo $result->condom_person;?></td>
              </tr>
              <tr>
                <th>Pill</th>
                <td><?php echo $result->pill;?></td>
              </tr>
              <tr>
                <th>Injectable</th>
                <td><?php echo $result->injectable;?></td>
              </tr>

              <tr bgcolor="#eee">
                <th colspan="2">2. Long Term Method</th>
              </tr>
              <tr>
                <th>IUD</th>
                <td><?php echo $result->iud;?></td>
              </tr>
              <tr>
                <th>Implant</th>
                <td><?php echo $result->implant;?></td>
              </tr>

              <tr bgcolor="#eee">
                <th colspan="2">3. Permanent Method</th>
              </tr>
              <tr>
                <th>Ligation</th>
                <td><?php echo $result->liagation;?></td>
              </tr>
              <tr>
                <th>NSV</th>
                <td><?php echo $result->nsv;?></td>
              </tr>

              <tr>
                <th colspan="2">&nbsp;</th>
              </tr>
              <tr>
                <th>PAC</th>
                <td><?php echo $result->pac;?></td>
              </tr>
              <tr>
                <th>VIA Test</th>
                <td><?php echo $result->via_test;?></td>
              </tr>
              <tr>
                <th>Condom Pieces</th>
                <td><?php echo $result->condom_pieces;?></td>
              </tr>
              </tbody>
            </table> 
          </div> 
          <div class="box-footer">
          </div>                
      </div>
  </div>
</div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>