<?php 
$arr = array("1" => "Jan-Mar", "2" => "Apr-June", "3" => "July-Sept", "4" => "Oct-Dec", "5" => "Jan-Dec");
if(array_key_exists($quarter, $arr)) {
  $quarter_str = $arr[$quarter].' '.$year;
  $quarter_str_1 = '('.$arr[$quarter].') '.$year;
} else {
  $quarter_str = $arr['5'].' '.$year;
  $quarter_str_1 = '('.$arr['5'].') '.$year;
}
?>
<style type="text/css">
  .text-left-custom { text-align: left !important; }
</style>
<table class="table table-bordered text-center">
  <tbody>
  <tr bgcolor="#ddd">
    <th colspan="16">MR,FP &amp; PAC Corner Performance report</th>
  </tr>
  <tr bgcolor="#ddd">
    <th colspan="16">Quarter: <?php echo $quarter_str;?></th>
  </tr>
  <tr bgcolor="#ddd">
    <th colspan="2">Project Area</th>
    <th colspan="3">MR Services Provided</th>
    <th colspan="11">Other Services  Provided</th>
  </tr>
  <tr bgcolor="#ddd">
    <th rowspan="2" style="vertical-align: middle;">Division</th>  
    <th rowspan="2" style="vertical-align: middle;">District</th>
    <th rowspan="2" style="vertical-align: middle;">MR Reject</th>
    <th rowspan="2" style="vertical-align: middle;">MR Successful</th>
    <th rowspan="2" style="vertical-align: middle;">MRM</th>
    <th colspan="3" style="vertical-align: middle;">Short term method</th>
    <th colspan="2" style="vertical-align: middle;">Long term method</th>
    <th colspan="2" style="vertical-align: middle;">Permanent method</th>
    <th rowspan="2" style="vertical-align: middle;">PAC</th>
    <th rowspan="2" style="vertical-align: middle;">VIA  test</th>
    <th rowspan="2" style="vertical-align: middle;">Total</th>
    <th rowspan="2" style="vertical-align: middle;">Condom Pieces</th>
  </tr>
  <tr bgcolor="#ddd">
    <th>Condom (Person)</th>
    <th>Pill</th>
    <th>Injectable</th>
    <th>IUD</th>
    <th>Implant</th>
    <th>Ligation</th>
    <th>NSV</th>
  </tr>
  <?php
  foreach ($result as $data){
    $qry_dist = $this->db->query("SELECT * FROM `district` WHERE `div_id` = '".$data->id."' ORDER BY `name` ASC");
    $num_dist = $qry_dist->num_rows();    
    $result_dist = $qry_dist->result();
    $i = 0;
    foreach ($result_dist as $data_dist){      
      ?>
      <tr bgcolor="#eee">
      <?php 
      if($i++ == 0) {?>
      <td class="text-left-custom" style="vertical-align: middle;" rowspan="<?php echo $num_dist;?>"><?php echo $data->division_name;?></td>
      <?php }?>
      <td><?php echo $data_dist->name;?></td>
      <?php 
      $sql = "SELECT * FROM `pr__labels_data` WHERE `project_id` = '".$project."' AND `year` = '".$year."' AND `quarter` = '".$quarter."' AND `division` = '".$data->id."' AND `district` = '".$data_dist->id."'";
      //echo $sql."<br/>";
      $qry = $this->db->query($sql);
      $result_data = $qry->row();
      ?>
      <td><?php echo @$result_data->mr_reject;?></td>
      <td><?php echo @$result_data->mr_success;?></td>
      <td><?php echo @$result_data->mrm;?></td>
      <td><?php echo @$result_data->condom_person;?></td>
      <td><?php echo @$result_data->pill;?></td>
      <td><?php echo @$result_data->injectable;?></td>
      <td><?php echo @$result_data->iud;?></td>
      <td><?php echo @$result_data->implant;?></td>
      <td><?php echo @$result_data->liagation;?></td>
      <td><?php echo @$result_data->nsv;?></td>
      <td><?php echo @$result_data->pac;?></td>
      <td><?php echo @$result_data->via_test;?></td>
      <td><?php echo @$total;?></td>
      <td><?php echo @$result_data->condom_pieces;?></td>      
    </tr>
    <?php   
    }
  }              
  ?>                    
  </tbody>
</table>