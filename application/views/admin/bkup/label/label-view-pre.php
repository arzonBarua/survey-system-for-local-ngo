<?php 
$data['title'] = 'View Master Activity Log';
$this->load->view('./admin/header', $data); 
?>
<style type="text/css">
  .report-mf .box-body { min-height: 200px; }
</style>
<div class="row">
  <div class="col-md-10">
      <div class="box box-primary">
          <div class="box-header">
              <h3 class="box-title">Master Activity Log View</h3>
          </div>
          
          <div class="box-body">
            <table class="table table-bordered" style="width: 50%;">
              <tbody>
              <tr>
                <th width="30%">Project</th>
                <td> 
                  <select class="selectBox form-control project_id" name="project_id">
                  <option value=""> Select Project </option>
                  <?php 
                    foreach($projects as $val){ ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>';
                      <?php }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>
                <th width="30%">Year</th>
                <td>
                  <select class="form-control year" name="year">
                  <option value=""> Select Year </option>
                  <?php   
                  $arr = range(2016, date("Y"));                           
                  foreach($arr as $val){
                    echo '<option value="'.$val.'">'.$val.'</option>';
                  }
                  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <th width="30%"></th>
                <td>
                  <input type="submit" name="submit" value="Search" class="btn btn-primary search" />
                </td>
              </tr>
              </tbody>
            </table>
          </div> 
          <div class="box-footer">
          </div>                
      </div>


      <!-- general form elements -->
      <div class="box box-primary report-mf">
          <div class="box-header">
              <h3 class="box-title">Master Activity Log Report</h3>
          </div>
          
          <div class="box-body">
            Select the Year and Project
          </div>           
      </div>
  </div>
</div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>
<script type="text/javascript">
  $(function(){
    var option_value = <?php echo ($this->session->userdata('project_id')) ? $this->session->userdata('project_id') : null ;  ?>;

    //alert(option_value);


    if(option_value != null)
        $('select>option:eq('+option_value+')').attr('selected', true).siblings().attr('disabled',true);
    
    $(".search").on("click", function(){
        var project_id = $(".project_id").val();
        var year       = $(".year").val();
        var base       = "<?php echo base_url();?>";

        if(project_id != "" && year != ""){
          $(".report-mf .box-body").html("<div style='margin: auto; width: 400px'><img src='"+base+"assets/img/loading.gif'/></div>");
          $(".report-mf .box-body").load(base+'admin/ajax/label_view_ajax/'+project_id+'/'+year);
        }else{
          $(".report-mf .box-body").html("<div style='margin: auto; width: 400px'><img src='"+base+"assets/img/400x300.jpg'/></div>");
        }
    });
  });
</script>
<?php if($this->session->userdata('project_id')): ?>
  <script>
    $(function(){
      var value = <?php echo $this->session->userdata('project_id'); ?>;

      $('.project_id option[val="' + value + '"]').prop('selected', true).siblings().attr('disabled',true);

    });
  </script
<?php endif; ?>