		<footer class="main-footer">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-18">
					<div class="col-md-4 col-sm-4 col-xs-6">
						<p><strong>Copyright &copy; 2017 <a>Nirapod 2</a>.</strong> All rights reserved.</p>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-10">
						<img src="<?php echo base_url(); ?>assets/img/admin-login-bg-footer.png" class="img-responsive" />
					</div>
				    <div class="col-md-2 col-sm-2 col-xs-2 text-right hidden-xs">
				        <p><a href="bol-online.com"><img src="<?php echo base_url()."assets/img/Powered-by-BOL.png"; ?>" style="padding: 42px 5px;"></a></p>
				    </div>
				</div>
			</div>
		    <!-- <div class="pull-right hidden-xs">
		        <b>Version</b> 1.0
		    </div>
		    <strong>Copyright &copy; 2016 <a href="#">Nirapod 2</a>.</strong> All rights reserved.
		    <img src="<?php echo base_url(); ?>assets/img/admin-login-bg-footer.png" style="" class="footer-image"> -->
		</footer>
	</div>
</body>

<script type="text/javascript">
	$(".active").parents("ul").addClass('menu-open').attr('style','display:block');
</script>

</html>