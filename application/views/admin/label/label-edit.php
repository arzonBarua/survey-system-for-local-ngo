<?php 
$data['title'] = 'Edit Master Activity Log';
$this->load->view('./admin/header', $data); 
?>

    <div class="row">
        <div class="col-md-10">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Master Activity Log Edit for <b><?php echo $this->Common_operation->addOrdinalNumberSuffix($_SESSION['mf_label']['quarter']) ."</b> quarter of <b>". $_SESSION['mf_label']['year'];?></b></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                $limit_start = (isset($page) && $page != "") ? $page : 0;
                $page_val = $limit_start + 1;
                if($limit_start >= 0 && $limit_start < ($num_page-1)){              
                  $btn_name = 'Save &amp; Continue';              
                } else if($limit_start == $num_page-1){             
                  $btn_name = 'Save';
                }

                echo form_open_multipart('admin/label/label_edit'); ?>                    
                    <div class="box-body">                      
                        <table class="table table-bordered">
                          <tbody>
                          <tr bgcolor="#ddd">
                            <th width="55%">Description of the component</th>
                            <th width="10%">Targets</th>
                            <th width="10%">Achive</th>
                            <!-- <th width="10%">Percent</th> -->
                            <th width="15%">Remarks</th>
                          </tr>
                          <?php
                          $qry = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '0' AND `status` = '1' ORDER BY `order_id` ASC LIMIT ".$limit_start.", 1");
                          $data = $qry->row();
                          ?>
                          <tr bgcolor="#eee">
                            <th colspan="5"><?php echo $data->label_name;?></th>
                          </tr>
                          <?php
                          $qry2 = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '".$data->id."' AND `status` = '1' ORDER BY `order_id` ASC");
                          $i = 0;
                          foreach ($qry2->result() as $data2){
                            if($data2->is_head){ ?>
                            <tr bgcolor="#ffe">
                              <th colspan="5"><?php echo $data2->label_name;?></th>                          
                            </tr>
                            <?php } else {
                              // echo '<pre>';
                              // print_r($_SESSION['mf_label']);
                              // echo $_SESSION['mf_label']['target'][$data->id][$i];
                            ?>
                            <tr bgcolor="#fff">
                              <th>
                                <?php echo $data2->label_name;?>
                                <input type="hidden" name="project" value="<?php echo $_SESSION['mf_label']['project_id'];?>">
                                <input type="hidden" name="quarter" value="<?php echo $_SESSION['mf_label']['quarter'];?>">
                                <input type="hidden" name="year" value="<?php echo $_SESSION['mf_label']['year'];?>">
                                <input type="hidden" name="page" value="<?php echo $page_val;?>">
                                <input type="hidden" name="page_amt" value="<?php echo $num_page;?>">
                                <input type="hidden" name="label_id[<?php echo $data->id;?>][]" value="<?php echo $data2->id;?>">
                                <input type="hidden" name="label_order[<?php echo $data->id;?>][]" value="<?php echo $data2->order_id;?>">
                              </th>
                              <th><input type='text' name='target[<?php echo $data->id;?>][]' value="<?php echo @$_SESSION['mf_label']['target'][$data->id][$i];?>" class='form-control'/></th>
                              <th><input type='text' name='achive[<?php echo $data->id;?>][]' value="<?php echo @$_SESSION['mf_label']['achive'][$data->id][$i];?>" class='form-control'/></th>
                              <!-- <th><input type='text' name='percent[<?php echo $data->id;?>][]' value="<?php echo @$_SESSION['mf_label']['percent'][$data->id][$i];?>" class='form-control'/></th> -->
                              <th><textarea name='remarks[<?php echo $data->id;?>][]' class='form-control'><?php echo @$_SESSION['mf_label']['remarks'][$data->id][$i];?></textarea></th>
                            </tr>
                            <?php
                            $i++;
                            }
                          }                
                          ?>                    
                          </tbody>
                        </table>                        
                    </div> 
                    <div class="box-footer">
                        <?php 
                        if($limit_start > 0) { ?>
                          <a class="btn bg-primary" href="<?php echo base_url();?>admin/label/label_edit/<?php echo @$_SESSION['mf_label']['project_id'];?>/<?php echo @$_SESSION['mf_label']['year'];?>/<?php echo @$_SESSION['mf_label']['quarter'];?>/<?php echo ($limit_start-1);?>">Back</a>&nbsp;&nbsp;&nbsp;
                        <?php 
                        }
                        ?>
                        <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                        <input class="btn bg-primary" type="submit" name="submit" value="<?php echo $btn_name;?>"/>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->
        </div>
    </div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>          