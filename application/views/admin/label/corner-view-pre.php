<?php 
$data['title'] = 'View MR,FP & PAC Corner';
$this->load->view('./admin/header', $data); 
?>
<style type="text/css">
  .report-pr .box-body { min-height: 200px; }
</style>
<div class="row">
  <div class="col-md-10">
      <div class="box box-primary">
          <div class="box-header">
              <h3 class="box-title">MR, FP &amp; PAC Corner View</h3>
          </div>
          <?php 
          if ($this->session->flashdata('success_message')): ?>
              <div class="alert alert-success">
                  <?php echo $this->session->flashdata('success_message'); ?>
              </div>
          <?php endif;
          ?>
          
          <div class="box-body">
            <table class="table table-bordered" style="width: 50%;">
              <tbody>
              <tr>
                <th width="30%">Organization</th>
                <td>
                  <select class="form-control project_id call_ajax_organization" name="project_id">
                  <option value=""> Select Organization </option>
                  <?php                     
                  foreach($projects as $val){
                    echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                  }
                  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <th width="30%">Year</th>
                <td>
                  <select class="form-control year" name="year">
                  <option value=""> Select Year </option>
                  <?php   
                  $arr = range(2016, date("Y"));                           
                  foreach($arr as $val){
                    echo '<option value="'.$val.'">'.$val.'</option>';
                  }
                  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <th width="30%">Division</th>
                <td>
                  <select class="form-control call_ajax division" name="division" required="">
                  <option value=""> --SELECT DIVISION-- </option>
                  <?php                                 
                  foreach($result as $val){
                    echo '<option value="'.$val->id.'">'.$val->division_name.'</option>';
                  }
                  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <th width="30%">Distict</th>
                <td>
                  <select class="form-control district" name="district" id="district" required="">
                    <option value=""> --SELECT DISTRICT-- </option>
                  </select>
                </td>
              </tr>
              <tr>
                <th width="30%"></th>
                <td>
                  <input type="submit" name="submit" value="Search" class="btn btn-primary search" />
                </td>
              </tr>
              </tbody>
            </table>
          </div> 
          <div class="box-footer">
          </div>                
      </div>


      <!-- general form elements -->
      <div class="box box-primary report-pr">
          <div class="box-header">
              <h3 class="box-title">MR, FP &amp; PAC Corner Report</h3>
          </div>
          
          <div class="box-body">
            Select the Project, Year, Division and District to view the report
          </div>           
      </div>
  </div>
</div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>

<?php if($this->session->userdata('project_id')): ?>
      <script type="text/javascript">
        $(function(){
          var project_id = <?php echo $this->session->userdata('project_id') ?>;
            $.ajax({
                url: '<?php echo base_url()."admin/ajax/" ?>show_project_divisions',
                data:{'project_id': project_id},
                type:"post",
                dataType: "text",
                success: function(data){
                  $('.call_ajax').html(data);
                  $("#district").html("<option value=''>--SELECT DISTRICT--</option>");
                }
            });
        });

      </script>
<?php  endif; ?> 

<script type="text/javascript">
  $(function(){

     var option_value = <?php echo ($this->session->userdata('project_id')) ? $this->session->userdata('project_id') : 'null' ;  ?>;

    //alert(option_value);
    if(option_value != 'null')
        $('select>option:eq('+option_value+')').attr('selected', true).siblings().attr('disabled',true);

    $(".search").on("click", function(){
        var project_id = $(".project_id").val();
        var year       = $(".year").val();
        var division   = $(".division").val();
        var district   = $(".district").val();
        var base       = "<?php echo base_url();?>";

        if(project_id != "" && year != "" && division != "" && district != ""){
          $(".report-pr .box-body").html("<div style='margin: auto; width: 400px'><img src='"+base+"assets/img/loading.gif'/></div>");
          $(".report-pr .box-body").load(base+'admin/ajax/corner_view_ajax/'+project_id+'/'+year+'/'+division+'/'+district);
        }else{
          $(".report-pr .box-body").html("<div style='margin: auto; width: 400px'><img src='"+base+"assets/img/400x300.jpg'/></div>");
        }
    });

    $(".call_ajax").on("change", function (e) {
      e.preventDefault();
      var id = $(this).val();
      var tableName = 'district';
      
      $.ajax({
           url: '<?php echo base_url()."admin/ajax/" ?>label_ajax',
          data: {'id': id, 'table' : tableName, 'field' : 'div_id' },
          type: "post",
          dataType: 'text',
          success: function (data) {
            console.log(data);
            $("#district").html(data);
          }
      });
      return false;
    });

    $(".call_ajax_organization").on("change",function(e){
      var project_id = $(this).val();
      $.ajax({
          url: '<?php echo base_url()."admin/ajax/" ?>show_project_divisions',
          data:{'project_id': project_id},
          type:"post",
          dataType: "text",
          success: function(data){
            $('.call_ajax').html(data);
            $("#district").html("<option value=''>--SELECT DISTRICT--</option>");
          }
      });
    }); 



  });
</script>
