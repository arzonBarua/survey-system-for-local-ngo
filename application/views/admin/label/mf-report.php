<?php 
$data['title'] = 'Report on Master Activity Log';
$this->load->view('./admin/header', $data); 
?>
<style type="text/css">
  .report-pr .box-body { min-height: 200px; }
</style>
<div class="row">
  <div class="col-md-10">
      <div class="box box-primary">
          <div class="box-header">
              <h3 class="box-title">Master Activity Log Report Search</h3>
          </div>
          
          <div class="box-body">
            <table class="table table-bordered" style="width: 50%;">
              <tbody>              
              <tr>
                <th width="30%">Year</th>
                <td>
                  <select class="form-control year" name="year">
                  <option value=""> Select Year </option>
                  <?php   
                  $arr = range(2016, date("Y"));                           
                  foreach($arr as $val){ ?>
                    <option value="<?php echo $val;?>"><?php echo $val;?></option>
                  <?php }?>
                  </select>
                </td>
              </tr>
              <tr>
                <th width="30%">Quarter</th>
                <td>
                  <select class="form-control quarter" name="quarter">                  
                  <?php   
                  $arr = range(1, 4);                           
                  foreach($arr as $val){ ?>
                    <option value="<?php echo $val;?>"><?php echo $val;?></option>
                  <?php } ?>
                  <option value="5"> All </option>
                  </select>
                </td>
              </tr>
              <tr>
                <th width="30%"></th>
                <td>
                  <input type="submit" name="submit" value="Search" class="btn btn-primary search" />
                </td>
              </tr>
              </tbody>
            </table>
          </div> 
          <div class="box-footer">
          </div>                
      </div>


      <!-- general form elements -->
      <div class="box box-primary report-pr">
          <div class="box-header">
              <h3 class="box-title">Master Activity Log Report Details</h3>
          </div>
          
          <div class="box-body">
            Select the Year and Quarter to view the Report
          </div>           
      </div>
  </div>
</div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>
<script type="text/javascript">
  $(function(){
    $(".search").on("click", function(){
        var quarter = $(".quarter").val();
        var year    = $(".year").val();
        var base    = "<?php echo base_url();?>";

        if(quarter != "" && year != ""){
          $(".report-pr .box-body").html("<div style='margin: auto; width: 400px'><img src='"+base+"assets/img/loading.gif'/></div>");
          $(".report-pr .box-body").load(base+'admin/ajax/mf_report_ajax/'+year+'/'+quarter);
        }else{
          $(".report-pr .box-body").html("<div style='margin: auto; width: 400px'><img src='"+base+"assets/img/400x300.jpg'/></div>");
        }
    });
  });
</script>