<?php 

$arr = array("1" => "Jan-Mar", "2" => "Apr-June", "3" => "July-Sept", "4" => "Oct-Dec", "5" => "Jan-Dec");

if(array_key_exists($quarter, $arr)) {

  $quarter_str = $arr[$quarter].' '.$year;

  $quarter_str_1 = '('.$arr[$quarter].') '.$year;

} else {

  $quarter_str = $arr['5'].' '.$year;

  $quarter_str_1 = '('.$arr['5'].') '.$year;

}

?>

<style type="text/css">

  .text-left-custom { text-align: left !important; }

  .congested td { padding: 1px !important; text-align: left !important; }

  .table-different > thead > tr > th,

  .table-different > tbody > tr > th,

  .table-different > tfoot > tr > th,

  .table-different > thead > tr > td,

  .table-different > tbody > tr > td,

  .table-different > tfoot > tr > td {

    border-top: 1px solid #999;

  }

  .table-different {

    border: 1px solid #999 !important;

  }

  .table-different > thead > tr > th,

  .table-different > tbody > tr > th,

  .table-different > tfoot > tr > th,

  .table-different > thead > tr > td,

  .table-different > tbody > tr > td,

  .table-different > tfoot > tr > td {

    border: 1px solid #999 !important;

  }

</style> 



<table class="table table-bordered text-center table-different">

  <tbody>

  <tr bgcolor="#ddd">

    <th colspan="21">Record of referral performance by <?php echo $data_project." ".$quarter_str;?></th>

  </tr>

  <tr bgcolor="#ddd">

    <th colspan="2">Project Area</th>

    <th colspan="16">MR Services Provided</th>

    <th colspan="3"></th>

  </tr>

  <tr bgcolor="#ddd">

    <th rowspan="2" style="vertical-align: middle;">Division</th>  

    <th rowspan="2" style="vertical-align: middle;">District</th>

    <th colspan="3" style="vertical-align: middle;">Short term method</th>

    <th colspan="2" style="vertical-align: middle;">Long term method</th>

    <th colspan="2" style="vertical-align: middle;">Permanent method</th>

    <th rowspan="2" style="vertical-align: middle;">MRM</th>

    <th rowspan="2" style="vertical-align: middle;">MR</th>

    <th rowspan="2" style="vertical-align: middle;">Preventing Early Marriage</th>

    <th rowspan="2" style="vertical-align: middle;">Preventing VAW</th>

    <th rowspan="2" style="vertical-align: middle;">Institutional Delivery</th>

    <th rowspan="2" style="vertical-align: middle;">VIA  (Visual Inspection with Acetic Acid) Test</th>

    <th rowspan="2" style="vertical-align: middle;">STI/RTI</th>

    <th rowspan="2" style="vertical-align: middle;">Other</th>

    <th rowspan="2" style="vertical-align: middle;">Total</th>

    <th rowspan="2" style="vertical-align: middle;">Where Clients are referred</th>

    <th rowspan="2" style="vertical-align: middle;">Total</th>

    <th rowspan="2" style="vertical-align: middle;">Grand Total</th>

  </tr>

  <tr bgcolor="#ddd">

    <th>Condom (Person)</th>

    <th>Pill</th>

    <th>Injectable</th>

    <th>IUD</th>

    <th>Implant</th>

    <th>Ligation</th>

    <th>NSV</th>

  </tr>

  <?php



  $arr = array(

      "1. CC",

      "2. FWC",

      "3. UHC",

      "4. MCWC",

      "5. District General Hospital",

      "6. Marie Stopes Bangladesh (Clinic)",

      "7. BAPSA",

      "8. RHSTEP",

      "9. FPAB",

      "10. Other Clinics (NGO &amp; Private)",

      "11. Union Porishod"

    );

  $column_arr = array();

  foreach ($result as $data){

    $qry_dist = $this->db->query("SELECT * FROM `district` WHERE `div_id` = '".$data->id."' ORDER BY `name` ASC");

    $num_dist = $qry_dist->num_rows();    

    $result_dist = $qry_dist->result();

    $i = 0;

    foreach ($result_dist as $data_dist){

      ?>

      <tr bgcolor="#eee">

      <?php 

      if($i++ == 0) {?>

      <td class="text-left-custom" style="vertical-align: middle;" rowspan="<?php echo ($num_dist*12);?>"><?php echo $data->division_name;?></td>

      <?php }?>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo $data_dist->name;?></td>

      <?php

      if($quarter == 5) $quarter = "1','2','3','4";



      $sql = "SELECT sum(`condom_person`) condom_person, sum(`pill`) pill, sum(`injectable`) injectable, sum(`iud`) iud, sum(`implant`) implant, sum(`liagation`) liagation, sum(`nsv`) nsv, sum(`mr`) mr, sum(`mrm`) mrm, sum(`prevent_em`) prevent_em, sum(`prevent_vaw`) prevent_vaw, sum(`delivery_institution`) delivery_institution, sum(`via`) via, sum(`sti_rti`) sti_rti, sum(`other`) other, sum(`cc`) cc, sum(`fwc`) fwc, sum(`uhc`) uhc, sum(`mcwc`) mcwc, sum(`dgh`) dgh, sum(`msbc`) msbc, sum(`bapsa`) bapsa, sum(`rhstep`) rhstep, sum(`fpab`) fpab, sum(`ngo_private`) ngo_private, sum(`union_porishod`) union_porishod FROM `rpc__labels_data` WHERE `project_id` IN (".$project.") AND `year` = '".$year."' AND `quarter` IN ('".$quarter."') AND `division` = '".$data->id."' AND `district` = '".$data_dist->id."'";

      // echo $sql."<br/>";

      // die;

      $qry = $this->db->query($sql);

      $result_data = $qry->row();

      $total = @$result_data->condom_person + @$result_data->pill + @$result_data->injectable + @$result_data->iud + @$result_data->implant + @$result_data->liagation + @$result_data->nsv + @$result_data->mr + @$result_data->mrm + @$result_data->prevent_em + @$result_data->prevent_vaw + @$result_data->delivery_institution + @$result_data->via + @$result_data->sti_rti + @$result_data->other;



      $column_arr['condom_person']  = isset($column_arr['condom_person'] ) ? $column_arr['condom_person'] + @$result_data->condom_person : @$result_data->condom_person;

      $column_arr['pill']           = isset($column_arr['pill'] ) ? $column_arr['pill'] + @$result_data->pill : @$result_data->pill;

      $column_arr['injectable']     = isset($column_arr['injectable'] ) ? $column_arr['injectable'] + @$result_data->injectable : @$result_data->injectable;

      $column_arr['iud']            = isset($column_arr['iud'] ) ? $column_arr['iud'] + @$result_data->iud : @$result_data->iud;

      $column_arr['implant']        = isset($column_arr['implant'] ) ? $column_arr['implant'] + @$result_data->implant : @$result_data->implant;

      $column_arr['liagation']      = isset($column_arr['liagation'] ) ? $column_arr['liagation'] + @$result_data->liagation : @$result_data->liagation;

      $column_arr['nsv']            = isset($column_arr['nsv'] ) ? $column_arr['nsv'] + @$result_data->nsv : @$result_data->nsv;

      $column_arr['mr']             = isset($column_arr['mr'] ) ? $column_arr['mr'] + @$result_data->mr : @$result_data->mr;

      $column_arr['mrm']            = isset($column_arr['mrm'] ) ? $column_arr['mrm'] + @$result_data->mrm : @$result_data->mrm;

      $column_arr['prevent_em']     = isset($column_arr['prevent_em'] ) ? $column_arr['prevent_em'] + @$result_data->prevent_em : @$result_data->prevent_em;

      $column_arr['prevent_vaw']    = isset($column_arr['prevent_vaw'] ) ? $column_arr['prevent_vaw'] + @$result_data->prevent_vaw : @$result_data->prevent_vaw;

      $column_arr['delivery_institution'] = isset($column_arr['delivery_institution'] ) ? $column_arr['delivery_institution'] + @$result_data->delivery_institution : @$result_data->delivery_institution;

      $column_arr['via']            = isset($column_arr['via'] ) ? $column_arr['via'] + @$result_data->via : @$result_data->via;

      $column_arr['sti_rti']        = isset($column_arr['sti_rti'] ) ? $column_arr['sti_rti'] + @$result_data->sti_rti : @$result_data->sti_rti;

      $column_arr['other']          = isset($column_arr['other'] ) ? $column_arr['other'] + @$result_data->other : @$result_data->other;

      $column_arr['total']          = isset($column_arr['total'] ) ? $column_arr['total'] + @$total : @$total;

      ?>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->condom_person;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->pill;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->injectable;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->iud;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->implant;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->liagation;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->nsv;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->mrm; ?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->mr;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->prevent_em;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->prevent_vaw;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->delivery_institution;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->via;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->sti_rti;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$result_data->other;?></td>

      <td rowspan="12" style="vertical-align: middle; text-align: center !important"><?php echo @$total;?></td>

    </tr>

    <?php 

      $arr_val = array(@$result_data->cc, @$result_data->fwc, @$result_data->uhc, @$result_data->mcwc, @$result_data->dgh, @$result_data->msbc, @$result_data->bapsa, @$result_data->rhstep, @$result_data->fpab, @$result_data->ngo_private, @$result_data->union_porishod);



      $column_arr['referral_client'] = isset($column_arr['referral_client'] ) ? $column_arr['referral_client'] + array_sum($arr_val) : array_sum($arr_val);

      echo '<tr bgcolor="#eee" class="congested">

          <td>'.$arr[0].'</td>

          <td>'.$arr_val[0].'</td>

          <td rowspan="11" style="vertical-align: middle; text-align: center !important">'.array_sum($arr_val).'</td>

        </tr>';

      for($j = 1; $j < 11; $j++){

        echo '<tr bgcolor="#eee" class="congested">

          <td>'.$arr[$j].'</td>

          <td>'.$arr_val[$j].'</td>

        </tr>';

      }  

    }

  }              

  ?>

    <tr>

      <td colspan="2">Grand Total</td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['condom_person'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['pill'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['injectable'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['iud'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['implant'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['liagation'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['nsv'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['mrm'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['mr'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['prevent_em'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['prevent_vaw'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['delivery_institution'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['via'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['sti_rti'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['other'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['total'];?></td>

      <td></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['referral_client'];?></td>

      <td style="vertical-align: middle; text-align: center !important"><?php echo @$column_arr['referral_client'];?></td>

    </tr>

  </tbody>

</table>