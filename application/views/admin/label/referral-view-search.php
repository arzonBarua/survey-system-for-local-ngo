<table class="table table-bordered">
  <tbody>
  <tr bgcolor="#ddd">
    <th width="25%">1st Quarter</th>
    <th width="25%">2nd Quarter</th>
    <th width="25%">3rd Quarter</th>
    <th width="25%">4th Quarter</th>
  </tr>
  <?php
  $quarter = array();
  $project_id = array();
  $year = array();
  foreach ($result as $key => $value) {
     $quarter[$value->quarter]     = $value->quarter;
     $project_id[$value->quarter]  = $value->project_id;
     $year[$value->quarter]        = $value->year;
     $division[$value->quarter]    = $value->division;
     $district[$value->quarter]    = $value->district;
  }
  ?>
 <tr>
  <td>
    <?php 
    if( isset($quarter['1']) && $quarter['1'] != ""){ ?>
    <a href="<?php echo base_url()?>admin/label/referral_view/<?php echo $project_id['1'];?>/<?php echo $year['1'];?>/<?php echo $quarter['1'];?>/<?php echo $division['1'];?>/<?php echo $district['1'];?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i> View</a>&nbsp;&nbsp;/&nbsp;&nbsp;
    <a href="<?php echo base_url()?>admin/label/referral_edit/<?php echo $project_id['1'];?>/<?php echo $year['1'];?>/<?php echo $quarter['1'];?>/<?php echo $division['1'];?>/<?php echo $district['1'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
    <?php } else {
      echo 'No data.';
    }?>
  </td>
  <td>
    <?php 
    if(isset($quarter['2']) && $quarter['2'] != ""){ ?>
    <a href="<?php echo base_url()?>admin/label/referral_view/<?php echo $project_id['2'];?>/<?php echo $year['2'];?>/<?php echo $quarter['2'];?>/<?php echo $division['2'];?>/<?php echo $district['2'];?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i> View</a>&nbsp;&nbsp;/&nbsp;&nbsp;
    <a href="<?php echo base_url()?>admin/label/referral_edit/<?php echo $project_id['2'];?>/<?php echo $year['2'];?>/<?php echo $quarter['2'];?>/<?php echo $division['2'];?>/<?php echo $district['2'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
    <?php } else {
      echo 'No data.';
    }?>
  </td>
  <td>
    <?php 
    if(isset($quarter['3']) && $quarter['3'] != ""){ ?>
    <a href="<?php echo base_url()?>admin/label/referral_view/<?php echo $project_id['3'];?>/<?php echo $year['3'];?>/<?php echo $quarter['3'];?>/<?php echo $division['3'];?>/<?php echo $district['3'];?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i> View</a>&nbsp;&nbsp;/&nbsp;&nbsp;
    <a href="<?php echo base_url()?>admin/label/referral_edit/<?php echo $project_id['3'];?>/<?php echo $year['3'];?>/<?php echo $quarter['3'];?>/<?php echo $division['3'];?>/<?php echo $district['3'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
    <?php } else {
      echo 'No data.';
    }?>
  </td>
  <td>
    <?php 
    if(isset($quarter['4']) && $quarter['4'] != ""){ ?>
    <a href="<?php echo base_url()?>admin/label/referral_view/<?php echo $project_id['4'];?>/<?php echo $year['4'];?>/<?php echo $quarter['4'];?>/<?php echo $division['4'];?>/<?php echo $district['4'];?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i> View</a>&nbsp;&nbsp;/&nbsp;&nbsp;
    <a href="<?php echo base_url()?>admin/label/referral_edit/<?php echo $project_id['4'];?>/<?php echo $year['4'];?>/<?php echo $quarter['4'];?>/<?php echo $division['4'];?>/<?php echo $district['4'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
    <?php } else {
      echo 'No data.';
    }?>
  </td>     
 </tr>
  </tbody>
</table>
          