<?php 
$data['title'] = 'Add Record of Referral Performance';
$this->load->view('./admin/header', $data); 
?>
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Record of Referral Performance Add for <b><?php echo $this->Common_operation->addOrdinalNumberSuffix($_SESSION['rpc_corner']['quarter']) ."</b> quarter of <b>". $_SESSION['rpc_corner']['year'];?></b></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
				echo form_open_multipart('admin/label/referral_add', array('id'=>'form')); ?>
					<input type="hidden" name="project_id" value="<?php echo $_SESSION['rpc_corner']['project_id'];?>">
					<input type="hidden" name="quarter" value="<?php echo $_SESSION['rpc_corner']['quarter'];?>">
					<input type="hidden" name="year" value="<?php echo $_SESSION['rpc_corner']['year'];?>">
					<input type="hidden" name="division" value="<?php echo $_SESSION['rpc_corner']['division'];?>">
					<input type="hidden" name="district" value="<?php echo $_SESSION['rpc_corner']['district'];?>">	
                    <div class="box-body">                      
                        <table class="table table-bordered">
		                  <tbody>
		                  <tr bgcolor="#eee">
		                    <th colspan="2">1. Short Term Method</th>
		                  </tr>
		                  <tr>
		                    <th>Condom (Person)</th>
		                    <td>
		                      <input type="text" name="condom_person" class="form-control general" value="<?php echo @$post['condom_person'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Pill</th>
		                    <td>
		                      <input type="text" name="pill" class="form-control general" value="<?php echo @$post['pill'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Injectable</th>
		                    <td>
		                      <input type="text" name="injectable" class="form-control general" value="<?php echo @$post['injectable'];?>">
		                    </td>
		                  </tr>

		                  <tr bgcolor="#eee">
		                    <th colspan="2">2. Long Term Method</th>
		                  </tr>
		                  <tr>
		                    <th>IUD</th>
		                    <td>
		                      <input type="text" name="iud" class="form-control general" value="<?php echo @$post['iud'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Implant</th>
		                    <td>
		                      <input type="text" name="implant" class="form-control general" value="<?php echo @$post['implant'];?>">
		                    </td>
		                  </tr>

		                  <tr bgcolor="#eee">
		                    <th colspan="2">3. Permanent Method</th>
		                  </tr>
		                  <tr>
		                    <th>Ligation</th>
		                    <td>
		                      <input type="text" name="liagation" class="form-control general" value="<?php echo @$post['liagation'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>NSV</th>
		                    <td>
		                      <input type="text" name="nsv" class="form-control general" value="<?php echo @$post['nsv'];?>">
		                    </td>
		                  </tr>

		                  <tr>
		                    <th colspan="2">&nbsp;</th>
		                  </tr>
		                  <tr>
		                    <th>MRM</th>
		                    <td>
		                      <input type="text" name="mrm" class="form-control general" value="<?php echo @$post['mrm'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>MR</th>
		                    <td>
		                      <input type="text" name="mr" class="form-control general" value="<?php echo @$post['mr'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Preventing Early Marriage</th>
		                    <td>
		                      <input type="text" name="prevent_em" class="form-control general" value="<?php echo @$post['prevent_em'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Preventing VAW</th>
		                    <td>
		                      <input type="text" name="prevent_vaw" class="form-control general" value="<?php echo @$post['prevent_vaw'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Institutional Delivery</th>
		                    <td>
		                      <input type="text" name="delivery_institution" class="form-control general" value="<?php echo @$post['delivery_institution'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>VIA  (Visual Inspection with Acetic Acid) Test</th>
		                    <td>
		                      <input type="text" name="via" class="form-control general" value="<?php echo @$post['via'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>STI/RTI</th>
		                    <td>
		                      <input type="text" name="sti_rti" class="form-control general" value="<?php echo @$post['sti_rti'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>Other</th>
		                    <td>
		                      <input type="text" name="other" class="form-control general" value="<?php echo @$post['other'];?>">
		                    </td>
		                  </tr>


		                  <tr bgcolor="#ddd">
		                    <th colspan="2">Where Clients are referred</th>
		                  </tr>
		                  <tr>
		                    <th>1. CC</th>
		                    <td>
		                      <input type="text" name="cc" class="form-control referred" value="<?php echo @$post['cc'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>2. FWC</th>
		                    <td>
		                      <input type="text" name="fwc" class="form-control referred" value="<?php echo @$post['fwc'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>3. UHC</th>
		                    <td>
		                      <input type="text" name="uhc" class="form-control referred" value="<?php echo @$post['uhc'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>4. MCWC</th>
		                    <td>
		                      <input type="text" name="mcwc" class="form-control referred" value="<?php echo @$post['mcwc'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>5. District General Hospital</th>
		                    <td>
		                      <input type="text" name="dgh" class="form-control referred" value="<?php echo @$post['dgh'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>6. Marie Stopes Bangladesh (Clinic)</th>
		                    <td>
		                      <input type="text" name="msbc" class="form-control referred" value="<?php echo @$post['msbc'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>7. BAPSA</th>
		                    <td>
		                      <input type="text" name="bapsa" class="form-control referred" value="<?php echo @$post['bapsa'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>8. RHSTEP</th>
		                    <td>
		                      <input type="text" name="rhstep" class="form-control referred" value="<?php echo @$post['rhstep'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>9. FPAB</th>
		                    <td>
		                      <input type="text" name="fpab" class="form-control referred" value="<?php echo @$post['fpab'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>10. Other Clinics (NGO &amp; Private)  </th>
		                    <td>
		                      <input type="text" name="ngo_private" class="form-control referred" value="<?php echo @$post['ngo_private'];?>">
		                    </td>
		                  </tr>
		                  <tr>
		                    <th>11. Union Porishod</th>
		                    <td>
		                      <input type="text" name="union_porishod" class="form-control referred" value="<?php echo @$post['union_porishod'];?>">
		                    </td>
		                  </tr>
		                  </tbody>
		                </table>
                    </div> 
                    <div class="box-footer">
                        <input class="btn btn-primary general_referred" type="submit" name="submit" value="Submit"/>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->
        </div>
    </div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>
<!-- <script type="text/javascript">
	$(function(){
		$(".general_referred").on("click", function(e){
			e.preventDefault();
			var general = 0;
			var referred = 0;
			$(".general").each(function(){ general = parseInt(general) + parseInt(($(this).val() == "") ? 0 : $(this).val()); console.log(general); });
			$(".referred").each(function(){ referred = parseInt(referred) + parseInt(($(this).val() == "") ? 0 : $(this).val()); console.log(referred);  });

			console.log(general+'-'+referred);
			if(general == referred && referred != 0 && general != 0){
				alert("Now its fine...");
				$("#form").submit();
			}
			else{
				alert("The sum of referred and general not same...");
			}
		});
	});
</script> -->