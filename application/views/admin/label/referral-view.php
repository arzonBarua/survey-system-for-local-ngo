<?php 
$data['title'] = 'View Record of Referral Performance';
$this->load->view('./admin/header', $data); 
?>
<div class="row">
  <div class="col-md-8">
      <!-- general form elements -->
      <div class="box box-primary">
          <div class="box-header">
              <h3 class="box-title">Record of Referral Performance View</h3>
                [
                <b>Organization: </b><?php echo $result->project_name;?>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Year: </b><?php echo $result->year;?>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Quarter: </b><?php echo $result->quarter;?>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Divisoin: </b><?php echo $result->division_name;?>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>District: </b><?php echo $result->district_name;?> 
                ]
                <a href="../../../../../rpc_download" class="btn btn-primary pull-right">Download</a>
          </div>
          
          <div class="box-body">
            <table class="table table-bordered" style="width: 40%">
              <tbody>                     
              <tr bgcolor="#eee">
                <th colspan="2">1. Short Term Method</th>
              </tr>
              <tr>
                <th>Condom (Person)</th>
                <td>
                  <?php echo $result->condom_person;?>
                </td>
              </tr>
              <tr>
                <th>Pill</th>
                <td>
                  <?php echo $result->pill;?>
                </td>
              </tr>
              <tr>
                <th>Injectable</th>
                <td>
                  <?php echo $result->injectable;?>
                </td>
              </tr>

              <tr bgcolor="#eee">
                <th colspan="2">2. Long Term Method</th>
              </tr>
              <tr>
                <th>IUD</th>
                <td>
                  <?php echo $result->iud;?>
                </td>
              </tr>
              <tr>
                <th>Implant</th>
                <td>
                  <?php echo $result->implant;?>
                </td>
              </tr>

              <tr bgcolor="#eee">
                <th colspan="2">3. Permanent Method</th>
              </tr>
              <tr>
                <th>Ligation</th>
                <td>
                  <?php echo $result->liagation;?>
                </td>
              </tr>
              <tr>
                <th>NSV</th>
                <td>
                  <?php echo $result->nsv;?>
                </td>
              </tr>

              <tr>
                <th colspan="2">&nbsp;</th>
              </tr>
              <tr>
                <th>MRM</th>
                <td>
                  <?php echo $result->mrm;?>
                </td>
              </tr>
              <tr>
                <th>MR</th>
                <td>
                  <?php echo $result->mr;?>
                </td>
              </tr>
              <tr>
                <th>Preventing Early Marriage</th>
                <td>
                  <?php echo $result->prevent_em;?>
                </td>
              </tr>
              <tr>
                <th>Preventing VAW</th>
                <td>
                  <?php echo $result->prevent_vaw;?>
                </td>
              </tr>
              <tr>
                <th>Institutional Delivery</th>
                <td>
                  <?php echo $result->delivery_institution;?>
                </td>
              </tr>
              <tr>
                <th>VIA  (Visual Inspection with Acetic Acid) Test</th>
                <td>
                  <?php echo $result->via;?>
                </td>
              </tr>
              <tr>
                <th>STI/RTI</th>
                <td>
                  <?php echo $result->sti_rti;?>
                </td>
              </tr>
              <tr>
                <th>Other</th>
                <td>
                  <?php echo $result->other;?>
                </td>
              </tr>


              <tr bgcolor="#ddd">
                <th colspan="2">Where Clients are referred</th>
              </tr>
              <tr>
                <th>1. CC</th>
                <td>
                  <?php echo $result->cc;?>
                </td>
              </tr>
              <tr>
                <th>2. FWC</th>
                <td>
                  <?php echo $result->fwc;?>
                </td>
              </tr>
              <tr>
                <th>3. UHC</th>
                <td>
                  <?php echo $result->uhc;?>
                </td>
              </tr>
              <tr>
                <th>4. MCWC</th>
                <td>
                  <?php echo $result->mcwc;?>
                </td>
              </tr>
              <tr>
                <th>5. District General Hospital</th>
                <td>
                  <?php echo $result->dgh;?>
                </td>
              </tr>
              <tr>
                <th>6. Marie Stopes Bangladesh (Clinic)</th>
                <td>
                  <?php echo $result->msbc;?>
                </td>
              </tr>
              <tr>
                <th>7. BAPSA</th>
                <td>
                  <?php echo $result->bapsa;?>
                </td>
              </tr>
              <tr>
                <th>8. RHSTEP</th>
                <td>
                  <?php echo $result->rhstep;?>
                </td>
              </tr>
              <tr>
                <th>9. FPAB</th>
                <td>
                  <?php echo $result->fpab;?>
                </td>
              </tr>
              <tr>
                <th>10. Other Clinics (NGO &amp; Private)  </th>
                <td>
                  <?php echo $result->ngo_private;?>
                </td>
              </tr>
              <tr>
                <th>11. Union Porishod</th>
                <td>
                  <?php echo $result->union_porishod;?>
                </td>
              </tr>
              </tbody>
            </table> 
          </div> 
          <div class="box-footer">
          </div>                
      </div>
  </div>
</div>
<?php
$this->load->view('./admin/footer-link');
$this->load->view('./admin/footer');
?>