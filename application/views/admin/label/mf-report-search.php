<?php 

$arr = array("1" => "Jan-Mar", "2" => "Apr-June", "3" => "July-Sept", "4" => "Oct-Dec", "5" => "Jan-Dec");

if(array_key_exists($quarter, $arr)) {

  $quarter_str = $arr[$quarter].' '.$year;

  $quarter_str_1 = '('.$arr[$quarter].') '.$year;

} else {

  $quarter_str = $arr['5'].' '.$year;

  $quarter_str_1 = '('.$arr['5'].') '.$year;

}

?>

<style type="text/css">

  .text-left-custom { text-align: left !important; }

  .table-different > thead > tr > th,

  .table-different > tbody > tr > th,

  .table-different > tfoot > tr > th,

  .table-different > thead > tr > td,

  .table-different > tbody > tr > td,

  .table-different > tfoot > tr > td {

    border-top: 1px solid #999;

  }

  .table-different {

    border: 1px solid #999 !important;

  }

  .table-different > thead > tr > th,

  .table-different > tbody > tr > th,

  .table-different > tfoot > tr > th,

  .table-different > thead > tr > td,

  .table-different > tbody > tr > td,

  .table-different > tfoot > tr > td {

    border: 1px solid #999 !important;

  }

</style> 

<table class="table table-bordered text-center table-different">

  <tbody>

  <tr bgcolor="#ddd">

    <th colspan="16">Master File: Activity Log</th>

  </tr>

  <tr bgcolor="#ddd">

    <th colspan="16">Quarter: <?php echo $quarter_str;?></th>

  </tr>

  <tr bgcolor="#ddd">

    <th rowspan="3" style="vertical-align: middle;">Description of the component</th>

    <th colspan="3">MSB</th>

    <th colspan="3">Shushilan</th>

    <th colspan="3">BAPSA</th>

    <th colspan="3">Phulki</th>    

    <th rowspan="2" style="vertical-align: middle;">Total Targets</th>

    <th rowspan="2" style="vertical-align: middle;">Total Achieve</th>

    <th rowspan="2" style="vertical-align: middle;">Total %</th>

  </tr>

  <tr bgcolor="#ddd">

    <th colspan="3"><?php echo $quarter_str_1;?></th>

    <th colspan="3"><?php echo $quarter_str_1;?></th>

    <th colspan="3"><?php echo $quarter_str_1;?></th>

    <th colspan="3"><?php echo $quarter_str_1;?></th>

  </tr>

  <tr bgcolor="#ddd">

    <th>Targets</th>

    <th>Achive</th>

    <th>Percent</th>

    <!-- <th>Remarks</th> -->

    <th>Targets</th>

    <th>Achive</th>

    <th>Percent</th>

    <!-- <th>Remarks</th> -->

    <th>Targets</th>

    <th>Achive</th>

    <th>Percent</th>

    <!-- <th>Remarks</th> -->

    <th>Targets</th>

    <th>Achive</th>

    <th>Percent</th>

    <!-- <th>Remarks</th> -->

    <th></th>

    <th></th>

    <th></th>

  </tr>

  <?php

  foreach ($result as $data){

  ?>

    <tr bgcolor="#eee">

      <th class="text-left-custom" colspan="20"><?php echo $data->label_name;?></th>

    </tr>

    <?php

    $qry2 = $this->db->query("SELECT * FROM `mf__labels` WHERE `parent_id` = '".$data->id."' AND `status` = '1' ORDER BY `order_id` ASC");

    $i = 0;

    foreach ($qry2->result() as $data2){      

      if($data2->is_head){ ?>

      <tr bgcolor="#ffe">

        <th class="text-left-custom" colspan="20"><?php echo $data2->label_name;?></th>

      </tr>

      <?php } else {

      ?>

      <tr bgcolor="#fff">

        <td class="text-left-custom"><?php echo $data2->label_name;?></td>

        <?php 

        $qry4 = $this->db->query("SELECT * FROM  `projects` WHERE `status` = '1'");

        $data4 = $qry4->result();

        $g_total_target  = 0;

        $g_total_achieve = 0;

        // $g_total_percent = 0;

        // $g_total_remarks = '';



        foreach ($data4 as $val){

          $qry3 = $this->db->query("SELECT * FROM `mf__labels_data` WHERE `year` = '".$year."' AND `quarter` IN (".$quarter.") AND `label_id` = '".$data2->id."' AND `project_id` = '".$val->id."' AND `status` = '1' ORDER BY `order_id`, `project_id` ASC, `entry_date` DESC");

          // if($this->session->userdata('id') == 1){
          //   echo "SELECT * FROM `mf__labels_data` WHERE `year` = '".$year."' AND `quarter` IN (".$quarter.") AND `label_id` = '".$data2->id."' AND `project_id` = '".$val->id."' AND `status` = '1' ORDER BY `order_id`, `project_id` ASC, `entry_date` DESC LIMIT 1"."<br>";
          // }

          if($qry3->num_rows() > 0){

            $target  = 0;

            $achieve = 0;

            $percent = 0;

            $remarks = '';

            $data3 = $qry3->result();

            // echo "<pre>";
            // print_r($data3);
            // echo "</pre>";


            foreach($data3 as $val1){

              $target  += $val1->target;

              $achieve += $val1->achieve;

              $percent += $val1->percent;

              // $remarks .= $val1->remarks;

            } ?>

            <td><?php echo $target;?></td>

            <td><?php echo $achieve?></td>

            <td><?php echo $percent?></td>

            <!-- <td><?php echo $remarks?></td> -->

          <?php 

          $g_total_target  += $target;

          $g_total_achieve += $achieve;

          // $g_total_percent += $percent;

          }else {?>

            <td></td>

            <td></td>

            <td></td>

            <!-- <td></td> -->

          <?php 

          }

        }

        $div = ($g_total_target != "") ? $g_total_target : 1;

        $g_total_percent  = ($g_total_achieve/$div) * 100;

        ?>

        <td><?php echo $g_total_target;?></td>

        <td><?php echo $g_total_achieve;?></td>

        <td><?php echo sprintf ("%.2f", $g_total_percent);?></td>       

      </tr>

      <?php

      $i++;

      }

    }

  }              

  ?>                    

  </tbody>

</table>