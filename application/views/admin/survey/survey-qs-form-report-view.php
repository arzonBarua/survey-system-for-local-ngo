<?php 
$data['title'] = 'View Survey Result';
$this->load->view('./admin/header', $data); 
?>
<!--Body Portin-->
<div class="row">
    <div class="col-md-8">
        <!-- <div class=""> -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title" style="text-align:center;display:block"><?php echo $survey_name ?></h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <?php if($result){ ?>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td colspan="3" style="text-align: center;"><b>উত্তরদাতা সম্পর্কিত তথ্য</b></td>
                                    </tr>
                                    <tr>
                                        
                                        <td style="width: 50%" colspan='2'>নাম</td>
                                        <td><?php echo $common_results[0]->ans_name?></td>
                                    </tr>
                                    <tr>
                                        
                                        <td colspan='2'>বয়স</td>
                                        <td><?php echo $common_results[0]->ans_age?></td>
                                    </tr>

                                    <tr>
                                        
                                        <td colspan='2'>উত্তরদাতার লিঙ্গ</td>
                                        <td><?php echo ($common_results[0]->ans_gender==1) ? 'পুরুষ' : 'মহিলা' ?></td>
                                    </tr>

                                    <?php if(!empty($common_results[0]->disname)){ ?>
                                        <tr>
                                            <td colspan='2'>জেলা</td>
                                            <td><?php echo $common_results[0]->disname ?></td>
                                        </tr>
                                    <?php } ?>  
                                    

                                    <?php if(!empty($common_results[0]->upname)){ ?>
                                    <tr>
                                        <td colspan='2'>উপজেলা</td>
                                        <td><?php echo $common_results[0]->upname ?></td>
                                    </tr>
                                    <?php } ?> 

                                    <?php if(!empty($common_results[0]->uniname)){ ?>
                                    <tr>
                                        <td colspan='2'>ইউনিয়ন</td>
                                        <td><?php echo $common_results[0]->uniname ?></td>
                                    </tr>
                                    <?php } ?> 

                                    <?php if(!empty($common_results[0]->wardname)){ ?>
                                    <tr>
                                        <td colspan='2'>গ্রাম / Ward</td>
                                        <td><?php echo $common_results[0]->wardname ?></td>
                                    </tr> 
                                    <?php } ?>


                                    <?php if(!empty($common_results[0]->name_garments)){ ?>
                                        <tr>
                                            <td colspan='2'>গার্মেন্টস এর নাম</td>
                                            <td><?php echo $common_results[0]->name_garments ?></td>
                                        </tr>
                                    <?php } ?>  
                                    

                                    <?php if(!empty($common_results[0]->department_name)){ ?>
                                    <tr>
                                        <td colspan='2'>কোন  বিভাগ কাজ করেন</td>
                                        <td><?php echo $common_results[0]->department_name ?></td>
                                    </tr>
                                    <?php } ?> 

                                    <?php if(!empty($common_results[0]->id_number)){ ?>
                                    <tr>
                                        <td colspan='2'>আইডি নাম্বার</td>
                                        <td><?php echo $common_results[0]->id_number ?></td>
                                    </tr>
                                    <?php } ?> 

                                    <?php if(!empty($common_results[0]->address_garments)){ ?>
                                    <tr>
                                        <td colspan='2'>গার্মেন্টস এর ঠিকানা</td>
                                        <td><?php echo $common_results[0]->address_garments ?></td>
                                    </tr> 
                                    <?php } ?>

                                    <tr>
                                        <td colspan='2'>মোবাইল নাম্বার</td>
                                        <td><?php echo $common_results[0]->mobile_number ?></td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan='2'>সহযোগী সংস্থার নাম</td>
                                        <td><?php echo $common_results[0]->projectName ?></td>
                                    </tr>  
                                    
                                    <tr>
                                        <td colspan='2'>সাক্ষাৎকার গ্রহিতার নাম</td>
                                        <td><?php echo $common_results[0]->appname ?></td>
                                    </tr> 

                                    <tr>
                                        <td colspan='2'>সাক্ষাৎকারের তারিখ</td>
                                        <td><?php echo date("d F, Y", strtotime($common_results[0]->created_at)); ?></td>
                                    </tr> 
                                    <tr>
                                        <td colspan="3" style="text-align: center;"><b>সাধারন তথ্য:</b></td>
                                    </tr>
                                    
                                    <tr>
                                      <th>#</th>
                                      <th>Question</th>
                                      <th>Question Answer</th>  
                                    </tr>
                                    
                                    <?php 
                                    $temp = 0;
                                    foreach($result as $key => $value){ 
                                        if($temp != $value->order_id){ ?>
                                            <tr>
                                                <td><?php echo $value->order_id ?></td>
                                                <td><?php echo $value->survey_qs ?></td>
                                                <td><?php echo !empty($value->option_value) ? $value->option_value : $value->review_ans ?></td>
                                            </tr>
                                            <?php $temp = $value->order_id;  
                                        }else{ ?>
                                            <tr>
                                                 <td><?php echo $value->order_id; ?></td>
                                                 <td>-</td>
                                                <td><?php echo !empty($value->option_value) ? $value->option_value : $value->review_ans ?></td>
                                            </tr>
                                        <?php } ?>    
                                    <?php 
                                    } ?>

                              </tbody>
                            </table>
                              
                        
                    <?php }else{ ?>
                        <div class="box">
                            <h3>No Data Found</h3>
                        </div>        
                    <?php  } ?>    
                </div>   
            </div><!-- /.box-body -->
        </div>         
    </div><!-- /.box-body -->
</div>



<?php $this->load->view('./admin/footer-link'); ?>


<?php $this->load->view('./admin/footer'); ?>
