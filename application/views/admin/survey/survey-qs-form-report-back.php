<?php 
$data['title'] = 'View Survey Result';
$this->load->view('./admin/header', $data); 
?>

<!--Body Portin-->
<div class="row">
    <?php if ($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">

            <?php
            if(validation_errors() || isset($error)){
                echo "<div class='alert alert-danger'>";
                echo validation_errors();
                echo isset($error) ? $error : "";
                echo "</div>";
            }

            echo form_open_multipart('admin/survey/survey_question_report'); ?>
            
            <div class="box-header">
                <h3 class="box-title">Survey Question Report</h3>
            </div>
            <div class="box-body">
<!--                <div class="box-body">-->
<!--                    <div class="form-group">-->
<!--                        <label>Select Questioner Form</label>-->
<!--                        <select class="form-control" name="survey_info_id" required >-->
<!--                            <option value="">--Select Type--</option>-->
<!--                            --><?php //foreach($result as $key => $val): ?>
<!--                                    <option value="--><?php //echo $val->id ?><!--" --><?php //echo ($survey_info_id==$val->id) ? 'selected' : '' ?><!-- >--><?php //echo $val->survey_name; ?><!--</option>-->
<!--                            --><?php //endforeach; ?>
<!--                        </select>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="box-body">-->
<!--                    <div class="form-group">-->
<!--                            <label>Project Name</label>-->
<!--                            <select class="form-control" name="projects_id" required >-->
<!--                                <option value="">--Select--</option>-->
<!--                                --><?php //foreach($projects_id as $key => $val): ?>
<!--                                        <option value="--><?php //echo $val->id ?><!--" --><?php //echo ($projects_ids==$val->id) ? 'selected' : '' ?><!-- >--><?php //echo $val->name; ?><!--</option>-->
<!--                                --><?php //endforeach; ?>
<!--                            </select>-->
<!--                    </div>-->
<!--                 </div>-->

                <table class="table table-bordered">
                    <tr>
                        <th width="20%">Select Questioner Form</th>
                        <td>
                            <select class="form-control" name="survey_info_id">
                                <option value="">--Select Type--</option>
                                <?php foreach($result as $key => $val): ?>
                                    <option value="<?php echo $val->id ?>" <?php echo ($survey_info_id==$val->id) ? 'selected' : '' ?> ><?php echo $val->survey_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th width="20%">Organization Name</th>
                        <td>
                            <select class="form-control projects" name="projects_id">
                                <option value="">--Select Organization--</option>
                                <?php foreach($projects_id as $key => $val): ?>
                                    <option value="<?php echo $val->id ?>" <?php echo ($projects_ids==$val->id) ? 'selected' : '' ?> ><?php echo $val->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th width="20%">Date Wise</th>
                        <td>
                            <!-- <input type="text" name="daterangepicker" class="datetimepicker form-control" /> -->
                            <div class="input-group">
                                <input type="text" class="form-control pull-right" id="reservation" name="daterangepicker" required>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Area Wise</th>
                        <td>
                            <label>District Name</label>
                            <select class="form-control union district call_ajax" name="district_id" id="upazila">
                                <option value=""> - SELECT - </option>
                                <?php
                                $qry = $this->db->query("SELECT * FROM `district` ORDER BY UPPER(`name`) ASC");
                                $result_1 = $qry->result();
                                foreach($result_1 as $key=> $val): ?>
                                    <option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
                                <?php endforeach; ?>
                            </select>

                            <label for="upazila_id">Upazilla Name</label>
                            <select class="form-control ward upazila call_ajax" name="upozila_id" id="union">
                                <option value=""> - SELECT -</option>
                            </select>

                            <label for="upazila_id">Union Name</label>
                            <select class="form-control ward-name union call_ajax" name="union_id" id="ward">
                                <option value=""> - SELECT -</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Surveyor Wise</th>
                        <td>
                            <select class="form-control" name="login_op_id">
                                <option value=""> - SELECT - </option>
                                <?php
                                //                                            $qry = $this->db->query("SELECT DISTINCT `users_id` FROM `meeting`");
                                //   
                               $acl_ext_query = "";
                                if(!empty($this->ids)){
                                   $acl_ext_query = " and app_user_info.id in (".implode(',',$this->ids).") ";
                                }

                                //echo "select app_user_info.id,app_user_info.`full_name`,app_user_info.`designation`, `projects`.`name` as project_name, admin_login.user_type, projects.id as project_id  from app_user_info join projects on app_user_info.project_id = projects.id left join admin_login on app_user_info.login_id = admin_login.id Where admin_login.user_type =5 ".$acl_ext_query."  order by projects.id asc";

                                $result_3 = $this->Post_model->custom_query("select app_user_info.id,app_user_info.`full_name`,app_user_info.`designation`, `projects`.`name` as project_name, admin_login.user_type, projects.id as project_id  from app_user_info join projects on app_user_info.project_id = projects.id left join admin_login on app_user_info.login_id = admin_login.id Where admin_login.user_type ='$_SESSION[user_type]' ".$acl_ext_query."  order by projects.id asc");

                                foreach($result_3 as $key=> $val){
                                     $app_user_type = 'PC';
                            if($val->project_id == 4){
                                $app_user_type = 'TO,CTO';
                            }
//                                                $qry_user = $this->db->query("SELECT `full_name` FROM `admin_login` WHERE `id` = '".$val->users_id."'");
//                                                $data_user = $qry_user->row(); ?>
                                    <option value="<?php echo $val->id; ?>"><?php echo $val->full_name.' | User type : '.$app_user_type.' | Organization : '.$val->project_name; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </table>

            </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Show Report</button>
                </div>
            </div><!-- /.box-body -->
            <?php echo form_close(); ?>

        </div>
          <div class="col-md-6">
        <div class="box box-primary">

            <?php
            if(validation_errors() || isset($error)){
                echo "<div class='alert alert-danger'>";
                echo validation_errors();
                echo isset($error) ? $error : "";
                echo "</div>";
            }

            echo form_open_multipart('admin/survey/survey_download'); ?>
            
            <div class="box-header">
                <h3 class="box-title">Download Survey Question Report</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th width="20%">Organization Name</th>
                        <td>
                            <select class="form-control projects_download" name="projects_id">
                                <option value="">--Select Organization--</option>
                                <?php foreach($projects_id as $key => $val): ?>
                                    <option value="<?php echo $val->id ?>" <?php echo ($projects_ids==$val->id) ? 'selected' : '' ?> ><?php echo $val->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th width="20%">Select Questioner Form</th>
                        <td>

                            <select class="form-control" name="survey_info_id"  >
                                <option value="">--Select Type--</option>
                                <?php foreach($result as $key => $val): ?>
                                    <option value="<?php echo $val->id ?>" <?php echo ($survey_info_id==$val->id) ? 'selected' : '' ?> ><?php echo $val->survey_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>    
                </table>

            </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Download</button>
                </div>
            </div><!-- /.box-body -->
            <?php echo form_close(); ?>

        </div>
    </div> 
      


                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Survey Question Report</h3>
                                <?php 
                                if( isset($search_detail) && count($search_detail) > 0){
                                    echo '[';
                                    if(isset($search_detail['from_date']) && $search_detail['from_date'] != "")
                                        echo '<b>&nbsp;&nbsp;Duration: </b> '.date("d, F Y", strtotime($search_detail['from_date'])).' - '.date("d, F Y", strtotime($search_detail['to_date']));

                                    if(isset($search_detail['survey_info_id']) && $search_detail['survey_info_id'] != ""){
                                        $qry = $this->db->query("SELECT `survey_name` FROM `survey_info` WHERE `id` = '".$search_detail['survey_info_id']."'");
                                        $data = $qry->row();
                                        echo "<b>&nbsp;&nbsp;Survey Name: </b>".$data->survey_name;
                                    }
                                    if(isset($search_detail['district_id']) && $search_detail['district_id'] != ""){
                                        $qry = $this->db->query("SELECT `name` FROM `district` WHERE `id` = '".$search_detail['district_id']."'");
                                        $data = $qry->row();
                                        echo "<b>&nbsp;&nbsp;District Name: </b>".$data->name;
                                    }

                                    if(isset($search_detail['upozila_id']) && $search_detail['upozila_id'] != ""){
                                        $qry = $this->db->query("SELECT `name` FROM `upazila` WHERE `id` = '".$search_detail['upozila_id']."'");
                                        $data = $qry->row();
                                        echo "<b>&nbsp;&nbsp;Upazila Name: </b>".$data->name;
                                    }

                                    if( isset($search_detail['union_id']) && $search_detail['union_id'] != ""){
                                        $qry = $this->db->query("SELECT `name` FROM `union` WHERE `id` = '".$search_detail['union_id']."'");
                                        $data = $qry->row();
                                        echo "<b>&nbsp;&nbsp;Union Name: </b>".$data->name;
                                    }

                                    if(isset($search_detail['projects_id']) && $search_detail['projects_id'] != ""){
                                        $qry = $this->db->query("SELECT `name` FROM `projects` WHERE `id` = '".$search_detail['projects_id']."'");
                                        $data = $qry->row();
                                        echo "<b>&nbsp;&nbsp;Project Name: </b>".$data->name;
                                    }

                                    if(isset($search_detail['login_op_id']) && $search_detail['login_op_id'] != ""){
                                        $qry = $this->db->query("SELECT `username` FROM `app_user_info` WHERE `id` = '".$search_detail['login_op_id']."'");
                                        $data = $qry->row();
                                        echo "<b>&nbsp;&nbsp;Surveyor Name: </b>".$data->username;
                                    }
                                    echo ' ]';
                                }
                                ?>
                                <!-- <a href="survey_download" class="btn btn-primary pull-right">Download</a> -->
                            </div>
                        <?php if($report_info != 0){ ?>
                            <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="custom">SL</th>
                                            <th>Surveyor</th>
                                            <th>Name of Respondent</th>
                                            <th>Survey Datetime</th>
                                            <th>Distritc Name</th>
                                            <th>Upazila Name</th>
                                            <th>Union Name</th>
                                            <th>Ward Name</th>
                                            <th class="custom_last">Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php
                                        $sl = 1;
                                        foreach ($report_info as $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo $sl; ?></td>
                                                <td><?php echo $value->username; ?></td>
                                                <td><?php echo $value->ans_name ?></td>
                                                <td><?php echo date("d F, Y", strtotime($value->created_at)); ?></td>
                                                <td><?php echo $value->disname ?></td>
                                                <td><?php echo $value->upaname  ?></td>
                                                <td><?php echo $value->unname ?></td>
                                                <td><?php echo $value->wardname ?></td>
                                                <td> 
                                                    <a href="survey_question_view/<?php echo $value->form_code ?>/<?php echo $value->survey_info_id ?>" title="view" target="_blank"><i class="fa fa-fw fa-eye"></i></a> /
                                                     <a href="survey_qs_edit/<?php echo $value->id ?>/<?php echo $value->survey_info_id ?>" title="edit"><i class="fa fa-fw fa-wrench"></i></a> /
                                                    <a href="survey_question_report_delete/<?php echo $value->form_code ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i>/

                                                    </a>
                                                </td>
                                            </tr>
                                            <?php $sl++;
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>

                        </div>
                        <!-- /.box -->
                    </div>
                </div>


        <?php }else{ ?>
            <div class="box-body">
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            </div>
        <?php  } ?>
       

    <!-- </div> --><!-- /.box -->

<!--End Body Portion-->


<?php $this->load->view('./admin/footer-link'); ?>



<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
         var option_value = <?php echo ($this->session->userdata('project_id')) ? $this->session->userdata('project_id') : 'null' ;  ?>;
         var base = "<?php echo base_url().'admin/' ?>";
    //alert(option_value);
        if(option_value != 'null'){
            $('.projects>option:eq('+option_value+')').attr('selected', true).siblings().attr('disabled',true);
            $('.projects_download>option:eq('+option_value+')').attr('selected', true).siblings().attr('disabled',true);
        }
        //Timepicker
        
        $('.overlay').hide();


        $(".timepicker").timepicker({
            showInputs: false
        });

        $(".datepicker").datepicker();

        $(".datepicker").on("change",function(e){
           $(this).datepicker('hide');
        });


        $(".call_ajax").on("change", function (e) {

            e.preventDefault();
            var id = $(this).val();
            var tableName = $(this).attr('id');
            var classNameArr = $(this).attr('class');
            var className = classNameArr.split(" ");


            $.ajax({
                url: '<?php echo base_url()."admin/ajax/" ?>get_ajax',
                data: {'id': id,'table' : tableName, 'field': className[2]},
                type: "post",
                dataType: 'text',
                success: function (data) {
                    $("#"+className[1]).html(data);
                }
            });
            return false;
        });

        /*----Form SUbmission----*/
        //  $( "#form_report" ).submit(function( event ) {
        //     event.preventDefault();
        //     $('.overlay').show();
           
        //     $.ajax({
        //         type: "POST",
        //         url:  "ajax_survey_qs_report_show",
        //         data: $("#form_report").serialize(),
        //         dataType: "text"
        //     }).done(function( data ){
        //        $('.overlay').hide();
        //        $('#table-report').html(data);
        //     });

        // });
        // $("#example1").DataTable({
        //     "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        //     initComplete: function () {
        //         this.api().columns([1,3,4,5,6]).every(function () {
        //             var column = this;
        //             var select = $('<select class="form-control"><option value="">-Select-</option></select>')
        //                 .appendTo($(column.footer()).empty())
        //                 .on('change', function () {
        //                     var val = $.fn.dataTable.util.escapeRegex(
        //                         $(this).val()
        //                     );

        //                     column
        //                         .search(val ? '^' + val + '$' : '', true, false)
        //                         .draw();
        //                 });

        //             column.data().unique().sort().each(function (d, j) {
        //                 select.append('<option value="' + d + '">' + d + '</option>')
        //             });
        //         });
        //     }
        // });

         $("#example1").DataTable({
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 8 ] }
            ],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": base+"ajax/data_table_survey",
            "order": [[ 3, "desc" ]]
        });

        
        // $("#example1").DataTable({
        //     "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        //      "aoColumnDefs": [
        //         { 'bSortable': false, 'aTargets': [ 10 ] }
        //     ],
        //     // "bProcessing": true,
        //     // "bServerSide": true,
        //     //"sAjaxSource": base+"ajax/data_table_survey",
        //     // "order": [[ 3, "desc" ]]
        // });

        $("#reservation").daterangepicker();



    });
</script>
<?php $this->load->view('./admin/footer'); ?>
