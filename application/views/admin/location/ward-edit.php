<?php 
$data['title'] = 'Edit Ward';
$this->load->view('./admin/header', $data); 
?>

<!--Body Portin-->
<div class="row">
<div class="col-md-6">
    <div class="box box-primary">

        <?php
        if(validation_errors() || isset($error)){
            echo "<div class='alert alert-danger'>";
            echo validation_errors();
            echo isset($error) ? $error : "";
            echo "</div>";
        }

        echo form_open_multipart('admin/location/ward_edit/'.$result[0]->id); ?>
        <div class="box-header">
            <h3 class="box-title">Ward Information</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="district_name">Division Name</label>
                <select class="form-control call_ajax" name="div_id" table="district-div">
                    <option value=""> Select </option>
                    <?php 
                    foreach ($division as $key => $value) {
                        $sel = ($value->id == $result[0]->div_id) ? 'selected=""' : '';
                        echo '<option '.$sel.' value="'.$value->id.'">'.$value->division_name .'</option>';
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label for="name">District Name</label>
                <select class="form-control call_ajax" name="district_id" id="district" table="upazila-district">
                    <option value=""> Select </option>
                    <?php 
                    foreach ($district as $key => $value) {
                        $sel = ($value->id == $result[0]->district_id) ? 'selected=""' : '';
                        echo '<option '.$sel.' value="'.$value->id.'">'.$value->name .'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Upazila Name</label>
                <select class="form-control call_ajax" name="upazila_id" id="upazila" table="union-upazila">
                    <option value=""> Select </option>
                    <?php 
                    foreach ($upazila as $key => $value) {
                        $sel = ($value->id == $result[0]->upazila_id) ? 'selected=""' : '';
                        echo '<option '.$sel.' value="'.$value->id.'">'.$value->name .'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Union Name</label>
                <select class="form-control" name="union_id" id="union" table="union">
                    <option value=""> Select </option>
                    <?php 
                    foreach ($union as $key => $value) {
                        $sel = ($value->id == $result[0]->union_id) ? 'selected=""' : '';
                        echo '<option '.$sel.' value="'.$value->id.'">'.$value->name .'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Ward Name</label>
                <input type="text" name="name" class="form-control" placeholder="Enter Ward Name"  value="<?php echo (set_value('name') ? set_value('name') : $result[0]->name);?>" autocomplete="off" required>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>


    </div><!-- /.box -->
</div>
    </div>
<!--End Body Portion-->
<?php $this->load->view('./admin/footer-link'); ?>

<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $(".call_ajax").on("change", function (e) {
            e.preventDefault();
            var id = $(this).val();
            var table_name = $(this).attr('table');
            var parameter = table_name.split("-");

            $.ajax({
                url: "<?php echo base_url().'admin/ajax/get_ajax';?>",
                data: {'id': id, 'table': parameter[0], 'field': parameter[1]},
                type: "post",
                dataType: 'text',
                success: function (data) {
                    $("#"+parameter[0]).html(data);
                }
            });
            return false;
        });
    });
</script>

<?php $this->load->view('./admin/footer'); ?>
