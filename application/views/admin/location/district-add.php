<?php 
$data['title'] = 'Add District';
$this->load->view('./admin/header', $data); 
?>

<!--Body Portin-->
<div class="row">
<div class="col-md-6">
    <div class="box box-primary">

        <?php
        if(validation_errors() || isset($error)){
            echo "<div class='alert alert-danger'>";
            echo validation_errors();
            echo isset($error) ? $error : "";
            echo "</div>";
        }

        echo form_open_multipart('admin/location/district_add'); ?>
        <div class="box-header">
            <h3 class="box-title">District Information</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="district_name">Division Name</label>
                <select class="form-control" name="div_id">
                    <option value=""> Select </option>
                    <?php 
                    foreach ($division as $key => $value) {
                    echo '<option value="'.$value->id.'">'.$value->division_name .'</option>';
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label for="name">District Name</label>
                <input type="text" name="name" class="form-control" placeholder="Enter District Name" autocomplete="off" required>
            </div>

            <div class="form-group">
                <label for="status">Status</label><br/>
                <input type="radio" name="status" value="1" checked="" required />&nbsp;Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="status" value="0" required />&nbsp;Inactive
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>


    </div><!-- /.box -->
</div>
    </div>
<!--End Body Portion-->
<?php $this->load->view('./admin/footer-link'); ?>

<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

<?php $this->load->view('./admin/footer'); ?>
