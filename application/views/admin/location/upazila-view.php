<?php 
$data['title'] = 'View Upazila';
$this->load->view('./admin/header', $data); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box" style="width: 60%;">
            <div class="box-header">
                <h3 class="box-title">Upazila Information</h3>
            </div>
            <?php if($result == 0){ ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } else {
                if($this->session->flashdata('success_message')): ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php endif; 
            }?>

            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="custom">Sl</th>
                            <th>Division Name</th>
                            <th>District Name</th>
                            <th>Upazila Name</th>
                            <!-- <th>Status</th> -->
                            <th class="custom_last">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sl = 1;
                    foreach ($result as $value) {
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $value->division_name; ?></td>
                            <td><?php echo $value->district_name; ?></td>
                            <td><?php echo $value->name; ?></td>
                            <!-- <td><?php echo ($value->status) ? "Active" : "Inactive";?></td> -->
                            <td>    
                                <a href="upazila_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                <a href="upazila_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')"
                                   title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <?php 
                        $sl++;
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

<?php $this->load->view('./admin/footer-link') ?>
<?php $this->load->view('./admin/footer'); ?>
