<?php 
$data['title'] = 'View Union';
$this->load->view('./admin/header', $data); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box" style="width: 60%;">
            <div class="box-header">
                <h3 class="box-title">Union Information</h3>
            </div>
            <?php if($result == 0){ ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } else {
                if($this->session->flashdata('success_message')): ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php endif; 
            }?>

            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th class="custom">Sl</th>
                            <th>Division Name</th>
                            <th>District Name</th>
                            <th>Upazila Name</th>
                            <th>Union Name</th>
                            <!-- <th>Status</th> -->
                            <th class="custom_last">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sl = 1;
                    foreach ($result as $value) {
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $value->division_name; ?></td>
                            <td><?php echo $value->district_name; ?></td>
                            <td><?php echo $value->upazila_name; ?></td>
                            <td><?php echo $value->name; ?></td>
                            <!-- <td><?php echo ($value->status) ? "Active" : "Inactive";?></td> -->
                            <td>    
                                <a href="union_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                <a href="union_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')"
                                   title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <?php 
                        $sl++;
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

<?php $this->load->view('./admin/footer-link') ?>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">

$(function () {
       $("#example1").DataTable({
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        initComplete: function () {
            this.api().columns([1,3]).every(function () {
                var column = this;
                var select = $('<select class="form-control"><option value="">-Select-</option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
});
</script>
<?php $this->load->view('./admin/footer'); ?>
