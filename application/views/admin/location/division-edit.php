<?php 
$data['title'] = 'Edit Division';
$this->load->view('./admin/header', $data); 
?>

<!--Body Portin-->
<div class="row">
<div class="col-md-6">
    <div class="box box-primary">

        <?php
        if(validation_errors() || isset($error)){
            echo "<div class='alert alert-danger'>";
            echo validation_errors();
            echo isset($error) ? $error : "";
            echo "</div>";
        }

        echo form_open_multipart('admin/location/division_edit/'.$result[0]->id); ?>
        <div class="box-header">
            <h3 class="box-title">Division Information</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="meeting_name">Division Name</label>
                <input type="text" name="division_name" class="form-control" id="division_name" value="<?php echo (set_value('division_name') ? set_value('division_name') : $result[0]->division_name); ?>" placeholder="Enter Division Name" autocomplete="off" required>
            </div>

            <div class="form-group">
                <label for="status">Status</label><br/>
                <?php $val = (set_value('status') ? set_value('status') : $result[0]->status); ?>
                <input type="radio" name="status" value="1" <?php echo ($val == '1' ? 'checked=""' : '');?> required />&nbsp;Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="status" value="0" <?php echo ($val == '0' ? 'checked=""' : '');?> required />&nbsp;Inactive
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>


    </div><!-- /.box -->
</div>
    </div>
<!--End Body Portion-->
<?php $this->load->view('./admin/footer-link'); ?>

<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

<?php $this->load->view('./admin/footer'); ?>
