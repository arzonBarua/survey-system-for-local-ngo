<?php $this->load->view('admin/login/header'); ?>
    <div class="login-logo">
        <b>Management Information System (MIS)</b> <br>for Nirapod-2
    </div><!-- /.login-logo -->
<div class="login-box">


    <div class="login-box-body">
        <p class="login-box-msg">Please Sign In</p>
        <?php if($this->session->flashdata('login_failed')): ?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('login_failed'); ?>
            </div>
        <?php endif; ?>
        <?php echo form_open('admin/user/form_process'); ?>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo isset($email) ? $email : '' ?>" required/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" value="<?php echo isset($password) ? $password : '' ?>" placeholder="Password" required/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <?php //if(isset($captcha)): ?>
               <!--  <div class="form-group has-feedback captcha">
                    <div class="g-recaptcha" data-sitekey="<?php //echo $captcha; ?>"></div>
                </div> -->
            <?php //endif; ?>

            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="checkbox" <?php echo isset($email) ? 'checked' : '' ?>> Remember Me
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div><!-- /.col -->
            </div>
        <?php echo form_close(); ?>
        <!-- <a href="#">I forgot my password</a><br> -->
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
<?php $this->load->view('admin/login/footer'); ?>