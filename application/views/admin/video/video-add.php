<?php 
$data['title'] = 'Add Video';
$this->load->view('./admin/header', $data); 
?>

    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Video Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                echo form_open_multipart('admin/video/video_add'); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label>User Name</label>
                            <select class="form-control margin-left" name="user_id" id="login_op_id" required>
                                <option value="">--Select Drop Down--</option>
                                <?php foreach($op_ids as $key=>$val){ 
                                    $app_user_type = 'PC';
                                    if($val->project_id == 4){
                                        $app_user_type = 'TO,CTO';
                                    }
                                    ?>
                                    <option value="<?php echo $val->id ?>"><?php echo $val->full_name.' | User type : '.$app_user_type.' | Organization : '.$val->project_name; ?></option>
                                <?php } ?>    
                            </select>    
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Video Title</label>
                            <input type="text" name="video_title" class="form-control" id="video_title" placeholder="Video Title goes here" autocomplete="off" value="<?php echo set_value('video_title'); ?>" required />
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Video Date</label>
                            <input type="text" name="video_date" class="form-control datepicker" id="meeting_date" placeholder="MM/DD/YYYY" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputFile">Video File input</label>
                            <input type="file" name="userfile" id="userfile">
                            <p class="help-block">Supported formats are: AVI / MPEG / MP4 / 3GP</p>
                        </div>

					</div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->
        </div>
    </div>
<?php $this->load->view('./admin/footer-link'); ?>
<script type="text/javascript">
    $(function () {
        $(".datepicker").datepicker();
    });
</script>
<?php $this->load->view('./admin/footer'); ?>
