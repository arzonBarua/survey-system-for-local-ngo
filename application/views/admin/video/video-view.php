<?php 
$data['title'] = 'View Video';
$this->load->view('./admin/header', $data); 
?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Table Video View</h3>
                </div><!-- /.box-header -->



                <?php if($result === false){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>

                    <?php if($this->session->flashdata('success_message')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="custom">Sl</th>
                                <th>User Name</th>
								<th>Video</th>
                                <th>Video Title</th>
                                <th>Video Date</th>
                                <th class="custom_last">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sl = 1;
                            foreach($result as $value){
                                $qry = $this->db->query("SELECT `username` FROM `app_user_info` WHERE `id` = '".$value->users_id."'");
                                $data = $qry->row();
                                ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $data->username; ?></td>
                                    <td>
                                    <?php if(!empty($value->video_name)): ?>
                                    <a target="_blank" href="<?php echo base_url() ?>assets/upload/video/<?php echo $value->video_name;?>"><i class="fa fa-2x fa-file-video-o" aria-hidden="true"></i></a>
                                    <?php endif; ?>
                                    </td>
                                    <td><?php echo $value->video_title; ?></td>
                                    <td><?php echo date('d F, Y', strtotime($value->video_date) );?></td>
                                    <td>                                    
                                        <a href="video_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                        <a href="video_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $sl++; } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                <?php } ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php $this->load->view('./admin/footer-link') ?>

    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").dataTable();
        });
    </script>


<?php $this->load->view('./admin/footer'); ?>
