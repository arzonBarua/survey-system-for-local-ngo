<table class="table table-bordered table-striped">
    <tbody>
    
            <tr>
                <td>Meeting Name</td>
                <td><?php echo $results[0]->meeting_name; ?></td>
            </tr>
            <tr>
                <td>User Name</td>
                <td><?php echo $results[0]->username; ?></td>
            </tr>
            <tr>
                <td>No of Participation (Start Meeting)</td>
                <td><?php echo $results[0]->no_of_participation; ?></td>
            </tr>
            <tr>
                <td>No of Participation (End Meeting)</td>
                <td><?php echo $results[0]->no_of_participation_end; ?></td>
            </tr>
            <tr>
                <td>District Name</td>
                <td><?php echo $results[0]->district_name; ?></td>
            </tr>
            <tr>
                <td>Upazila Name</td>
                <td><?php echo $results[0]->upazila_name; ?></td>
            </tr>
            <tr>
                <td>Union Name</td>
                <td><?php echo $results[0]->union_name; ?></td>
            </tr>
            <tr>
                <td>Ward Name</td>
                <td><?php echo $results[0]->ward_name; ?></td>
            </tr>
            <tr>
                <td>Meeting Start Time</td>
                <td><?php echo date("d F Y, h:i:s A", strtotime($results[0]->meeting_start_time)); ?></td>
            </tr>
            <tr>
                <td>Meeting End Time</td>
                <td><?php echo date("d F Y, h:i:s A", strtotime($results[0]->meeting_end_time)); ?></td>
            </tr>
            <tr>
                <td>Start Meeting Picture time</td>
                <td><?php echo date("d F Y, h:i:s A", strtotime($results[0]->start_picture_time)); ?></td>
            </tr>
            <tr>
                <td>End Meeting Picture Time</td>
                <td><?php echo date("d F Y, h:i:s A", strtotime($results[0]->end_picture_time)); ?></td>
            </tr>

            <tr>
                <td>Duration</td>
                <td><?php echo $this->Common_operation->get_time_duration($results[0]->meeting_end_time,$results[0]->start_picture_time); ?></td>
                <!-- <td><?php //echo $value->duration." Hour"; ?></td> -->
            </tr>
            <?php if(!empty($results[0]->meeting_start_picture_name)): ?>
                <tr>
                    <td>Meeting Start Picture</td>
                    <td><img src="<?php echo base_url() ?>assets/upload/meeting/<?php echo $results[0]->meeting_start_picture_name; ?>" style="width:340px"/></td>
                </tr>
            <?php endif; ?>
            <?php if(!empty($results[0]->meeting_start_picture_end)): ?>
                <tr>
                    <td>Meeting End Picture</td>
                    <td><img src="<?php echo base_url() ?>assets/upload/meeting/<?php echo $results[0]->meeting_start_picture_end; ?>" style="width:340px"/></td>
                </tr>
            <?php endif; ?>
    
    </tbody>
</table>