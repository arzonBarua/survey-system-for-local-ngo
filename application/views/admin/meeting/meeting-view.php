<?php 
$data['title'] = 'View Meeting';
$this->load->view('./admin/header', $data); 


?>
<div class="row">
    <div class="col-md-12">
        <div class="box" style="width: 60%;">
            <div class="box-header">
                <h3 class="box-title">Search Meeting Information</h3>
            </div>
            <div class="box-body">
                <?php echo form_open_multipart('admin/meeting/meeting_view');?>
                <table class="table table-bordered">
                    <tr>
                        <th width="20%">Date Wise</th>
                        <td>
                            <!-- <input type="text" name="daterangepicker" class="datetimepicker form-control" /> -->
                            <div class="input-group">
                                <input type="text" class="form-control pull-right" id="reservation" name="daterangepicker" >
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>                              
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Area Wise</th>
                        <td>
                            <label>District Name</label>
                            <select class="form-control union district call_ajax" name="district_id" id="upazila">
                                <option value=""> - SELECT - </option>
                                <?php 
                                $qry = $this->db->query("SELECT * FROM `district` ORDER BY UPPER(`name`) ASC");
                                $result_1 = $qry->result();
                                foreach($result_1 as $key=> $val): ?>
                                    <option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
                                <?php endforeach; ?>
                            </select>

                            <label for="upazila_id">Upazilla Name</label>
                            <select class="form-control ward upazila call_ajax" name="upozila_id" id="union">
                                <option value=""> - SELECT -</option>
                            </select>

                            <label for="upazila_id">Union Name</label>
                            <select class="form-control ward-name union call_ajax" name="union_id" id="ward">
                                <option value=""> - SELECT -</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Type Wise</th>
                        <td>
                            <select class="form-control" name="type_name" >
                                <option value=""> - SELECT - </option>
                                <?php 
                                $qry = $this->db->query("SELECT * FROM `meeting_type` WHERE `meeting_type` != '' ORDER BY UPPER(`meeting_type`) ASC");
                                $result_2 = $qry->result();
                                foreach($result_2 as $key=> $val): ?>
                                    <option value="<?php echo $val->meeting_type; ?>"><?php echo $val->meeting_type; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
            

                    <tr>
                        <th>Surveyor Wise</th>
                        <td>
                            <select class="form-control" name="user_name">
                                <option value=""> - SELECT - </option>
                                <?php
                                
                               $acl_ext_query = "";
                                if(!empty($this->ids)){
                                   $acl_ext_query = " and app_user_info.id in (".implode(',',$this->ids).") ";
                                }

                                $result_3 = $this->Post_model->custom_query("select app_user_info.id,app_user_info.`full_name`,app_user_info.`designation`, `projects`.`name` as project_name, admin_login.user_type, projects.id as project_id  from app_user_info join projects on app_user_info.project_id = projects.id left join admin_login on app_user_info.login_id = admin_login.id Where admin_login.user_type ='$_SESSION[user_type]' ".$acl_ext_query."  order by projects.id asc");

                                foreach($result_3 as $key=> $val){
                                     $app_user_type = 'PC';
                                        if($val->project_id == 4){
                                            $app_user_type = 'TO,CTO';
                                        }
//                                                $qry_user = $this->db->query("SELECT `full_name` FROM `admin_login` WHERE `id` = '".$val->users_id."'");
//                                                $data_user = $qry_user->row(); ?>
                                    <option value="<?php echo $val->id; ?>"><?php echo $val->full_name.' | User type : '.$app_user_type.' | Organization : '.$val->project_name; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <th></th>
                        <td>
                            <input type="submit" name="search" class="btn btn-primary" value="Search" />
                        </td>
                    </tr>
                </table>
                <?php echo form_close(); ?>
            </div>
        </div>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    Meeting Information 
                </h3>
                <?php 
                if( isset($search_detail) && count($search_detail) > 0){
                    echo '[';
                    if(isset($search_detail['from_date']) && $search_detail['from_date'] != "")
                        echo '<b>&nbsp;&nbsp;Duration: </b> '.date("d, F Y", strtotime($search_detail['from_date'])).' - '.date("d, F Y", strtotime($search_detail['to_date']));
                    if(isset($search_detail['district_id']) && $search_detail['district_id'] != ""){
                        $qry = $this->db->query("SELECT `name` FROM `district` WHERE `id` = '".$search_detail['district_id']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;District Name: </b>".$data->name;
                    }

                    if(isset($search_detail['upozila_id']) && $search_detail['upozila_id'] != ""){
                        $qry = $this->db->query("SELECT `name` FROM `upazila` WHERE `id` = '".$search_detail['upozila_id']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;Upazila Name: </b>".$data->name;
                    }

                    if( isset($search_detail['union_id']) && $search_detail['union_id'] != ""){
                        $qry = $this->db->query("SELECT `name` FROM `union` WHERE `id` = '".$search_detail['union_id']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;Union Name: </b>".$data->name;
                    }

                    if(isset($search_detail['type_name']) && $search_detail['type_name'] != ""){
                        $qry = $this->db->query("SELECT `meeting_type` FROM `meeting_type` WHERE `meeting_type` = '".$search_detail['type_name']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;Meeting Type: </b>".$data->meeting_type;
                    }

                    if(isset($search_detail['user_name']) && $search_detail['user_name'] != ""){
                        $qry = $this->db->query("SELECT `username`, `full_name` FROM `app_user_info` WHERE `id` = '".$search_detail['user_name']."'");
                        $data = $qry->row();
                        echo "<b>&nbsp;&nbsp;Surveyor Name: </b>".$data->full_name;
                    }
                    echo ' ]';
                }
                ?>
                <a href="meeting_download" class="btn btn-primary pull-right">Download</a>
            </div>
            <!-- /.box-header -->


            <?php if ($result === false) { ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } else { ?>

                <?php if ($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>

                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                       <thead>
                            <tr>
                                <th>SL</th>
                                <th>Device</th>
                                <th>Meeting Name</th>
                                <th>Meeting Start Time</th>
                                <th>Meeting End Time</th>
                                <th>Username</th>
                                <th>No of participation (Start)</th>
                                <th>No of participation (End)</th>
                                <th>District Name</th>
                                <th>Upazila Name</th>
                                <th>Union Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5" class="dataTables_empty">Loading data from server</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>SL</th>
                                <th>Device</th>
                                <th>Meeting Name</th>
                                <th>Meeting Start Time</th>
                                <th>Meeting End Time</th>
                                <th>Username</th>
                                <th>No of participation (Start)</th>
                                <th>No of participation (End)</th>
                                <th>District Name</th>
                                <th>Upazila Name</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        </table>
                </div>
            <?php } ?>
        </div>
        <!-- /.box -->
    </div>
</div>


<div class="modal fade" role="dialog" id="myModal">
    <div class="modal-dialog">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Meeting Information</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('./admin/footer-link') ?>

<!-- <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script> -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    base = "<?php echo base_url().'admin/' ?>";

    function modal_show(meeting_id){
        $("#myModal .modal-body").html("");
        $('#myModal').modal('show');
         //$("#myModal .modal-body").html("<div style='margin: auto; width: 400px'><img src='"+base+"assets/img/loading.gif'/></div>");
        $("#myModal .modal-body").load(base+'ajax/get_meeting_info/'+meeting_id);
    }

    $(function () {

       

        $("#example1").DataTable({
            "lengthMenu": [[25, 50, 100], [25, 50, 100]],
             "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 11 ] }
            ],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": base+"ajax/data_table",
            "order": [[ 3, "desc" ]]
        });

        $(".call_ajax").on("change", function (e) {

            e.preventDefault();
            var id = $(this).val();
            var tableName = $(this).attr('id');
            var classNameArr = $(this).attr('class');
            var className = classNameArr.split(" ");

            //alert('show');
            
            /*$.post("get_ajax", { 'id': id,'table' : tableName, 'field': className[2] }, function(data){
                // $("#"+className[1]).html(data);
                console.log(data);
            });*/

            $.ajax({
                //url: 'get_ajax',
                url: '<?php echo base_url()."admin/ajax/" ?>get_ajax',
                data: {'id': id,'table' : tableName, 'field': className[2]},
                type: "post",
                dataType: 'text',
                success: function (data) {
                    //console.log(data);
                    $("#"+className[1]).html(data);
                }
            });
            return false;
        });


        // $(".modal_show").on('click',function(){
        //     alert('show');
        // });


        $("#reservation").daterangepicker();
    });
</script>


<?php $this->load->view('./admin/footer'); ?>
