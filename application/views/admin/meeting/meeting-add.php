<?php 
$data['title'] = 'Add Meeting';
$this->load->view('./admin/header', $data); 

$arrayProjectName = $this->Common_operation->get_user_type();
?>

<!--Body Portin-->
<div class="row">
<div class="col-md-6">
    <div class="box box-primary">

        <?php
        if(validation_errors() || isset($error)){
            echo "<div class='alert alert-danger'>";
            echo validation_errors();
            echo isset($error) ? $error : "";
            echo "</div>";
        }

        echo form_open_multipart('admin/meeting/meeting_add'); ?>
        <div class="box-header">
            <h3 class="box-title">Meeting Information</h3>
        </div>
        <div class="box-body">

            <div class="form-group">
                <label>District Name</label>
                <select class="form-control union district call_ajax" name="district_id" id="upazila" required >
                    <option value="">--Select District--</option>
                    <?php foreach($result as $key=> $val): ?>
                        <option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group">
                <label for="upazila_id">Upazilla Name</label>
                <select class="form-control ward upazila call_ajax" name="upozila_id" id="union" required="">
                    <option value=""> - SELECT -</option>
                </select>
            </div>

            <div class="form-group">
                <label for="upazila_id">Union Name</label>
                <select class="form-control ward-name union call_ajax" name="union_id" id="ward" required="">
                    <option value=""> - SELECT -</option>
                </select>
            </div>

            <div class="form-group">
                <label for="ward">Ward</label>
                <select class="form-control" name="word_id" id="ward-name" required="">
                    <option value=""> - SELECT -</option>
                </select>
            </div>


            <div class="form-group">
                <label for="meeting_name">Meeting Name</label>
                 <select class="form-control" name="meeting_name" >
                        <option value=""> - SELECT - </option>
                        <?php $qry = $this->db->query("SELECT * FROM `meeting_type` WHERE `meeting_type` != '' ORDER BY UPPER(`meeting_type`) ASC");
                                    $result_2 = $qry->result();
                                    foreach($result_2 as $key=> $val): ?>
                                        <option value="<?php echo $val->meeting_type; ?>"><?php echo $val->meeting_type; ?></option>
                        <?php endforeach; ?>
                 </select>        
            </div>


            <div class="form-group">
                <label for="no_of_participation">Number of Participation (Start Meeting)</label>
                <input type="number" name="no_of_participation" class="form-control" id="no_of_participation" placeholder="Number of participent" autocomplete="off" required>
            </div>

            <div class="form-group">
                <label for="no_of_participation">Number of Participation (End Meeting)</label>
                <input type="number" name="no_of_participation_end" class="form-control" id="no_of_participation_end" placeholder="Number of participent" autocomplete="off" required>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Meeting Date</label>
                <input type="text" name="meeting_date" class="form-control datepicker" id="meeting_date" placeholder="MM/DD/YYYY" autocomplete="off" required>
            </div>

            <div class="bootstrap-timepicker">
                <div class="form-group">
                    <label>Meeting Start time:</label>
                    <div class="input-group">
                        <input type="text" class="form-control timepicker" name="start_time" placeholder="hr/min/AM or PM"/>
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                    </div><!-- /.input group -->
                </div><!-- /.form group -->
            </div>
            <div class="bootstrap-timepicker">
                <div class="form-group">
                    <label>Meeting End time:</label>
                    <div class="input-group">
                        <input type="text" class="form-control timepicker" name="end_time" placeholder="hr/min/AM or PM"/>
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                    </div><!-- /.input group -->
                </div><!-- /.form group -->
            </div>



            <div class="form-group">
                <label for="meeting_start_picture_name">Meeting Start Picture</label>
                <input type="file" name="meeting_start_picture_name" id="meeting_start_picture_name" required>
            </div>

            <div class="form-group">
                <label for="meeting_start_picture_end">Meeting End Picture</label>
                <input type="file" name="meeting_start_picture_end" id="meeting_start_picture_end" required>
            </div>

            <div class="form-group">
                <label>Project coordinator</label>
                <select class="form-control margin-left" name="users_id" id="users_id" required>
                    <option value="">--Select Drop Down--</option>
                    <?php foreach($op_ids as $key=>$val){
                            //$app_user_type = 'PC';
                            $user_type = $this->Common_operation->get_user_type();

                            if(($val->user_type==4) || ($val->user_type==3) ){
                                $app_user_type = $user_type[$val->project_id][$val->user_type];
                            }elseif($val->user_type == 5){
                                 $app_user_type = "PC";
                            }elseif($val->project_id == 4){
                                $app_user_type = 'TO,CTO';
                            }
                            
                    ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->full_name.' | User type : '.$app_user_type.' | Organization : '.$val->project_name; ?></option>
                    <?php 

                    } ?>
                </select>
            </div>


            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>


    </div><!-- /.box -->
</div>
    </div>
<!--End Body Portion-->


<?php $this->load->view('./admin/footer-link'); ?>



<!--Date picker time range-->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>




<script type="text/javascript">
    $(function () {
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });

//        $('.timepicker').val('');

        $(".datepicker").datepicker();

        $(".datepicker").on("change",function(e){
           $(this).datepicker('hide');
        });


        $(".call_ajax").on("change", function (e) {

            e.preventDefault();
            var id = $(this).val();
            var tableName = $(this).attr('id');
            var classNameArr = $(this).attr('class');
            var className = classNameArr.split(" ");


            $.ajax({
                 url: '<?php echo base_url()."admin/ajax/" ?>get_ajax',
                data: {'id': id,'table' : tableName, 'field': className[2]},
                type: "post",
                dataType: 'text',
                success: function (data) {
                    $("#"+className[1]).html(data);
                }
            });
            return false;
        });
    });
</script>
<?php $this->load->view('./admin/footer'); ?>
