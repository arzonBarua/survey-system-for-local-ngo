<?php 
$data['title'] = 'View User';
$this->load->view('./admin/header', $data); 

$arrayProjectName = $this->Common_operation->get_user_type();

// echo "<pre>";
// print_r($arrayProjectName);
// echo "</pre>";
// die;


?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Table User View</h3>
                </div><!-- /.box-header -->



                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>

                    <?php if($this->session->flashdata('success_message')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="custom">SL</th>
                                <th>Organization Name</th>
                                <th>User Name</th>
                                <th>User Type</th>
                                <th>Email</th>
                                <th class="custom_last">Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                            <tbody>
                            <?php $sl = 1;
                            foreach($result as $value){ ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value->project_name; ?></td>
                                    <td><?php echo $value->full_name; ?></td>
                                    <td><?php echo $arrayProjectName[$value->project_id][$value->user_type]; ?></td>
                                    <td><?php echo $value->email ?></td>
                                    <td>
                                        <div class="modal fade" id="<?php echo $value->id ?>" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">View User</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-hover table-nomargin table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <th class="text-right">Full Name</th>
                                                                    <td class="text-left">
                                                                        <?php echo $value->full_name; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-right">User Type</th>
                                                                    <td class="text-left">
                                                                        <?php echo $arrayProjectName[$value->project_id][$value->user_type]; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-right">Email</th>
                                                                    <td class="text-left">
                                                                        <?php echo $value->email; ?>
                                                                    </td>
                                                                </tr>                                                                
                                                                <tr>
                                                                    <th class="text-right">Department</th>
                                                                    <td class="text-left">
                                                                        <?php echo $value->department; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-right">Designation</th>
                                                                    <td class="text-left">
                                                                        <?php echo $value->designation; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-right">IP Address</th>
                                                                    <td class="text-left">
                                                                        <?php echo $value->ip_address; ?>
                                                                    </td>
                                                                </tr>
                                                                <?php if(!empty($value->picture)): ?>
                                                                    <tr>
                                                                        <th class="text-right">Picture</th>
                                                                        <td class="text-left">

                                                                            <img src="<?php echo base_url() ?>assets/upload/profile/thumb/<?php echo $this->Common_operation->show_thumb($value->picture); ?>">
                                                                        </td>
                                                                    </tr>
                                                                <?php endif; ?>


                                                            </tbody>
                                                        </table>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" title="view" data-toggle="modal" data-target="#<?php echo $value->id ?>" ><i class="fa fa-fw fa-eye"></i></a> /
                                        <a href="user_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                        <a href="user_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $sl++; } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                <?php } ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php $this->load->view('./admin/footer-link') ?>

    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
    
    $(function () {
           $("#example1").DataTable({
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            initComplete: function () {
                this.api().columns([1,3]).every(function () {
                    var column = this;
                    var select = $('<select class="form-control"><option value="">-Select-</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });
    });
    </script>


<?php $this->load->view('./admin/footer'); ?>