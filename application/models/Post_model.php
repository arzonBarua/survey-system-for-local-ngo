<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model
{
    private $table = "";

    function __construct()
    {
        parent::__construct();
    }

    function getAll($table, $field = 'id', $order = 'ASC', $id = null)
    {
		if( $id != null )
			$this->db->where('id', $id);
        $this->table = $table;
        $this->db->order_by($field, $order);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    function getAllByFieldName($table, $field_name, $field_val)
    {
        if($field_val != "" && $field_name != "")
            $this->db->where("`" . $field_name . "`", $field_val);
        $this->table = $table;
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    function getAllByFieldNameOpition($table, $field_name, $field_val,$orderfield=nulll)
    {
        if($field_val != "" && $field_name != "")
            $this->db->where("`" . $field_name . "`", $field_val);
        $this->table = $table;
        $this->db->order_by($orderfield, "asc");
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    function insert($table, $data)
    {
        $this->table = $table;
        
        if($this->db->insert($this->table, $data)){
            return true;
        }



        return false;
    }

    function mutipleInsert($table, $data){
        $this->table = $table;
        if($this->db->insert_batch($this->table, $data)){
            return true;
        }
        return false;
    }

    function update($table, $data, $id)
    {
        $this->table = $table;
        $this->db->where("id", $id);
        $this->db->update($this->table, $data);

        if($this->db->affected_rows() >= 0){
            return true;
        }

        return false;

    }

    function delete($table, $id, $path = null)
    {
        $this->table = $table;
        $this->db->where("id", $id);
        $this->db->delete($this->table);

        if (!empty($path)) {
            $this->Common_operation->file_delete($path);
        }
    }

    function getAjaxHTML($table, $field_name, $field_val)
    {
        $str = '<option value=""> - SELECT - </option>';
        $this->table = $table;
        $this->db->select('id, name');
        $this->db->where("`" . $field_name . "`", $field_val);
        $this->db->order_by("name", "asc");
        $q = $this->db->get($this->table);


        $str = '<option value="">--SELECT--</option>';
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $key => $value) {
                $str .= '<option value="' . $value->id . '">' . $value->name . '</option>';
            }
            //return $str;
        }
        return $str;
    }

    public function custom_query($query_string){

        $q = $this->db->query($query_string);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;

    }

    function en2bnNumber ($number){
        $replace_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
        $search_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
        $bn_number = str_replace($search_array, $replace_array, $number);

        return $bn_number;
    }

    public function custom_query_single_row($query_string){
        $query = $this->db->query($query_string);
        if( $query->num_rows() > 0 ){
            $row = $query->row();
            foreach( $row as $val ){
                return $val;
            }
        }
    }

    


}
