<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**

 * Created by PhpStorm.

 * User: arzon

 * Date: 1/16/2017

 * Time: 11:31 AM

 */







class Api_model extends CI_Model

{

     public $client_service = "operator";

     public $auth_key = "nirapodrestapi";







    public function check_auth_client(){

        $client_service = $this->input->get_request_header('Client-Service', TRUE);

        $auth_key  = $this->input->get_request_header('Auth-Key', TRUE);

        if($client_service == $this->client_service && $auth_key == $this->auth_key){

            return true;

        } else {

            return false;

        }

    }



    public function login($username,$password,$ime_no)

    {

        $array = array('username'=>$username,'status'=>1);



        $q  = $this->db->select('password,id,ime_no,district_id,upazila_id')->from('app_user_info')->where($array)->get()->row();

        if($q == ""){

            return array('status' => 204,'message' => 'Username not found.');

        } else {

            $hashed_password = $q->password;

            $id              = $q->id;

            $ime_no_database = $q->ime_no;

            $district_id = $q->district_id;

            $upazila_id = $q->upazila_id;







             if($hashed_password == md5($password)){

                          $flag = 1;



                         if(empty($ime_no_database)){

                            $this->db->where('id', $id)->update('app_user_info', array('ime_no' => $ime_no));

                         }else{

                            if($ime_no_database != $ime_no) {

                                $flag = 0;

                            }

                         }



                         if($flag) {

                             $last_login = date('Y-m-d H:i:s');

                             $token = substr(md5(microtime()),rand(0,26),15);

                             $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));

                             $this->db->trans_start();

                             $this->db->where('id', $id)->update('app_user_info', array('last_login' => $last_login));

                             $this->db->insert('users_authentication', array('users_id' => $id, 'token' => $token, 'expired_at' => $expired_at));

                             if ($this->db->trans_status() === FALSE) {

                                 $this->db->trans_rollback();

                                 return array('status' => 500, 'message' => 'Internal server error.','action'=>'failed');

                             } else {

                                 $this->db->trans_commit();

                                 return array('status' => 200, 'message' => 'success', 'id' => $id,'district_id'=> $district_id,'upazila_id'=>$upazila_id,'token' => $token,'action'=>'success');

                             }

                         }else{

                              return array('status' => 204,'message' => 'Ime Number is not found.','action'=>'failed');

                         }

                     

            } else {

                return array('status' => 204,'message' => 'Wrong password.');

            }

        }

    }





    public function auth()

    {

        $users_id  = $this->input->get_request_header('User-ID', TRUE);

        $headers = apache_request_headers();

        $token     = $this->input->get_request_header('Authorization', TRUE);
        
        $q  = $this->db->select('expired_at')->from('users_authentication')->where('users_id',$users_id)->where('token',$token)->get()->row();

        if($q == ""){

            return array('status' => 401,'message' => 'Unauthorized.','action'=>'failed');

        } else {

            if($q->expired_at < date('Y-m-d H:i:s')){

                return array('status' => 401,'message' => 'Your session has been expired.','action'=>'expired');

            } else {

                $updated_at = date('Y-m-d H:i:s');

                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));

                $this->db->where('users_id',$users_id)->where('token',$token)->update('users_authentication',array('expired_at' => $expired_at,'updated_at' => $updated_at));

                return array('status' => 200,'message' => 'Authorized.','users_id'=>$users_id);

            }

        }

    }





    public function logout()

    {

        $users_id  = $this->input->get_request_header('User-ID', TRUE);

        $token     = $this->input->get_request_header('Authorization', TRUE);

        $this->db->where('users_id',$users_id)->where('token',$token)->delete('users_authentication');



        unset($_SESSION['mf_label']);

        return array('status' => 200,'message' => 'Successfully logout.','action'=>'success');

    }





    public function insert_info($data,$type)

    {

        $data['users_id'] = $this->input->get_request_header('User-ID', TRUE);

        $this->db->insert($type, $data);



        if($type=="meeting"){

            $meeting_id = $this->db->insert_id();

            return array('status' => 201,'message' => 'Data has been inserted.','meeting_id'=>$meeting_id,'action'=>'success');

        }



        return array('status' => 201,'message' => 'Data has been inserted.','action'=>'success');

    }





    public function get_question_forms(){

        $array = array();



        $data['result'] = $this->Post_model->custom_query("select * from survey_info order by id ASC");



        $i = 0;

        foreach($data['result'] as $key => $val){

            $array['data'][$i]['id'] = $val->id;

            $array['data'][$i]['form_name'] = $val->survey_name; 

            $i++;

        }



        return $array;

    }





    public function show_question($id){



        $array = array();

        $arrayOb = new stdClass();    



        $array_main = array();



        $data['survey_question'] = $this->Post_model->custom_query("select * from survey_qs where survey_info_id='$id' order by order_id ASC");



             



        if ($data['survey_question']){



            

              $array['data'][0]["ques"] = "নাম";

              $array['data'][0]["q_id"] = 1000;

              $array['data'][0]["q_type"] = "text";

              $array['data'][0]["visibility"] = 1;





              $array['data'][1]["ques"] = "বয়স";

              $array['data'][1]["q_id"] = 1002;

              $array['data'][1]["q_type"] = "text";

              $array['data'][1]["visibility"] = 1;



                 $array['data'][2]["ques"] = "উত্তরদাতার লিঙ্গ";

                 $array['data'][2]["q_id"] = 1003;

                 $array['data'][2]["q_type"] = "radio";

                 $array['data'][2]["visibility"] = 1;





                    $array['data'][2]["option"][0]['item'] = "পুরুষ";

                    $array['data'][2]["option"][0]['cde'] = "1";

                    $array['data'][2]["option"][0]['d_id'] = 0;



                    $array['data'][2]["option"][1]['item'] = "মহিলা"; 

                    $array['data'][2]["option"][1]['cde'] = "2";

                    $array['data'][2]["option"][1]['d_id'] = 0;



              $i = 3;    



                



            if($id == 3){

                $array['data'][4]["ques"] = "গার্মেন্টস এর নাম";

                $array['data'][4]['q_id'] = 1005;

                $array['data'][4]["q_type"] = "text";

                $array['data'][4]["visibility"] = 1;





                $array['data'][5]["ques"] = "কোন বিভাগ কাজ করেন";

                $array['data'][5]['q_id'] = 1006;

                $array['data'][5]["q_type"] = "text";

                $array['data'][5]["visibility"] = 1;



                $array['data'][6]["ques"] = "আইডি নাম্বার";

                $array['data'][6]['q_id'] = 1007;

                $array['data'][6]["q_type"] = "text";

                $array['data'][6]["visibility"] = 1;





                $array['data'][7]["ques"] = "গার্মেন্টস এর ঠিকানা";

                $array['data'][7]['q_id'] = 1008;

                $array['data'][7]["q_type"] = "text";

                $array['data'][7]["visibility"] = 1;



                $i = 7; 

            }



            $array['data'][++$i]["ques"] = "মোবাইল নাম্বার";

            $array['data'][$i]['q_id'] = 1004;

            $array['data'][$i]["q_type"] = "text";

            $array['data'][$i]["visibility"] = 1;



            $i++; 

                    

            foreach($data['survey_question'] as $key => $val ){

                 $question_result = $this->Post_model->custom_query("Select * from  survey_option where survey_qs_id='$val->id' order by option_order ASC");



                 $j = 0;



                 foreach($question_result as $question_key => $question_val){

                    $array['data'][$i]["ques"] =  $val->survey_qs;

                    $array['data'][$i]["q_id"] =  $val->id;

                    $array['data'][$i]["order_id"] =  $val->order_id;

                    $array['data'][$i]["q_type"] =  $val->survey_qs_type;

                    $array['data'][$i]["visibility"] = $val->visibility;

                    



                    if(!empty($question_val->option_value)){

                        $array['data'][$i]["option"][$j]['item'] = $question_val->option_value;

                        $array['data'][$i]["option"][$j]['cde'] = $question_val->id;

                        $array['data'][$i]["option"][$j]['d_id'] = $question_val->d_id;

                    }



                    $j++;

                 }

                 $i++;

            }

            $array = array_values($array['data']);



            $arrayData['data'] = $array;

 

            return $arrayData;

            

        }else{

             return array('status' => 404,'message' => 'not found','action'=>'failed');

        }

    }







}