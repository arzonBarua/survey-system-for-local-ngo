<?php

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/13/2016
 * Time: 12:19 PM
 */
class Menu_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('permission');
    }

    public function create_submenu($parent_id){
        $sql = "SELECT * FROM menu_admin Where parent_id=? and status=?";
        $query = $this->db->query($sql,array($parent_id,1));
        $result = $query->row();

        if($result){
            echo "<ul class='treeview-menu'>";
            foreach($query->result() as $row){
                if($this->permission->has_permission($parent_id,$row->id)) {
                    echo "<li>";
                    $link = !empty($row->link) ? $row->link . ".html" : "#";
                    echo "<a href='" . base_url() . $link . "'><i class='fa fa-circle-o'></i> " . $row->title . "</a>";
                    $this->create_submenu($row->id);
                    echo '</li>';
                }
            }
            echo "</ul>";
        }
    }

    public function admin_menu(){
        $sql = "SELECT * FROM menu_admin where parent_id=? order by order_id ASC";
        $query = $this->db->query($sql,array(0));

        echo "<ul class='sidebar-menu'>";
            echo "<li class='header'>MAIN NAVIGATION</li>";

                foreach($query->result() as $row){
                    if($this->permission->has_permission($row->id)){
                        echo "<li class='treeview'>";
                            $link = !empty($row->link) ? $row->link . ".html" : "#";
                            $icon = !empty($row->alice) ? "fa fa-" . $row->alice : "fa fa-circle-o";
                            echo "<a href='" . base_url() . $link . "'><i class='" . $icon . "'></i> " . $row->title . "</a>";
                            $this->create_submenu($row->id);
                        echo "</li>";
                   }
                }

        echo "</ul>";
    }

    public function get_all_menu(){
        $sql = "SELECT * FROM menu_admin where acl_show='1'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_credential($link){
        $sql = "SELECT id, parent_id FROM menu_admin WHERE link =?";
        $query = $this->db->query($sql,$link);
        return $query->result();
    }

    public function check_by_database($id)
    {
        $sql = "SELECT id, parent_id FROM menu_admin where id=?";
        $query = $this->db->query($sql,array($id));
        return $query->result();
    }

    public function menu_model_info(){

        $sql = 'select full_name, user_type,designation, ip_restriction, ip_address, created_at, picture, acl_management.access from admin_login LEFT JOIN  acl_management on admin_login.id = acl_management.login_id where admin_login.id=?';
        $query = $this->db->query($sql,array($this->session->userdata('id')));
        $result = $query->row();

        $user_info = array(
            'full_name' => $result->full_name,
            'picture' => $result->picture,
            'designation' => $result->designation,
            'user_type' => $result->user_type,
            'access' => $result->access,
            'created_at' => $result->created_at,
        );
        $this->session->set_userdata($user_info);
    }

}