<?php

class User_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Post_model');
    }

    public function login_user($email, $password){
        $sql = "select admin_login.id,ip_restriction,ip_address, user_type, project_id from admin_login where email=? and password=?";
        $query = $this->db->query($sql,array($email, $password));
        $result = $query->row();
        
        if($result){
            /*---check IP restriction---*/
                if($result->ip_restriction){
                    if($this->input->ip_address() != $result->ip_address){
                        return false;
                    }
                }
            /*---End check IP restriction---*/

            /*--- Check Error Count ---*/
            if($this->session->userdata('error_count')){
                $this->session->unset_userdata('error_count');
            }
            /*--- Check Error Count ---*/
                $user_data = array(
                    'id' => $result->id,
                    'isLogedNirapod' => true
                );

            /*-- Get surveyor's id of specific project --*/  
            
            $project_id = $result->project_id;
            $user_type = $result->user_type;
            
            $this->session->set_userdata($user_data);
                return true;
        }else{
            return false;
        }
    }

    public function get_user_all(){
        $arrayProjectName = $this->Common_operation->get_user_type();
        $userList = array();
        $sql = "select admin_login.id, admin_login.full_name, admin_login.user_type,admin_login.designation, projects.name,projects.id as project_id from admin_login join projects on admin_login.project_id = projects.id where user_type != '1' order by projects.id asc";
        $query = $this->db->query($sql);

        foreach($query->result() as $row){
            $userList[$row->id] = $row->full_name." | User Type: ".$arrayProjectName[$row->project_id][$row->user_type]." | Organization: ".$row->name;
        }
        return $userList;
    }

    public function get_user_all_type(){
       
        $qry_str = "";

        if($this->session->userdata('project_id')){
            $project_id = $this->session->userdata('project_id');
            $qry_str = " WHERE project_id = '$project_id' ";
        }

        $sql = "select admin_login.id ,admin_login.full_name, admin_login.email, admin_login.department, admin_login.designation, admin_login.ip_address, admin_login.ip_address, admin_login.picture ,admin_login.created_at, admin_login.project_id, projects.name as project_name, admin_login.user_type from admin_login JOIN projects on admin_login.project_id = projects.id $qry_str order by admin_login.full_name asc";
        
        $query = $this->db->query($sql);
        $result = $query->row();

        if($result){
            return $query->result();
        }
            
        return 0;
    
    }


    public function change_pass_status(){

    }

    public function check_email($email){
        $sql = "SELECT * FROM admin_login where email=?";
        $query = $this->db->query($sql,array($email));
        $result = $query->row();
        if($result){
            return false;
        }
        return true;
    }

    /* Get app users */
    public function get_app_user($ids){
        /*For ACL*/
        $qry_str = "";

        if(!empty($ids)){
            $qry_str = " WHERE app_user_info.id in (".implode(',',$ids).") ";
        }

        $sql = "SELECT `app_user_info`.*, `district`.`name` AS district_name, `upazila`.`name` AS upazila_name, `admin_login`.`full_name` AS manager_name, `admin_login`.`full_name` as ass_full_name, `admin_login`.`user_type` as ass_user_type, `admin_login`.`project_id` as ass_project_id, `projects`.`name` as project_name
                FROM `app_user_info`
                LEFT JOIN `admin_login` ON `app_user_info`.`login_id` = `admin_login`.`id`
                LEFT JOIN `district` ON `app_user_info`.`district_id` = `district`.`id`
                LEFT JOIN `upazila` ON `app_user_info`.`upazila_id` = `upazila`.`id`
                LEFT JOIN `projects` ON `app_user_info`.`project_id` = `projects`.`id`".$qry_str."
                ORDER BY `app_user_info`.`full_name` ASC";
        $qry = $this->db->query($sql);

        return $qry->result();
    }

}