<?php

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 9:23 PM Common_operation
 */
class Common_operation extends CI_Model{

    public function __construct()
    {
        parent::__construct();

    }

    public function image_upload($file_name,$upload_path,$min_width=null,$min_height=null,$crop_width=null,$crop_height=null){
        $data['flag'] = true;
        $config['upload_path'] = $upload_path; //full path
        $config['allowed_types'] = 'gif|jpg|png|JPG|PNG|JPEG';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = 6000;
        $config['max_width'] = 3264; //min_width
        $config['max_height'] = 2448; //min_height

        if(!empty($min_width)){
            $config['min_width'] = $min_width;
        }

        if(!empty($min_height)){
            $config['min_height'] = $min_height;
        }


        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_name)) {
            $data['flag'] = false;
            $data['error'] = $this->upload->display_errors();
        }else {
            $image_data = $this->upload->data();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_data['full_path']; //full path
            $config['new_image'] = $upload_path.'thumb/';
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;

            $config['width'] = 125;
            $config['height'] = 100;

            if(!empty($crop_width)){
                $config['width'] = $crop_width;
            }

            if(!empty($crop_height)){
                $config['height'] = $crop_height;
            }


            $this->load->library('image_lib', $config);
            if (!$this->image_lib->resize()) {
                $data['flag'] = false;
                $data['error'] = $this->image_lib->display_errors();
            }
            $data['picture'] = $image_data["file_name"];
        }

        return $data;
    }

     public function video_upload($file_name, $upload_path){
        $data['flag'] = true;
        $config['upload_path'] = $upload_path; //full path
        $config['allowed_types'] = 'MP4|mp4|3gp';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_name)) {
            $data['flag'] = false;
            $data['error'] = $this->upload->display_errors();
        }else {
            $file_data = $this->upload->data();
            $data['file_name'] = $file_data["file_name"];
        }
        return $data;
    }

    public function file_delete($full_path){

        if (file_exists($full_path)) {
            unlink($full_path);
        }

        $path_parts = pathinfo($full_path);
        $file_name_folder = $path_parts['filename'].'_thumb.'.$path_parts['extension'];

        $full_path = $path_parts['dirname'].'/thumb/'.$file_name_folder;

        if (file_exists($full_path)) {
            unlink($full_path);
        }
    }

    public function show_thumb($full_path){
        $path_parts = pathinfo($full_path);
        $file_name_folder = $path_parts['filename'].'_thumb.'.$path_parts['extension'];
        return $file_name_folder;
    }

    /* The Following function added by Murad */
    public function read_more($string, $size){
        $string = strip_tags($string);

        if (strlen($string) > $size) {
            // truncate string
            $stringCut = substr($string, 0, $size);

            // make sure it ends in a word so assassinate doesn't become ass...
            $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
        }
        return $string;
    }

    public function date_compare($compareDate1, $compareDate){
        $datetime1 = new DateTime($compareDate1);
        $datetime2 = new DateTime($compareDate);
        $interval = $datetime1->diff($datetime2);
        $elapsed = $interval->format('%i');

        return $elapsed;
    }

    public function getTimeDiff($time, $_time){
        $tm = (strtotime($time) - strtotime($_time)) / 60;
        $mi = $tm % 60;
        $tm = $tm - $mi;
        $hr = $tm / 60;

        //$time_duration = $hr." Hour ".$mi." Min";
        $time_duration = $hr.".".$mi;
        //$time_duration = str_pad($hr, 2, "0", STR_PAD_LEFT).":".str_pad($mi, 2, "0", STR_PAD_LEFT).":00";
        return $time_duration;
    }

    /* The Following function added by Murad */
    public function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
          switch ($num % 10) {
            // Handle 1st, 2nd, 3rd
            case 1:  return $num.'st';
            case 2:  return $num.'nd';
            case 3:  return $num.'rd';
          }
        }
        return $num.'th';
      }

    public function get_user_type(){
        $arrayProjectName =  array('1'=>array(
                                            '2'=>'TL, M-IT, FM, AO MISO,CO,TO',
                                            '3'=>'PM',
                                            '4'=>'PO',
                                            '5'=>'AFO,APO'
                                        ),
                                    '2'=>array(
                                            '2'=>'ED',
                                            '3'=>'PM,AFO',
                                            '4'=>'PO',
                                            '5'=>'AFO,APO'
                                        ),
                                    '3'=>array(
                                            '2'=>'ED',
                                            '3'=>'PM,AFO',
                                            '4'=>'PO',
                                            '5'=>'AFO,APO'
                                        ),
                                    '4'=>array(
                                            '2'=>'ED',
                                            '5'=>'PM,AFO',
                                        ),
                                    '5'=>array(
                                            '1'=>'Super Admin',
                                            '6'=>'SD-MSB',
                                            '7'=>'GM-SP, MSB'
                                        )
                                    );

        return  $arrayProjectName;
    }


    /* The Following function added by Murad */
    public function get_time_duration($end_time, $start_time){  
        $start_date = new DateTime($start_time);
        $since_start = $start_date->diff(new DateTime($end_time));

       // $minutes = $since_start->days * 24 * 60;
        $minutes = 0;
        $minutes += $since_start->h * 60;
        $minutes += $since_start->i;

        $mi = $minutes % 60;
        $tm = $minutes - $mi;
        $hr = $tm / 60;

        $time_duration = intval($hr)." Hour ".$mi." Min";
        return $time_duration;
    }
}
