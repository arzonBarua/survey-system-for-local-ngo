# Survey System for Local NGO (Non Governmental Organization)

This is a Monitoring and survey system tool made for a local NGO (non governmental organisation). 
In Bangladesh most of the NGO workers do manual survey where they use paper. This project is based on two part one is monitoring system which is made of code ignitor 
(php framework). Another one is mobile apps based on android. From the backend questions bank for survey can be created based on criteria. 
and those question are shown in mobile app via API. 
For NGO workers specific user name can be created from the backend and NGO workers can login the app using their credential and conduct survey. 
records of survey are being saved in the backend using API. GPS location of NGO workers are also saved from the mobile app to backend to see where are the workings during survey. 